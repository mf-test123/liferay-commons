package com.placecube.initializer.service.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.LayoutSet;
import com.liferay.portal.kernel.model.LayoutSetPrototype;
import com.liferay.portal.kernel.security.auth.PrincipalThreadLocal;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.initializer.model.internal.SiteTemplateConfiguration;
import com.placecube.initializer.service.PermissionCheckerInitializer;
import com.placecube.initializer.service.internal.SiteTemplateUtil;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PrepareForTest(PrincipalThreadLocal.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class LayoutSetPrototypeInitializerImplTest extends PowerMockito {

	private static final long COMPANY_ID = 2;
	private static final long USER_ID = 1;

	@InjectMocks
	private LayoutSetPrototypeInitializerImpl layoutSetPrototypeInitializerImpl;

	@Mock
	private Group mockGroup;

	@Mock
	private LayoutSet mockLayoutSetPrivate;

	@Mock
	private LayoutSetPrototype mockLayoutSetPrototypePrivate;

	@Mock
	private LayoutSetPrototype mockLayoutSetPrototypePublic;

	@Mock
	private LayoutSet mockLayoutSetPublic;

	@Mock
	private PermissionCheckerInitializer mockPermissionCheckerInitializer;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private SiteTemplateConfiguration mockSiteTemplateConfigurationPrivate;

	@Mock
	private SiteTemplateConfiguration mockSiteTemplateConfigurationPublic;

	@Mock
	private SiteTemplateUtil mockSiteTemplateUtil;

	@Before
	public void activateSetup() {
		mockStatic(PrincipalThreadLocal.class);
	}

	@Test(expected = PortalException.class)
	@Parameters({ "true,true", "true,false", "false,false", "false,true" })
	public void applySiteTemplateToGroup_WhenAnExceptionIsThrownRetrievingThePrivateSiteTemplateConfiguration_ThenPortalExceptionIsThrown(boolean publicPropagationEnabled,
			boolean privatePropagationEnabled) throws Exception {
		when(PrincipalThreadLocal.getUserId()).thenReturn(USER_ID);
		when(mockGroup.getPublicLayoutSet()).thenReturn(mockLayoutSetPublic);
		when(mockGroup.getPrivateLayoutSet()).thenReturn(mockLayoutSetPrivate);
		when(mockSiteTemplateUtil.getSiteTemplateConfigurationToApply(mockLayoutSetPrivate, Optional.of(mockLayoutSetPrototypePrivate), privatePropagationEnabled)).thenThrow(new PortalException());

		layoutSetPrototypeInitializerImpl.applySiteTemplateToGroup(mockGroup, Optional.of(mockLayoutSetPrototypePublic), publicPropagationEnabled, Optional.of(mockLayoutSetPrototypePrivate),
				privatePropagationEnabled, mockServiceContext);

		verify(mockSiteTemplateUtil, never()).removeExistingPages(any(Group.class), any(SiteTemplateConfiguration.class), any(SiteTemplateConfiguration.class), any(ServiceContext.class));
		verify(mockSiteTemplateUtil, never()).applySiteTemplatesToGroup(any(Group.class), any(SiteTemplateConfiguration.class), any(SiteTemplateConfiguration.class));
		verify(mockSiteTemplateUtil, never()).applyThemeToGroup(any(Group.class), any(SiteTemplateConfiguration.class), any(SiteTemplateConfiguration.class));
		verify(mockPermissionCheckerInitializer, times(1)).runAsUser(USER_ID);
	}

	@Test(expected = PortalException.class)
	@Parameters({ "true,true", "true,false", "false,false", "false,true" })
	public void applySiteTemplateToGroup_WhenAnExceptionIsThrownRetrievingThePublicSiteTemplateConfiguration_ThenPortalExceptionIsThrown(boolean publicPropagationEnabled,
			boolean privatePropagationEnabled) throws Exception {
		when(PrincipalThreadLocal.getUserId()).thenReturn(USER_ID);
		when(mockGroup.getPublicLayoutSet()).thenReturn(mockLayoutSetPublic);
		when(mockSiteTemplateUtil.getSiteTemplateConfigurationToApply(mockLayoutSetPublic, Optional.of(mockLayoutSetPrototypePublic), publicPropagationEnabled)).thenThrow(new PortalException());

		layoutSetPrototypeInitializerImpl.applySiteTemplateToGroup(mockGroup, Optional.of(mockLayoutSetPrototypePublic), publicPropagationEnabled, Optional.of(mockLayoutSetPrototypePrivate),
				privatePropagationEnabled, mockServiceContext);

		verify(mockSiteTemplateUtil, never()).removeExistingPages(any(Group.class), any(SiteTemplateConfiguration.class), any(SiteTemplateConfiguration.class), any(ServiceContext.class));
		verify(mockSiteTemplateUtil, never()).applySiteTemplatesToGroup(any(Group.class), any(SiteTemplateConfiguration.class), any(SiteTemplateConfiguration.class));
		verify(mockSiteTemplateUtil, never()).applyThemeToGroup(any(Group.class), any(SiteTemplateConfiguration.class), any(SiteTemplateConfiguration.class));
		verify(mockPermissionCheckerInitializer, times(1)).runAsUser(USER_ID);
	}

	@Test(expected = PortalException.class)
	@Parameters({ "true,true", "true,false", "false,false", "false,true" })
	public void applySiteTemplateToGroup_WhenAnExceptionIsThrownRunningAsCompanyAdministrator_ThenPortalExceptionIsThrown(boolean publicPropagationEnabled, boolean privatePropagationEnabled)
			throws Exception {
		when(PrincipalThreadLocal.getUserId()).thenReturn(USER_ID);
		when(mockGroup.getCompanyId()).thenReturn(COMPANY_ID);
		doThrow(new PortalException()).when(mockPermissionCheckerInitializer).runAsCompanyAdministrator(COMPANY_ID);

		layoutSetPrototypeInitializerImpl.applySiteTemplateToGroup(mockGroup, Optional.of(mockLayoutSetPrototypePublic), publicPropagationEnabled, Optional.of(mockLayoutSetPrototypePrivate),
				privatePropagationEnabled, mockServiceContext);

		verifyZeroInteractions(mockSiteTemplateUtil);
		verify(mockPermissionCheckerInitializer, times(1)).runAsUser(USER_ID);
	}

	@Test(expected = PortalException.class)
	@Parameters({ "true,true", "true,false", "false,false", "false,true" })
	public void applySiteTemplateToGroup_WhenAnExceptionIsThrownWhenApplyingTheSiteTemplatesToTheGroup_ThenPortalExceptionIsThrown(boolean publicPropagationEnabled, boolean privatePropagationEnabled)
			throws Exception {
		when(PrincipalThreadLocal.getUserId()).thenReturn(USER_ID);
		when(mockGroup.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockGroup.getPublicLayoutSet()).thenReturn(mockLayoutSetPublic);
		when(mockGroup.getPrivateLayoutSet()).thenReturn(mockLayoutSetPrivate);
		when(mockSiteTemplateUtil.getSiteTemplateConfigurationToApply(mockLayoutSetPublic, Optional.of(mockLayoutSetPrototypePublic), publicPropagationEnabled))
				.thenReturn(mockSiteTemplateConfigurationPublic);
		when(mockSiteTemplateUtil.getSiteTemplateConfigurationToApply(mockLayoutSetPrivate, Optional.of(mockLayoutSetPrototypePrivate), privatePropagationEnabled))
				.thenReturn(mockSiteTemplateConfigurationPrivate);
		doThrow(new PortalException()).when(mockSiteTemplateUtil).applySiteTemplatesToGroup(mockGroup, mockSiteTemplateConfigurationPublic, mockSiteTemplateConfigurationPrivate);

		layoutSetPrototypeInitializerImpl.applySiteTemplateToGroup(mockGroup, Optional.of(mockLayoutSetPrototypePublic), publicPropagationEnabled, Optional.of(mockLayoutSetPrototypePrivate),
				privatePropagationEnabled, mockServiceContext);

		verify(mockPermissionCheckerInitializer, times(1)).runAsUser(USER_ID);
	}

	@Test(expected = PortalException.class)
	@Parameters({ "true,true", "true,false", "false,false", "false,true" })
	public void applySiteTemplateToGroup_WhenAnExceptionIsThrownWhenRemovingExistingPages_ThenPortalExceptionIsThrown(boolean publicPropagationEnabled, boolean privatePropagationEnabled)
			throws Exception {
		when(PrincipalThreadLocal.getUserId()).thenReturn(USER_ID);
		when(mockGroup.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockGroup.getPublicLayoutSet()).thenReturn(mockLayoutSetPublic);
		when(mockGroup.getPrivateLayoutSet()).thenReturn(mockLayoutSetPrivate);
		when(mockSiteTemplateUtil.getSiteTemplateConfigurationToApply(mockLayoutSetPublic, Optional.of(mockLayoutSetPrototypePublic), publicPropagationEnabled))
				.thenReturn(mockSiteTemplateConfigurationPublic);
		when(mockSiteTemplateUtil.getSiteTemplateConfigurationToApply(mockLayoutSetPrivate, Optional.of(mockLayoutSetPrototypePrivate), privatePropagationEnabled))
				.thenReturn(mockSiteTemplateConfigurationPrivate);
		doThrow(new PortalException()).when(mockSiteTemplateUtil).removeExistingPages(mockGroup, mockSiteTemplateConfigurationPublic, mockSiteTemplateConfigurationPrivate, mockServiceContext);

		layoutSetPrototypeInitializerImpl.applySiteTemplateToGroup(mockGroup, Optional.of(mockLayoutSetPrototypePublic), publicPropagationEnabled, Optional.of(mockLayoutSetPrototypePrivate),
				privatePropagationEnabled, mockServiceContext);

		verify(mockSiteTemplateUtil, never()).applySiteTemplatesToGroup(any(Group.class), any(SiteTemplateConfiguration.class), any(SiteTemplateConfiguration.class));
		verify(mockPermissionCheckerInitializer, times(1)).runAsUser(USER_ID);
	}

	@Test
	@Parameters({ "true,true", "true,false", "false,false", "false,true" })
	public void applySiteTemplateToGroup_WhenBothSiteTemplatesAreSpecified_ThenChangesAreAppliedToTheGroup(boolean publicPropagationEnabled, boolean privatePropagationEnabled) throws Exception {
		when(PrincipalThreadLocal.getUserId()).thenReturn(USER_ID);
		when(mockGroup.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockGroup.getPublicLayoutSet()).thenReturn(mockLayoutSetPublic);
		when(mockGroup.getPrivateLayoutSet()).thenReturn(mockLayoutSetPrivate);
		when(mockSiteTemplateUtil.getSiteTemplateConfigurationToApply(mockLayoutSetPublic, Optional.of(mockLayoutSetPrototypePublic), publicPropagationEnabled))
				.thenReturn(mockSiteTemplateConfigurationPublic);
		when(mockSiteTemplateUtil.getSiteTemplateConfigurationToApply(mockLayoutSetPrivate, Optional.of(mockLayoutSetPrototypePrivate), privatePropagationEnabled))
				.thenReturn(mockSiteTemplateConfigurationPrivate);

		layoutSetPrototypeInitializerImpl.applySiteTemplateToGroup(mockGroup, Optional.of(mockLayoutSetPrototypePublic), publicPropagationEnabled, Optional.of(mockLayoutSetPrototypePrivate),
				privatePropagationEnabled, mockServiceContext);

		InOrder inOrder = Mockito.inOrder(mockSiteTemplateUtil, mockPermissionCheckerInitializer);
		inOrder.verify(mockPermissionCheckerInitializer, times(1)).runAsCompanyAdministrator(COMPANY_ID);
		inOrder.verify(mockSiteTemplateUtil, times(1)).removeExistingPages(mockGroup, mockSiteTemplateConfigurationPublic, mockSiteTemplateConfigurationPrivate, mockServiceContext);
		inOrder.verify(mockSiteTemplateUtil, times(1)).applySiteTemplatesToGroup(mockGroup, mockSiteTemplateConfigurationPublic, mockSiteTemplateConfigurationPrivate);
		inOrder.verify(mockSiteTemplateUtil, times(1)).applyThemeToGroup(mockGroup, mockSiteTemplateConfigurationPublic, mockSiteTemplateConfigurationPrivate);
		inOrder.verify(mockPermissionCheckerInitializer, times(1)).runAsUser(USER_ID);
	}

	@Test
	@Parameters({ "true,true", "true,false", "false,false", "false,true" })
	public void applySiteTemplateToGroup_WhenNeitherOfTheSiteTemplatesAreSpecified_ThenNoActionsArePerformed(boolean publicPropagationEnabled, boolean privatePropagationEnabled) throws Exception {
		layoutSetPrototypeInitializerImpl.applySiteTemplateToGroup(mockGroup, Optional.empty(), publicPropagationEnabled, Optional.empty(), privatePropagationEnabled, mockServiceContext);

		verifyZeroInteractions(mockSiteTemplateUtil);
		verify(mockPermissionCheckerInitializer, times(1)).runAsUser(0);
	}

	@Test
	@Parameters({ "true,true", "true,false", "false,false", "false,true" })
	public void applySiteTemplateToGroup_WhenOnlyThePrivateSiteTemplatesIsSpecified_ThenChangesAreAppliedToTheGroup(boolean publicPropagationEnabled, boolean privatePropagationEnabled)
			throws Exception {
		when(PrincipalThreadLocal.getUserId()).thenReturn(USER_ID);
		when(mockGroup.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockGroup.getPublicLayoutSet()).thenReturn(mockLayoutSetPublic);
		when(mockGroup.getPrivateLayoutSet()).thenReturn(mockLayoutSetPrivate);
		when(mockSiteTemplateUtil.getSiteTemplateConfigurationToApply(mockLayoutSetPublic, Optional.empty(), publicPropagationEnabled)).thenReturn(mockSiteTemplateConfigurationPublic);
		when(mockSiteTemplateUtil.getSiteTemplateConfigurationToApply(mockLayoutSetPrivate, Optional.of(mockLayoutSetPrototypePrivate), privatePropagationEnabled))
				.thenReturn(mockSiteTemplateConfigurationPrivate);

		layoutSetPrototypeInitializerImpl.applySiteTemplateToGroup(mockGroup, Optional.empty(), publicPropagationEnabled, Optional.of(mockLayoutSetPrototypePrivate), privatePropagationEnabled,
				mockServiceContext);

		InOrder inOrder = Mockito.inOrder(mockSiteTemplateUtil, mockPermissionCheckerInitializer);
		inOrder.verify(mockPermissionCheckerInitializer, times(1)).runAsCompanyAdministrator(COMPANY_ID);
		inOrder.verify(mockSiteTemplateUtil, times(1)).removeExistingPages(mockGroup, mockSiteTemplateConfigurationPublic, mockSiteTemplateConfigurationPrivate, mockServiceContext);
		inOrder.verify(mockSiteTemplateUtil, times(1)).applySiteTemplatesToGroup(mockGroup, mockSiteTemplateConfigurationPublic, mockSiteTemplateConfigurationPrivate);
		inOrder.verify(mockSiteTemplateUtil, times(1)).applyThemeToGroup(mockGroup, mockSiteTemplateConfigurationPublic, mockSiteTemplateConfigurationPrivate);
		inOrder.verify(mockPermissionCheckerInitializer, times(1)).runAsUser(USER_ID);
	}

	@Test
	@Parameters({ "true,true", "true,false", "false,false", "false,true" })
	public void applySiteTemplateToGroup_WhenOnlyThePublicSiteTemplatesIsSpecified_ThenChangesAreAppliedToTheGroup(boolean publicPropagationEnabled, boolean privatePropagationEnabled)
			throws Exception {
		when(PrincipalThreadLocal.getUserId()).thenReturn(USER_ID);
		when(mockGroup.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockGroup.getPublicLayoutSet()).thenReturn(mockLayoutSetPublic);
		when(mockGroup.getPrivateLayoutSet()).thenReturn(mockLayoutSetPrivate);
		when(mockSiteTemplateUtil.getSiteTemplateConfigurationToApply(mockLayoutSetPublic, Optional.of(mockLayoutSetPrototypePublic), publicPropagationEnabled))
				.thenReturn(mockSiteTemplateConfigurationPublic);
		when(mockSiteTemplateUtil.getSiteTemplateConfigurationToApply(mockLayoutSetPrivate, Optional.empty(), privatePropagationEnabled)).thenReturn(mockSiteTemplateConfigurationPrivate);

		layoutSetPrototypeInitializerImpl.applySiteTemplateToGroup(mockGroup, Optional.of(mockLayoutSetPrototypePublic), publicPropagationEnabled, Optional.empty(), privatePropagationEnabled,
				mockServiceContext);

		InOrder inOrder = Mockito.inOrder(mockSiteTemplateUtil, mockPermissionCheckerInitializer);
		inOrder.verify(mockPermissionCheckerInitializer, times(1)).runAsCompanyAdministrator(COMPANY_ID);
		inOrder.verify(mockSiteTemplateUtil, times(1)).removeExistingPages(mockGroup, mockSiteTemplateConfigurationPublic, mockSiteTemplateConfigurationPrivate, mockServiceContext);
		inOrder.verify(mockSiteTemplateUtil, times(1)).applySiteTemplatesToGroup(mockGroup, mockSiteTemplateConfigurationPublic, mockSiteTemplateConfigurationPrivate);
		inOrder.verify(mockSiteTemplateUtil, times(1)).applyThemeToGroup(mockGroup, mockSiteTemplateConfigurationPublic, mockSiteTemplateConfigurationPrivate);
		inOrder.verify(mockPermissionCheckerInitializer, times(1)).runAsUser(USER_ID);
	}
}
