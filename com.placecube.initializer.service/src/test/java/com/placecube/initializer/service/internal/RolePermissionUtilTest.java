package com.placecube.initializer.service.internal;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.ResourceConstants;
import com.liferay.portal.kernel.service.ResourcePermissionLocalService;

public class RolePermissionUtilTest extends PowerMockito {

	private static final long COMPANY_ID = 1;
	private static final long ROLE_ID = 2;
	private static final long PRIM_KEY = 3;

	@InjectMocks
	private RolePermissionUtil rolePermissionUtil;

	@Mock
	private ResourcePermissionLocalService mockResourcePermissionLocalService;

	@Mock
	private Layout mockLayout;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test
	public void addPermissions_WhenNoError_ThenSetsTheResourcePermissions() throws PortalException {
		String[] actionIds = new String[] { "one", "two" };
		when(mockLayout.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockLayout.getPrimaryKey()).thenReturn(PRIM_KEY);

		rolePermissionUtil.addPermissions(mockLayout, ROLE_ID, actionIds);

		verify(mockResourcePermissionLocalService, times(1)).setResourcePermissions(COMPANY_ID, Layout.class.getName(), ResourceConstants.SCOPE_INDIVIDUAL, String.valueOf(PRIM_KEY), ROLE_ID,
				actionIds);
	}

	@Test(expected = PortalException.class)
	public void addPermissions_WhenExceptionSettingTheResourcePermissions_ThenThrowsPortalException() throws PortalException {
		String[] actionIds = new String[] { "one", "two" };
		when(mockLayout.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockLayout.getPrimaryKey()).thenReturn(PRIM_KEY);
		doThrow(new PortalException()).when(mockResourcePermissionLocalService).setResourcePermissions(COMPANY_ID, Layout.class.getName(), ResourceConstants.SCOPE_INDIVIDUAL, String.valueOf(PRIM_KEY),
				ROLE_ID, actionIds);

		rolePermissionUtil.addPermissions(mockLayout, ROLE_ID, actionIds);
	}

	@Test
	public void removePermission_WhenNoError_ThenRemovesTheResourcePermission() throws PortalException {
		String actionIds = "one";
		when(mockLayout.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockLayout.getPrimaryKey()).thenReturn(PRIM_KEY);

		rolePermissionUtil.removePermission(mockLayout, ROLE_ID, actionIds);

		verify(mockResourcePermissionLocalService, times(1)).removeResourcePermission(COMPANY_ID, Layout.class.getName(), ResourceConstants.SCOPE_INDIVIDUAL, String.valueOf(PRIM_KEY), ROLE_ID,
				actionIds);
	}

	@Test(expected = PortalException.class)
	public void removePermission_WhenExceptionRemovingTheResourcePermissions_ThenThrowsPortalException() throws PortalException {
		String actionId = "one";
		when(mockLayout.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockLayout.getPrimaryKey()).thenReturn(PRIM_KEY);
		doThrow(new PortalException()).when(mockResourcePermissionLocalService).removeResourcePermission(COMPANY_ID, Layout.class.getName(), ResourceConstants.SCOPE_INDIVIDUAL,
				String.valueOf(PRIM_KEY), ROLE_ID, actionId);

		rolePermissionUtil.removePermission(mockLayout, ROLE_ID, actionId);
	}

}
