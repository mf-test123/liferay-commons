package com.placecube.initializer.service.impl;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.fragment.model.FragmentCollection;
import com.liferay.fragment.model.FragmentEntry;
import com.liferay.fragment.service.FragmentCollectionLocalService;
import com.liferay.fragment.service.FragmentEntryLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.placecube.initializer.service.internal.FragmentUtil;

public class FragmentInitializerImplTest extends PowerMockito {

	private static final String BASE_PATH = "basePathValue";
	private static final Long GROUP_ID = 11l;
	private static final String CSS_VALUE = "cssValue";
	private static final String HTML_VALUE = "htmlValue";
	private static final String JS_VALUE = "jsValue";
	private static final String CONFIGURATION_VALUE = "configurationValue";
	private static final String NAME_VALUE = "nameValue";
	private static final long COLLECTION_ID = 456;
	private static final long USER_ID = 8794651;
	private static final String COLLECTION_KEY = "collectionKeyValue";
	private static final String COLLECTION_NAME = "collectionNameValue";
	private static final String FRAGMENT_ENTRY_KEY = "fragmentEntryKeyValue";

	@InjectMocks
	private FragmentInitializerImpl fragmentInitializerImpl;

	@Mock
	private FragmentCollectionLocalService mockFragmentCollectionLocalService;

	@Mock
	private FragmentUtil mockFragmentUtil;

	@Mock
	private FragmentEntryLocalService mockFragmentEntryLocalService;

	@Mock
	private FragmentCollection mockFragmentCollection;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private FragmentEntry mockFragmentEntry;

	@Mock
	private ClassLoader mockClassLoader;

	@Mock
	private JSONObject mockJSONObject;

	@Before
	public void setUp() {
		initMocks(this);
	}

	@Test(expected = PortalException.class)
	public void getOrCreateFragment_WhenExceptionRetrievingJSONFragment_ThenThrowsPortalException() throws Exception {
		when(mockFragmentUtil.getFragmentJSONObject(mockClassLoader, BASE_PATH)).thenThrow(new PortalException());

		fragmentInitializerImpl.getOrCreateFragment(mockFragmentCollection, mockClassLoader, BASE_PATH, mockServiceContext);
	}

	@Test
	public void getOrCreateFragment_WhenFragmentAlreadyExists_ThenReturnsTheFragmentEntry() throws Exception {
		when(mockFragmentUtil.getFragmentJSONObject(mockClassLoader, BASE_PATH)).thenReturn(mockJSONObject);
		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockJSONObject.getString("fragmentEntryKey")).thenReturn(FRAGMENT_ENTRY_KEY);
		when(mockFragmentEntryLocalService.fetchFragmentEntry(GROUP_ID, FRAGMENT_ENTRY_KEY)).thenReturn(mockFragmentEntry);

		FragmentEntry result = fragmentInitializerImpl.getOrCreateFragment(mockFragmentCollection, mockClassLoader, BASE_PATH, mockServiceContext);

		assertThat(result, sameInstance(mockFragmentEntry));
	}

	@Test
	public void getOrCreateFragment_WhenFragmentAlreadyExists_ThenDoesNotCreateAnewFragmentEntry() throws Exception {
		when(mockFragmentUtil.getFragmentJSONObject(mockClassLoader, BASE_PATH)).thenReturn(mockJSONObject);
		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockJSONObject.getString("fragmentEntryKey")).thenReturn(FRAGMENT_ENTRY_KEY);
		when(mockFragmentEntryLocalService.fetchFragmentEntry(GROUP_ID, FRAGMENT_ENTRY_KEY)).thenReturn(mockFragmentEntry);

		fragmentInitializerImpl.getOrCreateFragment(mockFragmentCollection, mockClassLoader, BASE_PATH, mockServiceContext);

		verify(mockFragmentEntryLocalService, never()).addFragmentEntry(anyLong(), anyLong(), anyLong(), anyString(), anyString(), anyString(), anyString(), anyString(), anyString(), anyLong(),
				anyInt(), anyInt(), any(ServiceContext.class));
	}

	@Test(expected = PortalException.class)
	public void getOrCreateFragment_WhenFragmentDoesNotAlreadyExistsAndExceptionCreatingFragment_ThenThrowsPortalException() throws Exception {
		when(mockFragmentUtil.getFragmentJSONObject(mockClassLoader, BASE_PATH)).thenReturn(mockJSONObject);
		when(mockFragmentEntryLocalService.fetchFragmentEntry(GROUP_ID, FRAGMENT_ENTRY_KEY)).thenReturn(null);
		mockServiceContextDetails();
		mockCollectionDetails();
		mockFragmentDetails("section");
		when(mockFragmentEntryLocalService.addFragmentEntry(USER_ID, GROUP_ID, COLLECTION_ID, FRAGMENT_ENTRY_KEY, NAME_VALUE, CSS_VALUE, HTML_VALUE, JS_VALUE, CONFIGURATION_VALUE, 0, 0,
				WorkflowConstants.STATUS_APPROVED, mockServiceContext)).thenThrow(new PortalException());

		fragmentInitializerImpl.getOrCreateFragment(mockFragmentCollection, mockClassLoader, BASE_PATH, mockServiceContext);
	}

	@Test
	public void getOrCreateFragment_WhenFragmentDoesNotAlreadyExistsAndTypeIsSection_ThenReturnsTheCreatedFragmentEntryWithTypeZero() throws Exception {
		when(mockFragmentUtil.getFragmentJSONObject(mockClassLoader, BASE_PATH)).thenReturn(mockJSONObject);
		when(mockFragmentEntryLocalService.fetchFragmentEntry(GROUP_ID, FRAGMENT_ENTRY_KEY)).thenReturn(null);
		mockServiceContextDetails();
		mockCollectionDetails();
		mockFragmentDetails("section");
		when(mockFragmentEntryLocalService.addFragmentEntry(USER_ID, GROUP_ID, COLLECTION_ID, FRAGMENT_ENTRY_KEY, NAME_VALUE, CSS_VALUE, HTML_VALUE, JS_VALUE, CONFIGURATION_VALUE, 0, 0,
				WorkflowConstants.STATUS_APPROVED, mockServiceContext)).thenReturn(mockFragmentEntry);

		FragmentEntry result = fragmentInitializerImpl.getOrCreateFragment(mockFragmentCollection, mockClassLoader, BASE_PATH, mockServiceContext);

		assertThat(result, sameInstance(mockFragmentEntry));
	}

	@Test
	public void getOrCreateFragment_WhenFragmentDoesNotAlreadyExistsAndTypeIsNotSection_ThenReturnsTheCreatedFragmentEntryWithTypeOne() throws Exception {
		when(mockFragmentUtil.getFragmentJSONObject(mockClassLoader, BASE_PATH)).thenReturn(mockJSONObject);
		when(mockFragmentEntryLocalService.fetchFragmentEntry(GROUP_ID, FRAGMENT_ENTRY_KEY)).thenReturn(null);
		mockServiceContextDetails();
		mockCollectionDetails();
		mockFragmentDetails("differentValue");
		when(mockFragmentEntryLocalService.addFragmentEntry(USER_ID, GROUP_ID, COLLECTION_ID, FRAGMENT_ENTRY_KEY, NAME_VALUE, CSS_VALUE, HTML_VALUE, JS_VALUE, CONFIGURATION_VALUE, 0, 1,
				WorkflowConstants.STATUS_APPROVED, mockServiceContext)).thenReturn(mockFragmentEntry);

		FragmentEntry result = fragmentInitializerImpl.getOrCreateFragment(mockFragmentCollection, mockClassLoader, BASE_PATH, mockServiceContext);

		assertThat(result, sameInstance(mockFragmentEntry));
	}

	private void mockCollectionDetails() throws PortalException {
		when(mockFragmentCollection.getFragmentCollectionId()).thenReturn(COLLECTION_ID);
	}

	private void mockFragmentDetails(String type) {
		when(mockJSONObject.getString("fragmentEntryKey")).thenReturn(FRAGMENT_ENTRY_KEY);
		when(mockFragmentUtil.getOptionalContent(mockClassLoader, BASE_PATH, mockJSONObject, "cssPath")).thenReturn(CSS_VALUE);
		when(mockFragmentUtil.getOptionalContent(mockClassLoader, BASE_PATH, mockJSONObject, "htmlPath")).thenReturn(HTML_VALUE);
		when(mockFragmentUtil.getOptionalContent(mockClassLoader, BASE_PATH, mockJSONObject, "jsPath")).thenReturn(JS_VALUE);
		when(mockFragmentUtil.getOptionalContent(mockClassLoader, BASE_PATH, mockJSONObject, "configurationPath")).thenReturn(CONFIGURATION_VALUE);
		when(mockJSONObject.getString("name")).thenReturn(NAME_VALUE);
		when(mockJSONObject.getString("type")).thenReturn(type);
	}

	@Test
	public void getOrCreateFragmentCollection_WhenCollectionIsFound_ThenDoesNotAttemptToCreateAnewCollection() throws PortalException {
		long groupId = 11;
		when(mockServiceContext.getScopeGroupId()).thenReturn(groupId);
		when(mockFragmentCollectionLocalService.fetchFragmentCollection(groupId, COLLECTION_KEY)).thenReturn(mockFragmentCollection);

		fragmentInitializerImpl.getOrCreateFragmentCollection(COLLECTION_KEY, COLLECTION_NAME, mockServiceContext);

		verify(mockFragmentCollectionLocalService, never()).addFragmentCollection(anyLong(), anyLong(), anyString(), anyString(), anyString(), any(ServiceContext.class));
	}

	@Test
	public void getOrCreateFragmentCollection_WhenCollectionIsFound_ThenReturnsTheCollection() throws PortalException {
		long groupId = 11;
		when(mockServiceContext.getScopeGroupId()).thenReturn(groupId);
		when(mockFragmentCollectionLocalService.fetchFragmentCollection(groupId, COLLECTION_KEY)).thenReturn(mockFragmentCollection);

		FragmentCollection result = fragmentInitializerImpl.getOrCreateFragmentCollection(COLLECTION_KEY, COLLECTION_NAME, mockServiceContext);

		assertThat(result, sameInstance(mockFragmentCollection));
	}

	@Test(expected = PortalException.class)
	public void getOrCreateFragmentCollection_WhenCollectionIsNotFoundAndExceptionCreatingNewCollection_ThenThrowsPortalException() throws PortalException {
		long groupId = 11;
		long userId = 22;
		when(mockServiceContext.getScopeGroupId()).thenReturn(groupId);
		when(mockServiceContext.getUserId()).thenReturn(userId);
		when(mockFragmentCollectionLocalService.fetchFragmentCollection(groupId, COLLECTION_KEY)).thenReturn(null);
		when(mockFragmentCollectionLocalService.addFragmentCollection(userId, groupId, COLLECTION_KEY, COLLECTION_NAME, StringPool.BLANK, mockServiceContext)).thenThrow(new PortalException());

		fragmentInitializerImpl.getOrCreateFragmentCollection(COLLECTION_KEY, COLLECTION_NAME, mockServiceContext);
	}

	@Test
	public void getOrCreateFragmentCollection_WhenCollectionIsNotFoundAndNoError_ThenReturnsTheNewCollection() throws PortalException {
		long groupId = 11;
		long userId = 22;
		when(mockServiceContext.getScopeGroupId()).thenReturn(groupId);
		when(mockServiceContext.getUserId()).thenReturn(userId);
		when(mockFragmentCollectionLocalService.fetchFragmentCollection(groupId, COLLECTION_KEY)).thenReturn(null);
		when(mockFragmentCollectionLocalService.addFragmentCollection(userId, groupId, COLLECTION_KEY, COLLECTION_NAME, StringPool.BLANK, mockServiceContext)).thenReturn(mockFragmentCollection);

		FragmentCollection result = fragmentInitializerImpl.getOrCreateFragmentCollection(COLLECTION_KEY, COLLECTION_NAME, mockServiceContext);

		assertThat(result, sameInstance(mockFragmentCollection));
	}

	private void mockServiceContextDetails() {
		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockServiceContext.getUserId()).thenReturn(USER_ID);
	}

}
