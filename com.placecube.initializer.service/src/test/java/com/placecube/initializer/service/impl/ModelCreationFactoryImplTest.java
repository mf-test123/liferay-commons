package com.placecube.initializer.service.impl;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.petra.string.StringPool;
import com.placecube.initializer.model.FragmentContext;
import com.placecube.initializer.model.LayoutContext;
import com.placecube.initializer.model.PortletContext;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class ModelCreationFactoryImplTest extends PowerMockito {

	@InjectMocks
	private ModelCreationFactoryImpl modelCreationFactoryImpl;

	@Test
	@Parameters({ "true", "false" })
	public void createContentPageLayoutContext_ThenReturnsItemWithConfiguredFriendlyURL(boolean privateLayout) {
		LayoutContext result = modelCreationFactoryImpl.createContentPageLayoutContext("nameValue", "friendlyURLValue", privateLayout);

		assertThat(result.getFriendlyURL(), equalTo("friendlyURLValue"));
	}

	@Test
	@Parameters({ "true", "false" })
	public void createContentPageLayoutContext_ThenReturnsItemWithConfiguredIsPrivate(boolean privateLayout) {
		LayoutContext result = modelCreationFactoryImpl.createContentPageLayoutContext("nameValue", "friendlyURLValue", privateLayout);

		assertThat(result.isPrivate(), equalTo(privateLayout));
	}

	@Test
	@Parameters({ "true", "false" })
	public void createContentPageLayoutContext_ThenReturnsItemWithConfiguredName(boolean privateLayout) {
		LayoutContext result = modelCreationFactoryImpl.createContentPageLayoutContext("nameValue", "friendlyURLValue", privateLayout);

		assertThat(result.getName(), equalTo("nameValue"));
	}

	@Test
	public void createFragmentContext_ThenReturnsItemWithConfiguredFieldPlaceholder() {
		String placeholder = "placeholderValue";

		FragmentContext result = modelCreationFactoryImpl.createFragmentContext("idVal", placeholder, "settingsVal");

		assertThat(result.getPlaceholder(), equalTo(placeholder));
	}

	@Test
	public void createFragmentContext_ThenReturnsItemWithConfiguredFieldSettings() {
		String settings = "settingsValue";

		FragmentContext result = modelCreationFactoryImpl.createFragmentContext("idVal", "placeholderVal", settings);

		assertThat(result.getSettings(), equalTo(settings));
	}

	@Test
	public void createFragmentContext_ThenReturnsItemWithConfiguredId() {
		String id = "idValue";

		FragmentContext result = modelCreationFactoryImpl.createFragmentContext(id, "placeholderVal", "settingsVal");

		assertThat(result.getId(), equalTo(id));
	}

	@Test
	@Parameters({ "true", "false" })
	public void createLayoutContext_ThenReturnsItemWithConfiguredFriendlyURL(boolean privateLayout) {
		LayoutContext result = modelCreationFactoryImpl.createLayoutContext("nameValue", "friendlyURLValue", privateLayout);

		assertThat(result.getFriendlyURL(), equalTo("friendlyURLValue"));
	}

	@Test
	@Parameters({ "true", "false" })
	public void createLayoutContext_ThenReturnsItemWithConfiguredIsPrivate(boolean privateLayout) {
		LayoutContext result = modelCreationFactoryImpl.createLayoutContext("nameValue", "friendlyURLValue", privateLayout);

		assertThat(result.isPrivate(), equalTo(privateLayout));
	}

	@Test
	@Parameters({ "true", "false" })
	public void createLayoutContext_ThenReturnsItemWithConfiguredName(boolean privateLayout) {
		LayoutContext result = modelCreationFactoryImpl.createLayoutContext("nameValue", "friendlyURLValue", privateLayout);

		assertThat(result.getName(), equalTo("nameValue"));
	}

	@Test
	public void createPageTemplateLayoutContext_ThenReturnsItemWithConfiguredName() {
		LayoutContext result = modelCreationFactoryImpl.createPageTemplateLayoutContext("nameValue");

		assertThat(result.getName(), equalTo("nameValue"));
	}

	@Test
	public void createPageTemplateLayoutContext_WithTemplateId_ThenReturnsItemWithConfiguredNameAndTemplateId() {
		String name = "nameValue";
		String templateId = "30_90";

		LayoutContext result = modelCreationFactoryImpl.createPageTemplateLayoutContext(name, templateId);

		assertThat(result.getName(), equalTo(name));
		assertThat(result.getLayoutTemplateId(), equalTo(templateId));
	}

	@Test
	public void createPortletContext_WithPortletIdAndPlaceholder_ThenReturnsItemWithConfiguredFieldPlaceholder() {
		String placeholder = "placeholderValue";

		PortletContext result = modelCreationFactoryImpl.createPortletContext("portletId", placeholder);

		assertThat(result.getPlaceholder(), equalTo(placeholder));
	}

	@Test
	public void createPortletContext_WithPortletIdAndPlaceholder_ThenReturnsItemWithConfiguredFieldPortletId() {
		String portletId = "portletIdValue";

		PortletContext result = modelCreationFactoryImpl.createPortletContext(portletId, "placeholder");

		assertThat(result.getPortletId(), equalTo(portletId));
	}

	@Test
	public void createPortletContext_WithPortletIdAndPlaceholder_ThenReturnsItemWithConfiguredFieldPreferencesAsEmptyMap() {
		PortletContext result = modelCreationFactoryImpl.createPortletContext("portletId", "placeholder");

		assertThat(result.getPortletPreferences().isEmpty(), equalTo(true));
	}

	@Test
	public void createPortletContext_WithPortletIdAndPlaceholder_ThenReturnsItemWithEmptyCustomInstanceId() {
		PortletContext result = modelCreationFactoryImpl.createPortletContext("portletId", "placeholderValue");

		assertThat(result.getCustomInstanceId(), equalTo(StringPool.BLANK));
	}

	@Test
	public void createPortletContext_WithPortletIdAndPlaceholderAndCustomInstanceId_ThenReturnsItemWithConfiguredCustomInstanceId() {
		PortletContext result = modelCreationFactoryImpl.createPortletContext("portletId", "placeholderValue", "customInstanceIdValue");

		assertThat(result.getCustomInstanceId(), equalTo("customInstanceIdValue"));
	}

	@Test
	public void createPortletContext_WithPortletIdAndPreferences_ThenReturnsItemWithConfiguredFieldPlaceholderAsEmptyString() {
		String portletId = "portletIdValue";
		Map<String, String> portletPreferences = new HashMap<>();
		portletPreferences.put("one", "oneV");

		PortletContext result = modelCreationFactoryImpl.createPortletContext(portletId, portletPreferences);

		assertThat(result.getPlaceholder(), equalTo(StringPool.BLANK));
	}

	@Test
	public void createPortletContext_WithPortletIdAndPreferences_ThenReturnsItemWithConfiguredFieldPortletId() {
		String portletId = "portletIdValue";
		Map<String, String> portletPreferences = new HashMap<>();

		PortletContext result = modelCreationFactoryImpl.createPortletContext(portletId, portletPreferences);

		assertThat(result.getPortletId(), equalTo(portletId));
	}

	@Test
	public void createPortletContext_WithPortletIdAndPreferences_ThenReturnsItemWithConfiguredFieldPreferences() {
		String portletId = "portletIdValue";
		Map<String, String> portletPreferences = new HashMap<>();
		portletPreferences.put("one", "oneV");

		PortletContext result = modelCreationFactoryImpl.createPortletContext(portletId, portletPreferences);

		assertThat(result.getPortletPreferences(), sameInstance(portletPreferences));
	}

	@Test
	public void createPortletContext_WithPortletIdAndPreferences_ThenReturnsItemWithEmptyCustomInstanceId() {
		String portletId = "portletIdValue";
		Map<String, String> portletPreferences = new HashMap<>();

		PortletContext result = modelCreationFactoryImpl.createPortletContext(portletId, portletPreferences);

		assertThat(result.getCustomInstanceId(), equalTo(StringPool.BLANK));
	}

	@Test
	public void createPortletContext_WithPortletIdAndPreferencesAndCustomInstanceId_ThenReturnsItemWithConfiguredCustomInstanceId() {
		String portletId = "portletIdValue";
		Map<String, String> portletPreferences = new HashMap<>();

		PortletContext result = modelCreationFactoryImpl.createPortletContext(portletId, portletPreferences, "customInstanceIdValue");

		assertThat(result.getCustomInstanceId(), equalTo("customInstanceIdValue"));
	}

	@Test
	@Parameters({ "true", "false" })
	public void createWidgetPageLayoutContext_ThenReturnsItemWithConfiguredFriendlyURL(boolean privateLayout) {
		LayoutContext result = modelCreationFactoryImpl.createWidgetPageLayoutContext("nameValue", "friendlyURLValue", privateLayout, "layoutTemplateId");

		assertThat(result.getFriendlyURL(), equalTo("friendlyURLValue"));
	}

	@Test
	@Parameters({ "true", "false" })
	public void createWidgetPageLayoutContext_ThenReturnsItemWithConfiguredIsPrivate(boolean privateLayout) {
		LayoutContext result = modelCreationFactoryImpl.createWidgetPageLayoutContext("nameValue", "friendlyURLValue", privateLayout, "layoutTemplateId");

		assertThat(result.isPrivate(), equalTo(privateLayout));
	}

	@Test
	@Parameters({ "true", "false" })
	public void createWidgetPageLayoutContext_ThenReturnsItemWithConfiguredLayoutTemplateId(boolean privateLayout) {
		LayoutContext result = modelCreationFactoryImpl.createWidgetPageLayoutContext("nameValue", "friendlyURLValue", privateLayout, "layoutTemplateId");

		assertThat(result.getLayoutTemplateId(), equalTo("layoutTemplateId"));
	}

	@Test
	@Parameters({ "true", "false" })
	public void createWidgetPageLayoutContext_ThenReturnsItemWithConfiguredName(boolean privateLayout) {
		LayoutContext result = modelCreationFactoryImpl.createWidgetPageLayoutContext("nameValue", "friendlyURLValue", privateLayout, "layoutTemplateId");

		assertThat(result.getName(), equalTo("nameValue"));
	}

}
