package com.placecube.initializer.service.impl;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;

import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.layout.page.template.constants.LayoutPageTemplateEntryTypeConstants;
import com.liferay.layout.page.template.model.LayoutPageTemplateEntry;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.LayoutConstants;
import com.liferay.portal.kernel.model.LayoutTypePortlet;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.initializer.model.FragmentContext;
import com.placecube.initializer.model.LayoutContext;
import com.placecube.initializer.model.PortletContext;
import com.placecube.initializer.service.internal.InitializerUtil;
import com.placecube.initializer.service.internal.PageTemplateUtil;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class LayoutInitializerImplTest extends PowerMockito {

	private static final int COLUMN_INDEX_1 = 11;
	private static final int COLUMN_INDEX_2 = 22;
	private static final String FRIENDLY_URL = "friendlyURLValue";
	private static final long GROUP_ID = 12345;
	private static final Long LAYOUT_ID = 3l;
	private static final String LAYOUT_TEMPLATE_ID = "layoutTemplateIdValue";
	private static final long LINK_LAYOUT_ID = 44;
	private static final String LINK_TO_LAYOUT_FRIENDLY_URL = "linkToLayoutFriendlyURL";
	private static final Locale LOCALE = Locale.FRENCH;
	private static final String NAME = "entryNameValue";
	private static final long PLID = 445566;
	private static final String STRUCTURE_CONTENT = "structureContentValue";
	private static final String TYPE_SETTINGS = "typeSettingsValue";
	private static final long USER_ID = 23456;

	@InjectMocks
	private LayoutInitializerImpl layoutInitializerImpl;

	@Mock
	private Layout mockDraftLayout;

	@Mock
	private Set<FragmentContext> mockFragmentContexts;

	@Mock
	private InitializerUtil mockInitializerUtil;

	@Mock
	private LayoutContext mockLayoutContext;

	@Mock
	private LayoutLocalService mockLayoutLocalService;

	@Mock
	private LayoutPageTemplateEntry mockLayoutPageTemplateEntry;

	@Mock
	private LayoutTypePortlet mockLayoutTypePortlet;

	@Mock
	private Layout mockLinkLayout;

	@Mock
	private Layout mockLiveLayout;

	@Mock
	private PageTemplateUtil mockPageTemplateUtil;

	@Mock
	private Set<PortletContext> mockPortletContexts;

	@Mock
	private Set<PortletContext> mockPortlets1;

	@Mock
	private Set<PortletContext> mockPortlets2;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private Map<String, String> mockTypeSettingsMap;

	@Test
	@Parameters({ "true", "false" })
	public void getOrCreateContentLayout_WhenLayoutAlreadyExists_ThenReturnsTheLayout(boolean privateLayout) throws PortalException {
		mockServiceContextAndClassNameDetails();
		mockLayoutContextDetails();
		when(mockLayoutContext.isPrivate()).thenReturn(privateLayout);
		when(mockLayoutLocalService.fetchLayoutByFriendlyURL(GROUP_ID, privateLayout, FRIENDLY_URL)).thenReturn(mockDraftLayout);

		Layout result = layoutInitializerImpl.getOrCreateContentLayout(mockLayoutContext, mockServiceContext);

		assertThat(result, sameInstance(mockDraftLayout));
	}

	@Test
	@Parameters({ "true", "false" })
	public void getOrCreateContentLayout_WhenLayoutDoesNotAlreadyExistAndNoError_ThenReturnsTheCreatedLayout(boolean privateLayout) throws PortalException {
		mockServiceContextAndClassNameDetails();
		mockLayoutContextDetails();
		when(mockLayoutContext.isPrivate()).thenReturn(privateLayout);
		when(mockLayoutLocalService.fetchLayoutByFriendlyURL(GROUP_ID, privateLayout, FRIENDLY_URL)).thenReturn(null);
		when(mockInitializerUtil.createLayout(mockLayoutContext, LayoutConstants.TYPE_CONTENT, mockServiceContext)).thenReturn(mockDraftLayout);
		when(mockDraftLayout.getPlid()).thenReturn(PLID);
		when(mockPageTemplateUtil.configureContentPage(mockFragmentContexts, mockPortletContexts, STRUCTURE_CONTENT, mockServiceContext, PLID)).thenReturn(mockLiveLayout);

		Layout result = layoutInitializerImpl.getOrCreateContentLayout(mockLayoutContext, mockServiceContext);

		assertThat(result, sameInstance(mockLiveLayout));
	}

	@Test(expected = PortalException.class)
	@Parameters({ "true", "false" })
	public void getOrCreateContentLayout_WhenLayoutDoesNotAlreadyExistsAndExceptionCreatingNewLayout_ThenThrowsPortalException(boolean privateLayout) throws PortalException {
		mockServiceContextAndClassNameDetails();
		mockLayoutContextDetails();
		when(mockLayoutContext.isPrivate()).thenReturn(privateLayout);
		when(mockLayoutLocalService.fetchLayoutByFriendlyURL(GROUP_ID, privateLayout, FRIENDLY_URL)).thenReturn(null);
		when(mockInitializerUtil.createLayout(mockLayoutContext, LayoutConstants.TYPE_CONTENT, mockServiceContext)).thenThrow(new PortalException());

		layoutInitializerImpl.getOrCreateContentLayout(mockLayoutContext, mockServiceContext);
	}

	@Test(expected = PortalException.class)
	@Parameters({ "true", "false" })
	public void getOrCreateLinkToPageLayout_WhenExceptionRetrievingLinkToLayout_ThenThrowsPortalException(boolean privateLayout) throws PortalException {
		mockServiceContextAndClassNameDetails();
		when(mockLayoutContext.isPrivate()).thenReturn(privateLayout);
		when(mockLayoutContext.getLinkToLayoutFriendlyURL()).thenReturn(LINK_TO_LAYOUT_FRIENDLY_URL);
		when(mockLayoutLocalService.getFriendlyURLLayout(GROUP_ID, privateLayout, LINK_TO_LAYOUT_FRIENDLY_URL)).thenThrow(new PortalException());

		layoutInitializerImpl.getOrCreateLinkToPageLayout(mockLayoutContext, mockServiceContext);
	}

	@Test
	@Parameters({ "true", "false" })
	public void getOrCreateLinkToPageLayout_WhenLayoutAlreadyExists_ThenReturnsTheLayout(boolean privateLayout) throws PortalException {
		mockServiceContextAndClassNameDetails();
		mockLayoutContextDetails();
		when(mockLayoutContext.isPrivate()).thenReturn(privateLayout);
		mockLinkToLayoutDetails(privateLayout);
		when(mockLayoutLocalService.fetchLayoutByFriendlyURL(GROUP_ID, privateLayout, FRIENDLY_URL)).thenReturn(mockDraftLayout);

		Layout result = layoutInitializerImpl.getOrCreateLinkToPageLayout(mockLayoutContext, mockServiceContext);

		assertThat(result, sameInstance(mockDraftLayout));
	}

	@Test
	@Parameters({ "true", "false" })
	public void getOrCreateLinkToPageLayout_WhenLayoutDoesNotAlreadyExistAndNoError_ThenConfiguresTheLinkToLayoutIdInTheTypeSettingsBeforeCreatingTheLayout(boolean privateLayout)
			throws PortalException {
		mockServiceContextAndClassNameDetails();
		mockLayoutContextDetails();
		when(mockLayoutContext.isPrivate()).thenReturn(privateLayout);
		mockLinkToLayoutDetails(privateLayout);
		when(mockLayoutLocalService.fetchLayoutByFriendlyURL(GROUP_ID, privateLayout, FRIENDLY_URL)).thenReturn(null);
		when(mockInitializerUtil.createLayout(mockLayoutContext, LayoutConstants.TYPE_LINK_TO_LAYOUT, mockServiceContext)).thenReturn(mockDraftLayout);

		layoutInitializerImpl.getOrCreateLinkToPageLayout(mockLayoutContext, mockServiceContext);

		InOrder inOrder = inOrder(mockInitializerUtil, mockTypeSettingsMap);
		inOrder.verify(mockTypeSettingsMap, times(1)).put("linkToLayoutId", String.valueOf(LINK_LAYOUT_ID));
		inOrder.verify(mockInitializerUtil, times(1)).createLayout(mockLayoutContext, LayoutConstants.TYPE_LINK_TO_LAYOUT, mockServiceContext);
	}

	@Test
	@Parameters({ "true", "false" })
	public void getOrCreateLinkToPageLayout_WhenLayoutDoesNotAlreadyExistAndNoError_ThenReturnsTheCreatedLayout(boolean privateLayout) throws PortalException {
		mockServiceContextAndClassNameDetails();
		mockLayoutContextDetails();
		when(mockLayoutContext.isPrivate()).thenReturn(privateLayout);
		mockLinkToLayoutDetails(privateLayout);
		when(mockLayoutLocalService.fetchLayoutByFriendlyURL(GROUP_ID, privateLayout, FRIENDLY_URL)).thenReturn(null);
		when(mockInitializerUtil.createLayout(mockLayoutContext, LayoutConstants.TYPE_LINK_TO_LAYOUT, mockServiceContext)).thenReturn(mockDraftLayout);

		Layout result = layoutInitializerImpl.getOrCreateLinkToPageLayout(mockLayoutContext, mockServiceContext);

		assertThat(result, sameInstance(mockDraftLayout));
	}

	@Test(expected = PortalException.class)
	@Parameters({ "true", "false" })
	public void getOrCreateLinkToPageLayout_WhenLayoutDoesNotAlreadyExistsAndExceptionCreatingNewLayout_ThenThrowsPortalException(boolean privateLayout) throws PortalException {
		mockServiceContextAndClassNameDetails();
		mockLayoutContextDetails();
		when(mockLayoutContext.isPrivate()).thenReturn(privateLayout);
		mockLinkToLayoutDetails(privateLayout);
		when(mockLayoutLocalService.fetchLayoutByFriendlyURL(GROUP_ID, privateLayout, FRIENDLY_URL)).thenReturn(null);
		when(mockInitializerUtil.createLayout(mockLayoutContext, LayoutConstants.TYPE_LINK_TO_LAYOUT, mockServiceContext)).thenThrow(new PortalException());

		layoutInitializerImpl.getOrCreateLinkToPageLayout(mockLayoutContext, mockServiceContext);
	}

	@Test
	@Parameters({ "true", "false" })
	public void getOrCreatePortletLayout_WhenLayoutAlreadyExists_ThenReturnsTheLayout(boolean privateLayout) throws PortalException {
		mockServiceContextAndClassNameDetails();
		mockLayoutContextDetails();
		when(mockLayoutContext.isPrivate()).thenReturn(privateLayout);
		when(mockLayoutLocalService.fetchLayoutByFriendlyURL(GROUP_ID, privateLayout, FRIENDLY_URL)).thenReturn(mockDraftLayout);

		Layout result = layoutInitializerImpl.getOrCreatePortletLayout(mockLayoutContext, mockServiceContext);

		assertThat(result, sameInstance(mockDraftLayout));
	}

	@Test(expected = PortalException.class)
	@Parameters({ "true", "false" })
	public void getOrCreatePortletLayout_WhenLayoutDoesNotAlreadyExistAndExceptionCreatingNewLayout_ThenThrowsPortalException(boolean privateLayout) throws PortalException {
		mockServiceContextAndClassNameDetails();
		mockLayoutContextDetails();
		when(mockLayoutContext.isPrivate()).thenReturn(privateLayout);
		when(mockLayoutLocalService.fetchLayoutByFriendlyURL(GROUP_ID, privateLayout, FRIENDLY_URL)).thenReturn(null);
		when(mockInitializerUtil.createLayout(mockLayoutContext, LayoutConstants.TYPE_PORTLET, mockServiceContext)).thenThrow(new PortalException());

		layoutInitializerImpl.getOrCreatePortletLayout(mockLayoutContext, mockServiceContext);
	}

	@Test
	@Parameters({ "true", "false" })
	public void getOrCreatePortletLayout_WhenLayoutDoesNotAlreadyExistsAndNoError_ThenConfiguresTheLayout(boolean privateLayout) throws PortalException {
		when(mockLayoutLocalService.fetchLayoutByFriendlyURL(GROUP_ID, privateLayout, FRIENDLY_URL)).thenReturn(null);
		mockServiceContextAndClassNameDetails();
		mockLayoutContextDetails();
		when(mockLayoutContext.isPrivate()).thenReturn(privateLayout);
		mockDraftLayoutCreation(privateLayout);
		mockColumnPortlets();

		layoutInitializerImpl.getOrCreatePortletLayout(mockLayoutContext, mockServiceContext);

		InOrder inOrder = inOrder(mockLayoutTypePortlet, mockInitializerUtil);
		inOrder.verify(mockLayoutTypePortlet, times(1)).setLayoutTemplateId(USER_ID, LAYOUT_TEMPLATE_ID, false);
		inOrder.verify(mockInitializerUtil, times(1)).configureLayoutColumn(mockDraftLayout, mockLayoutTypePortlet, COLUMN_INDEX_1, mockPortlets1);
		inOrder.verify(mockInitializerUtil, times(1)).configureLayoutColumn(mockDraftLayout, mockLayoutTypePortlet, COLUMN_INDEX_2, mockPortlets2);
	}

	@Test
	@Parameters({ "true", "false" })
	public void getOrCreatePortletLayout_WhenLayoutDoesNotAlreadyExistsAndNoError_ThenReturnsTheCreatedLayout(boolean privateLayout) throws PortalException {
		when(mockLayoutLocalService.fetchLayoutByFriendlyURL(GROUP_ID, privateLayout, FRIENDLY_URL)).thenReturn(null);
		mockServiceContextAndClassNameDetails();
		mockDraftLayoutCreation(privateLayout);
		mockColumnPortlets();
		mockLayoutContextDetails();
		when(mockLayoutContext.isPrivate()).thenReturn(privateLayout);
		when(mockLayoutLocalService.updateLayout(GROUP_ID, privateLayout, LAYOUT_ID, TYPE_SETTINGS)).thenReturn(mockLiveLayout);

		Layout result = layoutInitializerImpl.getOrCreatePortletLayout(mockLayoutContext, mockServiceContext);

		assertThat(result, sameInstance(mockLiveLayout));
	}

	@Test
	@Parameters({ "true", "false" })
	public void getOrCreatePageTemplateLayout_WhenLayoutAlreadyExists_ThenReturnsTheLayout(boolean privateLayout) throws PortalException {
		when(mockLayoutContext.isPrivate()).thenReturn(privateLayout);
		when(mockLayoutContext.getFriendlyURL()).thenReturn(FRIENDLY_URL);
		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockLayoutLocalService.fetchLayoutByFriendlyURL(GROUP_ID, privateLayout, FRIENDLY_URL)).thenReturn(mockDraftLayout);

		Layout result = layoutInitializerImpl.getOrCreatePageTemplateLayout(mockLayoutContext, mockLayoutPageTemplateEntry, mockServiceContext);

		assertThat(result, sameInstance(mockDraftLayout));
	}

	@Test(expected = PortalException.class)
	@Parameters({ "true", "false" })
	public void getOrCreatePageTemplateLayout_WhenLayoutDoesNotExistButTypeIsNotWidgetNorContent_ThenThrowsPortalException(boolean privateLayout) throws PortalException {
		when(mockLayoutContext.isPrivate()).thenReturn(privateLayout);
		when(mockLayoutContext.getFriendlyURL()).thenReturn(FRIENDLY_URL);
		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockLayoutLocalService.fetchLayoutByFriendlyURL(GROUP_ID, privateLayout, FRIENDLY_URL)).thenReturn(null);
		when(mockLayoutPageTemplateEntry.getType()).thenReturn(LayoutPageTemplateEntryTypeConstants.TYPE_DISPLAY_PAGE);

		layoutInitializerImpl.getOrCreatePageTemplateLayout(mockLayoutContext, mockLayoutPageTemplateEntry, mockServiceContext);
	}

	@Test(expected = PortalException.class)
	@Parameters({ "true, true", "true,false", "false,true", "false,false" })
	public void getOrCreatePageTemplateLayout_WhenLayoutDoesNotAlreadyExistAndExceptionCreatingNewLayout_ThenThrowsPortalException(boolean privateLayout, boolean widgetPage) throws PortalException {
		when(mockLayoutContext.isPrivate()).thenReturn(privateLayout);
		when(mockLayoutContext.getFriendlyURL()).thenReturn(FRIENDLY_URL);
		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockLayoutPageTemplateEntry.getType()).thenReturn(widgetPage ? LayoutPageTemplateEntryTypeConstants.TYPE_WIDGET_PAGE : LayoutPageTemplateEntryTypeConstants.TYPE_BASIC);
		when(mockLayoutLocalService.fetchLayoutByFriendlyURL(GROUP_ID, privateLayout, FRIENDLY_URL)).thenReturn(null);
		when(mockInitializerUtil.createLayout(mockLayoutContext, LayoutConstants.TYPE_PORTLET, mockServiceContext)).thenThrow(new PortalException());
		when(mockInitializerUtil.createLayout(mockLayoutContext, LayoutConstants.TYPE_CONTENT, mockServiceContext)).thenThrow(new PortalException());

		layoutInitializerImpl.getOrCreatePageTemplateLayout(mockLayoutContext, mockLayoutPageTemplateEntry, mockServiceContext);
	}

	@Test
	@Parameters({ "true, true", "true,false", "false,true", "false,false" })
	public void getOrCreatePageTemplateLayout_WhenLayoutDoesNotAlreadyExistsAndNoError_ThenCreatesAndReturnsALayoutCopiedFromTemplateLayout(boolean privateLayout, boolean widgetPage) throws PortalException {
		when(mockLayoutContext.isPrivate()).thenReturn(privateLayout);
		when(mockLayoutContext.getFriendlyURL()).thenReturn(FRIENDLY_URL);
		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockLayoutPageTemplateEntry.getType()).thenReturn(widgetPage ? LayoutPageTemplateEntryTypeConstants.TYPE_WIDGET_PAGE : LayoutPageTemplateEntryTypeConstants.TYPE_BASIC);
		when(mockLayoutLocalService.fetchLayoutByFriendlyURL(GROUP_ID, privateLayout, FRIENDLY_URL)).thenReturn(null);
		when(mockInitializerUtil.createLayout(mockLayoutContext, LayoutConstants.TYPE_PORTLET, mockServiceContext)).thenReturn(mockLinkLayout);
		when(mockInitializerUtil.createLayout(mockLayoutContext, LayoutConstants.TYPE_CONTENT, mockServiceContext)).thenReturn(mockLinkLayout);
		when(mockLayoutPageTemplateEntry.getPlid()).thenReturn(PLID);
		when(mockLayoutLocalService.getLayout(PLID)).thenReturn(mockDraftLayout);
		when(mockInitializerUtil.copyLayout(mockLinkLayout, mockDraftLayout)).thenReturn(mockLiveLayout);

		Layout layout = layoutInitializerImpl.getOrCreatePageTemplateLayout(mockLayoutContext, mockLayoutPageTemplateEntry, mockServiceContext);

		assertThat(layout, equalTo(mockLiveLayout));
	}

	private void mockColumnPortlets() {
		Map<Integer, Set<PortletContext>> portlets = new LinkedHashMap<>();
		portlets.put(COLUMN_INDEX_1, mockPortlets1);
		portlets.put(COLUMN_INDEX_2, mockPortlets2);
		when(mockLayoutContext.getColumnsPortletContexts()).thenReturn(portlets);
	}

	private void mockDraftLayoutCreation(boolean privateLayout) throws PortalException {
		when(mockInitializerUtil.createLayout(mockLayoutContext, LayoutConstants.TYPE_PORTLET, mockServiceContext)).thenReturn(mockDraftLayout);
		when(mockDraftLayout.getLayoutType()).thenReturn(mockLayoutTypePortlet);
		when(mockDraftLayout.getGroupId()).thenReturn(GROUP_ID);
		when(mockDraftLayout.isPrivateLayout()).thenReturn(privateLayout);
		when(mockDraftLayout.getLayoutId()).thenReturn(LAYOUT_ID);
		when(mockDraftLayout.getTypeSettings()).thenReturn(TYPE_SETTINGS);
	}

	private void mockLayoutContextDetails() {
		when(mockLayoutContext.getName()).thenReturn(NAME);
		when(mockLayoutContext.getStructureContent()).thenReturn(STRUCTURE_CONTENT);
		when(mockLayoutContext.getLayoutTemplateId()).thenReturn(LAYOUT_TEMPLATE_ID);
		when(mockLayoutContext.getFriendlyURL()).thenReturn(FRIENDLY_URL);
		when(mockLayoutContext.getFragmentContexts()).thenReturn(mockFragmentContexts);
		when(mockLayoutContext.getPortletContexts()).thenReturn(mockPortletContexts);
	}

	private void mockLinkToLayoutDetails(boolean privateLayout) throws PortalException {
		when(mockLayoutContext.getLinkToLayoutFriendlyURL()).thenReturn(LINK_TO_LAYOUT_FRIENDLY_URL);
		when(mockLayoutLocalService.getFriendlyURLLayout(GROUP_ID, privateLayout, LINK_TO_LAYOUT_FRIENDLY_URL)).thenReturn(mockLinkLayout);
		when(mockLinkLayout.getLayoutId()).thenReturn(LINK_LAYOUT_ID);
		when(mockLayoutContext.getTypeSettings()).thenReturn(mockTypeSettingsMap);
	}

	private void mockServiceContextAndClassNameDetails() {
		when(mockServiceContext.getUserId()).thenReturn(USER_ID);
		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockServiceContext.getLocale()).thenReturn(LOCALE);
	}
}
