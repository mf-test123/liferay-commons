package com.placecube.initializer.service.impl;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.asset.list.constants.AssetListEntryTypeConstants;
import com.liferay.asset.list.model.AssetListEntry;
import com.liferay.asset.list.service.AssetListEntryLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.Portal;
import com.placecube.initializer.service.internal.InitializerUtil;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class AssetListEntryInitializerImplTest extends PowerMockito {

	@InjectMocks
	private AssetListEntryInitializerImpl assetListEntryInitializerImpl;

	private static final long GROUP_ID = 12345;
	private static final long USER_ID = 23456;
	private static final String NAME = "entryNameValue";

	@Mock
	public AssetListEntryLocalService mockAssetListEntryLocalService;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private AssetListEntry mockAssetListEntry;

	@Mock
	private Portal mockPortal;

	@Mock
	private InitializerUtil mockInitializerUtil;

	@Test
	public void getContentSet_WhenNoError_ThenReturnsOptionalWithTheContentSet() {
		Optional<AssetListEntry> expected = Optional.of(mockAssetListEntry);
		when(mockInitializerUtil.getContentSet(GROUP_ID, NAME)).thenReturn(expected);

		Optional<AssetListEntry> result = assetListEntryInitializerImpl.getContentSet(GROUP_ID, NAME);

		assertThat(result, sameInstance(expected));
	}

	@Test
	@Parameters({ "true", "false" })
	public void getOrCreateContentSet_WhenAssetListEntryFound_ThenReturnsTheExistingEntry(boolean selectionTypeManual) throws Exception {
		mockServiceContextAndClassNameDetails();
		when(mockInitializerUtil.getContentSet(GROUP_ID, NAME)).thenReturn(Optional.of(mockAssetListEntry));

		AssetListEntry result = assetListEntryInitializerImpl.getOrCreateContentSet(NAME, selectionTypeManual, mockServiceContext);

		assertThat(result, sameInstance(mockAssetListEntry));
	}

	@Test(expected = PortalException.class)
	@Parameters({ "true", "false" })
	public void getOrCreateContentSet_WhenNoAssetListEntryFoundAndExceptionCreatingAssetListEntry_ThenThrowsPortalException(boolean selectionTypeManual) throws Exception {
		mockServiceContextAndClassNameDetails();
		when(mockInitializerUtil.getContentSet(GROUP_ID, NAME)).thenReturn(Optional.empty());
		when(mockAssetListEntryLocalService.addAssetListEntry(USER_ID, GROUP_ID, NAME, selectionTypeManual ? AssetListEntryTypeConstants.TYPE_MANUAL : AssetListEntryTypeConstants.TYPE_DYNAMIC,
				mockServiceContext)).thenThrow(new PortalException());

		assetListEntryInitializerImpl.getOrCreateContentSet(NAME, selectionTypeManual, mockServiceContext);
	}

	@Test
	public void getOrCreateContentSet_WhenNoAssetListEntryFoundAndNoErrorAndContentSetIsManualSelectionTrue_ThenReturnsTheCreatedContentSetOfTypeManual() throws Exception {
		mockServiceContextAndClassNameDetails();
		when(mockInitializerUtil.getContentSet(GROUP_ID, NAME)).thenReturn(Optional.empty());
		when(mockAssetListEntryLocalService.addAssetListEntry(USER_ID, GROUP_ID, NAME, AssetListEntryTypeConstants.TYPE_MANUAL, mockServiceContext)).thenReturn(mockAssetListEntry);

		AssetListEntry result = assetListEntryInitializerImpl.getOrCreateContentSet(NAME, true, mockServiceContext);

		assertThat(result, sameInstance(mockAssetListEntry));
	}

	@Test
	public void getOrCreateContentSet_WhenNoAssetListEntryFoundAndNoErrorAndContentSetIsManualSelectionFalse_ThenReturnsTheCreatedContentSetOfTypeDynamic() throws Exception {
		mockServiceContextAndClassNameDetails();
		when(mockInitializerUtil.getContentSet(GROUP_ID, NAME)).thenReturn(Optional.empty());
		when(mockAssetListEntryLocalService.addAssetListEntry(USER_ID, GROUP_ID, NAME, AssetListEntryTypeConstants.TYPE_DYNAMIC, mockServiceContext)).thenReturn(mockAssetListEntry);

		AssetListEntry result = assetListEntryInitializerImpl.getOrCreateContentSet(NAME, false, mockServiceContext);

		assertThat(result, sameInstance(mockAssetListEntry));
	}

	private void mockServiceContextAndClassNameDetails() {
		when(mockServiceContext.getUserId()).thenReturn(USER_ID);
		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
	}

}
