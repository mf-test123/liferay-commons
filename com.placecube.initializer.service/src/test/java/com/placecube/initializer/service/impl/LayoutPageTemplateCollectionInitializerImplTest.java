package com.placecube.initializer.service.impl;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.layout.page.template.model.LayoutPageTemplateCollection;
import com.liferay.layout.page.template.service.LayoutPageTemplateCollectionLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;

import junitparams.JUnitParamsRunner;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class LayoutPageTemplateCollectionInitializerImplTest extends PowerMockito {

	@InjectMocks
	private LayoutPageTemplateCollectionInitializerImpl layoutPageTemplateCollectionInitializerImpl;

	private static final long GROUP_ID = 12345;
	private static final long USER_ID = 23456;
	private static final String NAME = "entryNameValue";

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private LayoutPageTemplateCollectionLocalService mockLayoutPageTemplateCollectionLocalService;

	@Mock
	private LayoutPageTemplateCollection mockLayoutPageTemplateCollection1;

	@Mock
	private LayoutPageTemplateCollection mockLayoutPageTemplateCollection2;

	@Test
	public void getOrCreateLayoutPageTemplateCollection_WhenCollectionAlreadyExists_ThenReturnsTheCollection() throws PortalException {
		mockServiceContextAndClassNameDetails();
		List<LayoutPageTemplateCollection> collections = new ArrayList<>();
		collections.add(mockLayoutPageTemplateCollection1);
		collections.add(mockLayoutPageTemplateCollection2);
		when(mockLayoutPageTemplateCollection1.getName()).thenReturn("differentName");
		when(mockLayoutPageTemplateCollection2.getName()).thenReturn(NAME);
		when(mockLayoutPageTemplateCollectionLocalService.getLayoutPageTemplateCollections(GROUP_ID, QueryUtil.ALL_POS, QueryUtil.ALL_POS)).thenReturn(collections);

		LayoutPageTemplateCollection result = layoutPageTemplateCollectionInitializerImpl.getOrCreateLayoutPageTemplateCollection(NAME, mockServiceContext);

		assertThat(result, sameInstance(mockLayoutPageTemplateCollection2));
	}

	@Test(expected = PortalException.class)
	public void getOrCreateLayoutPageTemplateCollection_WhenCollectionDoesNotAlreadyExistAndExceptionCreatingCollection_ThenThrowsPortalException() throws PortalException {
		mockServiceContextAndClassNameDetails();
		List<LayoutPageTemplateCollection> collections = new ArrayList<>();
		collections.add(mockLayoutPageTemplateCollection1);
		collections.add(mockLayoutPageTemplateCollection2);
		when(mockLayoutPageTemplateCollection1.getName()).thenReturn("differentName");
		when(mockLayoutPageTemplateCollection2.getName()).thenReturn("anotherDifferentName");
		when(mockLayoutPageTemplateCollectionLocalService.getLayoutPageTemplateCollections(GROUP_ID, QueryUtil.ALL_POS, QueryUtil.ALL_POS)).thenReturn(collections);
		when(mockLayoutPageTemplateCollectionLocalService.addLayoutPageTemplateCollection(USER_ID, GROUP_ID, NAME, StringPool.BLANK, mockServiceContext)).thenThrow(new PortalException());

		layoutPageTemplateCollectionInitializerImpl.getOrCreateLayoutPageTemplateCollection(NAME, mockServiceContext);
	}

	@Test
	public void getOrCreateLayoutPageTemplateCollection_WhenCollectionDoesNotAlreadyExistAndNoError_ThenReturnsTheNewlyCreatedCollection() throws PortalException {
		mockServiceContextAndClassNameDetails();
		List<LayoutPageTemplateCollection> collections = new ArrayList<>();
		collections.add(mockLayoutPageTemplateCollection1);
		collections.add(mockLayoutPageTemplateCollection2);
		when(mockLayoutPageTemplateCollection1.getName()).thenReturn("differentName");
		when(mockLayoutPageTemplateCollection2.getName()).thenReturn("anotherDifferentName");
		when(mockLayoutPageTemplateCollectionLocalService.getLayoutPageTemplateCollections(GROUP_ID, QueryUtil.ALL_POS, QueryUtil.ALL_POS)).thenReturn(collections);
		when(mockLayoutPageTemplateCollectionLocalService.addLayoutPageTemplateCollection(USER_ID, GROUP_ID, NAME, StringPool.BLANK, mockServiceContext)).thenReturn(mockLayoutPageTemplateCollection1);

		LayoutPageTemplateCollection result = layoutPageTemplateCollectionInitializerImpl.getOrCreateLayoutPageTemplateCollection(NAME, mockServiceContext);

		assertThat(result, sameInstance(mockLayoutPageTemplateCollection1));
	}

	private void mockServiceContextAndClassNameDetails() {
		when(mockServiceContext.getUserId()).thenReturn(USER_ID);
		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
	}

}
