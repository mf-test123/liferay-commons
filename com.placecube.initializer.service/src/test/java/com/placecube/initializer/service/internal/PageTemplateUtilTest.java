package com.placecube.initializer.service.internal;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.layout.page.template.constants.LayoutPageTemplateEntryTypeConstants;
import com.liferay.layout.page.template.model.LayoutPageTemplateCollection;
import com.liferay.layout.page.template.model.LayoutPageTemplateEntry;
import com.liferay.layout.page.template.service.LayoutPageTemplateEntryLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.LayoutTypePortlet;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.placecube.initializer.model.FragmentContext;
import com.placecube.initializer.model.PortletContext;

public class PageTemplateUtilTest extends PowerMockito {

	private static final long GROUP_ID = 12345;
	private static final long USER_ID = 23456;
	private static final long CLASSNAME_ID_LAYOUT = 11111;
	private static final long PLID = 222220;
	private static final long PLID2 = 77720;
	private static final int PORTLET_CONTEXT_ENTRY1 = 1;
	private static final int PORTLET_CONTEXT_ENTRY2 = 2;
	private static final boolean IS_PRIVATE = true;
	private static final String LAYOUT_TEMPLATE_ID = "30-60";
	private static final String LAYOUT_TYPE_SETTINGS = "private_layout=true";
	private static final String PAGE_TEMPLATE_NAME = "pageTemplateNameValue";

	@InjectMocks
	private PageTemplateUtil pageTemplateUtil;

	@Mock
	private Portal mockPortal;

	@Mock
	private InitializerUtil mockInitializerUtil;

	@Mock
	private LayoutPageTemplateEntryLocalService mockLayoutPageTemplateEntryLocalService;

	@Mock
	private Layout mockLayout;

	@Mock
	private LayoutLocalService mockLayoutLocalService;

	@Mock
	private LayoutTypePortlet mockLayoutTypePortlet;

	@Mock
	private LayoutPageTemplateEntry mockLayoutPageTemplateEntry1;

	@Mock
	private LayoutPageTemplateEntry mockLayoutPageTemplateEntry2;

	@Mock
	private FragmentContext mockFragmentContext1;

	@Mock
	private FragmentContext mockFragmentContext2;

	@Mock
	private PortletContext mockPortletContext;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private LayoutPageTemplateCollection mockLayoutPageTemplateCollection;

	private Set<FragmentContext> fragmentContexts;

	private Set<PortletContext> portletContexts1;

	private Set<PortletContext> portletContexts2;

	private Map<Integer, Set<PortletContext>> columnsPortletContexts;

	private Map<String, String> expectedFragmentLinkIds;

	@Before
	public void setUp() {
		initMocks(this);

		fragmentContexts = new LinkedHashSet<>();
		portletContexts1 = new LinkedHashSet<>();
		portletContexts2 = new LinkedHashSet<>();
		expectedFragmentLinkIds = new LinkedHashMap<>();
		columnsPortletContexts = new HashMap<>();
		columnsPortletContexts.put(PORTLET_CONTEXT_ENTRY1, portletContexts1);
		columnsPortletContexts.put(PORTLET_CONTEXT_ENTRY2, portletContexts2);
	}

	@Test(expected = PortalException.class)
	public void configureContentPage_WhenExceptionAddingLayoutPageTemplateStructure_ThenThrowsPortalException() throws PortalException {
		when(mockPortal.getClassNameId(Layout.class)).thenReturn(CLASSNAME_ID_LAYOUT);
		when(mockInitializerUtil.getDraftLayout(PLID2, CLASSNAME_ID_LAYOUT)).thenReturn(mockLayout);
		when(mockLayout.getPlid()).thenReturn(PLID);
		mockFragmentLinkId(mockFragmentContext1, "fragmentPlaceholder1", "fragmentEntryLink1");
		mockFragmentLinkId(mockFragmentContext2, "fragmentPlaceholder2", "fragmentEntryLink2");
		mockFragmentLinkId(mockPortletContext, "portletPlaceholder1", "portletFragmentEntryLinkId1");
		doThrow(new PortalException()).when(mockInitializerUtil).addLayoutPageTemplateStructure(CLASSNAME_ID_LAYOUT, "structureContent", expectedFragmentLinkIds, mockServiceContext, PLID);

		pageTemplateUtil.configureContentPage(fragmentContexts, portletContexts1, "structureContent", mockServiceContext, PLID2);
	}

	@Test(expected = PortalException.class)
	public void configureContentPage_WhenExceptionCopyingTheLayout_ThenThrowsPortalException() throws PortalException {
		when(mockPortal.getClassNameId(Layout.class)).thenReturn(CLASSNAME_ID_LAYOUT);
		when(mockInitializerUtil.getDraftLayout(PLID2, CLASSNAME_ID_LAYOUT)).thenReturn(mockLayout);
		when(mockLayout.getPlid()).thenReturn(PLID);
		mockFragmentLinkId(mockFragmentContext1, "fragmentPlaceholder1", "fragmentEntryLink1");
		mockFragmentLinkId(mockFragmentContext2, "fragmentPlaceholder2", "fragmentEntryLink2");
		mockFragmentLinkId(mockPortletContext, "portletPlaceholder1", "portletFragmentEntryLinkId1");
		doThrow(new PortalException()).when(mockInitializerUtil).copyLayout(PLID2, mockLayout);

		pageTemplateUtil.configureContentPage(fragmentContexts, portletContexts1, "structureContent", mockServiceContext, PLID2);
	}

	@Test(expected = PortalException.class)
	public void configureContentPage_WhenExceptionRetrievingFragmentEntryLinkForFragment_ThenThrowsPortalException() throws PortalException {
		when(mockPortal.getClassNameId(Layout.class)).thenReturn(CLASSNAME_ID_LAYOUT);
		when(mockInitializerUtil.getDraftLayout(PLID2, CLASSNAME_ID_LAYOUT)).thenReturn(mockLayout);
		when(mockLayout.getPlid()).thenReturn(PLID);
		mockFragmentLinkId(mockFragmentContext1, "fragmentPlaceholder1", "fragmentEntryLink1");
		mockFragmentLinkId(mockFragmentContext2, "fragmentPlaceholder2", "fragmentEntryLink2");
		mockFragmentLinkId(mockPortletContext, "portletPlaceholder1", "portletFragmentEntryLinkId1");
		when(mockInitializerUtil.getFragmentEntryLinkIdForFragment(CLASSNAME_ID_LAYOUT, mockFragmentContext1, mockServiceContext, mockLayout)).thenThrow(new PortalException());

		pageTemplateUtil.configureContentPage(fragmentContexts, portletContexts1, "structureContent", mockServiceContext, PLID2);
	}

	@Test(expected = PortalException.class)
	public void configureContentPage_WhenExceptionRetrievingFragmentEntryLinkForPortlet_ThenThrowsPortalException() throws PortalException {
		when(mockPortal.getClassNameId(Layout.class)).thenReturn(CLASSNAME_ID_LAYOUT);
		when(mockInitializerUtil.getDraftLayout(PLID2, CLASSNAME_ID_LAYOUT)).thenReturn(mockLayout);
		when(mockLayout.getPlid()).thenReturn(PLID);
		mockFragmentLinkId(mockFragmentContext1, "fragmentPlaceholder1", "fragmentEntryLink1");
		mockFragmentLinkId(mockFragmentContext2, "fragmentPlaceholder2", "fragmentEntryLink2");
		mockFragmentLinkId(mockPortletContext, "portletPlaceholder1", "portletFragmentEntryLinkId1");
		when(mockInitializerUtil.getFragmentEntryLinkIdForPortlet(CLASSNAME_ID_LAYOUT, mockPortletContext, mockServiceContext, mockLayout)).thenThrow(new PortalException());

		pageTemplateUtil.configureContentPage(fragmentContexts, portletContexts1, "structureContent", mockServiceContext, PLID2);
	}

	@Test
	public void configureContentPage_WhenNoError_ThenConfigureAndCopiesTheLayout() throws PortalException {
		when(mockPortal.getClassNameId(Layout.class)).thenReturn(CLASSNAME_ID_LAYOUT);
		when(mockInitializerUtil.getDraftLayout(PLID2, CLASSNAME_ID_LAYOUT)).thenReturn(mockLayout);
		when(mockLayout.getPlid()).thenReturn(PLID);
		mockFragmentLinkId(mockFragmentContext1, "fragmentPlaceholder1", "fragmentEntryLink1");
		mockFragmentLinkId(mockFragmentContext2, "fragmentPlaceholder2", "fragmentEntryLink2");
		mockFragmentLinkId(mockPortletContext, "portletPlaceholder1", "portletFragmentEntryLinkId1");

		pageTemplateUtil.configureContentPage(fragmentContexts, portletContexts1, "structureContent", mockServiceContext, PLID2);

		InOrder inOrder = inOrder(mockInitializerUtil);
		inOrder.verify(mockInitializerUtil, times(1)).addLayoutPageTemplateStructure(CLASSNAME_ID_LAYOUT, "structureContent", expectedFragmentLinkIds, mockServiceContext, PLID);
		inOrder.verify(mockInitializerUtil, times(1)).copyLayout(PLID2, mockLayout);
	}

	@Test(expected = PortalException.class)
	public void configureWidgetPage_WhenExceptionGettingLayout_ThenThrowsPortalException() throws PortalException {
		when(mockLayoutLocalService.getLayout(PLID)).thenThrow(new PortalException());

		pageTemplateUtil.configureWidgetPage(LAYOUT_TEMPLATE_ID, columnsPortletContexts, mockServiceContext, PLID);
	}

	@Test(expected = PortalException.class)
	public void configureWidgetPage_WhenExceptionConfiguringLayoutColumns_ThenThrowsPortalException() throws PortalException {
		mockServiceContextDetails();

		when(mockLayoutLocalService.getLayout(PLID)).thenReturn(mockLayout);
		when(mockLayout.getLayoutType()).thenReturn(mockLayoutTypePortlet);
		doThrow(new PortalException()).when(mockInitializerUtil).configureLayoutColumn(mockLayout, mockLayoutTypePortlet, PORTLET_CONTEXT_ENTRY1, portletContexts1);

		pageTemplateUtil.configureWidgetPage(LAYOUT_TEMPLATE_ID, columnsPortletContexts, mockServiceContext, PLID);
	}

	@Test(expected = PortalException.class)
	public void configureWidgetPage_WhenExceptionUpdatingLayout_ThenThrowsPortalException() throws PortalException {
		mockServiceContextDetails();
		mockLayoutDetails();

		when(mockLayoutLocalService.getLayout(PLID)).thenReturn(mockLayout);
		when(mockLayout.getLayoutType()).thenReturn(mockLayoutTypePortlet);
		when(mockLayoutLocalService.updateLayout(GROUP_ID, IS_PRIVATE, PLID2, LAYOUT_TYPE_SETTINGS)).thenThrow(new PortalException());

		pageTemplateUtil.configureWidgetPage(LAYOUT_TEMPLATE_ID, columnsPortletContexts, mockServiceContext, PLID);
	}

	@Test
	public void configureWidgetPage_WhenNoError_ThenConfiguresAndUpdatesTheLayout() throws PortalException {
		mockServiceContextDetails();
		mockLayoutDetails();

		when(mockLayoutLocalService.getLayout(PLID)).thenReturn(mockLayout);
		when(mockLayout.getLayoutType()).thenReturn(mockLayoutTypePortlet);
		when(mockLayoutLocalService.updateLayout(GROUP_ID, IS_PRIVATE, PLID2, LAYOUT_TYPE_SETTINGS)).thenReturn(mockLayout);

		Layout result = pageTemplateUtil.configureWidgetPage(LAYOUT_TEMPLATE_ID, columnsPortletContexts, mockServiceContext, PLID);

		assertThat(result, equalTo(mockLayout));
		InOrder inOrder = inOrder(mockLayoutTypePortlet, mockInitializerUtil);
		inOrder.verify(mockLayoutTypePortlet, times(1)).setLayoutTemplateId(USER_ID, LAYOUT_TEMPLATE_ID, false);
		inOrder.verify(mockInitializerUtil, times(1)).configureLayoutColumn(mockLayout, mockLayoutTypePortlet, PORTLET_CONTEXT_ENTRY1, portletContexts1);
		inOrder.verify(mockInitializerUtil, times(1)).configureLayoutColumn(mockLayout, mockLayoutTypePortlet, PORTLET_CONTEXT_ENTRY2, portletContexts2);
	}

	@Test(expected = PortalException.class)
	public void createBasicPageTemplate_WhenException_ThenThrowsPortalException() throws PortalException {
		long collectionId = 1;
		when(mockLayoutPageTemplateCollection.getLayoutPageTemplateCollectionId()).thenReturn(collectionId);
		mockServiceContextDetails();
		when(mockLayoutPageTemplateEntryLocalService.addLayoutPageTemplateEntry(USER_ID, GROUP_ID, collectionId, PAGE_TEMPLATE_NAME, LayoutPageTemplateEntryTypeConstants.TYPE_BASIC,
				WorkflowConstants.STATUS_APPROVED, mockServiceContext)).thenThrow(new PortalException());

		pageTemplateUtil.createBasicPageTemplate(mockLayoutPageTemplateCollection, PAGE_TEMPLATE_NAME, mockServiceContext);
	}

	@Test
	public void createBasicPageTemplate_WhenNoError_ThenReturnsTheCreatedTemplate() throws PortalException {
		long collectionId = 1;
		when(mockLayoutPageTemplateCollection.getLayoutPageTemplateCollectionId()).thenReturn(collectionId);
		mockServiceContextDetails();
		when(mockLayoutPageTemplateEntryLocalService.addLayoutPageTemplateEntry(USER_ID, GROUP_ID, collectionId, PAGE_TEMPLATE_NAME, LayoutPageTemplateEntryTypeConstants.TYPE_BASIC,
				WorkflowConstants.STATUS_APPROVED, mockServiceContext)).thenReturn(mockLayoutPageTemplateEntry1);

		LayoutPageTemplateEntry result = pageTemplateUtil.createBasicPageTemplate(mockLayoutPageTemplateCollection, PAGE_TEMPLATE_NAME, mockServiceContext);

		assertThat(result, sameInstance(mockLayoutPageTemplateEntry1));
	}

	@Test(expected = PortalException.class)
	public void createDisplayPageTemplate_WhenException_ThenThrowsPortalException() throws PortalException {
		long classNameId = 5;
		long classTypeId = 3;
		mockServiceContextDetails();
		when(mockLayoutPageTemplateEntryLocalService.addLayoutPageTemplateEntry(USER_ID, GROUP_ID, 0, classNameId, classTypeId, PAGE_TEMPLATE_NAME,
				LayoutPageTemplateEntryTypeConstants.TYPE_DISPLAY_PAGE, WorkflowConstants.STATUS_APPROVED, mockServiceContext)).thenThrow(new PortalException());

		pageTemplateUtil.createDisplayPageTemplate(PAGE_TEMPLATE_NAME, classNameId, classTypeId, mockServiceContext);
	}

	@Test
	public void createDisplayPageTemplate_WhenNoError_ThenReturnsTheCreatedTemplate() throws PortalException {
		long classNameId = 5;
		long classTypeId = 3;
		mockServiceContextDetails();
		when(mockLayoutPageTemplateEntryLocalService.addLayoutPageTemplateEntry(USER_ID, GROUP_ID, 0, classNameId, classTypeId, PAGE_TEMPLATE_NAME,
				LayoutPageTemplateEntryTypeConstants.TYPE_DISPLAY_PAGE, WorkflowConstants.STATUS_APPROVED, mockServiceContext)).thenReturn(mockLayoutPageTemplateEntry1);
		when(mockLayoutPageTemplateEntryLocalService.updateLayoutPageTemplateEntry(mockLayoutPageTemplateEntry1)).thenReturn(mockLayoutPageTemplateEntry2);

		LayoutPageTemplateEntry result = pageTemplateUtil.createDisplayPageTemplate(PAGE_TEMPLATE_NAME, classNameId, classTypeId, mockServiceContext);

		assertThat(result, sameInstance(mockLayoutPageTemplateEntry2));
	}

	@Test
	public void createDisplayPageTemplate_WhenNoError_ThenUpdatesTheTemplateAsDefault() throws PortalException {
		long classNameId = 5;
		long classTypeId = 3;
		mockServiceContextDetails();
		when(mockLayoutPageTemplateEntryLocalService.addLayoutPageTemplateEntry(USER_ID, GROUP_ID, 0, classNameId, classTypeId, PAGE_TEMPLATE_NAME,
				LayoutPageTemplateEntryTypeConstants.TYPE_DISPLAY_PAGE, WorkflowConstants.STATUS_APPROVED, mockServiceContext)).thenReturn(mockLayoutPageTemplateEntry1);

		pageTemplateUtil.createDisplayPageTemplate(PAGE_TEMPLATE_NAME, classNameId, classTypeId, mockServiceContext);

		verify(mockLayoutPageTemplateEntry1, times(1)).setDefaultTemplate(true);
	}

	private void mockFragmentLinkId(FragmentContext fragmentContext, String placeholder, String fragmentEntryLinkId) throws PortalException {
		when(fragmentContext.getPlaceholder()).thenReturn(placeholder);
		when(mockInitializerUtil.getFragmentEntryLinkIdForFragment(CLASSNAME_ID_LAYOUT, fragmentContext, mockServiceContext, mockLayout)).thenReturn(fragmentEntryLinkId);
		fragmentContexts.add(fragmentContext);
		expectedFragmentLinkIds.put(placeholder, fragmentEntryLinkId);
	}

	private void mockFragmentLinkId(PortletContext portletContext, String placeholder, String fragmentEntryLinkId) throws PortalException {
		when(portletContext.getPlaceholder()).thenReturn(placeholder);
		when(mockInitializerUtil.getFragmentEntryLinkIdForPortlet(CLASSNAME_ID_LAYOUT, portletContext, mockServiceContext, mockLayout)).thenReturn(fragmentEntryLinkId);
		portletContexts1.add(portletContext);
		expectedFragmentLinkIds.put(placeholder, fragmentEntryLinkId);
	}

	private void mockServiceContextDetails() {
		when(mockServiceContext.getUserId()).thenReturn(USER_ID);
		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
	}

	private void mockLayoutDetails() {
		when(mockLayout.getGroupId()).thenReturn(GROUP_ID);
		when(mockLayout.isPrivateLayout()).thenReturn(IS_PRIVATE);
		when(mockLayout.getLayoutId()).thenReturn(PLID2);
		when(mockLayout.getTypeSettings()).thenReturn(LAYOUT_TYPE_SETTINGS);
	}

}
