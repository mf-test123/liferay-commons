package com.placecube.initializer.service.internal;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.asset.list.model.AssetListEntry;
import com.liferay.asset.list.service.AssetListEntryLocalService;
import com.liferay.fragment.contributor.FragmentCollectionContributorTracker;
import com.liferay.fragment.model.FragmentEntry;
import com.liferay.fragment.model.FragmentEntryLink;
import com.liferay.fragment.service.FragmentEntryLinkLocalService;
import com.liferay.fragment.service.FragmentEntryLocalService;
import com.liferay.layout.page.template.model.LayoutPageTemplateStructure;
import com.liferay.layout.page.template.service.LayoutPageTemplateStructureLocalService;
import com.liferay.layout.util.LayoutCopyHelper;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.LayoutConstants;
import com.liferay.portal.kernel.model.LayoutTypePortlet;
import com.liferay.portal.kernel.model.LayoutTypePortletConstants;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.FileUtil;
import com.placecube.initializer.model.FragmentContext;
import com.placecube.initializer.model.LayoutContext;
import com.placecube.initializer.model.PortletContext;
import com.placecube.initializer.service.internal.constants.NavigationMenuImportConstants;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ RestrictionsFactoryUtil.class, FileUtil.class })
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class InitializerUtilTest extends PowerMockito {

	private static final long CLASS_PK = 44;

	private static final long CLASSNAME_ID = 88;
	private static final String CONFIGURATION = "configValue";
	private static final String CSS = "cssValue";
	private static final Long ENTRY_ID = 33l;
	private static final String ENTRY_KEY = "entryIdValue";
	private static final String EVALUATED_TYPE_SETTINGS = "key1=val1" + StringPool.RETURN_NEW_LINE + "key2=val2";
	private static final long FRAGMENT_ENTRY_LINK_ID = 654651;
	private static final String FRIENDLY_URL = "friendlyURLValue";
	private static final long GROUP_ID = 234567;
	private static final String HTML = "htmlValue";
	private static final String JS = "jsValue";
	private static final String LAYOUT_NAME = "layoutNameValue";
	private static final String LAYOUT_TITLE = "layoutTitleValue";
	private static final String LAYOUT_TYPE = "layoutTypeValue";
	private static final Locale LOCALE = Locale.CANADA_FRENCH;
	private static final String PARENT_FRIENDLY_URL = "parentFriendlyURLValue";
	private static final long PLID = 77;
	private static final String RENDERER_KEY = "rendererKeyValue";
	private static final String STRUCTURE_SETTINGS = "settings";
	private static final long USER_ID = 12345;

	@InjectMocks
	private InitializerUtil initializerUtil;

	@Mock
	private AssetListEntry mockAssetListEntry;

	@Mock
	private AssetListEntryLocalService mockAssetListEntryLocalService;

	@Mock
	private Criterion mockCriterionGroupId;

	@Mock
	private Criterion mockCriterionTitle;

	@Mock
	private DynamicQuery mockDynamicQuery;

	@Mock
	private Map<String, Serializable> mockExpandoBridgeAttributes;

	@Mock
	private FragmentCollectionContributorTracker mockFragmentCollectionContributorTracker;

	@Mock
	private FragmentContext mockFragmentContext;

	@Mock
	private FragmentEntry mockFragmentEntry1;

	@Mock
	private FragmentEntry mockFragmentEntry2;

	@Mock
	private FragmentEntryLink mockFragmentEntryLink;

	@Mock
	private FragmentEntryLinkLocalService mockFragmentEntryLinkLocalService;

	@Mock
	private FragmentEntryLocalService mockFragmentEntryLocalService;

	@Mock
	private InitializerPrefsUtil mockInitializerPrefsUtil;

	@Mock
	private InputStream mockInputStream;

	@Mock
	private JSONObject mockJSONObject;

	@Mock
	private Layout mockLayout1;

	@Mock
	private Layout mockLayout2;

	@Mock
	private Layout mockLayout3;

	@Mock
	private LayoutContext mockLayoutContext;

	@Mock
	private LayoutCopyHelper mockLayoutCopyHelper;

	@Mock
	private LayoutLocalService mockLayoutLocalService;

	@Mock
	private LayoutPageTemplateStructure mockLayoutPageTemplateStructure;

	@Mock
	private LayoutPageTemplateStructureLocalService mockLayoutPageTemplateStructureLocalService;

	@Mock
	private LayoutTypePortlet mockLayoutTypePortlet;

	@Mock
	private PermissionUtil mockPermissionUtil;

	@Mock
	private PortletContext mockPortletContext1;

	@Mock
	private PortletContext mockPortletContext2;

	@Mock
	private Map<String, String[]> mockRolePermissionsToAdd;

	@Mock
	private Map<String, String[]> mockRolePermissionsToRemove;

	@Mock
	private ServiceContext mockServiceContext;

	@Before
	public void activateSetUp() {
		mockStatic(RestrictionsFactoryUtil.class, FileUtil.class);
	}

	@Test
	public void addLayoutPageTemplateStructure_WhenLayoutPageTemplateStructureFound_ThenDoesNotCreateAnewOne() throws PortalException {
		mockServiceContextDetails();
		when(mockLayoutPageTemplateStructureLocalService.fetchLayoutPageTemplateStructure(GROUP_ID, CLASSNAME_ID, PLID)).thenReturn(mockLayoutPageTemplateStructure);

		initializerUtil.addLayoutPageTemplateStructure(CLASSNAME_ID, "structureContent", Collections.emptyMap(), mockServiceContext, PLID);

		verify(mockLayoutPageTemplateStructureLocalService, never()).addLayoutPageTemplateStructure(anyLong(), anyLong(), anyLong(), anyLong(), anyString(), any(ServiceContext.class));
	}

	@Test
	public void addLayoutPageTemplateStructure_WhenNoLayoutPageTemplateStructureFound_ThenAddsLayoutPageTemplateStructure() throws PortalException {
		when(mockLayoutPageTemplateStructureLocalService.fetchLayoutPageTemplateStructure(GROUP_ID, CLASSNAME_ID, PLID)).thenReturn(null);
		String placeholder2 = "placeholder2";
		String placeholder1 = "placeholder1";
		String structureContent = "structureContent" + placeholder1 + "somemore" + placeholder2;
		String value1 = "value1";
		String value2 = "value2";
		String definition = "structureContent" + value1 + "somemore" + value2;
		mockServiceContextDetails();

		Map<String, String> fragmentEntryLinkIds = new HashMap<>();
		fragmentEntryLinkIds.put(placeholder1, value1);
		fragmentEntryLinkIds.put(placeholder2, value2);

		initializerUtil.addLayoutPageTemplateStructure(CLASSNAME_ID, structureContent, fragmentEntryLinkIds, mockServiceContext, PLID);

		verify(mockLayoutPageTemplateStructureLocalService, times(1)).addLayoutPageTemplateStructure(USER_ID, GROUP_ID, CLASSNAME_ID, PLID, definition, mockServiceContext);
	}

	@Test(expected = PortalException.class)
	public void addLayoutPageTemplateStructure_WhenNoLayoutPageTemplateStructureFoundAndExceptionCreatingIt_ThenThrowsPortalException() throws PortalException {
		when(mockLayoutPageTemplateStructureLocalService.fetchLayoutPageTemplateStructure(GROUP_ID, CLASSNAME_ID, PLID)).thenReturn(null);
		String placeholder2 = "placeholder2";
		String placeholder1 = "placeholder1";
		String structureContent = "structureContent" + placeholder1 + "somemore" + placeholder2;
		String value1 = "value1";
		String value2 = "value2";
		String definition = "structureContent" + value1 + "somemore" + value2;
		mockServiceContextDetails();
		Map<String, String> fragmentEntryLinkIds = new HashMap<>();
		fragmentEntryLinkIds.put(placeholder1, value1);
		fragmentEntryLinkIds.put(placeholder2, value2);
		when(mockLayoutPageTemplateStructureLocalService.addLayoutPageTemplateStructure(USER_ID, GROUP_ID, CLASSNAME_ID, PLID, definition, mockServiceContext)).thenThrow(new PortalException());

		initializerUtil.addLayoutPageTemplateStructure(CLASSNAME_ID, structureContent, fragmentEntryLinkIds, mockServiceContext, PLID);
	}

	@Test(expected = PortalException.class)
	public void configureLayoutColumn_WhenException_ThenThrowsPortalException() throws Exception {
		String portletId1 = "portletIdToAdd1";
		Integer columnIndex = 11;
		String columnId = LayoutTypePortletConstants.COLUMN_PREFIX + columnIndex;
		Set<PortletContext> portlets = new HashSet<>();
		portlets.add(mockPortletContext1);
		when(mockLayout1.getUserId()).thenReturn(USER_ID);
		mockPortletDetails(mockPortletContext1, "configuredPortletId1", "instanceConfigValue", "instanceIdValue");
		when(mockLayoutTypePortlet.addPortletId(USER_ID, "instanceIdValue", columnId, -1, false)).thenReturn(portletId1);
		doThrow(new PortalException()).when(mockInitializerPrefsUtil).savePortletPreferences(mockLayout1, portletId1, mockPortletContext1);

		initializerUtil.configureLayoutColumn(mockLayout1, mockLayoutTypePortlet, columnIndex, portlets);
	}

	@Test
	public void configureLayoutColumn_WhenNoError_ThenSavesEachPortletWithPreferences() throws Exception {
		Integer columnIndex = 11;
		String columnId = LayoutTypePortletConstants.COLUMN_PREFIX + columnIndex;
		String portletIdInstance1 = "portletIdToAdd1";
		String portletIdInstance2 = "portletIdToAdd2";
		when(mockLayout1.getUserId()).thenReturn(USER_ID);
		Set<PortletContext> portlets = new HashSet<>();
		portlets.add(mockPortletContext1);
		portlets.add(mockPortletContext2);
		mockPortletDetails(mockPortletContext1, "configuredPortletId1", "instanceConfigValue1", "instanceIdValue1");
		mockPortletDetails(mockPortletContext2, "configuredPortletId2", "instanceConfigValue2", "instanceIdValue2");
		when(mockLayoutTypePortlet.addPortletId(USER_ID, "instanceIdValue1", columnId, -1, false)).thenReturn(portletIdInstance1);
		when(mockLayoutTypePortlet.addPortletId(USER_ID, "instanceIdValue2", columnId, -1, false)).thenReturn(portletIdInstance2);

		initializerUtil.configureLayoutColumn(mockLayout1, mockLayoutTypePortlet, columnIndex, portlets);

		verify(mockInitializerPrefsUtil, times(1)).savePortletPreferences(mockLayout1, portletIdInstance1, mockPortletContext1);
		verify(mockInitializerPrefsUtil, times(1)).savePortletPreferences(mockLayout1, portletIdInstance2, mockPortletContext2);
	}

	@Test(expected = PortalException.class)
	public void copyLayout_CalledWithPlid_WhenException_ThenThrowsPortalException() throws PortalException {
		when(mockLayoutLocalService.getLayout(PLID)).thenThrow(new PortalException());

		initializerUtil.copyLayout(PLID, mockLayout2);
	}

	@Test
	@Parameters({ "true", "false" })
	public void copyLayout_CalledWithPlid_WhenNoError_ThenCopiesTheLayout(boolean isPrivateLayout) throws Exception {
		when(mockLayoutLocalService.getLayout(PLID)).thenReturn(mockLayout1);
		when(mockLayout1.getGroupId()).thenReturn(GROUP_ID);
		when(mockLayout1.isPrivateLayout()).thenReturn(isPrivateLayout);
		when(mockLayout1.getLayoutId()).thenReturn(ENTRY_ID);

		initializerUtil.copyLayout(PLID, mockLayout2);

		verify(mockLayoutCopyHelper, times(1)).copyLayout(mockLayout2, mockLayout1);
	}

	@Test
	@Parameters({ "true", "false" })
	public void copyLayout_CalledWithPlid_WhenNoError_ThenReturnsTheUpdatedLayout(boolean isPrivateLayout) throws Exception {
		when(mockLayoutLocalService.getLayout(PLID)).thenReturn(mockLayout1);
		when(mockLayout1.getGroupId()).thenReturn(GROUP_ID);
		when(mockLayout1.isPrivateLayout()).thenReturn(isPrivateLayout);
		when(mockLayout1.getLayoutId()).thenReturn(ENTRY_ID);
		when(mockLayoutLocalService.updateLayout(eq(GROUP_ID), eq(isPrivateLayout), eq(ENTRY_ID), any(Date.class))).thenReturn(mockLayout3);

		Layout result = initializerUtil.copyLayout(PLID, mockLayout2);

		assertThat(result, sameInstance(mockLayout3));
	}

	@Test
	@Parameters({ "true", "false" })
	public void copyLayout_CalledWithLayout_WhenNoError_ThenCopiesTheLayout(boolean isPrivateLayout) throws Exception {
		when(mockLayout1.getGroupId()).thenReturn(GROUP_ID);
		when(mockLayout1.isPrivateLayout()).thenReturn(isPrivateLayout);
		when(mockLayout1.getLayoutId()).thenReturn(ENTRY_ID);

		initializerUtil.copyLayout(mockLayout1, mockLayout2);

		verify(mockLayoutCopyHelper, times(1)).copyLayout(mockLayout2, mockLayout1);
	}

	@Test
	@Parameters({ "true", "false" })
	public void copyLayout_CalledWithLayout_WhenNoError_ThenReturnsTheUpdatedLayout(boolean isPrivateLayout) throws Exception {
		when(mockLayout1.getGroupId()).thenReturn(GROUP_ID);
		when(mockLayout1.isPrivateLayout()).thenReturn(isPrivateLayout);
		when(mockLayout1.getLayoutId()).thenReturn(ENTRY_ID);
		when(mockLayoutLocalService.updateLayout(eq(GROUP_ID), eq(isPrivateLayout), eq(ENTRY_ID), any(Date.class))).thenReturn(mockLayout3);

		Layout result = initializerUtil.copyLayout(mockLayout1, mockLayout2);

		assertThat(result, sameInstance(mockLayout3));
	}

	@Test(expected = PortalException.class)
	@Parameters({ "true,true", "true,false", "false,false", "false,true" })
	public void createLayout_WhenExceptionCreatingTheLayout_ThenThrowsPortalException(boolean isPrivate, boolean isHidden) throws PortalException {
		mockServiceContextDetails();
		mockLayoutContextDetails(isPrivate, isHidden);
		when(mockLayoutLocalService.addLayout(USER_ID, GROUP_ID, isPrivate, LayoutConstants.DEFAULT_PARENT_LAYOUT_ID, CLASSNAME_ID, CLASS_PK, Collections.singletonMap(LOCALE, LAYOUT_NAME),
				Collections.singletonMap(LOCALE, LAYOUT_TITLE), Collections.emptyMap(), null, null, LAYOUT_TYPE, EVALUATED_TYPE_SETTINGS, isHidden, false,
				Collections.singletonMap(LOCALE, FRIENDLY_URL), mockServiceContext)).thenThrow(new PortalException());

		initializerUtil.createLayout(mockLayoutContext, LAYOUT_TYPE, mockServiceContext);
	}

	@Test
	@Parameters({ "true,true", "true,false", "false,false", "false,true" })
	public void createLayout_WhenNoErrorAndIsAddGuestPermissionsIsFalse_ThenRemovesTheGuestViewPermissionBeforeUpdatingRolePermissions(boolean isPrivate, boolean isHidden) throws PortalException {
		mockServiceContextDetails();
		mockLayoutContextDetails(isPrivate, isHidden);
		when(mockLayoutLocalService.fetchLayoutByFriendlyURL(GROUP_ID, isPrivate, PARENT_FRIENDLY_URL)).thenReturn(null);
		long parentLayoutId = LayoutConstants.DEFAULT_PARENT_LAYOUT_ID;
		when(mockLayoutLocalService.addLayout(USER_ID, GROUP_ID, isPrivate, parentLayoutId, CLASSNAME_ID, CLASS_PK, Collections.singletonMap(LOCALE, LAYOUT_NAME),
				Collections.singletonMap(LOCALE, LAYOUT_TITLE), Collections.emptyMap(), null, null, LAYOUT_TYPE, EVALUATED_TYPE_SETTINGS, isHidden, false,
				Collections.singletonMap(LOCALE, FRIENDLY_URL), mockServiceContext)).thenReturn(mockLayout1);
		when(mockLayoutContext.isAddGuestPermissions()).thenReturn(false);
		when(mockLayoutContext.getRolePermissionsToAdd()).thenReturn(mockRolePermissionsToAdd);
		when(mockLayoutContext.getRolePermissionsToRemove()).thenReturn(mockRolePermissionsToRemove);

		initializerUtil.createLayout(mockLayoutContext, LAYOUT_TYPE, mockServiceContext);

		InOrder inOrder = inOrder(mockPermissionUtil);
		inOrder.verify(mockPermissionUtil, times(1)).removeGuestPermissionView(mockLayout1);
		inOrder.verify(mockPermissionUtil, times(1)).updateRolePermissions(mockLayout1, mockRolePermissionsToAdd, mockRolePermissionsToRemove);
	}

	@Test
	@Parameters({ "true,true", "true,false", "false,false", "false,true" })
	public void createLayout_WhenNoErrorAndIsAddGuestPermissionsIsTrue_ThenDoesNotRemoveTheGuestViewPermissionAndUpdatesRolePermissions(boolean isPrivate, boolean isHidden) throws PortalException {
		mockServiceContextDetails();
		mockLayoutContextDetails(isPrivate, isHidden);
		when(mockLayoutLocalService.fetchLayoutByFriendlyURL(GROUP_ID, isPrivate, PARENT_FRIENDLY_URL)).thenReturn(null);
		long parentLayoutId = LayoutConstants.DEFAULT_PARENT_LAYOUT_ID;
		when(mockLayoutLocalService.addLayout(USER_ID, GROUP_ID, isPrivate, parentLayoutId, CLASSNAME_ID, CLASS_PK, Collections.singletonMap(LOCALE, LAYOUT_NAME),
				Collections.singletonMap(LOCALE, LAYOUT_TITLE), Collections.emptyMap(), null, null, LAYOUT_TYPE, EVALUATED_TYPE_SETTINGS, isHidden, false,
				Collections.singletonMap(LOCALE, FRIENDLY_URL), mockServiceContext)).thenReturn(mockLayout1);
		when(mockLayoutContext.isAddGuestPermissions()).thenReturn(true);
		when(mockLayoutContext.getRolePermissionsToAdd()).thenReturn(mockRolePermissionsToAdd);
		when(mockLayoutContext.getRolePermissionsToRemove()).thenReturn(mockRolePermissionsToRemove);

		initializerUtil.createLayout(mockLayoutContext, LAYOUT_TYPE, mockServiceContext);

		verify(mockPermissionUtil, never()).removeGuestPermissionView(any(Layout.class));
		verify(mockPermissionUtil, times(1)).updateRolePermissions(mockLayout1, mockRolePermissionsToAdd, mockRolePermissionsToRemove);
	}

	@Test
	@Parameters({ "true,true", "true,false", "false,false", "false,true" })
	public void createLayout_WhenNoErrorAndPageIconNotSpecified_ThenDoesNotConfigureTheImageForTheLayoutAndTheCreatedLayoutIsReturned(boolean isPrivate, boolean isHidden) throws Exception {
		mockServiceContextDetails();
		mockLayoutContextDetails(isPrivate, isHidden);
		when(mockLayoutLocalService.fetchLayoutByFriendlyURL(GROUP_ID, isPrivate, PARENT_FRIENDLY_URL)).thenReturn(null);
		long parentLayoutId = LayoutConstants.DEFAULT_PARENT_LAYOUT_ID;
		when(mockLayoutLocalService.addLayout(USER_ID, GROUP_ID, isPrivate, parentLayoutId, CLASSNAME_ID, CLASS_PK, Collections.singletonMap(LOCALE, LAYOUT_NAME),
				Collections.singletonMap(LOCALE, LAYOUT_TITLE), Collections.emptyMap(), null, null, LAYOUT_TYPE, EVALUATED_TYPE_SETTINGS, isHidden, false,
				Collections.singletonMap(LOCALE, FRIENDLY_URL), mockServiceContext)).thenReturn(mockLayout1);
		when(mockLayoutContext.getPageIcon()).thenReturn(null);
		when(mockLayoutContext.getRolePermissionsToAdd()).thenReturn(mockRolePermissionsToAdd);
		when(mockLayoutContext.getRolePermissionsToRemove()).thenReturn(mockRolePermissionsToRemove);

		Layout result = initializerUtil.createLayout(mockLayoutContext, LAYOUT_TYPE, mockServiceContext);

		assertThat(result, sameInstance(mockLayout1));

		verify(mockLayoutLocalService, never()).updateIconImage(anyLong(), any());
	}

	@Test
	@Parameters({ "true,true", "true,false", "false,false", "false,true" })
	public void createLayout_WhenNoErrorAndPageIconSpecified_ThenConfiguresTheImageForTheLayoutAndReturnsTheUpdatedLayout(boolean isPrivate, boolean isHidden) throws Exception {
		byte[] bytes = new byte[2];
		mockServiceContextDetails();
		mockLayoutContextDetails(isPrivate, isHidden);
		when(mockLayoutLocalService.fetchLayoutByFriendlyURL(GROUP_ID, isPrivate, PARENT_FRIENDLY_URL)).thenReturn(null);
		long parentLayoutId = LayoutConstants.DEFAULT_PARENT_LAYOUT_ID;
		when(mockLayoutLocalService.addLayout(USER_ID, GROUP_ID, isPrivate, parentLayoutId, CLASSNAME_ID, CLASS_PK, Collections.singletonMap(LOCALE, LAYOUT_NAME),
				Collections.singletonMap(LOCALE, LAYOUT_TITLE), Collections.emptyMap(), null, null, LAYOUT_TYPE, EVALUATED_TYPE_SETTINGS, isHidden, false,
				Collections.singletonMap(LOCALE, FRIENDLY_URL), mockServiceContext)).thenReturn(mockLayout1);
		when(mockLayoutContext.getPageIcon()).thenReturn(mockInputStream);
		when(FileUtil.getBytes(mockInputStream)).thenReturn(bytes);
		when(mockLayoutContext.getRolePermissionsToAdd()).thenReturn(mockRolePermissionsToAdd);
		when(mockLayoutContext.getRolePermissionsToRemove()).thenReturn(mockRolePermissionsToRemove);
		when(mockLayout1.getPlid()).thenReturn(PLID);
		when(mockLayoutLocalService.updateIconImage(PLID, bytes)).thenReturn(mockLayout2);

		Layout result = initializerUtil.createLayout(mockLayoutContext, LAYOUT_TYPE, mockServiceContext);

		assertThat(result, sameInstance(mockLayout2));
	}

	@Test
	@Parameters({ "true,true", "true,false", "false,false", "false,true" })
	public void createLayout_WhenNoErrorAndPageIconSpecifiedAndExceptionConfiguringTheImage_ThenNoErrorIsThrownAndTheCreatedlayoutIsReturned(boolean isPrivate, boolean isHidden) throws Exception {
		byte[] bytes = new byte[2];
		mockServiceContextDetails();
		mockLayoutContextDetails(isPrivate, isHidden);
		when(mockLayoutLocalService.fetchLayoutByFriendlyURL(GROUP_ID, isPrivate, PARENT_FRIENDLY_URL)).thenReturn(null);
		long parentLayoutId = LayoutConstants.DEFAULT_PARENT_LAYOUT_ID;
		when(mockLayoutLocalService.addLayout(USER_ID, GROUP_ID, isPrivate, parentLayoutId, CLASSNAME_ID, CLASS_PK, Collections.singletonMap(LOCALE, LAYOUT_NAME),
				Collections.singletonMap(LOCALE, LAYOUT_TITLE), Collections.emptyMap(), null, null, LAYOUT_TYPE, EVALUATED_TYPE_SETTINGS, isHidden, false,
				Collections.singletonMap(LOCALE, FRIENDLY_URL), mockServiceContext)).thenReturn(mockLayout1);
		when(mockLayoutContext.getPageIcon()).thenReturn(mockInputStream);
		when(FileUtil.getBytes(mockInputStream)).thenReturn(bytes);
		when(mockLayoutContext.getRolePermissionsToAdd()).thenReturn(mockRolePermissionsToAdd);
		when(mockLayoutContext.getRolePermissionsToRemove()).thenReturn(mockRolePermissionsToRemove);
		when(mockLayout1.getPlid()).thenReturn(PLID);
		when(mockLayoutLocalService.updateIconImage(PLID, bytes)).thenThrow(new PortalException());

		Layout result = initializerUtil.createLayout(mockLayoutContext, LAYOUT_TYPE, mockServiceContext);

		assertThat(result, sameInstance(mockLayout1));

	}

	@Test
	@Parameters({ "true,true", "true,false", "false,false", "false,true" })
	public void createLayout_WhenNoErrorAndParentLayoutURLIsEmpty_ThenReturnsTheCreatedLayoutWithDefaultParent(boolean isPrivate, boolean isHidden) throws PortalException {
		mockServiceContextDetails();
		mockLayoutContextDetails(isPrivate, isHidden);
		when(mockLayoutContext.getParentLayoutFriendlyURL()).thenReturn(StringPool.THREE_SPACES);
		long parentLayoutId = LayoutConstants.DEFAULT_PARENT_LAYOUT_ID;
		when(mockLayoutLocalService.addLayout(USER_ID, GROUP_ID, isPrivate, parentLayoutId, CLASSNAME_ID, CLASS_PK, Collections.singletonMap(LOCALE, LAYOUT_NAME),
				Collections.singletonMap(LOCALE, LAYOUT_TITLE), Collections.emptyMap(), null, null, LAYOUT_TYPE, EVALUATED_TYPE_SETTINGS, isHidden, false,
				Collections.singletonMap(LOCALE, FRIENDLY_URL), mockServiceContext)).thenReturn(mockLayout1);

		Layout result = initializerUtil.createLayout(mockLayoutContext, LAYOUT_TYPE, mockServiceContext);

		assertThat(result, sameInstance(mockLayout1));
		InOrder inOrder = inOrder(mockServiceContext, mockLayoutLocalService);
		inOrder.verify(mockServiceContext, times(1)).setExpandoBridgeAttributes(mockExpandoBridgeAttributes);
		inOrder.verify(mockLayoutLocalService, times(1)).addLayout(USER_ID, GROUP_ID, isPrivate, parentLayoutId, CLASSNAME_ID, CLASS_PK, Collections.singletonMap(LOCALE, LAYOUT_NAME),
				Collections.singletonMap(LOCALE, LAYOUT_TITLE), Collections.emptyMap(), null, null, LAYOUT_TYPE, EVALUATED_TYPE_SETTINGS, isHidden, false,
				Collections.singletonMap(LOCALE, FRIENDLY_URL), mockServiceContext);
	}

	@Test
	@Parameters({ "true,true", "true,false", "false,false", "false,true" })
	public void createLayout_WhenNoErrorAndParentLayoutURLIsNull_ThenReturnsTheCreatedLayoutWithDefaultParent(boolean isPrivate, boolean isHidden) throws PortalException {
		mockServiceContextDetails();
		mockLayoutContextDetails(isPrivate, isHidden);
		when(mockLayoutContext.getParentLayoutFriendlyURL()).thenReturn(null);
		long parentLayoutId = LayoutConstants.DEFAULT_PARENT_LAYOUT_ID;
		when(mockLayoutLocalService.addLayout(USER_ID, GROUP_ID, isPrivate, parentLayoutId, CLASSNAME_ID, CLASS_PK, Collections.singletonMap(LOCALE, LAYOUT_NAME),
				Collections.singletonMap(LOCALE, LAYOUT_TITLE), Collections.emptyMap(), null, null, LAYOUT_TYPE, EVALUATED_TYPE_SETTINGS, isHidden, false,
				Collections.singletonMap(LOCALE, FRIENDLY_URL), mockServiceContext)).thenReturn(mockLayout1);

		Layout result = initializerUtil.createLayout(mockLayoutContext, LAYOUT_TYPE, mockServiceContext);

		assertThat(result, sameInstance(mockLayout1));
		InOrder inOrder = inOrder(mockServiceContext, mockLayoutLocalService);
		inOrder.verify(mockServiceContext, times(1)).setExpandoBridgeAttributes(mockExpandoBridgeAttributes);
		inOrder.verify(mockLayoutLocalService, times(1)).addLayout(USER_ID, GROUP_ID, isPrivate, parentLayoutId, CLASSNAME_ID, CLASS_PK, Collections.singletonMap(LOCALE, LAYOUT_NAME),
				Collections.singletonMap(LOCALE, LAYOUT_TITLE), Collections.emptyMap(), null, null, LAYOUT_TYPE, EVALUATED_TYPE_SETTINGS, isHidden, false,
				Collections.singletonMap(LOCALE, FRIENDLY_URL), mockServiceContext);
	}

	@Test
	@Parameters({ "true,true", "true,false", "false,false", "false,true" })
	public void createLayout_WhenNoErrorAndParentLayoutURLIsSpecified_ThenReturnsTheCreatedLayoutWithTheSpecifiedParentLayout(boolean isPrivate, boolean isHidden) throws PortalException {
		mockServiceContextDetails();
		mockLayoutContextDetails(isPrivate, isHidden);
		when(mockLayoutLocalService.fetchLayoutByFriendlyURL(GROUP_ID, isPrivate, PARENT_FRIENDLY_URL)).thenReturn(mockLayout2);
		long parentLayoutId = 8974651;
		when(mockLayout2.getLayoutId()).thenReturn(parentLayoutId);
		when(mockLayoutLocalService.addLayout(USER_ID, GROUP_ID, isPrivate, parentLayoutId, CLASSNAME_ID, CLASS_PK, Collections.singletonMap(LOCALE, LAYOUT_NAME),
				Collections.singletonMap(LOCALE, LAYOUT_TITLE), Collections.emptyMap(), null, null, LAYOUT_TYPE, EVALUATED_TYPE_SETTINGS, isHidden, false,
				Collections.singletonMap(LOCALE, FRIENDLY_URL), mockServiceContext)).thenReturn(mockLayout1);

		Layout result = initializerUtil.createLayout(mockLayoutContext, LAYOUT_TYPE, mockServiceContext);

		assertThat(result, sameInstance(mockLayout1));
		InOrder inOrder = inOrder(mockServiceContext, mockLayoutLocalService);
		inOrder.verify(mockServiceContext, times(1)).setExpandoBridgeAttributes(mockExpandoBridgeAttributes);
		inOrder.verify(mockLayoutLocalService, times(1)).addLayout(USER_ID, GROUP_ID, isPrivate, parentLayoutId, CLASSNAME_ID, CLASS_PK, Collections.singletonMap(LOCALE, LAYOUT_NAME),
				Collections.singletonMap(LOCALE, LAYOUT_TITLE), Collections.emptyMap(), null, null, LAYOUT_TYPE, EVALUATED_TYPE_SETTINGS, isHidden, false,
				Collections.singletonMap(LOCALE, FRIENDLY_URL), mockServiceContext);
	}

	@Test
	@Parameters({ "true,true", "true,false", "false,false", "false,true" })
	public void createLayout_WhenNoErrorAndParentLayoutURLIsSpecifiedButNoLayoutFound_ThenReturnsTheCreatedLayoutWithDefaultParent(boolean isPrivate, boolean isHidden) throws PortalException {
		mockServiceContextDetails();
		mockLayoutContextDetails(isPrivate, isHidden);
		when(mockLayoutLocalService.fetchLayoutByFriendlyURL(GROUP_ID, isPrivate, PARENT_FRIENDLY_URL)).thenReturn(null);
		long parentLayoutId = LayoutConstants.DEFAULT_PARENT_LAYOUT_ID;
		when(mockLayoutLocalService.addLayout(USER_ID, GROUP_ID, isPrivate, parentLayoutId, CLASSNAME_ID, CLASS_PK, Collections.singletonMap(LOCALE, LAYOUT_NAME),
				Collections.singletonMap(LOCALE, LAYOUT_TITLE), Collections.emptyMap(), null, null, LAYOUT_TYPE, EVALUATED_TYPE_SETTINGS, isHidden, false,
				Collections.singletonMap(LOCALE, FRIENDLY_URL), mockServiceContext)).thenReturn(mockLayout1);

		Layout result = initializerUtil.createLayout(mockLayoutContext, LAYOUT_TYPE, mockServiceContext);

		assertThat(result, sameInstance(mockLayout1));
		InOrder inOrder = inOrder(mockServiceContext, mockLayoutLocalService);
		inOrder.verify(mockServiceContext, times(1)).setExpandoBridgeAttributes(mockExpandoBridgeAttributes);
		inOrder.verify(mockLayoutLocalService, times(1)).addLayout(USER_ID, GROUP_ID, isPrivate, parentLayoutId, CLASSNAME_ID, CLASS_PK, Collections.singletonMap(LOCALE, LAYOUT_NAME),
				Collections.singletonMap(LOCALE, LAYOUT_TITLE), Collections.emptyMap(), null, null, LAYOUT_TYPE, EVALUATED_TYPE_SETTINGS, isHidden, false,
				Collections.singletonMap(LOCALE, FRIENDLY_URL), mockServiceContext);
	}

	@Test
	public void getContentSet_WhenAssetListEntryIsFound_ThenReturnsOptionalWithAssetListEntry() throws Exception {
		List<Object> assetListEntriesFound = new ArrayList<>();
		assetListEntriesFound.add(mockAssetListEntry);
		when(mockAssetListEntryLocalService.dynamicQuery()).thenReturn(mockDynamicQuery);
		when(mockAssetListEntryLocalService.dynamicQuery(mockDynamicQuery)).thenReturn(assetListEntriesFound);

		Optional<AssetListEntry> result = initializerUtil.getContentSet(GROUP_ID, ENTRY_KEY);

		assertThat(result.get(), sameInstance(mockAssetListEntry));
	}

	@Test
	public void getContentSet_WhenAssetListEntryNotFound_ThenReturnsEmptyOptional() throws Exception {
		when(mockAssetListEntryLocalService.dynamicQuery()).thenReturn(mockDynamicQuery);
		when(mockAssetListEntryLocalService.dynamicQuery(mockDynamicQuery)).thenReturn(Collections.emptyList());

		Optional<AssetListEntry> result = initializerUtil.getContentSet(GROUP_ID, ENTRY_KEY);

		assertThat(result.isPresent(), equalTo(false));
	}

	@Test
	public void getContentSet_WhenNoError_ThenConfiguresDynamiQuery() throws Exception {
		when(mockAssetListEntryLocalService.dynamicQuery()).thenReturn(mockDynamicQuery);
		when(RestrictionsFactoryUtil.eq("groupId", GROUP_ID)).thenReturn(mockCriterionGroupId);
		when(RestrictionsFactoryUtil.eq("title", ENTRY_KEY)).thenReturn(mockCriterionTitle);
		when(mockAssetListEntryLocalService.dynamicQuery(mockDynamicQuery)).thenReturn(Collections.emptyList());

		initializerUtil.getContentSet(GROUP_ID, ENTRY_KEY);

		InOrder inOrder = inOrder(mockAssetListEntryLocalService, mockDynamicQuery);
		inOrder.verify(mockDynamicQuery, times(1)).add(mockCriterionGroupId);
		inOrder.verify(mockDynamicQuery, times(1)).add(mockCriterionTitle);
		inOrder.verify(mockAssetListEntryLocalService, times(1)).dynamicQuery(mockDynamicQuery);
	}

	@Test
	public void getDraftLayout_WhenNoError_ThenReturnsLayout() {
		when(mockLayoutLocalService.fetchLayout(CLASSNAME_ID, PLID)).thenReturn(mockLayout1);

		Layout result = initializerUtil.getDraftLayout(PLID, CLASSNAME_ID);

		assertThat(result, sameInstance(mockLayout1));
	}

	@Test(expected = PortalException.class)
	public void getFragmentEntryLinkIdForFragment_WhenException_ThenThrowsPortalException() throws PortalException {
		mockServiceContextDetails();
		when(mockLayout1.getPlid()).thenReturn(PLID);
		when(mockFragmentContext.getId()).thenReturn(ENTRY_KEY);
		when(mockFragmentContext.getSettings()).thenReturn(STRUCTURE_SETTINGS);
		when(mockFragmentEntryLocalService.fetchFragmentEntry(GROUP_ID, ENTRY_KEY)).thenReturn(mockFragmentEntry1);
		mockFragmentEntry1Details();
		when(mockFragmentEntryLinkLocalService.addFragmentEntryLink(USER_ID, GROUP_ID, 0, ENTRY_ID, CLASSNAME_ID, PLID, CSS, HTML, JS, CONFIGURATION, STRUCTURE_SETTINGS, StringPool.BLANK, 0,
				RENDERER_KEY, mockServiceContext)).thenThrow(new PortalException());

		initializerUtil.getFragmentEntryLinkIdForFragment(CLASSNAME_ID, mockFragmentContext, mockServiceContext, mockLayout1);
	}

	@Test
	public void getFragmentEntryLinkIdForFragment_WhenFragmentFoundByKey_ThenReturnsTheCreatedFragmentEntryLinkId() throws PortalException {
		mockServiceContextDetails();
		when(mockFragmentEntryLocalService.fetchFragmentEntry(GROUP_ID, ENTRY_KEY)).thenReturn(mockFragmentEntry1);
		mockFragmentEntry1Details();
		when(mockLayout1.getPlid()).thenReturn(PLID);
		when(mockFragmentContext.getId()).thenReturn(ENTRY_KEY);
		when(mockFragmentContext.getSettings()).thenReturn(STRUCTURE_SETTINGS);
		mockFragmentEntyLinkCreation();

		String result = initializerUtil.getFragmentEntryLinkIdForFragment(CLASSNAME_ID, mockFragmentContext, mockServiceContext, mockLayout1);

		assertThat(result, equalTo(String.valueOf(FRAGMENT_ENTRY_LINK_ID)));
	}

	@Test
	public void getFragmentEntryLinkIdForFragment_WhenFragmentNotFoundByKeyButFoundViaTrackerContributor_ThenReturnsTheCreatedFragmentEntryLinkId() throws PortalException {
		Locale locale = Locale.CANADA;
		mockServiceContextDetails();
		when(mockFragmentEntryLocalService.fetchFragmentEntry(GROUP_ID, ENTRY_KEY)).thenReturn(null);
		when(mockServiceContext.getLocale()).thenReturn(locale);
		Map<String, FragmentEntry> fragments = new HashMap<>();
		fragments.put(ENTRY_KEY, mockFragmentEntry1);
		fragments.put("differentFragmentEntryKey", mockFragmentEntry2);
		when(mockLayout1.getPlid()).thenReturn(PLID);
		when(mockFragmentContext.getId()).thenReturn(ENTRY_KEY);
		when(mockFragmentContext.getSettings()).thenReturn(STRUCTURE_SETTINGS);
		when(mockFragmentCollectionContributorTracker.getFragmentEntries(locale)).thenReturn(fragments);
		mockFragmentEntry1Details();
		mockFragmentEntyLinkCreation();

		String result = initializerUtil.getFragmentEntryLinkIdForFragment(CLASSNAME_ID, mockFragmentContext, mockServiceContext, mockLayout1);

		assertThat(result, equalTo(String.valueOf(FRAGMENT_ENTRY_LINK_ID)));
	}

	@Test(expected = PortalException.class)
	public void getFragmentEntryLinkIdForPortlet_WhenException_ThenThrowsPortalException() throws PortalException {
		mockServiceContextDetails();
		mockPortletDetails(mockPortletContext1, "configuredPortletId1", "instanceConfigValue1", "instanceIdValue1");
		when(mockInitializerPrefsUtil.getFragmentSettingsForPortlet("configuredPortletId1", "instanceConfigValue1")).thenReturn(STRUCTURE_SETTINGS);
		when(mockLayout1.getPlid()).thenReturn(PLID);
		when(mockFragmentEntryLinkLocalService.addFragmentEntryLink(USER_ID, GROUP_ID, 0, 0, CLASSNAME_ID, PLID, StringPool.BLANK, StringPool.BLANK, StringPool.BLANK, StringPool.BLANK,
				STRUCTURE_SETTINGS, StringPool.BLANK, 0, null, mockServiceContext)).thenThrow(new PortalException());

		initializerUtil.getFragmentEntryLinkIdForPortlet(CLASSNAME_ID, mockPortletContext1, mockServiceContext, mockLayout1);
	}

	@Test
	public void getFragmentEntryLinkIdForPortlet_WhenNoError_ThenReturnsFragmentEntyLinkId() throws PortalException {
		mockServiceContextDetails();
		mockPortletDetails(mockPortletContext1, "configuredPortletId1", "instanceConfigValue1", "instanceIdValue1");
		when(mockInitializerPrefsUtil.getFragmentSettingsForPortlet("configuredPortletId1", "instanceConfigValue1")).thenReturn(STRUCTURE_SETTINGS);
		when(mockLayout1.getPlid()).thenReturn(PLID);
		when(mockFragmentEntryLinkLocalService.addFragmentEntryLink(USER_ID, GROUP_ID, 0, 0, CLASSNAME_ID, PLID, StringPool.BLANK, StringPool.BLANK, StringPool.BLANK, StringPool.BLANK,
				STRUCTURE_SETTINGS, StringPool.BLANK, 0, null, mockServiceContext)).thenReturn(mockFragmentEntryLink);
		when(mockFragmentEntryLink.getFragmentEntryLinkId()).thenReturn(FRAGMENT_ENTRY_LINK_ID);

		String result = initializerUtil.getFragmentEntryLinkIdForPortlet(CLASSNAME_ID, mockPortletContext1, mockServiceContext, mockLayout1);

		assertThat(result, equalTo(String.valueOf(FRAGMENT_ENTRY_LINK_ID)));
	}

	@Test
	public void getFragmentEntryLinkIdForPortlet_WhenNoError_ThenSavesPortletPreferences() throws PortalException {
		mockServiceContextDetails();
		mockPortletDetails(mockPortletContext1, "configuredPortletId1", "instanceConfigValue1", "instanceIdValue1");
		when(mockInitializerPrefsUtil.getFragmentSettingsForPortlet("configuredPortletId1", "instanceConfigValue1")).thenReturn(STRUCTURE_SETTINGS);
		when(mockLayout1.getPlid()).thenReturn(PLID);
		when(mockFragmentEntryLinkLocalService.addFragmentEntryLink(USER_ID, GROUP_ID, 0, 0, CLASSNAME_ID, PLID, StringPool.BLANK, StringPool.BLANK, StringPool.BLANK, StringPool.BLANK,
				STRUCTURE_SETTINGS, StringPool.BLANK, 0, null, mockServiceContext)).thenReturn(mockFragmentEntryLink);

		initializerUtil.getFragmentEntryLinkIdForPortlet(CLASSNAME_ID, mockPortletContext1, mockServiceContext, mockLayout1);

		verify(mockInitializerPrefsUtil, times(1)).savePortletPreferences(mockLayout1, "instanceIdValue1", mockPortletContext1);
	}

	@Test
	@Parameters({ "true", "false" })
	public void getLayoutTypeSettingsForMenuItem_WhenNoError_ThenReturnsTheTypeSettings(boolean privateLayout) {
		String uuid = "uuidValue";
		String languageId = "languageIdValue";
		String title = "titleValue";
		String expected = "groupId=" + GROUP_ID + StringPool.NEW_LINE + "layoutUuid=" + uuid + StringPool.NEW_LINE + "privateLayout=" + privateLayout + StringPool.NEW_LINE + "title=" + title
				+ StringPool.NEW_LINE;
		when(mockLayout1.getGroupId()).thenReturn(GROUP_ID);
		when(mockLayout1.getUuid()).thenReturn(uuid);
		when(mockLayout1.isPrivateLayout()).thenReturn(privateLayout);
		when(mockLayout1.getDefaultLanguageId()).thenReturn(languageId);
		when(mockLayout1.getTitle(languageId)).thenReturn(title);

		String result = initializerUtil.getLayoutTypeSettingsForMenuItem(mockLayout1);

		assertThat(result, equalTo(expected));
	}

	@Test
	public void getTypeSettingsForSubmenu_WhenNoError_ThenReturnsTheTypeSettings() {
		String menuItemName = "Submenu";
		when(mockJSONObject.getString(NavigationMenuImportConstants.NAVIGATION_MENU_NAME)).thenReturn(menuItemName);

		String result = initializerUtil.getTypeSettingsForSubmenu(mockJSONObject);

		assertEquals("name="+menuItemName, result);
	}

	@Test
	public void getTypeSettingsForURL_WhenNoError_ThenReturnsTheTypeSettings() {
		String menuItemName = "URL";
		String url = "/url";
		String expected = "name="+menuItemName + StringPool.NEW_LINE + "url=" + url + StringPool.NEW_LINE;
		when(mockJSONObject.getString(NavigationMenuImportConstants.NAVIGATION_MENU_NAME)).thenReturn(menuItemName);
		when(mockJSONObject.getString(NavigationMenuImportConstants.URL)).thenReturn(url);

		String result = initializerUtil.getTypeSettingsForURL(mockJSONObject);

		assertEquals(expected, result);
	}

	private void mockFragmentEntry1Details() {
		when(mockFragmentEntry1.getFragmentEntryId()).thenReturn(ENTRY_ID);
		when(mockFragmentEntry1.getCss()).thenReturn(CSS);
		when(mockFragmentEntry1.getHtml()).thenReturn(HTML);
		when(mockFragmentEntry1.getJs()).thenReturn(JS);
		when(mockFragmentEntry1.getConfiguration()).thenReturn(CONFIGURATION);
		when(mockFragmentEntry1.getFragmentEntryKey()).thenReturn(RENDERER_KEY);
	}

	private void mockFragmentEntyLinkCreation() throws PortalException {
		when(mockFragmentEntryLinkLocalService.addFragmentEntryLink(USER_ID, GROUP_ID, 0, ENTRY_ID, CLASSNAME_ID, PLID, CSS, HTML, JS, CONFIGURATION, STRUCTURE_SETTINGS, StringPool.BLANK, 0,
				RENDERER_KEY, mockServiceContext)).thenReturn(mockFragmentEntryLink);
		when(mockFragmentEntryLink.getFragmentEntryLinkId()).thenReturn(FRAGMENT_ENTRY_LINK_ID);
	}

	private void mockLayoutContextDetails(boolean isPrivate, boolean isHidden) {
		when(mockLayoutContext.isPrivate()).thenReturn(isPrivate);
		when(mockLayoutContext.isHidden()).thenReturn(isHidden);
		when(mockLayoutContext.getName()).thenReturn(LAYOUT_NAME);
		Map<String, String> typeSettingsValues = new LinkedHashMap<>();
		typeSettingsValues.put("key1", "val1");
		typeSettingsValues.put("key2", "val2");
		when(mockLayoutContext.getTypeSettings()).thenReturn(typeSettingsValues);
		when(mockLayoutContext.getFriendlyURL()).thenReturn(FRIENDLY_URL);
		when(mockLayoutContext.getTitle()).thenReturn(LAYOUT_TITLE);
		when(mockLayoutContext.getParentLayoutFriendlyURL()).thenReturn(PARENT_FRIENDLY_URL);
		when(mockLayoutContext.getClassNameId()).thenReturn(CLASSNAME_ID);
		when(mockLayoutContext.getClassPK()).thenReturn(CLASS_PK);
		when(mockLayoutContext.getExpandoValues()).thenReturn(mockExpandoBridgeAttributes);
	}

	private void mockPortletDetails(PortletContext portletContext, String portletId, String configuredInstanceId, String portletInstanceId) {
		when(portletContext.getPortletId()).thenReturn(portletId);
		when(mockInitializerPrefsUtil.getPortletInstanceId(portletContext)).thenReturn(configuredInstanceId);
		when(mockInitializerPrefsUtil.getPortletIdWithInstanceIdDetails(portletId, configuredInstanceId)).thenReturn(portletInstanceId);
	}

	private void mockServiceContextDetails() {
		when(mockServiceContext.getLocale()).thenReturn(LOCALE);
		when(mockServiceContext.getUserId()).thenReturn(USER_ID);
		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
	}
}
