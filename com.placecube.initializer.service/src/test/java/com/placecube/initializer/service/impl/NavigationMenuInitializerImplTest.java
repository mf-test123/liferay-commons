package com.placecube.initializer.service.impl;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.site.navigation.model.SiteNavigationMenu;
import com.liferay.site.navigation.model.SiteNavigationMenuItem;
import com.liferay.site.navigation.service.SiteNavigationMenuItemLocalService;
import com.liferay.site.navigation.service.SiteNavigationMenuLocalService;
import com.placecube.initializer.service.internal.InitializerUtil;
import com.placecube.initializer.service.internal.NavigationMenuImportUtil;
import com.placecube.initializer.service.internal.constants.NavigationMenuImportConstants;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ StringUtil.class })
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class NavigationMenuInitializerImplTest extends PowerMockito {

	private static final long GROUP_ID = 10;
	private static final long USER_ID = 20;
	private static final long SITE_NAVIGATION_MENU_ID = 30;
	private static final long MENU_ITEM_ID = 50;
	private static final String NAME = "nameValue";
	private static final int TYPE = 456;
	private static final String PATH = "pathValue";
	private static final String JSON_VALUE = "jsonValue";

	@InjectMocks
	private NavigationMenuInitializerImpl navigationMenuInitializerImpl;

	@Mock
	private InitializerUtil mockInitializerUtil;

	@Mock
	private JSONFactory mockJSONFactory;

	@Mock
	private LayoutLocalService mockLayoutLocalService;

	@Mock
	private NavigationMenuImportUtil mockNavigationMenuImportUtil;

	@Mock
	private SiteNavigationMenuLocalService mockSiteNavigationMenuLocalService;

	@Mock
	private SiteNavigationMenuItemLocalService mockSiteNavigationMenuItemLocalService;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private SiteNavigationMenu mockSiteNavigationMenu1;

	@Mock
	private SiteNavigationMenu mockSiteNavigationMenu2;

	@Mock
	private SiteNavigationMenuItem mockSiteNavigationMenuItem;

	@Mock
	private SiteNavigationMenuItem mockSiteNavigationMenuItem2;

	@Mock
	private SiteNavigationMenuItem mockSiteNavigationMenuItem3;

	@Mock
	private SiteNavigationMenuItem mockSiteNavigationMenuItemNested;

	@Mock
	private JSONObject mockJSONObject;

	@Mock
	private JSONObject mockJSONObject2;

	@Mock
	private JSONObject mockJSONObject3;

	@Mock
	private JSONObject mockJSONObject4;

	@Mock
	private JSONArray mockJSONArray;

	@Mock
	private JSONArray mockJSONArrayNested;

	@Mock
	private ClassLoader mockClassLoader;

	private List<SiteNavigationMenu> siteNavigationMenus;

	@Before
	public void setUp() {
		mockStatic(StringUtil.class);

		siteNavigationMenus = new ArrayList<>();
	}

	@Test(expected = PortalException.class)
	public void configureSiteNavigationMenuEntries_WhenExceptionReadingResource_ThenThrowsPortalException() throws Exception {
		mockServiceContextDetails();
		when(StringUtil.read(mockClassLoader, PATH)).thenThrow(new IOException());

		navigationMenuInitializerImpl.configureSiteNavigationMenuEntries(mockServiceContext, mockClassLoader, PATH);
	}

	@Test(expected = PortalException.class)
	public void configureSiteNavigationMenuEntries_WhenExceptionParsingResource_ThenThrowsPortalException() throws Exception {
		mockServiceContextDetails();
		when(StringUtil.read(mockClassLoader, PATH)).thenReturn(JSON_VALUE);
		when(mockJSONFactory.createJSONObject(JSON_VALUE)).thenThrow(new JSONException());

		navigationMenuInitializerImpl.configureSiteNavigationMenuEntries(mockServiceContext, mockClassLoader, PATH);
	}

	@Test
	public void configureSiteNavigationMenuEntries_WhenNoNavigationMenuFound_ThenDoesNotCreateAnyEntry() throws Exception {
		mockServiceContextDetails();
		mockJsonResourceDetails();
		when(mockSiteNavigationMenuLocalService.getSiteNavigationMenus(GROUP_ID)).thenReturn(siteNavigationMenus);
		mockSiteNavigationMenu(mockSiteNavigationMenu1, "differentValue");

		navigationMenuInitializerImpl.configureSiteNavigationMenuEntries(mockServiceContext, mockClassLoader, PATH);

		verifyZeroInteractions(mockSiteNavigationMenuItemLocalService);
	}

	@Test
	public void configureSiteNavigationMenuEntries_WhenNavigationMenuFoundAlreadyHasPages_ThenDoesNotCreateAnyEntry() throws Exception {
		mockServiceContextDetails();
		mockJsonResourceDetails();
		when(mockSiteNavigationMenuLocalService.getSiteNavigationMenus(GROUP_ID)).thenReturn(siteNavigationMenus);
		mockSiteNavigationMenu(mockSiteNavigationMenu1, "differentValue");
		mockSiteNavigationMenu(mockSiteNavigationMenu2, NAME);
		when(mockSiteNavigationMenu2.getSiteNavigationMenuId()).thenReturn(SITE_NAVIGATION_MENU_ID);
		when(mockSiteNavigationMenuItemLocalService.getSiteNavigationMenuItemsCount(SITE_NAVIGATION_MENU_ID)).thenReturn(1);

		navigationMenuInitializerImpl.configureSiteNavigationMenuEntries(mockServiceContext, mockClassLoader, PATH);

		verifyZeroInteractions(mockLayoutLocalService, mockInitializerUtil, mockNavigationMenuImportUtil);
	}

	@Test(expected = PortalException.class)
	@Parameters({ "true", "false" })
	public void configureSiteNavigationMenuEntries_WhenExceptionImportingItem_ThenThrowsPortalException(boolean isLayout) throws Exception {
		mockServiceContextDetails();
		mockJsonResourceDetails();
		when(mockSiteNavigationMenuLocalService.getSiteNavigationMenus(GROUP_ID)).thenReturn(siteNavigationMenus);
		mockSiteNavigationMenu(mockSiteNavigationMenu2, "differentValue");
		mockSiteNavigationMenu(mockSiteNavigationMenu1, NAME);
		when(mockSiteNavigationMenu1.getSiteNavigationMenuId()).thenReturn(SITE_NAVIGATION_MENU_ID);
		when(mockJSONArray.length()).thenReturn(1);
		when(mockJSONArray.getJSONObject(0)).thenReturn(mockJSONObject2);
		if (isLayout) {
			when(mockNavigationMenuImportUtil.importAsLayoutItem(mockJSONObject2, mockServiceContext, SITE_NAVIGATION_MENU_ID, 0)).thenThrow(new PortalException());
		} else {
			when(mockNavigationMenuImportUtil.importAsSubmenuItem(mockJSONObject2, mockServiceContext, SITE_NAVIGATION_MENU_ID, 0)).thenThrow(new PortalException());
		}

		navigationMenuInitializerImpl.configureSiteNavigationMenuEntries(mockServiceContext, mockClassLoader, PATH);
	}

	@Test
	public void configureSiteNavigationMenuEntries_WhenNoError_ThenImportsEachItemAsSiteNavigationMenuItem() throws Exception {
		mockServiceContextDetails();
		mockJsonResourceDetails();
		when(mockSiteNavigationMenuLocalService.getSiteNavigationMenus(GROUP_ID)).thenReturn(siteNavigationMenus);
		mockSiteNavigationMenu(mockSiteNavigationMenu1, NAME);
		when(mockSiteNavigationMenu1.getSiteNavigationMenuId()).thenReturn(SITE_NAVIGATION_MENU_ID);

		when(mockJSONArray.length()).thenReturn(3);
		when(mockJSONArray.getJSONObject(0)).thenReturn(mockJSONObject2);
		when(mockJSONArray.getJSONObject(1)).thenReturn(mockJSONObject3);
		when(mockJSONArray.getJSONObject(2)).thenReturn(mockJSONObject4);


		when(mockNavigationMenuImportUtil.isLayout(mockJSONObject2)).thenReturn(true);
		when(mockNavigationMenuImportUtil.isLayout(mockJSONObject3)).thenReturn(false);
		when(mockNavigationMenuImportUtil.isLayout(mockJSONObject4)).thenReturn(false);

		when(mockNavigationMenuImportUtil.isURL(mockJSONObject3)).thenReturn(true);
		when(mockNavigationMenuImportUtil.isURL(mockJSONObject4)).thenReturn(false);

		when(mockNavigationMenuImportUtil.importAsLayoutItem(mockJSONObject2, mockServiceContext, SITE_NAVIGATION_MENU_ID, 0)).thenReturn(mockSiteNavigationMenuItem);
		when(mockNavigationMenuImportUtil.importAsURLItem(mockJSONObject3, mockServiceContext, SITE_NAVIGATION_MENU_ID, 0)).thenReturn(mockSiteNavigationMenuItem3);
		when(mockNavigationMenuImportUtil.importAsSubmenuItem(mockJSONObject4, mockServiceContext, SITE_NAVIGATION_MENU_ID, 0)).thenReturn(mockSiteNavigationMenuItem2);

		when(mockSiteNavigationMenuItem.getSiteNavigationMenuItemId()).thenReturn(MENU_ITEM_ID);

		navigationMenuInitializerImpl.configureSiteNavigationMenuEntries(mockServiceContext, mockClassLoader, PATH);

		verify(mockNavigationMenuImportUtil, times(1)).importAsLayoutItem(mockJSONObject2, mockServiceContext, SITE_NAVIGATION_MENU_ID, 0);
		verify(mockNavigationMenuImportUtil, times(1)).importAsURLItem(mockJSONObject3, mockServiceContext, SITE_NAVIGATION_MENU_ID, 0);
		verify(mockNavigationMenuImportUtil, times(1)).importAsSubmenuItem(mockJSONObject4, mockServiceContext, SITE_NAVIGATION_MENU_ID, 0);

	}

	@Test
	public void configureSiteNavigationMenuEntries_WhenNoError_ThenCallsItselfForNestedItems() throws Exception {
		mockServiceContextDetails();
		mockJsonResourceDetails();
		when(mockSiteNavigationMenuLocalService.getSiteNavigationMenus(GROUP_ID)).thenReturn(siteNavigationMenus);
		mockSiteNavigationMenu(mockSiteNavigationMenu1, NAME);
		when(mockSiteNavigationMenu1.getSiteNavigationMenuId()).thenReturn(SITE_NAVIGATION_MENU_ID);

		when(mockJSONArray.length()).thenReturn(1);
		when(mockJSONArray.getJSONObject(0)).thenReturn(mockJSONObject2);
		when(mockNavigationMenuImportUtil.isLayout(mockJSONObject2)).thenReturn(true);
		when(mockNavigationMenuImportUtil.importAsLayoutItem(mockJSONObject2, mockServiceContext, SITE_NAVIGATION_MENU_ID, 0)).thenReturn(mockSiteNavigationMenuItem);
		when(mockSiteNavigationMenuItem.getSiteNavigationMenuItemId()).thenReturn(MENU_ITEM_ID);

		when(mockJSONObject2.getJSONArray(NavigationMenuImportConstants.LAYOUTS)).thenReturn(mockJSONArrayNested);

		when(mockJSONArrayNested.length()).thenReturn(1);
		when(mockJSONArrayNested.getJSONObject(0)).thenReturn(mockJSONObject3);
		when(mockNavigationMenuImportUtil.isLayout(mockJSONObject3)).thenReturn(true);
		when(mockNavigationMenuImportUtil.importAsLayoutItem(mockJSONObject3, mockServiceContext, SITE_NAVIGATION_MENU_ID, MENU_ITEM_ID)).thenReturn(mockSiteNavigationMenuItemNested);
		when(mockSiteNavigationMenuItemNested.getSiteNavigationMenuItemId()).thenReturn(40L);

		navigationMenuInitializerImpl.configureSiteNavigationMenuEntries(mockServiceContext, mockClassLoader, PATH);

		verify(mockJSONObject2, times(1)).getJSONArray(NavigationMenuImportConstants.LAYOUTS);
	}

	@Test
	public void getOrCreateSiteNavigationMenu_WhenSiteNavigationMenuFoundWithTheSameName_ThenReturnsTheSiteNavigationMenu() throws PortalException {
		mockServiceContextDetails();
		when(mockSiteNavigationMenuLocalService.getSiteNavigationMenus(GROUP_ID)).thenReturn(siteNavigationMenus);
		mockSiteNavigationMenu(mockSiteNavigationMenu1, "differentValue");
		mockSiteNavigationMenu(mockSiteNavigationMenu2, NAME);

		SiteNavigationMenu result = navigationMenuInitializerImpl.getOrCreateSiteNavigationMenu(mockServiceContext, NAME, TYPE);

		assertThat(result, sameInstance(mockSiteNavigationMenu2));
	}

	@Test
	public void getOrCreateSiteNavigationMenu_WhenSiteNavigationMenuFoundWithTheSameName_ThenDoesNotCreateAnewSiteNavigationMenu() throws PortalException {
		mockServiceContextDetails();
		when(mockSiteNavigationMenuLocalService.getSiteNavigationMenus(GROUP_ID)).thenReturn(siteNavigationMenus);
		mockSiteNavigationMenu(mockSiteNavigationMenu1, "differentValue");
		mockSiteNavigationMenu(mockSiteNavigationMenu2, NAME);

		navigationMenuInitializerImpl.getOrCreateSiteNavigationMenu(mockServiceContext, NAME, TYPE);

		verify(mockSiteNavigationMenuLocalService, never()).addSiteNavigationMenu(anyLong(), anyLong(), anyString(), anyInt(), any(ServiceContext.class));
	}

	@Test(expected = PortalException.class)
	public void getOrCreateSiteNavigationMenu_WhenNoSiteNavigationMenuFoundWithTheSameNameAndExceptionCreatingNewMenu_ThenThrowsPortalException() throws PortalException {
		mockServiceContextDetails();
		when(mockSiteNavigationMenuLocalService.getSiteNavigationMenus(GROUP_ID)).thenReturn(siteNavigationMenus);
		mockSiteNavigationMenu(mockSiteNavigationMenu1, "differentValue");
		when(mockSiteNavigationMenuLocalService.addSiteNavigationMenu(USER_ID, GROUP_ID, NAME, TYPE, mockServiceContext)).thenThrow(new PortalException());

		navigationMenuInitializerImpl.getOrCreateSiteNavigationMenu(mockServiceContext, NAME, TYPE);
	}

	@Test
	public void getOrCreateSiteNavigationMenu_WhenNoSiteNavigationMenuFoundWithTheSameNameAndNoError_ThenReturnsTheNewlyCreatedMenu() throws PortalException {
		mockServiceContextDetails();
		when(mockSiteNavigationMenuLocalService.getSiteNavigationMenus(GROUP_ID)).thenReturn(siteNavigationMenus);
		mockSiteNavigationMenu(mockSiteNavigationMenu1, "differentValue");
		when(mockSiteNavigationMenuLocalService.addSiteNavigationMenu(USER_ID, GROUP_ID, NAME, TYPE, mockServiceContext)).thenReturn(mockSiteNavigationMenu2);

		SiteNavigationMenu result = navigationMenuInitializerImpl.getOrCreateSiteNavigationMenu(mockServiceContext, NAME, TYPE);

		assertThat(result, sameInstance(mockSiteNavigationMenu2));
	}

	private void mockServiceContextDetails() {
		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockServiceContext.getUserId()).thenReturn(USER_ID);
	}

	private void mockSiteNavigationMenu(SiteNavigationMenu siteNavigationMenu, String name) {
		siteNavigationMenus.add(siteNavigationMenu);
		when(siteNavigationMenu.getName()).thenReturn(name);
	}

	private void mockJsonResourceDetails() throws IOException, JSONException {
		when(StringUtil.read(mockClassLoader, PATH)).thenReturn(JSON_VALUE);
		when(mockJSONFactory.createJSONObject(JSON_VALUE)).thenReturn(mockJSONObject);
		when(mockJSONObject.getString(NavigationMenuImportConstants.NAVIGATION_MENU_NAME)).thenReturn(NAME);
		when(mockJSONObject.getJSONArray(NavigationMenuImportConstants.LAYOUTS)).thenReturn(mockJSONArray);
	}
}
