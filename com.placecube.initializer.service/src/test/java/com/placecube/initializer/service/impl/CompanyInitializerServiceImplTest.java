package com.placecube.initializer.service.impl;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.MockitoAnnotations.initMocks;

import java.io.InputStream;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.blogs.model.BlogsEntry;
import com.liferay.document.library.kernel.model.DLFileEntry;
import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.journal.model.JournalArticle;
import com.liferay.knowledge.base.model.KBArticle;
import com.liferay.message.boards.model.MBDiscussion;
import com.liferay.message.boards.model.MBMessage;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.PasswordPolicy;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.kernel.service.PasswordPolicyLocalService;
import com.liferay.portal.kernel.util.UnicodeProperties;
import com.liferay.ratings.kernel.RatingsType;
import com.liferay.wiki.model.WikiPage;
import com.placecube.initializer.model.CompanyContext;

public class CompanyInitializerServiceImplTest extends PowerMockito {

	private static final long COMPANY_ID = 123;
	private static final int MAX_USERS = 456;

	@InjectMocks
	private CompanyInitializerServiceImpl companyInitializerServiceImpl;

	@Mock
	private ClassLoader mockClassLoader;

	@Mock
	private Company mockCompany;

	@Mock
	private CompanyContext mockCompanyContext;

	@Mock
	private CompanyLocalService mockCompanyLocalService;

	@Mock
	private Company mockCompanyUpdatedLogo;

	@Mock
	private Company mockCompanyUpdatedUsers;

	@Mock
	private InputStream mockInputStream;

	@Mock
	private PasswordPolicy mockPasswordPolicy;

	@Mock
	private PasswordPolicyLocalService mockPasswordPolicyLocalService;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test(expected = PortalException.class)
	public void configureCompany_WhenDefaultRatingsTypesSetAndExceptionConfiguringTheRatingsTypes_ThenThrowsPortalException() throws PortalException {
		RatingsType ratingsType = RatingsType.LIKE;
		mockCompanyContextDetails(ratingsType);
		UnicodeProperties properties = getMockedUnicodePropertiesDetails();
		when(mockCompany.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockCompanyLocalService.updateLogo(COMPANY_ID, mockInputStream)).thenReturn(mockCompanyUpdatedLogo);
		when(mockCompanyLocalService.updateCompany(mockCompanyUpdatedLogo)).thenReturn(mockCompanyUpdatedUsers);
		doThrow(new PortalException()).when(mockCompanyLocalService).updatePreferences(eq(COMPANY_ID), eq(properties));

		companyInitializerServiceImpl.configureCompany(mockCompanyContext);
	}

	@Test(expected = PortalException.class)
	public void configureCompany_WhenExceptionUpdatingTheLogo_ThenThrowsPortalException() throws PortalException {
		mockCompanyContextDetails(null);
		when(mockCompany.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockCompanyLocalService.updateCompany(mockCompany)).thenReturn(mockCompanyUpdatedUsers);
		when(mockCompanyLocalService.updateLogo(COMPANY_ID, mockInputStream)).thenThrow(new PortalException());

		companyInitializerServiceImpl.configureCompany(mockCompanyContext);
	}

	@Test
	public void configureCompany_WhenNoErrorAndDefaultRatingsTypesNotSet_ThenReturnsTheConfiguredCompany() throws PortalException {
		mockCompanyContextDetails(null);
		when(mockCompany.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockCompanyLocalService.updateLogo(COMPANY_ID, mockInputStream)).thenReturn(mockCompanyUpdatedLogo);
		when(mockCompanyLocalService.updateCompany(mockCompanyUpdatedLogo)).thenReturn(mockCompanyUpdatedUsers);

		Company result = companyInitializerServiceImpl.configureCompany(mockCompanyContext);

		assertThat(result, sameInstance(mockCompanyUpdatedUsers));

		InOrder inOrder = inOrder(mockCompanyUpdatedLogo, mockCompanyLocalService);
		inOrder.verify(mockCompanyLocalService, times(1)).updateLogo(COMPANY_ID, mockInputStream);
		inOrder.verify(mockCompanyUpdatedLogo, times(1)).setMaxUsers(MAX_USERS);
		inOrder.verify(mockCompanyLocalService, times(1)).updateCompany(mockCompanyUpdatedLogo);
	}

	@Test
	public void configureCompany_WhenNoErrorAndDefaultRatingsTypesSet_ThenReturnsTheConfiguredCompanyAndConfiguresTheDefaultRatingsType() throws PortalException {
		RatingsType ratingsType = RatingsType.LIKE;
		mockCompanyContextDetails(ratingsType);
		UnicodeProperties properties = getMockedUnicodePropertiesDetails();
		when(mockCompany.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockCompanyLocalService.updateLogo(COMPANY_ID, mockInputStream)).thenReturn(mockCompanyUpdatedLogo);
		when(mockCompanyLocalService.updateCompany(mockCompanyUpdatedLogo)).thenReturn(mockCompanyUpdatedUsers);

		Company result = companyInitializerServiceImpl.configureCompany(mockCompanyContext);

		assertThat(result, sameInstance(mockCompanyUpdatedUsers));

		InOrder inOrder = inOrder(mockCompanyUpdatedLogo, mockCompanyLocalService);
		inOrder.verify(mockCompanyLocalService, times(1)).updateLogo(COMPANY_ID, mockInputStream);
		inOrder.verify(mockCompanyUpdatedLogo, times(1)).setMaxUsers(MAX_USERS);
		inOrder.verify(mockCompanyLocalService, times(1)).updateCompany(mockCompanyUpdatedLogo);
		inOrder.verify(mockCompanyLocalService, times(1)).updatePreferences(COMPANY_ID, properties);
	}

	@Test
	public void createCompanyContext_WhenNoError_ThenReturnsTheConfiguredCompanyContext() {
		String logoPath = "logoPathValue";
		when(mockClassLoader.getResourceAsStream(logoPath)).thenReturn(mockInputStream);

		CompanyContext result = companyInitializerServiceImpl.createCompanyContext(mockCompany, MAX_USERS, mockClassLoader, logoPath);

		assertThat(result.getCompany(), sameInstance(mockCompany));
		assertThat(result.getCompanyLogo(), sameInstance(mockInputStream));
		assertThat(result.getMaxUsers(), equalTo(MAX_USERS));
		assertNull(result.getDefaultRatingsType());
	}

	private UnicodeProperties getMockedUnicodePropertiesDetails() {
		UnicodeProperties properties = new UnicodeProperties(true);
		properties.put(BlogsEntry.class.getName() + "_RatingsType", "like");
		properties.put(DLFileEntry.class.getName() + "_RatingsType", "like");
		properties.put(DDLRecord.class.getName() + "_RatingsType", "like");
		properties.put(JournalArticle.class.getName() + "_RatingsType", "like");
		properties.put(KBArticle.class.getName() + "_RatingsType", "like");
		properties.put(MBDiscussion.class.getName() + "_RatingsType", "like");
		properties.put(MBMessage.class.getName() + "_RatingsType", "like");
		properties.put(WikiPage.class.getName() + "_RatingsType", "like");
		return properties;
	}

	private void mockCompanyContextDetails(RatingsType ratingsType) {
		when(mockCompanyContext.getDefaultRatingsType()).thenReturn(ratingsType);
		when(mockCompanyContext.getCompany()).thenReturn(mockCompany);
		when(mockCompanyContext.getMaxUsers()).thenReturn(MAX_USERS);
		when(mockCompanyContext.getCompanyLogo()).thenReturn(mockInputStream);
	}

}
