package com.placecube.initializer.service.impl;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.layout.page.template.model.LayoutPageTemplateCollection;
import com.liferay.layout.page.template.model.LayoutPageTemplateEntry;
import com.liferay.layout.page.template.service.LayoutPageTemplateEntryLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.LayoutPrototype;
import com.liferay.portal.kernel.service.LayoutPrototypeService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.initializer.model.FragmentContext;
import com.placecube.initializer.model.LayoutContext;
import com.placecube.initializer.model.PortletContext;
import com.placecube.initializer.service.internal.PageTemplateUtil;

import junitparams.JUnitParamsRunner;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class LayoutPageTemplateEntryInitializerImplTest extends PowerMockito {

	@InjectMocks
	private LayoutPageTemplateEntryInitializerImpl layoutPageTemplateEntryInitializerImpl;

	private static final long CLASSNAME_ID_LAYOUT = 34567;
	private static final long CLASSNAME_ID_PORTLET_DISPLAY_TEMPLATE = 567891l;
	private static final long GROUP_ID = 123;
	private static final long LAYOUT_PROTOTYPE_ID = 456;
	private static final long TEMPLATE_COLLECTION_ID = 789;
	private static final long PLID = 445566;
	private static final String NAME = "entryNameValue";
	private static final String FRIENDLY_URL = "friendlyURLValue";
	private static final String LAYOUT_TEMPLATE_ID = "layoutTemplateIdValue";
	private static final String STRUCTURE_CONTENT = "structureContentValue";
	private static final Locale LOCALE = Locale.CANADA;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private PageTemplateUtil mockPageTemplateUtil;

	@Mock
	private LayoutPageTemplateCollection mockLayoutPageTemplateCollection1;

	@Mock
	private LayoutPageTemplateEntry mockLayoutPageTemplateEntry;

	@Mock
	private LayoutPageTemplateEntryLocalService mockLayoutPageTemplateEntryLocalService;

	@Mock
	private LayoutPrototype mockLayoutPrototype;

	@Mock
	private LayoutPrototypeService mockLayoutPrototypeService;

	@Mock
	private Set<FragmentContext> mockFragmentContexts;

	@Mock
	private Set<PortletContext> mockPortletContexts;

	@Mock
	private Map<Integer, Set<PortletContext>> mockColumnsPortletContexts;

	@Mock
	private LayoutContext mockLayoutContext;

	@Test(expected = PortalException.class)
	public void createContentPageTemplate_WhenExceptionCreatingBasicPageTemplate_ThenThrowsPortalException() throws Exception {
		mockLayoutContextDetails();
		when(mockPageTemplateUtil.createBasicPageTemplate(mockLayoutPageTemplateCollection1, NAME, mockServiceContext)).thenThrow(new PortalException());

		layoutPageTemplateEntryInitializerImpl.createContentPageTemplate(mockLayoutPageTemplateCollection1, mockLayoutContext, mockServiceContext);
	}

	@Test(expected = PortalException.class)
	public void createContentPageTemplate_WhenExceptionConfiguringThePageTemplate_ThenThrowsPortalException() throws Exception {
		mockLayoutContextDetails();
		when(mockLayoutPageTemplateEntry.getPlid()).thenReturn(PLID);
		when(mockPageTemplateUtil.createBasicPageTemplate(mockLayoutPageTemplateCollection1, NAME, mockServiceContext)).thenReturn(mockLayoutPageTemplateEntry);
		doThrow(new PortalException()).when(mockPageTemplateUtil).configureContentPage(mockFragmentContexts, mockPortletContexts, STRUCTURE_CONTENT, mockServiceContext, PLID);

		layoutPageTemplateEntryInitializerImpl.createContentPageTemplate(mockLayoutPageTemplateCollection1, mockLayoutContext, mockServiceContext);
	}

	@Test
	public void createContentPageTemplate_WhenNoError_ThenReturnsTheLayoutPageTemplateEntry() throws Exception {
		mockLayoutContextDetails();
		when(mockPageTemplateUtil.createBasicPageTemplate(mockLayoutPageTemplateCollection1, NAME, mockServiceContext)).thenReturn(mockLayoutPageTemplateEntry);

		LayoutPageTemplateEntry result = layoutPageTemplateEntryInitializerImpl.createContentPageTemplate(mockLayoutPageTemplateCollection1, mockLayoutContext, mockServiceContext);

		assertThat(result, sameInstance(mockLayoutPageTemplateEntry));
	}

	@Test
	public void createContentPageTemplate_WhenNoError_ThenConfiguresThePageTemplate() throws Exception {
		mockLayoutContextDetails();
		when(mockLayoutPageTemplateEntry.getPlid()).thenReturn(PLID);
		when(mockPageTemplateUtil.createBasicPageTemplate(mockLayoutPageTemplateCollection1, NAME, mockServiceContext)).thenReturn(mockLayoutPageTemplateEntry);

		layoutPageTemplateEntryInitializerImpl.createContentPageTemplate(mockLayoutPageTemplateCollection1, mockLayoutContext, mockServiceContext);

		verify(mockPageTemplateUtil, times(1)).configureContentPage(mockFragmentContexts, mockPortletContexts, STRUCTURE_CONTENT, mockServiceContext, PLID);
	}

	@Test(expected = PortalException.class)
	public void createDisplayPageTemplate_WhenExceptionCreatingDisplayPageTemplate_ThenThrowsPortalException() throws Exception {
		mockLayoutContextDetails();
		when(mockPageTemplateUtil.createDisplayPageTemplate(NAME, CLASSNAME_ID_LAYOUT, CLASSNAME_ID_PORTLET_DISPLAY_TEMPLATE, mockServiceContext)).thenThrow(new PortalException());

		layoutPageTemplateEntryInitializerImpl.createDisplayPageTemplate(mockLayoutContext, CLASSNAME_ID_LAYOUT, CLASSNAME_ID_PORTLET_DISPLAY_TEMPLATE, mockServiceContext);
	}

	@Test(expected = PortalException.class)
	public void createDisplayPageTemplate_WhenExceptionConfiguringThePageTemplate_ThenThrowsPortalException() throws Exception {
		mockLayoutContextDetails();
		when(mockLayoutPageTemplateEntry.getPlid()).thenReturn(PLID);
		when(mockPageTemplateUtil.createDisplayPageTemplate(NAME, CLASSNAME_ID_LAYOUT, CLASSNAME_ID_PORTLET_DISPLAY_TEMPLATE, mockServiceContext)).thenReturn(mockLayoutPageTemplateEntry);
		doThrow(new PortalException()).when(mockPageTemplateUtil).configureContentPage(mockFragmentContexts, mockPortletContexts, STRUCTURE_CONTENT, mockServiceContext, PLID);

		layoutPageTemplateEntryInitializerImpl.createDisplayPageTemplate(mockLayoutContext, CLASSNAME_ID_LAYOUT, CLASSNAME_ID_PORTLET_DISPLAY_TEMPLATE, mockServiceContext);
	}

	@Test
	public void createDisplayPageTemplate_WhenNoError_ThenReturnsTheLayoutPageTemplateEntry() throws Exception {
		mockLayoutContextDetails();
		when(mockPageTemplateUtil.createDisplayPageTemplate(NAME, CLASSNAME_ID_LAYOUT, CLASSNAME_ID_PORTLET_DISPLAY_TEMPLATE, mockServiceContext)).thenReturn(mockLayoutPageTemplateEntry);

		LayoutPageTemplateEntry result = layoutPageTemplateEntryInitializerImpl.createDisplayPageTemplate(mockLayoutContext, CLASSNAME_ID_LAYOUT, CLASSNAME_ID_PORTLET_DISPLAY_TEMPLATE,
				mockServiceContext);

		assertThat(result, sameInstance(mockLayoutPageTemplateEntry));
	}

	@Test
	public void createDisplayPageTemplate_WhenNoError_ThenConfiguresThePageTemplate() throws Exception {
		mockLayoutContextDetails();
		when(mockLayoutPageTemplateEntry.getPlid()).thenReturn(PLID);
		when(mockPageTemplateUtil.createDisplayPageTemplate(NAME, CLASSNAME_ID_LAYOUT, CLASSNAME_ID_PORTLET_DISPLAY_TEMPLATE, mockServiceContext)).thenReturn(mockLayoutPageTemplateEntry);

		layoutPageTemplateEntryInitializerImpl.createDisplayPageTemplate(mockLayoutContext, CLASSNAME_ID_LAYOUT, CLASSNAME_ID_PORTLET_DISPLAY_TEMPLATE, mockServiceContext);

		verify(mockPageTemplateUtil, times(1)).configureContentPage(mockFragmentContexts, mockPortletContexts, STRUCTURE_CONTENT, mockServiceContext, PLID);
	}

	/*

	 */

	@Test(expected = PortalException.class)
	public void createWidgetPageTemplate_WhenExceptionAddingLayoutPrototype_ThenThrowsPortalException() throws Exception {
		mockLayoutContextDetails();
		mockServiceContextDetails();

		when(mockLayoutPrototypeService.addLayoutPrototype(getNameMap(), new HashMap<>(), true, mockServiceContext)).thenThrow(new PortalException());

		layoutPageTemplateEntryInitializerImpl.createWidgetPageTemplate(mockLayoutPageTemplateCollection1, mockLayoutContext, mockServiceContext);
	}

	@Test(expected = PortalException.class)
	public void createWidgetPageTemplate_WhenLayoutPageTemplateEntryWasNotFound_ThenThrowsPortalException() throws Exception {
		mockLayoutContextDetails();
		mockServiceContextDetails();

		when(mockLayoutPrototypeService.addLayoutPrototype(getNameMap(), new HashMap<>(), true, mockServiceContext)).thenReturn(mockLayoutPrototype);
		when(mockLayoutPrototype.getLayoutPrototypeId()).thenReturn(LAYOUT_PROTOTYPE_ID);
		when(mockLayoutPageTemplateEntryLocalService.fetchFirstLayoutPageTemplateEntry(LAYOUT_PROTOTYPE_ID)).thenReturn(null);

		layoutPageTemplateEntryInitializerImpl.createWidgetPageTemplate(mockLayoutPageTemplateCollection1, mockLayoutContext, mockServiceContext);
	}

	@Test
	public void createWidgetPageTemplate_WhenNoErrorAndLayoutPageTemplateEntryWasFound_ThenReturnsTheLayoutPageTemplateEntry() throws Exception {
		mockLayoutContextDetails();
		mockServiceContextDetails();

		when(mockLayoutPrototypeService.addLayoutPrototype(getNameMap(), new HashMap<>(), true, mockServiceContext)).thenReturn(mockLayoutPrototype);
		when(mockLayoutPrototype.getLayoutPrototypeId()).thenReturn(LAYOUT_PROTOTYPE_ID);
		when(mockLayoutPageTemplateEntryLocalService.fetchFirstLayoutPageTemplateEntry(LAYOUT_PROTOTYPE_ID)).thenReturn(mockLayoutPageTemplateEntry);
		when(mockLayoutPageTemplateCollection1.getLayoutPageTemplateCollectionId()).thenReturn(TEMPLATE_COLLECTION_ID);
		when(mockLayoutPageTemplateEntryLocalService.updateLayoutPageTemplateEntry(mockLayoutPageTemplateEntry)).thenReturn(mockLayoutPageTemplateEntry);

		LayoutPageTemplateEntry result = layoutPageTemplateEntryInitializerImpl.createWidgetPageTemplate(mockLayoutPageTemplateCollection1, mockLayoutContext, mockServiceContext);

		assertThat(result, sameInstance(mockLayoutPageTemplateEntry));
		InOrder inOrder = Mockito.inOrder(mockLayoutPageTemplateEntry, mockLayoutPageTemplateEntryLocalService);
		inOrder.verify(mockLayoutPageTemplateEntry, times(1)).setGroupId(GROUP_ID);
		inOrder.verify(mockLayoutPageTemplateEntry, times(1)).setLayoutPageTemplateCollectionId(TEMPLATE_COLLECTION_ID);
		inOrder.verify(mockLayoutPageTemplateEntryLocalService, times(1)).updateLayoutPageTemplateEntry(mockLayoutPageTemplateEntry);
	}

	@Test(expected = PortalException.class)
	public void createWidgetPageTemplate_WhenExceptionOnConfiguringWidgetPage_ThenThrowsPortalException() throws Exception {
		mockLayoutContextDetails();
		mockServiceContextDetails();

		when(mockLayoutPrototypeService.addLayoutPrototype(getNameMap(), new HashMap<>(), true, mockServiceContext)).thenReturn(mockLayoutPrototype);
		when(mockLayoutPrototype.getLayoutPrototypeId()).thenReturn(LAYOUT_PROTOTYPE_ID);
		when(mockLayoutPageTemplateEntryLocalService.fetchFirstLayoutPageTemplateEntry(LAYOUT_PROTOTYPE_ID)).thenReturn(mockLayoutPageTemplateEntry);
		when(mockLayoutPageTemplateCollection1.getLayoutPageTemplateCollectionId()).thenReturn(TEMPLATE_COLLECTION_ID);
		when(mockLayoutPageTemplateEntryLocalService.updateLayoutPageTemplateEntry(mockLayoutPageTemplateEntry)).thenReturn(mockLayoutPageTemplateEntry);
		when(mockLayoutPageTemplateEntry.getPlid()).thenReturn(PLID);
		when(mockPageTemplateUtil.configureWidgetPage(LAYOUT_TEMPLATE_ID, mockColumnsPortletContexts, mockServiceContext, PLID)).thenThrow(new PortalException());

		layoutPageTemplateEntryInitializerImpl.createWidgetPageTemplate(mockLayoutPageTemplateCollection1, mockLayoutContext, mockServiceContext);

	}

	@Test
	public void createWidgetPageTemplate_WhenNoError_ThenConfiguresThePageTemplate() throws Exception {
		mockLayoutContextDetails();
		mockServiceContextDetails();

		when(mockLayoutPrototypeService.addLayoutPrototype(getNameMap(), new HashMap<>(), true, mockServiceContext)).thenReturn(mockLayoutPrototype);
		when(mockLayoutPrototype.getLayoutPrototypeId()).thenReturn(LAYOUT_PROTOTYPE_ID);
		when(mockLayoutPageTemplateEntryLocalService.fetchFirstLayoutPageTemplateEntry(LAYOUT_PROTOTYPE_ID)).thenReturn(mockLayoutPageTemplateEntry);
		when(mockLayoutPageTemplateCollection1.getLayoutPageTemplateCollectionId()).thenReturn(TEMPLATE_COLLECTION_ID);
		when(mockLayoutPageTemplateEntryLocalService.updateLayoutPageTemplateEntry(mockLayoutPageTemplateEntry)).thenReturn(mockLayoutPageTemplateEntry);
		when(mockLayoutPageTemplateEntry.getPlid()).thenReturn(PLID);

		layoutPageTemplateEntryInitializerImpl.createWidgetPageTemplate(mockLayoutPageTemplateCollection1, mockLayoutContext, mockServiceContext);

		verify(mockPageTemplateUtil, times(1)).configureWidgetPage(LAYOUT_TEMPLATE_ID, mockColumnsPortletContexts, mockServiceContext, PLID);
	}

	private void mockLayoutContextDetails() {
		when(mockLayoutContext.getName()).thenReturn(NAME);
		when(mockLayoutContext.getStructureContent()).thenReturn(STRUCTURE_CONTENT);
		when(mockLayoutContext.getLayoutTemplateId()).thenReturn(LAYOUT_TEMPLATE_ID);
		when(mockLayoutContext.getFriendlyURL()).thenReturn(FRIENDLY_URL);
		when(mockLayoutContext.getFragmentContexts()).thenReturn(mockFragmentContexts);
		when(mockLayoutContext.getPortletContexts()).thenReturn(mockPortletContexts);
		when(mockLayoutContext.getColumnsPortletContexts()).thenReturn(mockColumnsPortletContexts);
	}

	private void mockServiceContextDetails() {
		when(mockServiceContext.getLocale()).thenReturn(LOCALE);
		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID).thenReturn(GROUP_ID);
	}
	private Map<Locale, String> getNameMap() {
		Map<Locale, String> nameMap = new HashMap<>();
		nameMap.put(LOCALE, NAME);
		return nameMap;
	}

}
