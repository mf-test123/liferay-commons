package com.placecube.initializer.service.internal;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.portlet.PortletPreferences;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.fragment.processor.FragmentEntryProcessorRegistry;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.Portlet;
import com.liferay.portal.kernel.portlet.PortletIdCodec;
import com.liferay.portal.kernel.portlet.PortletPreferencesFactoryUtil;
import com.liferay.portal.kernel.service.PortletLocalService;
import com.placecube.initializer.model.PortletContext;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PortletIdCodec.class, PortletPreferencesFactoryUtil.class })
public class InitializerPrefsUtilTest extends PowerMockito {

	@InjectMocks
	private InitializerPrefsUtil initializerPrefsUtil;

	@Mock
	private FragmentEntryProcessorRegistry mockFragmentEntryProcessorRegistry;

	@Mock
	private JSONObject mockJSONObject;

	@Mock
	private Layout mockLayout;

	@Mock
	private Portlet mockPortlet;

	@Mock
	private PortletContext mockPortletContext;

	@Mock
	private PortletLocalService mockPortletLocalService;

	@Mock
	private PortletPreferences mockPortletPreferences;

	@Before
	public void activateSetUp() {
		mockStatic(PortletPreferencesFactoryUtil.class, PortletIdCodec.class);
	}

	@Test
	public void getFragmentSettingsForPortlet_WhenNoError_ThenConfiguresTheDefaultEditableValuesWithTheInstanceId() {
		String instanceId = "instanceIdValue";
		when(mockFragmentEntryProcessorRegistry.getDefaultEditableValuesJSONObject(StringPool.BLANK, StringPool.BLANK)).thenReturn(mockJSONObject);

		initializerPrefsUtil.getFragmentSettingsForPortlet("portletIdVal", instanceId);

		verify(mockJSONObject, times(1)).put("instanceId", instanceId);
	}

	@Test
	public void getFragmentSettingsForPortlet_WhenNoError_ThenConfiguresTheDefaultEditableValuesWithThePortletId() {
		String portletId = "portletIdValue";
		when(mockFragmentEntryProcessorRegistry.getDefaultEditableValuesJSONObject(StringPool.BLANK, StringPool.BLANK)).thenReturn(mockJSONObject);

		initializerPrefsUtil.getFragmentSettingsForPortlet(portletId, "instanceIdVal");

		verify(mockJSONObject, times(1)).put("portletId", portletId);
	}

	@Test
	public void getFragmentSettingsForPortlet_WhenNoError_ThenReturnsTheDefaultEditableValues() {
		String expected = "expectedValue";
		when(mockFragmentEntryProcessorRegistry.getDefaultEditableValuesJSONObject(StringPool.BLANK, StringPool.BLANK)).thenReturn(mockJSONObject);
		when(mockJSONObject.toJSONString()).thenReturn(expected);

		String result = initializerPrefsUtil.getFragmentSettingsForPortlet("portletIdVal", "instanceIdVal");

		assertThat(result, equalTo(expected));
	}

	@Test
	public void getPortletIdWithInstanceIdDetails_WhenInstanceIdIsEmpty_ThenReturnsThePortletId() {
		String result = initializerPrefsUtil.getPortletIdWithInstanceIdDetails("portletIdValue", StringPool.THREE_SPACES);

		assertThat(result, equalTo("portletIdValue"));
	}

	@Test
	public void getPortletIdWithInstanceIdDetails_WhenInstanceIdIsNotEmptyAndPortletIdDoesAlreadyContainInstanceDetails_ThenReturnsThePortletId() {
		String result = initializerPrefsUtil.getPortletIdWithInstanceIdDetails("portletIdValue_INSTANCE_12", "instanceIdValue");

		assertThat(result, equalTo("portletIdValue_INSTANCE_12"));
	}

	@Test
	public void getPortletIdWithInstanceIdDetails_WhenInstanceIdIsNotEmptyAndPortletIdDoesNotYetContainInstanceDetails_ThenReturnsThePortletIdWithTheInstanceIdAppended() {
		String result = initializerPrefsUtil.getPortletIdWithInstanceIdDetails("portletIdValue", "instanceIdValue");

		assertThat(result, equalTo("portletIdValue_INSTANCE_instanceIdValue"));
	}

	@Test
	public void getPortletIdWithInstanceIdDetails_WhenInstanceIdIsNull_ThenReturnsThePortletId() {
		String result = initializerPrefsUtil.getPortletIdWithInstanceIdDetails("portletIdValue", null);

		assertThat(result, equalTo("portletIdValue"));
	}

	@Test
	public void getPortletInstanceId_WhenCustomInstanceIdIsSpecified_ThenReturnsTheCustomInstanceId() {
		String customInstanceId = "customInstanceIdValue";
		String portletId = "portletIdValue";
		when(mockPortletContext.getPortletId()).thenReturn(portletId);
		when(mockPortletContext.getCustomInstanceId()).thenReturn(customInstanceId);

		String result = initializerPrefsUtil.getPortletInstanceId(mockPortletContext);

		assertThat(result, equalTo(customInstanceId));

		verifyZeroInteractions(mockPortletLocalService);
	}

	@Test
	public void getPortletInstanceId_WhenCustomInstanceIdNotSpecifiedAndPortletIsFoundAndIsInstanceable_ThenReturnsTheRandomInstanceId() {
		String randomInstanceId = "randomInstanceIdValue";
		String portletId = "portletIdValue";
		when(mockPortletContext.getPortletId()).thenReturn(portletId);
		when(mockPortletContext.getCustomInstanceId()).thenReturn(StringPool.BLANK);
		when(mockPortletLocalService.getPortletById(portletId)).thenReturn(mockPortlet);
		when(mockPortlet.isInstanceable()).thenReturn(true);
		when(PortletIdCodec.generateInstanceId()).thenReturn(randomInstanceId);

		String result = initializerPrefsUtil.getPortletInstanceId(mockPortletContext);

		assertThat(result, equalTo(randomInstanceId));
	}

	@Test
	public void getPortletInstanceId_WhenCustomInstanceIdNotSpecifiedAndPortletIsFoundAndIsNotInstanceable_ThenReturnsEmptyString() {
		String portletId = "portletIdValue";
		when(mockPortletContext.getCustomInstanceId()).thenReturn(StringPool.BLANK);
		when(mockPortletContext.getPortletId()).thenReturn(portletId);
		when(mockPortletLocalService.getPortletById(portletId)).thenReturn(mockPortlet);
		when(mockPortlet.isInstanceable()).thenReturn(false);

		String result = initializerPrefsUtil.getPortletInstanceId(mockPortletContext);

		assertThat(result, equalTo(StringPool.BLANK));
	}

	@Test
	public void getPortletInstanceId_WhenCustomInstanceIdNotSpecifiedAndPortletIsNotFound_ThenReturnsEmptyString() {
		String portletId = "portletIdValue";
		when(mockPortletContext.getCustomInstanceId()).thenReturn(StringPool.BLANK);
		when(mockPortletContext.getPortletId()).thenReturn(portletId);
		when(mockPortletLocalService.getPortletById(portletId)).thenReturn(null);

		String result = initializerPrefsUtil.getPortletInstanceId(mockPortletContext);

		assertThat(result, equalTo(StringPool.BLANK));
	}

	@Test(expected = PortalException.class)
	public void savePortletPreferences_WhenException_ThenThrowsPortalException() throws Exception {
		String portletId = "portletIdValue";
		Map<String, String> preferences = new HashMap<>();
		preferences.put("key1", "v1");
		when(mockPortletContext.getPortletPreferences()).thenReturn(preferences);

		Map<String, String[]> multivaluedPreferences = new HashMap<>();
		multivaluedPreferences.put("keyM1", new String[] { "p1", "p2" });
		when(mockPortletContext.getPortletMultivaluedPreferences()).thenReturn(multivaluedPreferences);

		when(PortletPreferencesFactoryUtil.getLayoutPortletSetup(mockLayout, portletId)).thenReturn(mockPortletPreferences);
		doThrow(new IOException()).when(mockPortletPreferences).store();

		initializerPrefsUtil.savePortletPreferences(mockLayout, portletId, mockPortletContext);
	}

	@Test
	public void savePortletPreferences_WhenNoError_ThenConfiguresAndSavesThePortletPreferences() throws Exception {
		String portletId = "portletIdValue";
		Map<String, String> preferences = new HashMap<>();
		preferences.put("key1", "v1");
		preferences.put("key2", "v2");
		when(mockPortletContext.getPortletPreferences()).thenReturn(preferences);

		Map<String, String[]> multivaluedPreferences = new HashMap<>();
		multivaluedPreferences.put("keyM1", new String[] { "p1", "p2" });
		when(mockPortletContext.getPortletMultivaluedPreferences()).thenReturn(multivaluedPreferences);

		when(PortletPreferencesFactoryUtil.getLayoutPortletSetup(mockLayout, portletId)).thenReturn(mockPortletPreferences);

		initializerPrefsUtil.savePortletPreferences(mockLayout, portletId, mockPortletContext);

		InOrder inOrder = inOrder(mockPortletPreferences);
		inOrder.verify(mockPortletPreferences, times(1)).setValue("key1", "v1");
		inOrder.verify(mockPortletPreferences, times(1)).setValue("key2", "v2");
		inOrder.verify(mockPortletPreferences, times(1)).setValues("keyM1", new String[] { "p1", "p2" });
		inOrder.verify(mockPortletPreferences, times(1)).store();
	}

}
