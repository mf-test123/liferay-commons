package com.placecube.initializer.service.internal;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.site.navigation.menu.item.layout.constants.SiteNavigationMenuItemTypeConstants;
import com.liferay.site.navigation.service.SiteNavigationMenuItemLocalService;
import com.placecube.initializer.service.internal.constants.NavigationMenuImportConstants;

@RunWith(PowerMockRunner.class)
public class NavigationMenuImportUtilTest extends Mockito {

	private long SCOPE_GROUP_ID = 14;
	private long SITE_NAVIGATION_MENU_ID = 13;
	private long PARENT_ID = 0;
	private long USER_ID = 345;
	private String FRIENDLY_URL = "/url";
	private String TYPE_SETTINGS = "typeSettings";

	@InjectMocks
	private NavigationMenuImportUtil navigationMenuImportUtil;

	@Mock
	private InitializerUtil mockInitializerUtil;

	@Mock
	private JSONObject mockJSONObject;

	@Mock
	private Layout mockLayout;

	@Mock
	private LayoutLocalService mockLayoutLocalService;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private SiteNavigationMenuItemLocalService mockSiteNavigationMenuItemLocalService;

	@Test
	public void importAsSubmenu_WhenNoError_ThenAddsSubmenuMenuItem() throws PortalException {
		when(mockInitializerUtil.getTypeSettingsForSubmenu(mockJSONObject)).thenReturn(TYPE_SETTINGS);
		when(mockServiceContext.getScopeGroupId()).thenReturn(SCOPE_GROUP_ID);
		when(mockServiceContext.getUserId()).thenReturn(USER_ID);

		navigationMenuImportUtil.importAsSubmenuItem(mockJSONObject, mockServiceContext, SITE_NAVIGATION_MENU_ID, PARENT_ID);

		verify(mockSiteNavigationMenuItemLocalService, times(1))
				.addSiteNavigationMenuItem(USER_ID, SCOPE_GROUP_ID, SITE_NAVIGATION_MENU_ID, PARENT_ID, SiteNavigationMenuItemTypeConstants.NODE, TYPE_SETTINGS, mockServiceContext);
	}

	@Test(expected = PortalException.class)
	public void importAsSubmenu_WhenExceptionAddingMenuItem_ThenThrowsPortalException() throws PortalException {
		when(mockInitializerUtil.getTypeSettingsForSubmenu(mockJSONObject)).thenReturn(TYPE_SETTINGS);
		when(mockServiceContext.getScopeGroupId()).thenReturn(SCOPE_GROUP_ID);
		when(mockServiceContext.getUserId()).thenReturn(USER_ID);
		when(mockSiteNavigationMenuItemLocalService
				.addSiteNavigationMenuItem(USER_ID, SCOPE_GROUP_ID, SITE_NAVIGATION_MENU_ID, PARENT_ID, SiteNavigationMenuItemTypeConstants.NODE, TYPE_SETTINGS, mockServiceContext))
				.thenThrow(new PortalException());

		navigationMenuImportUtil.importAsSubmenuItem(mockJSONObject, mockServiceContext, SITE_NAVIGATION_MENU_ID, PARENT_ID);
	}

	@Test
	public void importAsURL_WhenNoError_ThenAddsSubmenuMenuItem() throws PortalException {
		when(mockInitializerUtil.getTypeSettingsForURL(mockJSONObject)).thenReturn(TYPE_SETTINGS);
		when(mockServiceContext.getScopeGroupId()).thenReturn(SCOPE_GROUP_ID);
		when(mockServiceContext.getUserId()).thenReturn(USER_ID);

		navigationMenuImportUtil.importAsURLItem(mockJSONObject, mockServiceContext, SITE_NAVIGATION_MENU_ID, PARENT_ID);

		verify(mockSiteNavigationMenuItemLocalService, times(1))
				.addSiteNavigationMenuItem(USER_ID, SCOPE_GROUP_ID, SITE_NAVIGATION_MENU_ID, PARENT_ID, SiteNavigationMenuItemTypeConstants.URL, TYPE_SETTINGS, mockServiceContext);
	}

	@Test(expected = PortalException.class)
	public void importAsURL_WhenExceptionAddingMenuItem_ThenThrowsPortalException() throws PortalException {
		when(mockInitializerUtil.getTypeSettingsForURL(mockJSONObject)).thenReturn(TYPE_SETTINGS);
		when(mockServiceContext.getScopeGroupId()).thenReturn(SCOPE_GROUP_ID);
		when(mockServiceContext.getUserId()).thenReturn(USER_ID);
		when(mockSiteNavigationMenuItemLocalService
				.addSiteNavigationMenuItem(USER_ID, SCOPE_GROUP_ID, SITE_NAVIGATION_MENU_ID, PARENT_ID, SiteNavigationMenuItemTypeConstants.URL, TYPE_SETTINGS, mockServiceContext))
				.thenThrow(new PortalException());

		navigationMenuImportUtil.importAsURLItem(mockJSONObject, mockServiceContext, SITE_NAVIGATION_MENU_ID, PARENT_ID);
	}

	@Test(expected = PortalException.class)
	public void importAsLayout_WhenExceptionGettingLayout_ThenThrowsPortalException() throws PortalException {
		boolean privateLayout = true;

		when(mockServiceContext.getScopeGroupId()).thenReturn(SCOPE_GROUP_ID);
		when(mockJSONObject.getBoolean(NavigationMenuImportConstants.PRIVATE)).thenReturn(privateLayout);
		when(mockJSONObject.getString(NavigationMenuImportConstants.FRIENDLY_URL)).thenReturn(FRIENDLY_URL);
		when(mockLayoutLocalService.getFriendlyURLLayout(SCOPE_GROUP_ID, privateLayout, FRIENDLY_URL)).thenThrow(new PortalException());

		navigationMenuImportUtil.importAsLayoutItem(mockJSONObject, mockServiceContext, SITE_NAVIGATION_MENU_ID, PARENT_ID);
	}

	@Test(expected = PortalException.class)
	public void importAsLayout_WhenExceptionAddingMenuItem_ThenThrowsPortalException() throws PortalException {
		boolean privateLayout = true;

		when(mockServiceContext.getScopeGroupId()).thenReturn(SCOPE_GROUP_ID);
		when(mockJSONObject.getBoolean(NavigationMenuImportConstants.PRIVATE)).thenReturn(privateLayout);
		when(mockJSONObject.getString(NavigationMenuImportConstants.FRIENDLY_URL)).thenReturn(FRIENDLY_URL);
		when(mockLayoutLocalService.getFriendlyURLLayout(SCOPE_GROUP_ID, privateLayout, FRIENDLY_URL)).thenReturn(mockLayout);
		when(mockServiceContext.getUserId()).thenReturn(USER_ID);
		when(mockInitializerUtil.getLayoutTypeSettingsForMenuItem(mockLayout)).thenReturn(TYPE_SETTINGS);
		when(mockSiteNavigationMenuItemLocalService
				.addSiteNavigationMenuItem(USER_ID, SCOPE_GROUP_ID, SITE_NAVIGATION_MENU_ID, PARENT_ID, SiteNavigationMenuItemTypeConstants.LAYOUT, TYPE_SETTINGS, mockServiceContext))
				.thenThrow(new PortalException());

		navigationMenuImportUtil.importAsLayoutItem(mockJSONObject, mockServiceContext, SITE_NAVIGATION_MENU_ID, PARENT_ID);
	}

	@Test
	public void importAsLayout_WhenNoException_ThenAddsMenuItem() throws PortalException {
		boolean privateLayout = true;

		when(mockServiceContext.getScopeGroupId()).thenReturn(SCOPE_GROUP_ID);
		when(mockJSONObject.getBoolean(NavigationMenuImportConstants.PRIVATE)).thenReturn(privateLayout);
		when(mockJSONObject.getString(NavigationMenuImportConstants.FRIENDLY_URL)).thenReturn(FRIENDLY_URL);
		when(mockLayoutLocalService.getFriendlyURLLayout(SCOPE_GROUP_ID, privateLayout, FRIENDLY_URL)).thenReturn(mockLayout);
		when(mockServiceContext.getUserId()).thenReturn(USER_ID);
		when(mockInitializerUtil.getLayoutTypeSettingsForMenuItem(mockLayout)).thenReturn(TYPE_SETTINGS);

		navigationMenuImportUtil.importAsLayoutItem(mockJSONObject, mockServiceContext, SITE_NAVIGATION_MENU_ID, PARENT_ID);

		verify(mockSiteNavigationMenuItemLocalService, times(1))
				.addSiteNavigationMenuItem(USER_ID, SCOPE_GROUP_ID, SITE_NAVIGATION_MENU_ID, PARENT_ID, SiteNavigationMenuItemTypeConstants.LAYOUT, TYPE_SETTINGS, mockServiceContext);
	}

	@Test
	public void isLayout_whenNoErrorAndJSONObjectHasFriedlyURLKey_ThenReturnsTrue() {
		when(mockJSONObject.has(NavigationMenuImportConstants.FRIENDLY_URL)).thenReturn(true);

		boolean result = navigationMenuImportUtil.isLayout(mockJSONObject);

		assertTrue(result);
	}

	@Test
	public void isLayout_whenNoErrorAndJSONObjectHasNotFriendlyURLKey_ThenReturnsFalse() {
		when(mockJSONObject.has(NavigationMenuImportConstants.FRIENDLY_URL)).thenReturn(false);

		boolean result = navigationMenuImportUtil.isLayout(mockJSONObject);

		assertFalse(result);
	}

	@Test
	public void isURL_whenNoErrorAndJSONObjectHasFriendlyURLKey_ThenReturnsTrue() {
		when(mockJSONObject.has(NavigationMenuImportConstants.URL)).thenReturn(true);

		boolean result = navigationMenuImportUtil.isURL(mockJSONObject);

		assertTrue(result);
	}

	@Test
	public void isURL_whenNoErrorAndJSONObjectHasNotFriendlyURLKey_ThenReturnsFalse() {
		when(mockJSONObject.has(NavigationMenuImportConstants.URL)).thenReturn(false);

		boolean result = navigationMenuImportUtil.isURL(mockJSONObject);

		assertFalse(result);
	}
}