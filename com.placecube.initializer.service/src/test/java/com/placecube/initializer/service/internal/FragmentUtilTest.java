package com.placecube.initializer.service.internal;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.StringUtil;

@RunWith(PowerMockRunner.class)
@PrepareForTest(StringUtil.class)
public class FragmentUtilTest extends PowerMockito {

	private static final String BASE_PATH = "basePathValue";

	@InjectMocks
	private FragmentUtil fragmentUtil;

	@Mock
	private JSONFactory mockJsonFactory;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private JSONObject mockJSONObject;

	@Mock
	private ClassLoader mockClassLoader;

	@Before
	public void setUp() {
		mockStatic(StringUtil.class);
	}

	@Test(expected = PortalException.class)
	public void getFragmentJSONObject_WhenExceptionCreatingJsonObject_ThenThrowsPortalException() throws Exception {
		String jsonString = "jsonStringValue";
		when(StringUtil.read(mockClassLoader, BASE_PATH + "fragment.json")).thenReturn(jsonString);
		when(mockJsonFactory.createJSONObject(jsonString)).thenThrow(new JSONException());

		fragmentUtil.getFragmentJSONObject(mockClassLoader, BASE_PATH);
	}

	@Test(expected = PortalException.class)
	public void getFragmentJSONObject_WhenExceptionReadingContent_ThenThrowsPortalException() throws Exception {
		when(StringUtil.read(mockClassLoader, BASE_PATH + "fragment.json")).thenThrow(new IOException());

		fragmentUtil.getFragmentJSONObject(mockClassLoader, BASE_PATH);
	}

	@Test
	public void getFragmentJSONObject_WhenNoError_ThenReturnsTheFragmentJSONObject() throws Exception {
		String jsonString = "jsonStringValue";
		when(StringUtil.read(mockClassLoader, BASE_PATH + "fragment.json")).thenReturn(jsonString);
		when(mockJsonFactory.createJSONObject(jsonString)).thenReturn(mockJSONObject);

		JSONObject result = fragmentUtil.getFragmentJSONObject(mockClassLoader, BASE_PATH);

		assertThat(result, sameInstance(mockJSONObject));
	}

	@Test
	public void getOptionalContent_WhenExceptionRetrievingContent_ThenReturnsEmptyString() throws Exception {
		String propertyValue = "propertyValue";
		String propertyName = "propertyNameValue";
		when(mockJSONObject.getString(propertyName)).thenReturn(propertyValue);
		when(StringUtil.read(mockClassLoader, BASE_PATH + propertyValue)).thenThrow(new IOException());

		String result = fragmentUtil.getOptionalContent(mockClassLoader, BASE_PATH, mockJSONObject, propertyName);

		assertThat(result, equalTo(StringPool.BLANK));
	}

	@Test
	public void getOptionalContent_WhenNoError_ThenRetrievesTheValueOfTheFileRead() throws Exception {
		String propertyValue = "propertyValue";
		String propertyName = "propertyNameValue";
		String fileValue = "fileValue";
		when(mockJSONObject.getString(propertyName)).thenReturn(propertyValue);
		when(StringUtil.read(mockClassLoader, BASE_PATH + propertyValue)).thenReturn(fileValue);

		String result = fragmentUtil.getOptionalContent(mockClassLoader, BASE_PATH, mockJSONObject, propertyName);

		assertThat(result, equalTo(fileValue));
	}

}
