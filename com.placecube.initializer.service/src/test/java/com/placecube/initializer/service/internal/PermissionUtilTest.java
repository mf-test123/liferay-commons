package com.placecube.initializer.service.internal;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.model.RoleConstants;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.security.permission.ActionKeys;
import com.liferay.portal.kernel.service.RoleLocalService;

public class PermissionUtilTest extends PowerMockito {

	private final static long COMPANY_ID = 1;
	private final static long ROLE_ID = 2;
	private final static long ROLE_ID_2 = 3;

	@Mock
	private Layout mockLayout;

	@Mock
	private Role mockRole;

	@Mock
	private Role mockRole2;

	@Mock
	private RoleLocalService mockRoleLocalService;

	@Mock
	private RolePermissionUtil mockRolePermissionUtil;

	@Mock
	private User mockUser;

	@Mock
	private User mockUser2;

	@InjectMocks
	private PermissionUtil permissionUtil;

	@Before
	public void activateSetup() {
		initMocks(this);
	}

	@Test(expected = PortalException.class)
	public void removeGuestPermissionView_WhenExceptionRemovingThePermission_ThenPortalExceptionisThrown() throws PortalException {
		when(mockLayout.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockRoleLocalService.getRole(COMPANY_ID, RoleConstants.GUEST)).thenReturn(mockRole);
		when(mockRole.getRoleId()).thenReturn(ROLE_ID);
		doThrow(new PortalException()).when(mockRolePermissionUtil).removePermission(mockLayout, ROLE_ID, ActionKeys.VIEW);

		permissionUtil.removeGuestPermissionView(mockLayout);
	}

	@Test(expected = PortalException.class)
	public void removeGuestPermissionView_WhenExceptionRetrievingTheGuestRole_ThenPortalExceptionisThrown() throws PortalException {
		when(mockLayout.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockRoleLocalService.getRole(COMPANY_ID, RoleConstants.GUEST)).thenThrow(new PortalException());

		permissionUtil.removeGuestPermissionView(mockLayout);
	}

	@Test
	public void removeGuestPermissionView_WhenNoError_ThenRemovesGuestViewPermission() throws PortalException {
		when(mockLayout.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockRoleLocalService.getRole(COMPANY_ID, RoleConstants.GUEST)).thenReturn(mockRole);
		when(mockRole.getRoleId()).thenReturn(ROLE_ID);

		permissionUtil.removeGuestPermissionView(mockLayout);

		verify(mockRolePermissionUtil, times(1)).removePermission(mockLayout, ROLE_ID, ActionKeys.VIEW);
	}

	@Test(expected = PortalException.class)
	public void updateRolePermissions_WhenExceptionAddingAPermission_ThenPortalExceptionisThrown() throws PortalException {
		Map<String, String[]> rolePermissionsToAdd = new HashMap<>();
		rolePermissionsToAdd.put("RoleName1", new String[] { "one", "two" });
		Map<String, String[]> rolePermissionsToRemove = new HashMap<>();
		when(mockLayout.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockRoleLocalService.getRole(COMPANY_ID, "RoleName1")).thenReturn(mockRole);
		when(mockRole.getRoleId()).thenReturn(ROLE_ID);
		doThrow(new PortalException()).when(mockRolePermissionUtil).addPermissions(mockLayout, ROLE_ID, new String[] { "one", "two" });

		permissionUtil.updateRolePermissions(mockLayout, rolePermissionsToAdd, rolePermissionsToRemove);
	}

	@Test(expected = PortalException.class)
	public void updateRolePermissions_WhenExceptionRemovingAPermission_ThenPortalExceptionisThrown() throws PortalException {
		Map<String, String[]> rolePermissionsToAdd = new HashMap<>();
		Map<String, String[]> rolePermissionsToRemove = new HashMap<>();
		rolePermissionsToRemove.put("RoleName1", new String[] { "one", "two" });
		when(mockLayout.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockRoleLocalService.getRole(COMPANY_ID, "RoleName1")).thenReturn(mockRole);
		when(mockRole.getRoleId()).thenReturn(ROLE_ID);
		doThrow(new PortalException()).when(mockRolePermissionUtil).removePermission(mockLayout, ROLE_ID, "one");

		permissionUtil.updateRolePermissions(mockLayout, rolePermissionsToAdd, rolePermissionsToRemove);
	}

	@Test(expected = PortalException.class)
	public void updateRolePermissions_WhenExceptionRetrievingARole_ThenPortalExceptionisThrown() throws PortalException {
		Map<String, String[]> rolePermissionsToAdd = new HashMap<>();
		rolePermissionsToAdd.put("RoleName1", new String[] { "one", "two" });
		Map<String, String[]> rolePermissionsToRemove = new HashMap<>();
		when(mockLayout.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockRoleLocalService.getRole(COMPANY_ID, "RoleName1")).thenThrow(new PortalException());

		permissionUtil.updateRolePermissions(mockLayout, rolePermissionsToAdd, rolePermissionsToRemove);
	}

	@Test
	public void updateRolePermissions_WhenNoError_ThenAddsThePermissionsToAddAndRemovesThePermissionsToRemove() throws PortalException {
		Map<String, String[]> rolePermissionsToAdd = new HashMap<>();
		rolePermissionsToAdd.put("RoleName1", new String[] { "one", "two" });
		Map<String, String[]> rolePermissionsToRemove = new HashMap<>();
		rolePermissionsToRemove.put("RoleName2", new String[] { "four", "five" });
		when(mockLayout.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockLayout.getCompanyId()).thenReturn(COMPANY_ID);
		when(mockRoleLocalService.getRole(COMPANY_ID, "RoleName1")).thenReturn(mockRole);
		when(mockRoleLocalService.getRole(COMPANY_ID, "RoleName2")).thenReturn(mockRole2);
		when(mockRole.getRoleId()).thenReturn(ROLE_ID);
		when(mockRole2.getRoleId()).thenReturn(ROLE_ID_2);

		permissionUtil.updateRolePermissions(mockLayout, rolePermissionsToAdd, rolePermissionsToRemove);

		InOrder inOrder = Mockito.inOrder(mockRolePermissionUtil);
		inOrder.verify(mockRolePermissionUtil, times(1)).addPermissions(mockLayout, ROLE_ID, new String[] { "one", "two" });
		inOrder.verify(mockRolePermissionUtil, times(1)).removePermission(mockLayout, ROLE_ID_2, "four");
		inOrder.verify(mockRolePermissionUtil, times(1)).removePermission(mockLayout, ROLE_ID_2, "five");
	}

}
