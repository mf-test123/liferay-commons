package com.placecube.initializer.service.impl;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;

import java.io.IOException;
import java.util.Locale;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.dynamic.data.lists.model.DDLRecordSet;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMStructureConstants;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.model.DDMTemplateConstants;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalService;
import com.liferay.dynamic.data.mapping.util.DefaultDDMStructureHelper;
import com.liferay.journal.model.JournalArticle;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.template.TemplateConstants;
import com.liferay.portal.kernel.theme.NavItem;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portlet.display.template.PortletDisplayTemplate;
import com.placecube.initializer.model.DDMTemplateContext;
import com.placecube.initializer.model.impl.DDMTemplateContextImpl;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ StringUtil.class})
public class DDMInitializerImplTest extends PowerMockito {

	public static final long CLASS_NAME_ID_DDL_RECORD_SET = 2L;

	public static final long CLASS_NAME_ID_DDM_STRUCTURE = 1L;

	public static final long CLASS_NAME_ID_JOURNAL_ARTICLE = 3L;

	public static final long CLASS_NAME_ID_NAV_ITEM = 4L;

	public static final long CLASS_NAME_ID_PORTLET_DISPLAY_TEMPLATE = 5L;

	public static final long GROUP_ID = 6L;

	public static final String RESOURCE_PATH = "resourcePath";

	public static final long STRUCTURE_ID = 7L;

	public static final String STRUCTURE_KEY = "structureKey";

	public static final String TEMPLATE_KEY = "templateKey";

	public static final String TEMPLATE_NAME = "templateName";

	public static final long USER_ID = 8L;

	@InjectMocks
	private DDMInitializerImpl ddmInitializerImpl;

	@Mock
	private ClassLoader mockClassLoader;

	@Mock
	private DDMStructure mockDDMStructure;

	@Mock
	private DDMStructureLocalService mockDDMStructureLocalService;

	@Mock
	private DDMTemplate mockDDMTemplate;

	@Mock
	private DDMTemplateLocalService mockDDMTemplateLocalService;

	@Mock
	private DefaultDDMStructureHelper mockDefaultDDMStructureHelper;

	@Mock
	private Portal mockPortal;

	@Mock
	private ServiceContext mockServiceContext;

	@Before
	public void activateSetUp() {

		mockStatic(StringUtil.class);

		when(mockServiceContext.getLocale()).thenReturn(Locale.ENGLISH);
		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockServiceContext.getUserId()).thenReturn(USER_ID);

	}

	@Test
	public void getDDMTemplateContextForDDL_WhenNoErrors_ThenReturnsDDMTemplateContextForDDL() {

		when(mockPortal.getClassNameId(DDMStructure.class)).thenReturn(CLASS_NAME_ID_DDM_STRUCTURE);
		when(mockPortal.getClassNameId(DDLRecordSet.class)).thenReturn(CLASS_NAME_ID_DDL_RECORD_SET);

		DDMTemplateContext ddmTemplateContext = ddmInitializerImpl.getDDMTemplateContextForDDL(TEMPLATE_NAME, TEMPLATE_KEY, STRUCTURE_ID, RESOURCE_PATH, mockClassLoader, mockServiceContext);

		assertThat(ddmTemplateContext.getCacheable(), equalTo(true));
		assertThat(ddmTemplateContext.getClassNameId(), equalTo(CLASS_NAME_ID_DDM_STRUCTURE));
		assertThat(ddmTemplateContext.getDescription().isEmpty(), equalTo(true));
		assertThat(ddmTemplateContext.getLanguage(), equalTo(TemplateConstants.LANG_TYPE_FTL));
		assertThat(ddmTemplateContext.getMode(), equalTo(DDMTemplateConstants.TEMPLATE_MODE_CREATE));
		assertThat(ddmTemplateContext.getTemplateName().size(), equalTo(1));
		assertThat(ddmTemplateContext.getTemplateName().get(Locale.ENGLISH), equalTo(TEMPLATE_NAME));
		assertThat(ddmTemplateContext.getResourceClassLoader(), equalTo(mockClassLoader));
		assertThat(ddmTemplateContext.getResourceClassNameId(), equalTo(CLASS_NAME_ID_DDL_RECORD_SET));
		assertThat(ddmTemplateContext.getResourcePath(), equalTo(RESOURCE_PATH));
		assertThat(ddmTemplateContext.getServiceContext(), sameInstance(mockServiceContext));
		assertThat(ddmTemplateContext.getSmallImage(), equalTo(false));
		assertThat(ddmTemplateContext.getSmallImageFile(), equalTo(null));
		assertThat(ddmTemplateContext.getSmallImageUrl(), equalTo(StringPool.BLANK));
		assertThat(ddmTemplateContext.getStructureId(), equalTo(STRUCTURE_ID));
		assertThat(ddmTemplateContext.getTemplateKey(), equalTo(TEMPLATE_KEY));
		assertThat(ddmTemplateContext.getType(), equalTo(DDMTemplateConstants.TEMPLATE_TYPE_DISPLAY));

	}

	@Test
	public void getDDMTemplateContextForPortletDisplay_WhenNoErrors_ThenReturnsDDMTemplateContextForPortletDisplay() {

		Class aClass = NavItem.class;
		when(mockPortal.getClassNameId(aClass)).thenReturn(CLASS_NAME_ID_NAV_ITEM);
		when(mockPortal.getClassNameId(PortletDisplayTemplate.class)).thenReturn(CLASS_NAME_ID_PORTLET_DISPLAY_TEMPLATE);

		DDMTemplateContext ddmTemplateContext = ddmInitializerImpl.getDDMTemplateContextForPortletDisplay(TEMPLATE_NAME, TEMPLATE_KEY, aClass, RESOURCE_PATH, mockClassLoader, mockServiceContext);

		assertThat(ddmTemplateContext.getCacheable(), equalTo(true));
		assertThat(ddmTemplateContext.getClassNameId(), equalTo(CLASS_NAME_ID_NAV_ITEM));
		assertThat(ddmTemplateContext.getDescription().isEmpty(), equalTo(true));
		assertThat(ddmTemplateContext.getLanguage(), equalTo(TemplateConstants.LANG_TYPE_FTL));
		assertThat(ddmTemplateContext.getMode(), equalTo(DDMTemplateConstants.TEMPLATE_MODE_CREATE));
		assertThat(ddmTemplateContext.getTemplateName().size(), equalTo(1));
		assertThat(ddmTemplateContext.getTemplateName().get(Locale.ENGLISH), equalTo(TEMPLATE_NAME));
		assertThat(ddmTemplateContext.getResourceClassLoader(), equalTo(mockClassLoader));
		assertThat(ddmTemplateContext.getResourceClassNameId(), equalTo(CLASS_NAME_ID_PORTLET_DISPLAY_TEMPLATE));
		assertThat(ddmTemplateContext.getResourcePath(), equalTo(RESOURCE_PATH));
		assertThat(ddmTemplateContext.getServiceContext(), sameInstance(mockServiceContext));
		assertThat(ddmTemplateContext.getSmallImage(), equalTo(false));
		assertThat(ddmTemplateContext.getSmallImageFile(), equalTo(null));
		assertThat(ddmTemplateContext.getSmallImageUrl(), equalTo(StringPool.BLANK));
		assertThat(ddmTemplateContext.getStructureId(), equalTo(DDMStructureConstants.DEFAULT_PARENT_STRUCTURE_ID));
		assertThat(ddmTemplateContext.getTemplateKey(), equalTo(TEMPLATE_KEY));
		assertThat(ddmTemplateContext.getType(), equalTo(DDMTemplateConstants.TEMPLATE_TYPE_DISPLAY));
	}

	@Test
	public void getDDMTemplateContextForWebContent_WhenNoErrors_ThenReturnsDDMTemplateContextForWebContent() {

		when(mockPortal.getClassNameId(DDMStructure.class)).thenReturn(CLASS_NAME_ID_DDM_STRUCTURE);
		when(mockPortal.getClassNameId(JournalArticle.class)).thenReturn(CLASS_NAME_ID_JOURNAL_ARTICLE);

		DDMTemplateContext ddmTemplateContext = ddmInitializerImpl.getDDMTemplateContextForWebContent(TEMPLATE_NAME, TEMPLATE_KEY, STRUCTURE_ID, RESOURCE_PATH, mockClassLoader, mockServiceContext);

		assertThat(ddmTemplateContext.getCacheable(), equalTo(true));
		assertThat(ddmTemplateContext.getClassNameId(), equalTo(CLASS_NAME_ID_DDM_STRUCTURE));
		assertThat(ddmTemplateContext.getDescription().isEmpty(), equalTo(true));
		assertThat(ddmTemplateContext.getLanguage(), equalTo(TemplateConstants.LANG_TYPE_FTL));
		assertThat(ddmTemplateContext.getMode(), equalTo(DDMTemplateConstants.TEMPLATE_MODE_CREATE));
		assertThat(ddmTemplateContext.getTemplateName().size(), equalTo(1));
		assertThat(ddmTemplateContext.getTemplateName().get(Locale.ENGLISH), equalTo(TEMPLATE_NAME));
		assertThat(ddmTemplateContext.getResourceClassLoader(), equalTo(mockClassLoader));
		assertThat(ddmTemplateContext.getResourceClassNameId(), equalTo(CLASS_NAME_ID_JOURNAL_ARTICLE));
		assertThat(ddmTemplateContext.getResourcePath(), equalTo(RESOURCE_PATH));
		assertThat(ddmTemplateContext.getServiceContext(), sameInstance(mockServiceContext));
		assertThat(ddmTemplateContext.getSmallImage(), equalTo(false));
		assertThat(ddmTemplateContext.getSmallImageFile(), equalTo(null));
		assertThat(ddmTemplateContext.getSmallImageUrl(), equalTo(StringPool.BLANK));
		assertThat(ddmTemplateContext.getStructureId(), equalTo(STRUCTURE_ID));
		assertThat(ddmTemplateContext.getTemplateKey(), equalTo(TEMPLATE_KEY));
		assertThat(ddmTemplateContext.getType(), equalTo(DDMTemplateConstants.TEMPLATE_TYPE_DISPLAY));
	}


	@Test(expected = PortalException.class)
	public void getOrCreateDDMStructure_WhenErrorAddingStructure_ThenThrowsException() throws Exception {

		when(mockPortal.getClassNameId(JournalArticle.class)).thenReturn(CLASS_NAME_ID_DDM_STRUCTURE);
		doThrow(new Exception()).when(mockDefaultDDMStructureHelper).addDDMStructures(USER_ID, GROUP_ID, CLASS_NAME_ID_DDM_STRUCTURE, mockClassLoader, RESOURCE_PATH, mockServiceContext);

		ddmInitializerImpl.getOrCreateDDMStructure(STRUCTURE_KEY, RESOURCE_PATH, mockClassLoader, JournalArticle.class, mockServiceContext);

	}

	@Test(expected = PortalException.class)
	public void getOrCreateDDMStructure_WhenErrorRetrievingStructure_ThenThrowsException() throws PortalException {

		when(mockPortal.getClassNameId(JournalArticle.class)).thenReturn(CLASS_NAME_ID_DDM_STRUCTURE);
		when(mockDDMStructureLocalService.getStructure(GROUP_ID, CLASS_NAME_ID_DDM_STRUCTURE, STRUCTURE_KEY)).thenThrow(new PortalException());

		ddmInitializerImpl.getOrCreateDDMStructure(STRUCTURE_KEY, RESOURCE_PATH, mockClassLoader, JournalArticle.class, mockServiceContext);

	}

	@Test
	public void getOrCreateDDMStructure_WhenNoErrors_ThenReturnsDDMStructure() throws PortalException {

		when(mockPortal.getClassNameId(JournalArticle.class)).thenReturn(CLASS_NAME_ID_DDM_STRUCTURE);
		when(mockDDMStructureLocalService.getStructure(GROUP_ID, CLASS_NAME_ID_DDM_STRUCTURE, STRUCTURE_KEY)).thenReturn(mockDDMStructure);

		DDMStructure ddmStructure = ddmInitializerImpl.getOrCreateDDMStructure(STRUCTURE_KEY, RESOURCE_PATH, mockClassLoader, JournalArticle.class, mockServiceContext);

		assertThat(ddmStructure, sameInstance(mockDDMStructure));

	}
	@Test
	public void getOrCreateDDMTemplate_WhenTemplateAlreadyExistsAndNoErrors_ThenReturnsDDMTemplate() throws PortalException {

		DDMTemplateContextImpl ddmTemplateContext = new DDMTemplateContextImpl(TEMPLATE_NAME, TEMPLATE_KEY, CLASS_NAME_ID_DDL_RECORD_SET, CLASS_NAME_ID_PORTLET_DISPLAY_TEMPLATE, STRUCTURE_ID,
				RESOURCE_PATH, mockClassLoader, mockServiceContext);
		when(mockDDMTemplateLocalService.fetchTemplate(GROUP_ID, CLASS_NAME_ID_DDL_RECORD_SET, TEMPLATE_KEY)).thenReturn(mockDDMTemplate);

		DDMTemplate ddmTemplate = ddmInitializerImpl.getOrCreateDDMTemplate(ddmTemplateContext);

		assertThat(ddmTemplate, sameInstance(mockDDMTemplate));

	}

	@Test(expected = PortalException.class)
	public void getOrCreateDDMTemplate_WhenTemplateDoesNotExistAndErrorCreatingTemplate_ThenThrowsException() throws PortalException, IOException {
		String templateContent = "templateContent";
		String templateContentWithPlaceholders = "templateContentWithPlaceholders";

		when(StringUtil.read(mockClassLoader, RESOURCE_PATH)).thenReturn(templateContent);
		when(StringUtil.replace(templateContent, new String[] { "[$PLACEHOLDER_ONE$]", "[$PLACEHOLDER_TWO$]" }, new String[] { "placeholderValue1", "placeholderValue2" }))
				.thenReturn(templateContentWithPlaceholders);

		DDMTemplateContextImpl ddmTemplateContext = new DDMTemplateContextImpl(TEMPLATE_NAME, TEMPLATE_KEY, STRUCTURE_ID, CLASS_NAME_ID_DDL_RECORD_SET, CLASS_NAME_ID_PORTLET_DISPLAY_TEMPLATE,
				RESOURCE_PATH, mockClassLoader, mockServiceContext);
		ddmTemplateContext.addContentPlaceholder("[$PLACEHOLDER_ONE$]", "placeholderValue1");
		ddmTemplateContext.addContentPlaceholder("[$PLACEHOLDER_TWO$]", "placeholderValue2");

		when(mockDDMTemplateLocalService.addTemplate(mockServiceContext.getUserId(), //
				mockServiceContext.getScopeGroupId(), //
				ddmTemplateContext.getClassNameId(), //
				ddmTemplateContext.getStructureId(), //
				ddmTemplateContext.getResourceClassNameId(), //
				ddmTemplateContext.getTemplateKey(), //
				ddmTemplateContext.getTemplateName(), //
				ddmTemplateContext.getDescription(), //
				ddmTemplateContext.getType(), //
				ddmTemplateContext.getMode(), //
				ddmTemplateContext.getLanguage(), //
				templateContentWithPlaceholders, //
				ddmTemplateContext.getCacheable(), //
				ddmTemplateContext.getSmallImage(), //
				ddmTemplateContext.getSmallImageUrl(), //
				ddmTemplateContext.getSmallImageFile(), //
				mockServiceContext)).thenThrow(new PortalException());

		ddmInitializerImpl.getOrCreateDDMTemplate(ddmTemplateContext);

	}

	@Test(expected = PortalException.class)
	public void getOrCreateDDMTemplate_WhenTemplateDoesNotExistAndErrorReadingTheTemplateContent_ThenThrowsPortalException() throws PortalException, IOException {
		when(StringUtil.read(mockClassLoader, RESOURCE_PATH)).thenThrow(new IOException());

		DDMTemplateContextImpl ddmTemplateContext = new DDMTemplateContextImpl(TEMPLATE_NAME, TEMPLATE_KEY, STRUCTURE_ID, CLASS_NAME_ID_DDL_RECORD_SET, CLASS_NAME_ID_PORTLET_DISPLAY_TEMPLATE,
				RESOURCE_PATH, mockClassLoader, mockServiceContext);

		ddmInitializerImpl.getOrCreateDDMTemplate(ddmTemplateContext);
	}

	@Test
	public void getOrCreateDDMTemplate_WhenTemplateDoesNotExistAndNoErrors_ThenCreatesAndReturnsNewDDMTemplate() throws PortalException, IOException {
		String templateContent = "templateContent";
		String templateContentWithPlaceholders = "templateContentWithPlaceholders";

		when(StringUtil.read(mockClassLoader, RESOURCE_PATH)).thenReturn(templateContent);
		when(StringUtil.replace(templateContent, new String[] { "[$PLACEHOLDER_ONE$]", "[$PLACEHOLDER_TWO$]" }, new String[] { "placeholderValue1", "placeholderValue2" }))
				.thenReturn(templateContentWithPlaceholders);

		DDMTemplateContextImpl ddmTemplateContext = new DDMTemplateContextImpl(TEMPLATE_NAME, TEMPLATE_KEY, STRUCTURE_ID, CLASS_NAME_ID_DDL_RECORD_SET, CLASS_NAME_ID_PORTLET_DISPLAY_TEMPLATE,
				RESOURCE_PATH, mockClassLoader, mockServiceContext);
		ddmTemplateContext.addContentPlaceholder("[$PLACEHOLDER_ONE$]", "placeholderValue1");
		ddmTemplateContext.addContentPlaceholder("[$PLACEHOLDER_TWO$]", "placeholderValue2");

		when(mockDDMTemplateLocalService.addTemplate(mockServiceContext.getUserId(), //
				mockServiceContext.getScopeGroupId(), //
				ddmTemplateContext.getClassNameId(), //
				ddmTemplateContext.getStructureId(), //
				ddmTemplateContext.getResourceClassNameId(), //
				ddmTemplateContext.getTemplateKey(), //
				ddmTemplateContext.getTemplateName(), //
				ddmTemplateContext.getDescription(), //
				ddmTemplateContext.getType(), //
				ddmTemplateContext.getMode(), //
				ddmTemplateContext.getLanguage(), //
				templateContentWithPlaceholders, //
				ddmTemplateContext.getCacheable(), //
				ddmTemplateContext.getSmallImage(), //
				ddmTemplateContext.getSmallImageUrl(), //
				ddmTemplateContext.getSmallImageFile(), //
				mockServiceContext)).thenReturn(mockDDMTemplate);

		DDMTemplate ddmTemplate = ddmInitializerImpl.getOrCreateDDMTemplate(ddmTemplateContext);

		assertThat(ddmTemplate, sameInstance(mockDDMTemplate));

	}

}
