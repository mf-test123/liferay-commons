package com.placecube.initializer.service.impl;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.service.ServiceContext;

public class ServiceContextInitializerImplTest extends PowerMockito {

	@Mock
	private Group mockGroup;

	@InjectMocks
	private ServiceContextInitializerImpl serviceContextInitializerImpl;

	@Before
	public void activateSetUp() {
		initMocks(this);
	}

	@Test
	public void getServiceContext_WhenNoError_ThenReturnsTheConfiguredServiceContext() {
		long companyId = 11;
		long groupId = 22;
		long userId = 33;
		when(mockGroup.getCompanyId()).thenReturn(companyId);
		when(mockGroup.getGroupId()).thenReturn(groupId);
		when(mockGroup.getCreatorUserId()).thenReturn(userId);

		ServiceContext result = serviceContextInitializerImpl.getServiceContext(mockGroup);

		assertThat(result.getCompanyId(), equalTo(companyId));
		assertThat(result.getScopeGroupId(), equalTo(groupId));
		assertThat(result.getUserId(), equalTo(userId));
	}

}
