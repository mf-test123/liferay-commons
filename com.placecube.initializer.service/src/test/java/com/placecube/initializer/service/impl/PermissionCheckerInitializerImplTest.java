package com.placecube.initializer.service.impl;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;

import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.model.RoleConstants;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.security.auth.PrincipalThreadLocal;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.liferay.portal.kernel.security.permission.PermissionCheckerFactory;
import com.liferay.portal.kernel.security.permission.PermissionThreadLocal;
import com.liferay.portal.kernel.service.RoleLocalService;
import com.liferay.portal.kernel.service.UserLocalService;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ PermissionThreadLocal.class, PrincipalThreadLocal.class })
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class PermissionCheckerInitializerImplTest extends PowerMockito {

	private final static long COMPANY_ID = 1;
	private final static long ROLE_ID = 2;
	private static final long USER_ID = 4;

	@Mock
	private PermissionChecker mockPermissionChecker;

	@Mock
	private PermissionCheckerFactory mockPermissionCheckerFactory;

	@Mock
	private Role mockRole;

	@Mock
	private RoleLocalService mockRoleLocalService;

	@Mock
	private User mockUser;

	@Mock
	private User mockUser2;

	@Mock
	private UserLocalService mockUserLocalService;

	@InjectMocks
	private PermissionCheckerInitializerImpl permissionCheckerInitializerImpl;

	@Before
	public void activateSetup() {
		mockStatic(PermissionThreadLocal.class, PrincipalThreadLocal.class);
	}

	@Test(expected = PortalException.class)
	public void runAsCompanyAdministrator_WhenExceptionRetrievingAdministratorRole_ThenThrowsPortalException() throws Exception {
		when(mockRoleLocalService.getRole(COMPANY_ID, RoleConstants.ADMINISTRATOR)).thenThrow(new PortalException());

		permissionCheckerInitializerImpl.runAsCompanyAdministrator(COMPANY_ID);
	}

	@Test
	public void runAsCompanyAdministrator_WhenNoError_ThenSetsTheFirstAdminUserInThePermissionChecker() throws Exception {
		when(mockRoleLocalService.getRole(COMPANY_ID, RoleConstants.ADMINISTRATOR)).thenReturn(mockRole);
		when(mockRole.getRoleId()).thenReturn(ROLE_ID);
		List<User> users = new LinkedList<>();
		users.add(mockUser);
		users.add(mockUser2);
		when(mockUserLocalService.getRoleUsers(ROLE_ID)).thenReturn(users);
		when(mockUser.getUserId()).thenReturn(USER_ID);
		when(mockPermissionCheckerFactory.create(mockUser)).thenReturn(mockPermissionChecker);

		permissionCheckerInitializerImpl.runAsCompanyAdministrator(COMPANY_ID);

		verifyStatic(PrincipalThreadLocal.class, times(1));
		PrincipalThreadLocal.setName(USER_ID);

		verifyStatic(PermissionThreadLocal.class, times(1));
		PermissionThreadLocal.setPermissionChecker(mockPermissionChecker);
	}

	@Test
	public void runAsUser_WhenExceptionRetrievingUser_ThenNoChangesAreMadeToThePermissionChecker() throws Exception {
		when(mockUserLocalService.getUser(USER_ID)).thenThrow(new PortalException());

		permissionCheckerInitializerImpl.runAsUser(USER_ID);

		verifyStatic(PrincipalThreadLocal.class, never());
		PrincipalThreadLocal.setName(anyLong());

		verifyStatic(PermissionThreadLocal.class, never());
		PermissionThreadLocal.setPermissionChecker(any(PermissionChecker.class));
	}

	@Test
	public void runAsUser_WhenNoError_ThenSetsTheUserInThePermissionChecker() throws Exception {
		when(mockUserLocalService.getUser(USER_ID)).thenReturn(mockUser);
		when(mockUser.getUserId()).thenReturn(USER_ID);
		when(mockPermissionCheckerFactory.create(mockUser)).thenReturn(mockPermissionChecker);

		permissionCheckerInitializerImpl.runAsUser(USER_ID);

		verifyStatic(PrincipalThreadLocal.class, times(1));
		PrincipalThreadLocal.setName(USER_ID);

		verifyStatic(PermissionThreadLocal.class, times(1));
		PermissionThreadLocal.setPermissionChecker(mockPermissionChecker);
	}

	@Test
	@Parameters({ "0", "-1" })
	public void runAsUser_WhenUserIdNotGreaterThanZero_ThenNoChangesAreMadeToThePermissionChecker(long userId) {
		permissionCheckerInitializerImpl.runAsUser(userId);

		verifyStatic(PrincipalThreadLocal.class, never());
		PrincipalThreadLocal.setName(anyLong());

		verifyStatic(PermissionThreadLocal.class, never());
		PermissionThreadLocal.setPermissionChecker(any(PermissionChecker.class));
	}
}
