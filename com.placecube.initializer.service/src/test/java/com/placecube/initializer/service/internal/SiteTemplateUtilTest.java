package com.placecube.initializer.service.internal;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.doThrow;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.verifyStatic;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.LayoutSet;
import com.liferay.portal.kernel.model.LayoutSetPrototype;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.liferay.portal.kernel.service.LayoutSetLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.sites.kernel.util.SitesUtil;
import com.placecube.initializer.model.internal.SiteTemplateConfiguration;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PrepareForTest(SitesUtil.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class SiteTemplateUtilTest {

	private static final long PRIVATE_ID = 1;
	private static final long PUBLIC_ID = 2;

	@Mock
	private Group mockGroup;

	@Mock
	private LayoutLocalService mockLayoutLocalService;

	@Mock
	private LayoutSet mockLayoutSet;

	@Mock
	private LayoutSet mockLayoutSetForPrototype;

	@Mock
	private LayoutSetLocalService mockLayoutSetLocalService;

	@Mock
	private LayoutSetPrototype mockLayoutSetPrototype;

	@Mock
	private ServiceContext mockServiceContext;

	@Mock
	private SiteTemplateConfiguration mockSiteTemplateConfigurationPrivate;

	@Mock
	private SiteTemplateConfiguration mockSiteTemplateConfigurationPublic;

	@InjectMocks
	private SiteTemplateUtil siteTemplateUtil;

	@Before
	public void activateSetup() {
		mockStatic(SitesUtil.class);
	}

	@Test(expected = Exception.class)
	@Parameters({ "true,true", "true,false", "false,true", "false,false" })
	public void applySiteTemplatesToGroup_WhenExceptionUpdatingTheLayouts_ThenThrowsException(boolean publicPropagationEnabled, boolean privatePropagationEnabled) throws Exception {
		when(mockSiteTemplateConfigurationPublic.getLayoutSetPrototypeId()).thenReturn(PUBLIC_ID);
		when(mockSiteTemplateConfigurationPublic.isLayoutSetPrototypeLinkEnabled()).thenReturn(publicPropagationEnabled);
		when(mockSiteTemplateConfigurationPrivate.isLayoutSetPrototypeLinkEnabled()).thenReturn(privatePropagationEnabled);
		when(mockSiteTemplateConfigurationPrivate.getLayoutSetPrototypeId()).thenReturn(PRIVATE_ID);

		mockStatic(SitesUtil.class);
		doThrow(new Exception()).when(SitesUtil.class);
		SitesUtil.updateLayoutSetPrototypesLinks(mockGroup, PUBLIC_ID, PRIVATE_ID, publicPropagationEnabled, privatePropagationEnabled);

		siteTemplateUtil.applySiteTemplatesToGroup(mockGroup, mockSiteTemplateConfigurationPublic, mockSiteTemplateConfigurationPrivate);
	}

	@Test
	@Parameters({ "true,true", "true,false", "false,true", "false,false" })
	public void applySiteTemplatesToGroup_WhenNoError_ThenUpdatesTheGroupLayoutSetPrototypeLinks(boolean publicPropagationEnabled, boolean privatePropagationEnabled) throws Exception {
		when(mockSiteTemplateConfigurationPublic.getLayoutSetPrototypeId()).thenReturn(PUBLIC_ID);
		when(mockSiteTemplateConfigurationPublic.isLayoutSetPrototypeLinkEnabled()).thenReturn(publicPropagationEnabled);
		when(mockSiteTemplateConfigurationPrivate.isLayoutSetPrototypeLinkEnabled()).thenReturn(privatePropagationEnabled);
		when(mockSiteTemplateConfigurationPrivate.getLayoutSetPrototypeId()).thenReturn(PRIVATE_ID);

		siteTemplateUtil.applySiteTemplatesToGroup(mockGroup, mockSiteTemplateConfigurationPublic, mockSiteTemplateConfigurationPrivate);

		verifyStatic(SitesUtil.class, times(1));
		SitesUtil.updateLayoutSetPrototypesLinks(mockGroup, PUBLIC_ID, PRIVATE_ID, publicPropagationEnabled, privatePropagationEnabled);
	}

	@Test
	public void applyThemeToGroup_WhenPrivatePagesDoNotHaveCustomLookAndFeel_ThenDoesNotUpdateThePrivatePagesLookAndFeel() throws PortalException {
		when(mockSiteTemplateConfigurationPrivate.hasCustomLookAndFeel()).thenReturn(false);
		when(mockGroup.getGroupId()).thenReturn(PRIVATE_ID);

		siteTemplateUtil.applyThemeToGroup(mockGroup, mockSiteTemplateConfigurationPublic, mockSiteTemplateConfigurationPrivate);

		verify(mockLayoutSetLocalService, never()).updateLookAndFeel(eq(PRIVATE_ID), eq(true), anyString(), anyString(), anyString());
	}

	@Test
	public void applyThemeToGroup_WhenPrivatePagesHaveCustomLookAndFeel_ThenUpdatesThePrivatePagesLookAndFeel() throws PortalException {
		when(mockSiteTemplateConfigurationPrivate.hasCustomLookAndFeel()).thenReturn(true);
		when(mockSiteTemplateConfigurationPrivate.getThemeId()).thenReturn("themeIdVal");
		when(mockSiteTemplateConfigurationPrivate.getColorSchemeId()).thenReturn("colorSchemeIdVal");
		when(mockSiteTemplateConfigurationPrivate.getCss()).thenReturn("cssVal");
		when(mockGroup.getGroupId()).thenReturn(PRIVATE_ID);

		siteTemplateUtil.applyThemeToGroup(mockGroup, mockSiteTemplateConfigurationPublic, mockSiteTemplateConfigurationPrivate);

		verify(mockLayoutSetLocalService, times(1)).updateLookAndFeel(PRIVATE_ID, true, "themeIdVal", "colorSchemeIdVal", "cssVal");
	}

	@Test(expected = PortalException.class)
	public void applyThemeToGroup_WhenPrivatePagesHaveCustomLookAndFeelAndExceptionUpdatingTheLookAndFeel_ThenThrowsPortalException() throws PortalException {
		when(mockSiteTemplateConfigurationPrivate.hasCustomLookAndFeel()).thenReturn(true);
		when(mockSiteTemplateConfigurationPrivate.getThemeId()).thenReturn("themeIdVal");
		when(mockSiteTemplateConfigurationPrivate.getColorSchemeId()).thenReturn("colorSchemeIdVal");
		when(mockSiteTemplateConfigurationPrivate.getCss()).thenReturn("cssVal");
		when(mockGroup.getGroupId()).thenReturn(PRIVATE_ID);
		when(mockLayoutSetLocalService.updateLookAndFeel(PRIVATE_ID, true, "themeIdVal", "colorSchemeIdVal", "cssVal")).thenThrow(new PortalException());

		siteTemplateUtil.applyThemeToGroup(mockGroup, mockSiteTemplateConfigurationPublic, mockSiteTemplateConfigurationPrivate);

	}

	@Test
	public void applyThemeToGroup_WhenPublicPagesDoNotHaveCustomLookAndFeel_ThenDoesNotUpdateThePublicPagesLookAndFeel() throws PortalException {
		when(mockSiteTemplateConfigurationPublic.hasCustomLookAndFeel()).thenReturn(false);
		when(mockGroup.getGroupId()).thenReturn(PRIVATE_ID);

		siteTemplateUtil.applyThemeToGroup(mockGroup, mockSiteTemplateConfigurationPublic, mockSiteTemplateConfigurationPrivate);

		verify(mockLayoutSetLocalService, never()).updateLookAndFeel(eq(PRIVATE_ID), eq(false), anyString(), anyString(), anyString());
	}

	@Test
	public void applyThemeToGroup_WhenPublicPagesHaveCustomLookAndFeel_ThenUpdatesThePublicPagesLookAndFeel() throws PortalException {
		when(mockSiteTemplateConfigurationPublic.hasCustomLookAndFeel()).thenReturn(true);
		when(mockSiteTemplateConfigurationPublic.getThemeId()).thenReturn("themeIdVal");
		when(mockSiteTemplateConfigurationPublic.getColorSchemeId()).thenReturn("colorSchemeIdVal");
		when(mockSiteTemplateConfigurationPublic.getCss()).thenReturn("cssVal");
		when(mockGroup.getGroupId()).thenReturn(PRIVATE_ID);

		siteTemplateUtil.applyThemeToGroup(mockGroup, mockSiteTemplateConfigurationPublic, mockSiteTemplateConfigurationPrivate);

		verify(mockLayoutSetLocalService, times(1)).updateLookAndFeel(PRIVATE_ID, false, "themeIdVal", "colorSchemeIdVal", "cssVal");
	}

	@Test(expected = PortalException.class)
	public void applyThemeToGroup_WhenPublicPagesHaveCustomLookAndFeelAndExceptionUpdatingTheLookAndFeel_ThenThrowsPortalException() throws PortalException {
		when(mockSiteTemplateConfigurationPublic.hasCustomLookAndFeel()).thenReturn(true);
		when(mockSiteTemplateConfigurationPublic.getThemeId()).thenReturn("themeIdVal");
		when(mockSiteTemplateConfigurationPublic.getColorSchemeId()).thenReturn("colorSchemeIdVal");
		when(mockSiteTemplateConfigurationPublic.getCss()).thenReturn("cssVal");
		when(mockGroup.getGroupId()).thenReturn(PRIVATE_ID);
		when(mockLayoutSetLocalService.updateLookAndFeel(PRIVATE_ID, false, "themeIdVal", "colorSchemeIdVal", "cssVal")).thenThrow(new PortalException());

		siteTemplateUtil.applyThemeToGroup(mockGroup, mockSiteTemplateConfigurationPublic, mockSiteTemplateConfigurationPrivate);

	}

	@Test
	@Parameters({ "true,true", "true,false", "false,true", "false,false" })
	public void getSiteTemplateConfigurationToApply_WhenLayoutSetPrototypeNotSpecified_ThenReturnsTheExpectedConfiguredSiteTemplate(boolean propagationEnabled, boolean layoutSetPropagationEnabled)
			throws PortalException {
		when(mockLayoutSet.getLayoutSetPrototypeId()).thenReturn(PUBLIC_ID);
		when(mockLayoutSet.isLayoutSetPrototypeLinkEnabled()).thenReturn(layoutSetPropagationEnabled);
		when(mockLayoutSet.getThemeId()).thenReturn("themeIdVal");
		when(mockLayoutSet.getCss()).thenReturn("cssVal");
		when(mockLayoutSet.getColorSchemeId()).thenReturn("colorSchemeIdVal");

		SiteTemplateConfiguration result = siteTemplateUtil.getSiteTemplateConfigurationToApply(mockLayoutSet, Optional.empty(), propagationEnabled);

		assertFalse(result.isDeleteLayouts());
		assertThat(result.getLayoutSetPrototypeId(), equalTo(PUBLIC_ID));
		assertThat(result.isLayoutSetPrototypeLinkEnabled(), equalTo(layoutSetPropagationEnabled));
		assertThat(result.getThemeId(), equalTo("themeIdVal"));
		assertThat(result.getCss(), equalTo("cssVal"));
		assertThat(result.getColorSchemeId(), equalTo("colorSchemeIdVal"));
	}

	@Test(expected = PortalException.class)
	@Parameters({ "true", "false" })
	public void getSiteTemplateConfigurationToApply_WhenLayoutSetPrototypeNotSpecifiedAndExceptionRetrievingLayoutSetId_ThenThrowsPortalException(boolean propagationEnabled) throws PortalException {
		when(mockLayoutSet.getLayoutSetPrototypeId()).thenThrow(new PortalException());

		siteTemplateUtil.getSiteTemplateConfigurationToApply(mockLayoutSet, Optional.empty(), propagationEnabled);
	}

	@Test
	@Parameters({ "true", "false" })
	public void getSiteTemplateConfigurationToApply_WhenLayoutSetPrototypeNotSpecifiedAndLayoutSetIsNull_ThenReturnsTheSiteTemplateConfigurationWithTheDefaultValues(boolean propagationEnabled)
			throws PortalException {

		SiteTemplateConfiguration result = siteTemplateUtil.getSiteTemplateConfigurationToApply(null, Optional.empty(), propagationEnabled);

		assertFalse(result.isDeleteLayouts());
		assertThat(result.getLayoutSetPrototypeId(), equalTo(0L));
		assertFalse(result.isLayoutSetPrototypeLinkEnabled());
		assertThat(result.getThemeId(), equalTo(StringPool.BLANK));
		assertThat(result.getCss(), equalTo(StringPool.BLANK));
		assertThat(result.getColorSchemeId(), equalTo(StringPool.BLANK));
	}

	@Test
	@Parameters({ "true", "false" })
	public void getSiteTemplateConfigurationToApply_WhenLayoutSetPrototypeSpecified_ThenReturnsTheExpectedConfiguredSiteTemplate(Boolean propagationEnabled) throws PortalException {
		when(mockLayoutSetPrototype.getLayoutSetPrototypeId()).thenReturn(PUBLIC_ID);
		when(mockLayoutSetPrototype.getLayoutSet()).thenReturn(mockLayoutSetForPrototype);
		when(mockLayoutSetForPrototype.getThemeId()).thenReturn("themeIdVal");
		when(mockLayoutSetForPrototype.getCss()).thenReturn("cssVal");
		when(mockLayoutSetForPrototype.getColorSchemeId()).thenReturn("colorSchemeIdVal");

		SiteTemplateConfiguration result = siteTemplateUtil.getSiteTemplateConfigurationToApply(mockLayoutSet, Optional.of(mockLayoutSetPrototype), propagationEnabled);

		assertTrue(result.isDeleteLayouts());
		assertThat(result.getLayoutSetPrototypeId(), equalTo(PUBLIC_ID));
		assertThat(result.isLayoutSetPrototypeLinkEnabled(), equalTo(propagationEnabled));
		assertThat(result.getThemeId(), equalTo("themeIdVal"));
		assertThat(result.getCss(), equalTo("cssVal"));
		assertThat(result.getColorSchemeId(), equalTo("colorSchemeIdVal"));
	}

	@Test
	@Parameters({ "true", "false" })
	public void removeExistingPages_WhenPrivatePagesShouldBeRemoved_ThenDeletesThePrivatePagesForTheGroup(boolean deletePublic) throws PortalException {
		when(mockSiteTemplateConfigurationPrivate.isDeleteLayouts()).thenReturn(true);
		when(mockSiteTemplateConfigurationPublic.isDeleteLayouts()).thenReturn(deletePublic);
		when(mockGroup.getGroupId()).thenReturn(PRIVATE_ID);

		siteTemplateUtil.removeExistingPages(mockGroup, mockSiteTemplateConfigurationPublic, mockSiteTemplateConfigurationPrivate, mockServiceContext);

		verify(mockLayoutLocalService, times(1)).deleteLayouts(PRIVATE_ID, true, mockServiceContext);
	}

	@Test(expected = PortalException.class)
	@Parameters({ "true", "false" })
	public void removeExistingPages_WhenPrivatePagesShouldBeRemovedAndExceptionRemovingThePages_ThenThrowsPortalException(boolean deletePublic) throws PortalException {
		when(mockSiteTemplateConfigurationPrivate.isDeleteLayouts()).thenReturn(true);
		when(mockSiteTemplateConfigurationPublic.isDeleteLayouts()).thenReturn(deletePublic);
		when(mockGroup.getGroupId()).thenReturn(PRIVATE_ID);
		Mockito.doThrow(new PortalException()).when(mockLayoutLocalService).deleteLayouts(PRIVATE_ID, true, mockServiceContext);

		siteTemplateUtil.removeExistingPages(mockGroup, mockSiteTemplateConfigurationPublic, mockSiteTemplateConfigurationPrivate, mockServiceContext);

	}

	@Test
	@Parameters({ "true", "false" })
	public void removeExistingPages_WhenPrivatePagesShouldNotBeRemoved_ThenDoesNotDeleteTheGroupPrivatePages(boolean deletePublic) throws PortalException {
		when(mockSiteTemplateConfigurationPrivate.isDeleteLayouts()).thenReturn(false);
		when(mockSiteTemplateConfigurationPublic.isDeleteLayouts()).thenReturn(deletePublic);
		when(mockGroup.getGroupId()).thenReturn(PRIVATE_ID);

		siteTemplateUtil.removeExistingPages(mockGroup, mockSiteTemplateConfigurationPublic, mockSiteTemplateConfigurationPrivate, mockServiceContext);

		verify(mockLayoutLocalService, never()).deleteLayouts(PRIVATE_ID, true, mockServiceContext);
	}

	@Test
	@Parameters({ "true", "false" })
	public void removeExistingPages_WhenPublicPagesShouldBeRemoved_ThenDeletesThePublicPagesForTheGroup(boolean deletePrivate) throws PortalException {
		when(mockSiteTemplateConfigurationPublic.isDeleteLayouts()).thenReturn(true);
		when(mockSiteTemplateConfigurationPrivate.isDeleteLayouts()).thenReturn(deletePrivate);
		when(mockGroup.getGroupId()).thenReturn(PRIVATE_ID);

		siteTemplateUtil.removeExistingPages(mockGroup, mockSiteTemplateConfigurationPublic, mockSiteTemplateConfigurationPrivate, mockServiceContext);

		verify(mockLayoutLocalService, times(1)).deleteLayouts(PRIVATE_ID, false, mockServiceContext);
	}

	@Test(expected = PortalException.class)
	@Parameters({ "true", "false" })
	public void removeExistingPages_WhenPublicPagesShouldBeRemovedAndExceptionRemovingThePages_ThenThrowsPortalException(boolean deletePrivate) throws PortalException {
		when(mockSiteTemplateConfigurationPublic.isDeleteLayouts()).thenReturn(true);
		when(mockSiteTemplateConfigurationPrivate.isDeleteLayouts()).thenReturn(deletePrivate);
		when(mockGroup.getGroupId()).thenReturn(PRIVATE_ID);
		Mockito.doThrow(new PortalException()).when(mockLayoutLocalService).deleteLayouts(PRIVATE_ID, false, mockServiceContext);

		siteTemplateUtil.removeExistingPages(mockGroup, mockSiteTemplateConfigurationPublic, mockSiteTemplateConfigurationPrivate, mockServiceContext);

	}

	@Test
	@Parameters({ "true", "false" })
	public void removeExistingPages_WhenPublicPagesShouldNotBeRemoved_ThenDoesNotDeleteTheGroupPublicPages(boolean deletePrivate) throws PortalException {
		when(mockSiteTemplateConfigurationPublic.isDeleteLayouts()).thenReturn(false);
		when(mockSiteTemplateConfigurationPrivate.isDeleteLayouts()).thenReturn(deletePrivate);
		when(mockGroup.getGroupId()).thenReturn(PRIVATE_ID);

		siteTemplateUtil.removeExistingPages(mockGroup, mockSiteTemplateConfigurationPublic, mockSiteTemplateConfigurationPrivate, mockServiceContext);

		verify(mockLayoutLocalService, never()).deleteLayouts(PRIVATE_ID, false, mockServiceContext);
	}

}
