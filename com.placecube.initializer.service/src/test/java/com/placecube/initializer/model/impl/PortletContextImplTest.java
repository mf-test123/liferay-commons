package com.placecube.initializer.model.impl;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.liferay.petra.string.StringPool;

public class PortletContextImplTest {

	@Test
	public void addMultivaluedPreference_WhenNoError_ThenAddsTheMultivaluedPreference() {
		String key = "prefKey";
		String[] values = new String[] { "prefValue", "anotherPref" };

		PortletContextImpl result = new PortletContextImpl("portletId", "placeholder", new HashMap<>(), "customInstanceId");
		result.addMultivaluedPreference(key, values);

		assertThat(result.getPortletMultivaluedPreferences().get(key), equalTo(values));
	}

	@Test
	public void addPreference_WhenNoError_ThenAddsThePreference() {
		String key = "prefKey";
		String value = "prefValue";
		Map<String, String> portletPreferences = new HashMap<>();
		PortletContextImpl result = new PortletContextImpl("portletId", "placeholder", portletPreferences, "customInstanceId");
		result.addPreference(key, value);

		assertThat(result.getPortletPreferences().get(key), equalTo(value));
	}

	@Test
	public void newInstance_WhenCustomInstanceIdNull_ThenConfiguresEmptyStringAsTheValue() {
		PortletContextImpl result = new PortletContextImpl("portletId", "placeholder", new HashMap<>(), null);

		assertThat(result.getCustomInstanceId(), equalTo(StringPool.BLANK));
	}

	@Test
	public void newInstance_WhenCustomInstanceIdSet_ThenConfiguresTheValue() {
		PortletContextImpl result = new PortletContextImpl("portletId", "placeholder", new HashMap<>(), "customInstanceId");

		assertThat(result.getCustomInstanceId(), equalTo("customInstanceId"));
	}
}
