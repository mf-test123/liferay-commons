package com.placecube.initializer.model.internal;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class SiteTemplateConfigurationTest {

	@Test
	@Parameters({ "themeIdVal,cssVal,colorSchemeIdVal", "themeIdVal,cssVal,", "themeIdVal,,colorSchemeIdVal", ",cssVal,colorSchemeIdVal", "themeIdVal,,", ",cssVal,", ",,colorSchemeIdVal" })
	public void hasCustomLookAndFeel_WhenAtLeastOneValueBetweenThemeIdOrCssOrColorSchemeIdIsSpecified_ThenReturnsTrue(String themeId, String css, String colorSchemeId) {
		SiteTemplateConfiguration siteTemplateConfiguration = new SiteTemplateConfiguration();
		siteTemplateConfiguration.setThemeId(themeId);
		siteTemplateConfiguration.setCss(css);
		siteTemplateConfiguration.setColorSchemeId(colorSchemeId);

		boolean result = siteTemplateConfiguration.hasCustomLookAndFeel();

		assertTrue(result);
	}

	@Test
	public void hasCustomLookAndFeel_WhenThemeIdAndCssAndColorSchemeIdAreNotSpecified_ThenReturnsFalse() {
		SiteTemplateConfiguration siteTemplateConfiguration = new SiteTemplateConfiguration();

		boolean result = siteTemplateConfiguration.hasCustomLookAndFeel();

		assertFalse(result);
	}

}
