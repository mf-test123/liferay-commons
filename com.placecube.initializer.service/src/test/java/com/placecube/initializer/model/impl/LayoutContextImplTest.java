package com.placecube.initializer.model.impl;

import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import com.liferay.petra.string.StringPool;

public class LayoutContextImplTest {

	@Test
	public void newInstance_ThenReturnsItemWithEmptyFragments() {
		LayoutContextImpl result = new LayoutContextImpl("nameVal");

		assertThat(result.getFragmentContexts(), empty());
	}

	@Test
	public void newInstance_ThenReturnsItemWithEmptyColumnPortlets() {
		LayoutContextImpl result = new LayoutContextImpl("nameVal");

		assertThat(result.getColumnsPortletContexts().entrySet(), empty());
	}

	@Test
	public void newInstance_ThenReturnsItemWithEmptyPortlets() {
		LayoutContextImpl result = new LayoutContextImpl("nameVal");

		assertThat(result.getPortletContexts(), empty());
	}

	@Test
	public void newInstance_ThenReturnsItemWithEmptyTypeSettings() {
		LayoutContextImpl result = new LayoutContextImpl("nameVal");

		assertThat(result.getTypeSettings().entrySet(), empty());
	}

	@Test
	public void newInstance_ThenReturnsItemWithName() {
		LayoutContextImpl result = new LayoutContextImpl("nameVal");

		assertThat(result.getName(), equalTo("nameVal"));
	}

	@Test
	public void newInstance_ThenReturnsItemWithHiddenFalse() {
		LayoutContextImpl result = new LayoutContextImpl("nameVal");

		assertThat(result.isHidden(), equalTo(false));
	}

	@Test
	public void getTitle_WhenTitleIsNotSet_ThenReturnsTheName() {
		LayoutContextImpl result = new LayoutContextImpl("nameVal");

		assertThat(result.getTitle(), equalTo("nameVal"));
	}

	@Test
	public void getTitle_WhenTitleIsSetAsNull_ThenReturnsTheName() {
		LayoutContextImpl result = new LayoutContextImpl("nameVal");
		result.setTitle(null);

		assertThat(result.getTitle(), equalTo("nameVal"));
	}

	@Test
	public void getTitle_WhenTitleIsSetAsEmptyString_ThenReturnsTheName() {
		LayoutContextImpl result = new LayoutContextImpl("nameVal");
		result.setTitle(StringPool.THREE_SPACES);

		assertThat(result.getTitle(), equalTo("nameVal"));
	}

	@Test
	public void getTitle_WhenTitleIsSet_ThenReturnsTheTitle() {
		LayoutContextImpl result = new LayoutContextImpl("nameVal");
		result.setTitle("titleVal");

		assertThat(result.getTitle(), equalTo("titleVal"));
	}
}
