package com.placecube.initializer.model.impl;

import com.placecube.initializer.model.FragmentContext;

public class FragmentContextImpl implements FragmentContext {

	private final String id;
	private final String placeholder;
	private final String settings;

	public FragmentContextImpl(String id, String placeholder, String settings) {
		this.id = id;
		this.placeholder = placeholder;
		this.settings = settings;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public String getPlaceholder() {
		return placeholder;
	}

	@Override
	public String getSettings() {
		return settings;
	}

}
