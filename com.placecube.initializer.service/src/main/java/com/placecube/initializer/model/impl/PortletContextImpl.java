package com.placecube.initializer.model.impl;

import java.util.HashMap;
import java.util.Map;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.initializer.model.PortletContext;

public class PortletContextImpl implements PortletContext {

	private final String customInstanceId;
	private final String placeholder;
	private final String portletId;
	private final Map<String, String[]> portletMultivaluedPreferences;
	private final Map<String, String> portletPreferences;

	public PortletContextImpl(String portletId, String placeholder, Map<String, String> portletPreferences, String customInstanceId) {
		this.portletId = portletId;
		this.placeholder = placeholder;
		this.portletPreferences = portletPreferences;
		this.customInstanceId = Validator.isNotNull(customInstanceId) ? customInstanceId : StringPool.BLANK;
		portletMultivaluedPreferences = new HashMap<>();
	}

	@Override
	public void addMultivaluedPreference(String key, String[] values) {
		portletMultivaluedPreferences.put(key, values);
	}

	@Override
	public void addPreference(String key, String value) {
		portletPreferences.put(key, value);
	}

	@Override
	public String getCustomInstanceId() {
		return customInstanceId;
	}

	@Override
	public String getPlaceholder() {
		return placeholder;
	}

	@Override
	public String getPortletId() {
		return portletId;
	}

	@Override
	public Map<String, String[]> getPortletMultivaluedPreferences() {
		return portletMultivaluedPreferences;
	}

	@Override
	public Map<String, String> getPortletPreferences() {
		return portletPreferences;
	}
}
