package com.placecube.initializer.model.impl;

import java.io.InputStream;

import com.liferay.portal.kernel.model.Company;
import com.liferay.ratings.kernel.RatingsType;
import com.placecube.initializer.model.CompanyContext;

public class CompanyContextImpl implements CompanyContext {

	private final Company company;
	private final InputStream companyLogo;
	private final int maxUsers;
	private RatingsType ratingsType;

	public CompanyContextImpl(Company company, int maxUsers, InputStream companyLogo) {
		this.company = company;
		this.maxUsers = maxUsers;
		this.companyLogo = companyLogo;
		ratingsType = null;
	}

	@Override
	public Company getCompany() {
		return company;
	}

	@Override
	public InputStream getCompanyLogo() {
		return companyLogo;
	}

	@Override
	public RatingsType getDefaultRatingsType() {
		return ratingsType;
	}

	@Override
	public int getMaxUsers() {
		return maxUsers;
	}

	@Override
	public void setRatingsType(RatingsType ratingsType) {
		this.ratingsType = ratingsType;
	}

}
