package com.placecube.initializer.model.impl;

import java.io.InputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.liferay.portal.kernel.util.Validator;
import com.placecube.initializer.model.FragmentContext;
import com.placecube.initializer.model.LayoutContext;
import com.placecube.initializer.model.PortletContext;

public class LayoutContextImpl implements LayoutContext {

	private boolean addGuestPermissions;
	private long classNameId;
	private long classPK;
	private Map<Integer, Set<PortletContext>> columnsPortletContexts;
	private Map<String, Serializable> expandoValues;
	private Set<FragmentContext> fragmentContexts;
	private String friendlyURL;
	private boolean isHidden;
	private boolean isPrivate;
	private String layoutTemplateId;
	private String linkToLayoutFriendlyURL;
	private final String name;
	private InputStream pageIcon;
	private String parentLayoutFriendlyURL;
	private Set<PortletContext> portletContexts;
	private Map<String, String[]> rolePermissionsToAdd;
	private Map<String, String[]> rolePermissionsToRemove;
	private String structureContent;
	private String title;
	private Map<String, String> typeSettings;

	public LayoutContextImpl(String name) {
		this.name = name;
		fragmentContexts = new HashSet<>();
		columnsPortletContexts = new HashMap<>();
		expandoValues = new HashMap<>();
		portletContexts = new HashSet<>();
		typeSettings = new HashMap<>();
		isHidden = false;
		rolePermissionsToRemove = new HashMap<>();
		rolePermissionsToAdd = new HashMap<>();
	}

	@Override
	public long getClassNameId() {
		return classNameId;
	}

	@Override
	public long getClassPK() {
		return classPK;
	}

	@Override
	public Map<Integer, Set<PortletContext>> getColumnsPortletContexts() {
		return columnsPortletContexts;
	}

	@Override
	public Map<String, Serializable> getExpandoValues() {
		return expandoValues;
	}

	@Override
	public Set<FragmentContext> getFragmentContexts() {
		return fragmentContexts;
	}

	@Override
	public String getFriendlyURL() {
		return friendlyURL;
	}

	@Override
	public String getLayoutTemplateId() {
		return layoutTemplateId;
	}

	@Override
	public String getLinkToLayoutFriendlyURL() {
		return linkToLayoutFriendlyURL;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public InputStream getPageIcon() {
		return pageIcon;
	}

	@Override
	public String getParentLayoutFriendlyURL() {
		return parentLayoutFriendlyURL;
	}

	@Override
	public Set<PortletContext> getPortletContexts() {
		return portletContexts;
	}

	@Override
	public Map<String, String[]> getRolePermissionsToAdd() {
		return rolePermissionsToAdd;
	}

	@Override
	public Map<String, String[]> getRolePermissionsToRemove() {
		return rolePermissionsToRemove;
	}

	@Override
	public String getStructureContent() {
		return structureContent;
	}

	@Override
	public String getTitle() {
		return Validator.isNotNull(title) ? title : name;
	}

	@Override
	public Map<String, String> getTypeSettings() {
		return typeSettings;
	}

	@Override
	public boolean isAddGuestPermissions() {
		return addGuestPermissions;
	}

	@Override
	public boolean isHidden() {
		return isHidden;
	}

	@Override
	public boolean isPrivate() {
		return isPrivate;
	}

	@Override
	public void setAddGuestPermissions(boolean addGuestPermissions) {
		this.addGuestPermissions = addGuestPermissions;
	}

	@Override
	public void setClassNameId(long classNameId) {
		this.classNameId = classNameId;
	}

	@Override
	public void setClassPK(long classPK) {
		this.classPK = classPK;
	}

	@Override
	public void setColumnPortletContexts(Map<Integer, Set<PortletContext>> columnsPortletContexts) {
		this.columnsPortletContexts = columnsPortletContexts;
	}

	@Override
	public void setExpandoValues(Map<String, Serializable> expandoValues) {
		this.expandoValues = expandoValues;
	}

	@Override
	public void setFragmentContexts(Set<FragmentContext> fragmentContexts) {
		this.fragmentContexts = fragmentContexts;
	}

	public void setFriendlyURL(String friendlyURL) {
		this.friendlyURL = friendlyURL;
	}

	@Override
	public void setHidden(boolean hidden) {
		isHidden = hidden;
	}

	public void setLayoutTemplateId(String layoutTemplateId) {
		this.layoutTemplateId = layoutTemplateId;
	}

	@Override
	public void setLinkToLayoutFriendlyURL(String linkToLayoutFriendlyURL) {
		this.linkToLayoutFriendlyURL = linkToLayoutFriendlyURL;
	}

	@Override
	public void setPageIcon(InputStream pageIcon) {
		this.pageIcon = pageIcon;
	}

	@Override
	public void setParentLayoutFriendlyURL(String parentLayoutFriendlyURL) {
		this.parentLayoutFriendlyURL = parentLayoutFriendlyURL;
	}

	@Override
	public void setPortletContexts(Set<PortletContext> portletContexts) {
		this.portletContexts = portletContexts;
	}

	public void setPrivate(boolean isPrivate) {
		this.isPrivate = isPrivate;
	}

	@Override
	public void setRolePermissionsToAdd(Map<String, String[]> rolePermissionsToAdd) {
		this.rolePermissionsToAdd = rolePermissionsToAdd;
	}

	@Override
	public void setRolePermissionsToRemove(Map<String, String[]> rolePermissionsToRemove) {
		this.rolePermissionsToRemove = rolePermissionsToRemove;
	}

	@Override
	public void setStructureContent(String structureContent) {
		this.structureContent = structureContent;
	}

	@Override
	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public void setTypeSettings(Map<String, String> typeSettings) {
		this.typeSettings = typeSettings;
	}

}
