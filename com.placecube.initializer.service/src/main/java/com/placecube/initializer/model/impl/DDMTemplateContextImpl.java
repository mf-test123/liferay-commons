package com.placecube.initializer.model.impl;

import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import com.liferay.dynamic.data.mapping.model.DDMTemplateConstants;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.template.TemplateConstants;
import com.placecube.initializer.model.DDMTemplateContext;

public class DDMTemplateContextImpl implements DDMTemplateContext {

	private final long classNameId;
	private final Map<String, String> contentPlaceholders;
	private final ClassLoader resourceClassLoader;
	private final long resourceClassNameId;
	private final String resourcePath;
	private final ServiceContext serviceContext;
	private final long structureId;
	private final String templateKey;
	private final String templateName;

	public DDMTemplateContextImpl(String templateName, String templateKey, long classNameId, long resourceClassNameId, long structureId, String resourcePath, ClassLoader resourceClassLoader,
			ServiceContext serviceContext) {
		this.templateName = templateName;
		this.templateKey = templateKey;
		this.classNameId = classNameId;
		this.resourceClassNameId = resourceClassNameId;
		this.structureId = structureId;
		this.resourcePath = resourcePath;
		this.resourceClassLoader = resourceClassLoader;
		this.serviceContext = serviceContext;
		contentPlaceholders = new HashMap<>();
	}

	@Override
	public void addContentPlaceholder(String placeholderKey, String placeholderValue) {
		contentPlaceholders.put(placeholderKey, placeholderValue);
	}

	@Override
	public boolean getCacheable() {
		return true;
	}

	@Override
	public long getClassNameId() {
		return classNameId;
	}

	@Override
	public Map<String, String> getContentPlaceholders() {
		return contentPlaceholders;
	}

	@Override
	public Map<Locale, String> getDescription() {
		return Collections.emptyMap();
	}

	@Override
	public String getLanguage() {
		return TemplateConstants.LANG_TYPE_FTL;
	}

	@Override
	public String getMode() {
		return DDMTemplateConstants.TEMPLATE_MODE_CREATE;
	}

	@Override
	public ClassLoader getResourceClassLoader() {
		return resourceClassLoader;
	}

	@Override
	public long getResourceClassNameId() {
		return resourceClassNameId;
	}

	@Override
	public String getResourcePath() {
		return resourcePath;
	}

	@Override
	public ServiceContext getServiceContext() {
		return serviceContext;
	}

	@Override
	public boolean getSmallImage() {
		return false;
	}

	@Override
	public File getSmallImageFile() {
		return null;
	}

	@Override
	public String getSmallImageUrl() {
		return StringPool.BLANK;
	}

	@Override
	public long getStructureId() {
		return structureId;
	}

	@Override
	public String getTemplateKey() {
		return templateKey;
	}

	@Override
	public Map<Locale, String> getTemplateName() {
		return Collections.singletonMap(serviceContext.getLocale(), templateName);
	}

	@Override
	public String getType() {
		return DDMTemplateConstants.TEMPLATE_TYPE_DISPLAY;
	}

}
