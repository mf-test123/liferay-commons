package com.placecube.initializer.model.internal;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.util.Validator;

public class SiteTemplateConfiguration {

	private String colorSchemeId = StringPool.BLANK;
	private String css = StringPool.BLANK;
	private boolean deleteLayouts = false;
	private long layoutSetPrototypeId = 0;
	private boolean layoutSetPrototypeLinkEnabled = false;
	private String themeId = StringPool.BLANK;

	public String getColorSchemeId() {
		return colorSchemeId;
	}

	public String getCss() {
		return css;
	}

	public long getLayoutSetPrototypeId() {
		return layoutSetPrototypeId;
	}

	public String getThemeId() {
		return themeId;
	}

	public boolean hasCustomLookAndFeel() {
		return Validator.isNotNull(themeId) || Validator.isNotNull(css) || Validator.isNotNull(colorSchemeId);
	}

	public boolean isDeleteLayouts() {
		return deleteLayouts;
	}

	public boolean isLayoutSetPrototypeLinkEnabled() {
		return layoutSetPrototypeLinkEnabled;
	}

	public void setColorSchemeId(String colorSchemeId) {
		this.colorSchemeId = colorSchemeId;
	}

	public void setCss(String css) {
		this.css = css;
	}

	public void setDeleteLayouts(boolean deleteLayouts) {
		this.deleteLayouts = deleteLayouts;
	}

	public void setLayoutSetPrototypeId(long layoutSetPrototypeId) {
		this.layoutSetPrototypeId = layoutSetPrototypeId;
	}

	public void setLayoutSetPrototypeLinkEnabled(boolean layoutSetPrototypeLinkEnabled) {
		this.layoutSetPrototypeLinkEnabled = layoutSetPrototypeLinkEnabled;
	}

	public void setThemeId(String themeId) {
		this.themeId = themeId;
	}

}
