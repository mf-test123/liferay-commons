package com.placecube.initializer.service.internal;

import java.util.Map.Entry;

import javax.portlet.PortletPreferences;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.fragment.processor.FragmentEntryProcessorRegistry;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.Portlet;
import com.liferay.portal.kernel.portlet.PortletIdCodec;
import com.liferay.portal.kernel.portlet.PortletPreferencesFactoryUtil;
import com.liferay.portal.kernel.service.PortletLocalService;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.initializer.model.PortletContext;

@Component(immediate = true, service = InitializerPrefsUtil.class)
public class InitializerPrefsUtil {

	@Reference
	private FragmentEntryProcessorRegistry fragmentEntryProcessorRegistry;

	@Reference
	private PortletLocalService portletLocalService;

	public String getFragmentSettingsForPortlet(String portletId, String instanceId) {
		JSONObject editableValueJSONObject = fragmentEntryProcessorRegistry.getDefaultEditableValuesJSONObject(StringPool.BLANK, StringPool.BLANK);
		editableValueJSONObject.put("instanceId", instanceId);
		editableValueJSONObject.put("portletId", portletId);
		return editableValueJSONObject.toJSONString();
	}

	public String getPortletIdWithInstanceIdDetails(String portletId, String instanceId) {
		return Validator.isNotNull(instanceId) && !portletId.contains("_INSTANCE_") ? portletId.concat("_INSTANCE_").concat(instanceId) : portletId;
	}

	public String getPortletInstanceId(PortletContext portletContext) {
		String instanceId = portletContext.getCustomInstanceId();

		if (Validator.isNull(instanceId)) {
			Portlet portletById = portletLocalService.getPortletById(portletContext.getPortletId());
			if (Validator.isNotNull(portletById) && portletById.isInstanceable()) {
				instanceId = PortletIdCodec.generateInstanceId();
			}
		}

		return instanceId;
	}

	public void savePortletPreferences(Layout layout, String portletId, PortletContext portletContext) throws PortalException {
		try {
			PortletPreferences portletPreferences = PortletPreferencesFactoryUtil.getLayoutPortletSetup(layout, portletId);

			for (Entry<String, String> portletPref : portletContext.getPortletPreferences().entrySet()) {
				portletPreferences.setValue(portletPref.getKey(), portletPref.getValue());
			}
			for (Entry<String, String[]> portletPref : portletContext.getPortletMultivaluedPreferences().entrySet()) {
				portletPreferences.setValues(portletPref.getKey(), portletPref.getValue());
			}

			portletPreferences.store();
		} catch (Exception e) {
			throw new PortalException(e);
		}
	}

}
