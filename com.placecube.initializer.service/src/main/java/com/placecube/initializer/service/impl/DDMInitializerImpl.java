package com.placecube.initializer.service.impl;

import java.io.IOException;
import java.util.Map;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.dynamic.data.lists.model.DDLRecordSet;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.dynamic.data.mapping.service.DDMStructureLocalService;
import com.liferay.dynamic.data.mapping.service.DDMTemplateLocalService;
import com.liferay.dynamic.data.mapping.util.DefaultDDMStructureHelper;
import com.liferay.journal.model.JournalArticle;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portlet.display.template.PortletDisplayTemplate;
import com.placecube.initializer.model.DDMTemplateContext;
import com.placecube.initializer.model.impl.DDMTemplateContextImpl;
import com.placecube.initializer.service.DDMInitializer;

@Component(immediate = true, service = DDMInitializer.class)
public class DDMInitializerImpl implements DDMInitializer {

	public static final long EMPTY_STRUCTURE_ID = 0L;

	@Reference
	private DDMStructureLocalService ddmStructureLocalService;

	@Reference
	private DDMTemplateLocalService ddmTemplateLocalService;

	@Reference
	private DefaultDDMStructureHelper defaultDDMStructureHelper;

	@Reference
	private Portal portal;

	@Override
	public DDMTemplateContext getDDMTemplateContextForDDL(String templateName, String templateKey, long structureId, String resourcePath, ClassLoader resourceClassLoader,
			ServiceContext serviceContext) {

		long classNameId = portal.getClassNameId(DDMStructure.class);
		long resourceClassNameId = portal.getClassNameId(DDLRecordSet.class);

		return new DDMTemplateContextImpl(templateName, templateKey, classNameId, resourceClassNameId, structureId, resourcePath, resourceClassLoader, serviceContext);

	}

	@Override
	public DDMTemplateContext getDDMTemplateContextForPortletDisplay(String templateName, String templateKey, Class templateClass, String resourcePath, ClassLoader resourceClassLoader,
			ServiceContext serviceContext) {

		long classNameId = portal.getClassNameId(templateClass);
		long resourceClassNameId = portal.getClassNameId(PortletDisplayTemplate.class);

		return new DDMTemplateContextImpl(templateName, templateKey, classNameId, resourceClassNameId, EMPTY_STRUCTURE_ID, resourcePath, resourceClassLoader, serviceContext);

	}

	@Override
	public DDMTemplateContext getDDMTemplateContextForWebContent(String templateName, String templateKey, long structureId, String resourcePath, ClassLoader resourceClassLoader,
			ServiceContext serviceContext) {

		long classNameId = portal.getClassNameId(DDMStructure.class);
		long resourceClassNameId = portal.getClassNameId(JournalArticle.class);

		return new DDMTemplateContextImpl(templateName, templateKey, classNameId, resourceClassNameId, structureId, resourcePath, resourceClassLoader, serviceContext);

	}

	@Override
	public DDMStructure getOrCreateDDMStructure(String structureKey, String resourcePath, ClassLoader resourceClassLoader, Class structureClass, ServiceContext serviceContext) throws PortalException {

		try {

			long scopeGroupId = serviceContext.getScopeGroupId();
			long classNameId = portal.getClassNameId(structureClass);

			defaultDDMStructureHelper.addDDMStructures(serviceContext.getUserId(), scopeGroupId, classNameId, resourceClassLoader, resourcePath, serviceContext);

			return ddmStructureLocalService.getStructure(scopeGroupId, classNameId, structureKey);

		} catch (Exception e) {
			throw new PortalException(e);
		}

	}

	@Override
	public DDMTemplate getOrCreateDDMTemplate(DDMTemplateContext ddmTemplateContext) throws PortalException {

		ServiceContext serviceContext = ddmTemplateContext.getServiceContext();

		DDMTemplate ddmTemplate = ddmTemplateLocalService.fetchTemplate(serviceContext.getScopeGroupId(), ddmTemplateContext.getClassNameId(), ddmTemplateContext.getTemplateKey());

		if (ddmTemplate == null) {

			try {

				String templateContentWithPlaceholders = getTemplateContentWithPlaceholders(ddmTemplateContext);

				ddmTemplate = ddmTemplateLocalService.addTemplate(serviceContext.getUserId(), //
						serviceContext.getScopeGroupId(), //
						ddmTemplateContext.getClassNameId(), //
						ddmTemplateContext.getStructureId(), //
						ddmTemplateContext.getResourceClassNameId(), //
						ddmTemplateContext.getTemplateKey(), //
						ddmTemplateContext.getTemplateName(), //
						ddmTemplateContext.getDescription(), //
						ddmTemplateContext.getType(), //
						ddmTemplateContext.getMode(), //
						ddmTemplateContext.getLanguage(), //
						templateContentWithPlaceholders, //
						ddmTemplateContext.getCacheable(), //
						ddmTemplateContext.getSmallImage(), //
						ddmTemplateContext.getSmallImageUrl(), //
						ddmTemplateContext.getSmallImageFile(), //
						serviceContext);

			} catch (IOException e) {

				throw new PortalException(e);

			}

		}

		return ddmTemplate;

	}

	private String getTemplateContentWithPlaceholders(DDMTemplateContext ddmTemplateContext) throws IOException {
		String templateContent = StringUtil.read(ddmTemplateContext.getResourceClassLoader(), ddmTemplateContext.getResourcePath());

		Map<String, String> contentPlaceholders = ddmTemplateContext.getContentPlaceholders();
		String[] placeholderKeys = contentPlaceholders.keySet().toArray(new String[contentPlaceholders.size()]);
		String[] placeholderValues = contentPlaceholders.values().toArray(new String[contentPlaceholders.size()]);

		return StringUtil.replace(templateContent, placeholderKeys, placeholderValues);
	}

}
