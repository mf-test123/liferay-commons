package com.placecube.initializer.service.internal;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.site.navigation.menu.item.layout.constants.SiteNavigationMenuItemTypeConstants;
import com.liferay.site.navigation.model.SiteNavigationMenuItem;
import com.liferay.site.navigation.service.SiteNavigationMenuItemLocalService;
import com.placecube.initializer.service.internal.constants.NavigationMenuImportConstants;

@Component(immediate = true, service = NavigationMenuImportUtil.class)
public class NavigationMenuImportUtil {

	@Reference
	private InitializerUtil initializerUtil;

	@Reference
	private LayoutLocalService layoutLocalService;

	@Reference
	private SiteNavigationMenuItemLocalService siteNavigationMenuItemLocalService;

	public SiteNavigationMenuItem importAsSubmenuItem(JSONObject jsonObject, ServiceContext serviceContext, long siteNavigationMenuId, long parentId) throws PortalException {

		String typeSettings = initializerUtil.getTypeSettingsForSubmenu(jsonObject);

		return siteNavigationMenuItemLocalService
				.addSiteNavigationMenuItem(serviceContext.getUserId(), serviceContext.getScopeGroupId(), siteNavigationMenuId, parentId, SiteNavigationMenuItemTypeConstants.NODE, typeSettings,
						serviceContext);
	}

	public SiteNavigationMenuItem importAsURLItem(JSONObject jsonObject, ServiceContext serviceContext, long siteNavigationMenuId, long parentId) throws PortalException {
		String typeSettings = initializerUtil.getTypeSettingsForURL(jsonObject);

		return siteNavigationMenuItemLocalService.addSiteNavigationMenuItem( //
				serviceContext.getUserId(), //
				serviceContext.getScopeGroupId(), //
				siteNavigationMenuId, //
				parentId, //
				SiteNavigationMenuItemTypeConstants.URL, //
				typeSettings, //
				serviceContext);
	}

	public SiteNavigationMenuItem importAsLayoutItem(JSONObject menuItemJSON, ServiceContext serviceContext, long siteNavigationMenuId, long parentId) throws PortalException {
		Layout layout = layoutLocalService.getFriendlyURLLayout(serviceContext.getScopeGroupId(), menuItemJSON.getBoolean(NavigationMenuImportConstants.PRIVATE),
				menuItemJSON.getString(NavigationMenuImportConstants.FRIENDLY_URL));

		String typeSettings = initializerUtil.getLayoutTypeSettingsForMenuItem(layout);

		return siteNavigationMenuItemLocalService.addSiteNavigationMenuItem( //
				serviceContext.getUserId(), //
				serviceContext.getScopeGroupId(), // 
				siteNavigationMenuId, //
				parentId, //
				SiteNavigationMenuItemTypeConstants.LAYOUT, // 
				typeSettings, //
				serviceContext);
	}

	public boolean isLayout(JSONObject jsonObject) {
		return jsonObject.has(NavigationMenuImportConstants.FRIENDLY_URL);
	}

	public boolean isURL(JSONObject jsonObject) {
		return jsonObject.has(NavigationMenuImportConstants.URL);
	}

}
