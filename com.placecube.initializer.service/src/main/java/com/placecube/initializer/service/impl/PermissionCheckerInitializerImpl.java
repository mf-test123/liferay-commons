package com.placecube.initializer.service.impl;

import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.model.RoleConstants;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.security.auth.PrincipalThreadLocal;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.liferay.portal.kernel.security.permission.PermissionCheckerFactory;
import com.liferay.portal.kernel.security.permission.PermissionThreadLocal;
import com.liferay.portal.kernel.service.RoleLocalService;
import com.liferay.portal.kernel.service.UserLocalService;
import com.placecube.initializer.service.PermissionCheckerInitializer;
import com.placecube.initializer.service.internal.PermissionUtil;

@Component(immediate = true, service = PermissionCheckerInitializer.class)
public class PermissionCheckerInitializerImpl implements PermissionCheckerInitializer {

	private static final Log LOG = LogFactoryUtil.getLog(PermissionUtil.class);

	@Reference
	private PermissionCheckerFactory permissionCheckerFactory;

	@Reference
	private RoleLocalService roleLocalService;

	@Reference
	private UserLocalService userLocalService;

	@Override
	public void runAsCompanyAdministrator(long companyId) throws PortalException {
		Role adminRole = roleLocalService.getRole(companyId, RoleConstants.ADMINISTRATOR);
		List<User> adminUsers = userLocalService.getRoleUsers(adminRole.getRoleId());
		runAsUser(adminUsers.get(0));
	}

	@Override
	public void runAsUser(long userId) {
		if (userId > 0) {
			try {
				runAsUser(userLocalService.getUser(userId));
			} catch (Exception e) {
				LOG.warn("Exception switching back to run as current user", e);
			}
		}
	}

	private void runAsUser(User user) {
		PrincipalThreadLocal.setName(user.getUserId());
		PermissionChecker permissionChecker = permissionCheckerFactory.create(user);
		PermissionThreadLocal.setPermissionChecker(permissionChecker);
	}
}
