package com.placecube.initializer.service.impl;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.fragment.model.FragmentCollection;
import com.liferay.fragment.model.FragmentEntry;
import com.liferay.fragment.service.FragmentCollectionLocalService;
import com.liferay.fragment.service.FragmentEntryLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.placecube.initializer.service.FragmentInitializer;
import com.placecube.initializer.service.internal.FragmentUtil;

@Component(immediate = true, service = FragmentInitializer.class)
public class FragmentInitializerImpl implements FragmentInitializer {

	private static final Log LOG = LogFactoryUtil.getLog(FragmentInitializerImpl.class);

	@Reference
	private FragmentUtil fragmentUtil;

	@Reference
	private FragmentEntryLocalService fragmentEntryLocalService;

	@Reference
	private FragmentCollectionLocalService fragmentCollectionLocalService;

	@Override
	public FragmentEntry getOrCreateFragment(FragmentCollection fragmentCollection, ClassLoader classLoader, String basePath, ServiceContext serviceContext) throws PortalException {
		JSONObject fragment = fragmentUtil.getFragmentJSONObject(classLoader, basePath);

		FragmentEntry fragmentEntry = fragmentEntryLocalService.fetchFragmentEntry(serviceContext.getScopeGroupId(), fragment.getString("fragmentEntryKey"));

		return Validator.isNotNull(fragmentEntry) ? fragmentEntry : addFragment(fragmentCollection, serviceContext, fragment, classLoader, basePath);
	}

	@Override
	public FragmentCollection getOrCreateFragmentCollection(String fragmentCollectionKey, String fragmentCollectionLabel, ServiceContext serviceContext) throws PortalException {
		FragmentCollection fragmentCollection = fragmentCollectionLocalService.fetchFragmentCollection(serviceContext.getScopeGroupId(), fragmentCollectionKey);
		return Validator.isNotNull(fragmentCollection) ? fragmentCollection
				: fragmentCollectionLocalService.addFragmentCollection(serviceContext.getUserId(), serviceContext.getScopeGroupId(), fragmentCollectionKey, fragmentCollectionLabel, StringPool.BLANK,
						serviceContext);
	}

	private FragmentEntry addFragment(FragmentCollection fragmentCollection, ServiceContext serviceContext, JSONObject fragment, ClassLoader classLoader, String basePath) throws PortalException {

		String name = fragment.getString("name");

		LOG.info("Creating fragment with name: " + name);

		String css = fragmentUtil.getOptionalContent(classLoader, basePath, fragment, "cssPath");
		String html = fragmentUtil.getOptionalContent(classLoader, basePath, fragment, "htmlPath");
		String js = fragmentUtil.getOptionalContent(classLoader, basePath, fragment, "jsPath");
		String configuration = fragmentUtil.getOptionalContent(classLoader, basePath, fragment, "configurationPath");
		long previewFileEntryId = 0;
		int type = "section".equalsIgnoreCase(fragment.getString("type")) ? 0 : 1;

		return fragmentEntryLocalService.addFragmentEntry(serviceContext.getUserId(), serviceContext.getScopeGroupId(), fragmentCollection.getFragmentCollectionId(),
				fragment.getString("fragmentEntryKey"), name, css, html, js, configuration, previewFileEntryId, type, WorkflowConstants.STATUS_APPROVED, serviceContext);
	}

}
