package com.placecube.initializer.service.impl;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.layout.page.template.model.LayoutPageTemplateCollection;
import com.liferay.layout.page.template.model.LayoutPageTemplateEntry;
import com.liferay.layout.page.template.service.LayoutPageTemplateEntryLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.LayoutPrototype;
import com.liferay.portal.kernel.service.LayoutPrototypeService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.initializer.model.LayoutContext;
import com.placecube.initializer.service.LayoutPageTemplateEntryInitializer;
import com.placecube.initializer.service.internal.PageTemplateUtil;

@Component(immediate = true, service = LayoutPageTemplateEntryInitializer.class)
public class LayoutPageTemplateEntryInitializerImpl implements LayoutPageTemplateEntryInitializer {

	@Reference
	private LayoutPrototypeService layoutPrototypeService;

	@Reference
	private LayoutPageTemplateEntryLocalService layoutPageTemplateEntryLocalService;

	@Reference
	private PageTemplateUtil pageTemplateUtil;

	@Override
	public LayoutPageTemplateEntry createContentPageTemplate(LayoutPageTemplateCollection layoutPageTemplateCollection, LayoutContext layoutContext, ServiceContext serviceContext)
			throws PortalException {

		LayoutPageTemplateEntry layoutPageTemplateEntry = pageTemplateUtil.createBasicPageTemplate(layoutPageTemplateCollection, layoutContext.getName(), serviceContext);

		pageTemplateUtil.configureContentPage(layoutContext.getFragmentContexts(), layoutContext.getPortletContexts(), layoutContext.getStructureContent(), serviceContext,
				layoutPageTemplateEntry.getPlid());

		return layoutPageTemplateEntry;
	}

	@Override
	public LayoutPageTemplateEntry createDisplayPageTemplate(LayoutContext layoutContext, long classNameId, long classTypeId, ServiceContext serviceContext) throws PortalException {

		LayoutPageTemplateEntry layoutPageTemplateEntry = pageTemplateUtil.createDisplayPageTemplate(layoutContext.getName(), classNameId, classTypeId, serviceContext);

		pageTemplateUtil.configureContentPage(layoutContext.getFragmentContexts(), layoutContext.getPortletContexts(), layoutContext.getStructureContent(), serviceContext,
				layoutPageTemplateEntry.getPlid());

		return layoutPageTemplateEntry;
	}

	@Override
	public LayoutPageTemplateEntry createWidgetPageTemplate(LayoutPageTemplateCollection layoutPageTemplateCollection, LayoutContext layoutContext, ServiceContext serviceContext)
			throws PortalException {

		LayoutPrototype layoutPrototype = layoutPrototypeService.addLayoutPrototype(getNameMap(layoutContext.getName(), serviceContext), new HashMap<>(), true, serviceContext);

		LayoutPageTemplateEntry layoutPageTemplateEntry = layoutPageTemplateEntryLocalService.fetchFirstLayoutPageTemplateEntry(layoutPrototype.getLayoutPrototypeId());

		if (layoutPageTemplateEntry == null) {
			throw new PortalException("LayoutPageTemplateEntry with id " + layoutPrototype.getLayoutPrototypeId() + " was not found");
		}

		layoutPageTemplateEntry.setGroupId(serviceContext.getScopeGroupId());
		layoutPageTemplateEntry.setLayoutPageTemplateCollectionId(layoutPageTemplateCollection.getLayoutPageTemplateCollectionId());
		layoutPageTemplateEntry = layoutPageTemplateEntryLocalService.updateLayoutPageTemplateEntry(layoutPageTemplateEntry);

		pageTemplateUtil.configureWidgetPage(layoutContext.getLayoutTemplateId(), layoutContext.getColumnsPortletContexts(), serviceContext, layoutPageTemplateEntry.getPlid());

		return layoutPageTemplateEntry;
	}

	private Map<Locale, String> getNameMap(String name, ServiceContext serviceContext) {
		Map<Locale, String> nameMap = new HashMap<>();
		nameMap.put(serviceContext.getLocale(), name);
		return nameMap;
	}
}
