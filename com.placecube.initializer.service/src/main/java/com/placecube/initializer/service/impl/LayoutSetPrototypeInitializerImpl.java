package com.placecube.initializer.service.impl;

import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.LayoutSetPrototype;
import com.liferay.portal.kernel.security.auth.PrincipalThreadLocal;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.initializer.model.internal.SiteTemplateConfiguration;
import com.placecube.initializer.service.LayoutSetPrototypeInitializer;
import com.placecube.initializer.service.PermissionCheckerInitializer;
import com.placecube.initializer.service.internal.SiteTemplateUtil;

@Component(service = LayoutSetPrototypeInitializer.class)
public class LayoutSetPrototypeInitializerImpl implements LayoutSetPrototypeInitializer {

	@Reference
	private PermissionCheckerInitializer permissionCheckerInitializer;

	@Reference
	private SiteTemplateUtil siteTemplateUtil;

	@Override
	public void applySiteTemplateToGroup(Group group, Optional<LayoutSetPrototype> publicPagesLayoutSetPrototype, boolean publicLayoutSetPrototypeLinkEnabled,
			Optional<LayoutSetPrototype> privatePagesLayoutSetPrototype, boolean privateLayoutSetPrototypeLinkEnabled, ServiceContext serviceContext) throws PortalException {

		long currentPermissionUserId = 0;

		try {
			if (publicPagesLayoutSetPrototype.isPresent() || privatePagesLayoutSetPrototype.isPresent()) {

				currentPermissionUserId = PrincipalThreadLocal.getUserId();

				permissionCheckerInitializer.runAsCompanyAdministrator(group.getCompanyId());

				SiteTemplateConfiguration publicPagesConfig = siteTemplateUtil.getSiteTemplateConfigurationToApply(group.getPublicLayoutSet(), publicPagesLayoutSetPrototype,
						publicLayoutSetPrototypeLinkEnabled);

				SiteTemplateConfiguration privatePagesConfig = siteTemplateUtil.getSiteTemplateConfigurationToApply(group.getPrivateLayoutSet(), privatePagesLayoutSetPrototype,
						privateLayoutSetPrototypeLinkEnabled);

				siteTemplateUtil.removeExistingPages(group, publicPagesConfig, privatePagesConfig, serviceContext);
				siteTemplateUtil.applySiteTemplatesToGroup(group, publicPagesConfig, privatePagesConfig);
				siteTemplateUtil.applyThemeToGroup(group, publicPagesConfig, privatePagesConfig);
			}
		} catch (Exception e) {
			throw new PortalException(e);

		} finally {
			permissionCheckerInitializer.runAsUser(currentPermissionUserId);
		}
	}

}
