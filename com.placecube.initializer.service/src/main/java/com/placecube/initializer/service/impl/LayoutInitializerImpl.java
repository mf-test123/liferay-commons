package com.placecube.initializer.service.impl;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.layout.page.template.constants.LayoutPageTemplateEntryTypeConstants;
import com.liferay.layout.page.template.model.LayoutPageTemplateEntry;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.LayoutConstants;
import com.liferay.portal.kernel.model.LayoutTypePortlet;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.initializer.model.LayoutContext;
import com.placecube.initializer.model.PortletContext;
import com.placecube.initializer.service.LayoutInitializer;
import com.placecube.initializer.service.internal.InitializerUtil;
import com.placecube.initializer.service.internal.PageTemplateUtil;

@Component(immediate = true, service = LayoutInitializer.class)
public class LayoutInitializerImpl implements LayoutInitializer {

	@Reference
	private InitializerUtil initializerUtil;

	@Reference
	private LayoutLocalService layoutLocalService;

	@Reference
	private PageTemplateUtil pageTemplateUtil;

	@Override
	public Layout getOrCreateContentLayout(LayoutContext layoutContext, ServiceContext serviceContext) throws PortalException {
		Layout layout = layoutLocalService.fetchLayoutByFriendlyURL(serviceContext.getScopeGroupId(), layoutContext.isPrivate(), layoutContext.getFriendlyURL());
		if (Validator.isNotNull(layout)) {
			return layout;
		}
		layout = initializerUtil.createLayout(layoutContext, LayoutConstants.TYPE_CONTENT, serviceContext);
		return pageTemplateUtil.configureContentPage(layoutContext.getFragmentContexts(), layoutContext.getPortletContexts(), layoutContext.getStructureContent(), serviceContext, layout.getPlid());
	}

	@Override
	public Layout getOrCreateLinkToPageLayout(LayoutContext layoutContext, ServiceContext serviceContext) throws PortalException {
		Layout linkToPageLayout = layoutLocalService.getFriendlyURLLayout(serviceContext.getScopeGroupId(), layoutContext.isPrivate(), layoutContext.getLinkToLayoutFriendlyURL());

		Layout layout = layoutLocalService.fetchLayoutByFriendlyURL(serviceContext.getScopeGroupId(), layoutContext.isPrivate(), layoutContext.getFriendlyURL());
		if (Validator.isNotNull(layout)) {
			return layout;
		}

		Map<String, String> typeSettings = layoutContext.getTypeSettings();
		typeSettings.put("linkToLayoutId", String.valueOf(linkToPageLayout.getLayoutId()));

		return initializerUtil.createLayout(layoutContext, LayoutConstants.TYPE_LINK_TO_LAYOUT, serviceContext);
	}

	@Override
	public Layout getOrCreatePortletLayout(LayoutContext layoutContext, ServiceContext serviceContext) throws PortalException {
		Layout layout = layoutLocalService.fetchLayoutByFriendlyURL(serviceContext.getScopeGroupId(), layoutContext.isPrivate(), layoutContext.getFriendlyURL());
		return Validator.isNotNull(layout) ? layout : addLayout(layoutContext, serviceContext);
	}

	@Override
	public Layout getOrCreatePageTemplateLayout(LayoutContext layoutContext, LayoutPageTemplateEntry layoutPageTemplateEntry, ServiceContext serviceContext) throws PortalException {
		Layout layout = layoutLocalService.fetchLayoutByFriendlyURL(serviceContext.getScopeGroupId(), layoutContext.isPrivate(), layoutContext.getFriendlyURL());
		if (Validator.isNotNull(layout)) {
			return layout;
		} else {
			int layoutType = layoutPageTemplateEntry.getType();

			switch (layoutType) {
			case LayoutPageTemplateEntryTypeConstants.TYPE_WIDGET_PAGE:
				layout = initializerUtil.createLayout(layoutContext, LayoutConstants.TYPE_PORTLET, serviceContext);
				break;
			case LayoutPageTemplateEntryTypeConstants.TYPE_BASIC:
				layout = initializerUtil.createLayout(layoutContext, LayoutConstants.TYPE_CONTENT, serviceContext);
				break;
			default:
				throw new PortalException("It is possible to create a layout only from widget or content page templates");
			}

			Layout draftLayout = layoutLocalService.getLayout(layoutPageTemplateEntry.getPlid());
			return initializerUtil.copyLayout(layout, draftLayout);
		}
	}

	private Layout addLayout(LayoutContext layoutContext, ServiceContext serviceContext) throws PortalException {
		Layout layout = initializerUtil.createLayout(layoutContext, LayoutConstants.TYPE_PORTLET, serviceContext);

		LayoutTypePortlet layoutTypePortlet = (LayoutTypePortlet) layout.getLayoutType();
		layoutTypePortlet.setLayoutTemplateId(serviceContext.getUserId(), layoutContext.getLayoutTemplateId(), false);

		for (Entry<Integer, Set<PortletContext>> columnPortlets : layoutContext.getColumnsPortletContexts().entrySet()) {
			initializerUtil.configureLayoutColumn(layout, layoutTypePortlet, columnPortlets.getKey(), columnPortlets.getValue());
		}
		return layoutLocalService.updateLayout(layout.getGroupId(), layout.isPrivateLayout(), layout.getLayoutId(), layout.getTypeSettings());
	}

}
