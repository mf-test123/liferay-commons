package com.placecube.initializer.service.impl;

import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.list.constants.AssetListEntryTypeConstants;
import com.liferay.asset.list.model.AssetListEntry;
import com.liferay.asset.list.service.AssetListEntryLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.initializer.service.AssetListEntryInitializer;
import com.placecube.initializer.service.internal.InitializerUtil;

@Component(immediate = true, service = AssetListEntryInitializer.class)
public class AssetListEntryInitializerImpl implements AssetListEntryInitializer {

	@Reference
	private AssetListEntryLocalService assetListEntryLocalService;

	@Reference
	private InitializerUtil initializerUtil;

	@Override
	public Optional<AssetListEntry> getContentSet(Long groupId, String contentSetName) {
		return initializerUtil.getContentSet(groupId, contentSetName);
	}

	@Override
	public AssetListEntry getOrCreateContentSet(String contentSetName, Boolean selectionTypeManual, ServiceContext serviceContext) throws PortalException {
		Optional<AssetListEntry> contentSet = initializerUtil.getContentSet(serviceContext.getScopeGroupId(), contentSetName);
		if (contentSet.isPresent()) {
			return contentSet.get();
		} else {
			int assetSelectionType = selectionTypeManual ? AssetListEntryTypeConstants.TYPE_MANUAL : AssetListEntryTypeConstants.TYPE_DYNAMIC;
			return assetListEntryLocalService.addAssetListEntry(serviceContext.getUserId(), serviceContext.getScopeGroupId(), contentSetName, assetSelectionType, serviceContext);
		}
	}

}
