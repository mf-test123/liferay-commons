package com.placecube.initializer.service.impl;

import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.layout.page.template.model.LayoutPageTemplateCollection;
import com.liferay.layout.page.template.service.LayoutPageTemplateCollectionLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.initializer.service.LayoutPageTemplateCollectionInitializer;

@Component(immediate = true, service = LayoutPageTemplateCollectionInitializer.class)
public class LayoutPageTemplateCollectionInitializerImpl implements LayoutPageTemplateCollectionInitializer {

	@Reference
	private LayoutPageTemplateCollectionLocalService layoutPageTemplateCollectionLocalService;

	@Override
	public LayoutPageTemplateCollection getOrCreateLayoutPageTemplateCollection(String collectionName, ServiceContext serviceContext) throws PortalException {
		Optional<LayoutPageTemplateCollection> existingCollection = layoutPageTemplateCollectionLocalService
				.getLayoutPageTemplateCollections(serviceContext.getScopeGroupId(), QueryUtil.ALL_POS, QueryUtil.ALL_POS).stream().filter(entry -> entry.getName().equals(collectionName)).findFirst();

		return existingCollection.isPresent() ? existingCollection.get()
				: layoutPageTemplateCollectionLocalService.addLayoutPageTemplateCollection(serviceContext.getUserId(), serviceContext.getScopeGroupId(), collectionName, StringPool.BLANK,
						serviceContext);
	}

}
