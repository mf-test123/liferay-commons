package com.placecube.initializer.service.impl;

import java.io.InputStream;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.blogs.model.BlogsEntry;
import com.liferay.document.library.kernel.model.DLFileEntry;
import com.liferay.dynamic.data.lists.model.DDLRecord;
import com.liferay.journal.model.JournalArticle;
import com.liferay.knowledge.base.model.KBArticle;
import com.liferay.message.boards.model.MBDiscussion;
import com.liferay.message.boards.model.MBMessage;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.kernel.util.UnicodeProperties;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.ratings.kernel.RatingsType;
import com.liferay.wiki.model.WikiPage;
import com.placecube.initializer.model.CompanyContext;
import com.placecube.initializer.model.impl.CompanyContextImpl;
import com.placecube.initializer.service.CompanyInitializerService;

@Component(immediate = true, service = CompanyInitializerService.class)
public class CompanyInitializerServiceImpl implements CompanyInitializerService {

	@Reference
	private CompanyLocalService companyLocalService;

	@Override
	public Company configureCompany(CompanyContext companyContext) throws PortalException {
		Company company = companyContext.getCompany();
		long companyId = company.getCompanyId();

		company = companyLocalService.updateLogo(companyId, companyContext.getCompanyLogo());
		company = setMaxUsers(company, companyContext);

		RatingsType defaultRatingsType = companyContext.getDefaultRatingsType();
		if (Validator.isNotNull(defaultRatingsType)) {
			configureDefaultRatingTypes(companyId, defaultRatingsType);
		}
		return company;
	}

	@Override
	public CompanyContext createCompanyContext(Company company, int maxUsers, ClassLoader resourceClassLoader, String logoResourcePath) {
		InputStream companyLogo = resourceClassLoader.getResourceAsStream(logoResourcePath);
		return new CompanyContextImpl(company, maxUsers, companyLogo);
	}

	private void configureDefaultRatingTypes(long companyId, RatingsType ratingsType) throws PortalException {
		String ratingTypeValue = ratingsType.getValue();
		UnicodeProperties properties = new UnicodeProperties(true);

		properties.put(getRatingTypePropertyKey(BlogsEntry.class.getName()), ratingTypeValue);
		properties.put(getRatingTypePropertyKey(DLFileEntry.class.getName()), ratingTypeValue);
		properties.put(getRatingTypePropertyKey(DDLRecord.class.getName()), ratingTypeValue);
		properties.put(getRatingTypePropertyKey(JournalArticle.class.getName()), ratingTypeValue);
		properties.put(getRatingTypePropertyKey(KBArticle.class.getName()), ratingTypeValue);
		properties.put(getRatingTypePropertyKey(MBDiscussion.class.getName()), ratingTypeValue);
		properties.put(getRatingTypePropertyKey(MBMessage.class.getName()), ratingTypeValue);
		properties.put(getRatingTypePropertyKey(WikiPage.class.getName()), ratingTypeValue);

		companyLocalService.updatePreferences(companyId, properties);
	}

	private String getRatingTypePropertyKey(String className) {
		return className + "_RatingsType";
	}

	private Company setMaxUsers(Company company, CompanyContext companyContext) {
		company.setMaxUsers(companyContext.getMaxUsers());
		return companyLocalService.updateCompany(company);
	}
}