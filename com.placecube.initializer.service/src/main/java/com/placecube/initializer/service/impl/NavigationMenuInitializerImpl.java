package com.placecube.initializer.service.impl;

import java.util.List;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.site.navigation.model.SiteNavigationMenu;
import com.liferay.site.navigation.model.SiteNavigationMenuItem;
import com.liferay.site.navigation.service.SiteNavigationMenuItemLocalService;
import com.liferay.site.navigation.service.SiteNavigationMenuLocalService;
import com.placecube.initializer.service.NavigationMenuInitializer;
import com.placecube.initializer.service.internal.NavigationMenuImportUtil;
import com.placecube.initializer.service.internal.constants.NavigationMenuImportConstants;

@Component(immediate = true, service = NavigationMenuInitializer.class)
public class NavigationMenuInitializerImpl implements NavigationMenuInitializer {

	private static final Log LOG = LogFactoryUtil.getLog(NavigationMenuInitializerImpl.class);

	@Reference
	private JSONFactory jsonFactory;

	@Reference
	private NavigationMenuImportUtil navigationMenuImportUtil;

	@Reference
	private SiteNavigationMenuItemLocalService siteNavigationMenuItemLocalService;

	@Reference
	private SiteNavigationMenuLocalService siteNavigationMenuLocalService;

	@Override
	public void configureSiteNavigationMenuEntries(ServiceContext serviceContext, ClassLoader classLoader, String resourcePath) throws PortalException {
		try {
			JSONObject jsonObject = jsonFactory.createJSONObject(StringUtil.read(classLoader, resourcePath));

			Optional<SiteNavigationMenu> navigationMenuByName = getSiteNavigationMenuByName(serviceContext, jsonObject.getString(NavigationMenuImportConstants.NAVIGATION_MENU_NAME));
			if (navigationMenuByName.isPresent()) {
				long siteNavigationMenuId = navigationMenuByName.get().getSiteNavigationMenuId();

				int siteNavigationMenuItemsCount = siteNavigationMenuItemLocalService.getSiteNavigationMenuItemsCount(siteNavigationMenuId);
				if (siteNavigationMenuItemsCount == 0) {

					addNavigationItems(jsonObject, siteNavigationMenuId, 0, serviceContext);

				} else {
					LOG.info("Not adding site navigation menu entries to menu: " + siteNavigationMenuId + " as pages already configured");
				}
			}
		} catch (Exception e) {
			throw new PortalException(e);
		}
	}

	@Override
	public SiteNavigationMenu getOrCreateSiteNavigationMenu(ServiceContext serviceContext, String name, int type) throws PortalException {
		Optional<SiteNavigationMenu> existingMenu = getSiteNavigationMenuByName(serviceContext, name);

		return existingMenu.isPresent() ?
				existingMenu.get() :
				siteNavigationMenuLocalService.addSiteNavigationMenu(serviceContext.getUserId(), serviceContext.getScopeGroupId(), name, type, serviceContext);
	}

	private Optional<SiteNavigationMenu> getSiteNavigationMenuByName(ServiceContext serviceContext, String name) {
		List<SiteNavigationMenu> siteNavigationMenus = siteNavigationMenuLocalService.getSiteNavigationMenus(serviceContext.getScopeGroupId());

		return siteNavigationMenus.stream().filter(entry -> name.equalsIgnoreCase(entry.getName())).findFirst();
	}

	private void addNavigationItems(JSONObject jsonObject, long siteNavigationMenuId, long parentId, ServiceContext serviceContext) throws PortalException {
		JSONArray layouts = jsonObject.getJSONArray(NavigationMenuImportConstants.LAYOUTS);

		if (Validator.isNotNull(layouts)) {
			for (int i = 0; i < layouts.length(); i++) {

				JSONObject menuItemJSON = layouts.getJSONObject(i);

				SiteNavigationMenuItem siteNavigationMenuItem;

				if (navigationMenuImportUtil.isLayout(menuItemJSON)) {
					siteNavigationMenuItem = navigationMenuImportUtil.importAsLayoutItem(menuItemJSON, serviceContext, siteNavigationMenuId, parentId);
				} else if (navigationMenuImportUtil.isURL(menuItemJSON)) {
					siteNavigationMenuItem = navigationMenuImportUtil.importAsURLItem(menuItemJSON, serviceContext, siteNavigationMenuId, parentId);
				} else {
					siteNavigationMenuItem = navigationMenuImportUtil.importAsSubmenuItem(menuItemJSON, serviceContext, siteNavigationMenuId, parentId);
				}

				addNavigationItems(menuItemJSON, siteNavigationMenuId, siteNavigationMenuItem.getSiteNavigationMenuItemId(), serviceContext);
			}
		}
	}
}
