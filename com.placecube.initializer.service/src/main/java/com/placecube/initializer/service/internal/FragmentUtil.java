package com.placecube.initializer.service.internal;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONFactory;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.StringUtil;

@Component(immediate = true, service = FragmentUtil.class)
public class FragmentUtil {

	private static final Log LOG = LogFactoryUtil.getLog(FragmentUtil.class);

	@Reference
	private JSONFactory jsonFactory;

	public JSONObject getFragmentJSONObject(ClassLoader classLoader, String basePath) throws PortalException {
		try {
			String fragmentJSON = StringUtil.read(classLoader, basePath + "fragment.json");
			return jsonFactory.createJSONObject(fragmentJSON);
		} catch (Exception e) {
			throw new PortalException(e);
		}
	}

	public String getOptionalContent(ClassLoader classLoader, String basePath, JSONObject fragment, String propertyName) {
		try {
			return StringUtil.read(classLoader, basePath + fragment.getString(propertyName));
		} catch (Exception e) {
			LOG.warn("Unable to get fragment content for property: " + propertyName + " - " + e.getMessage());
			LOG.debug(e);
			return StringPool.BLANK;
		}
	}

}
