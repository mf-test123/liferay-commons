package com.placecube.initializer.service.internal;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.layout.page.template.constants.LayoutPageTemplateEntryTypeConstants;
import com.liferay.layout.page.template.model.LayoutPageTemplateCollection;
import com.liferay.layout.page.template.model.LayoutPageTemplateEntry;
import com.liferay.layout.page.template.service.LayoutPageTemplateEntryLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.LayoutTypePortlet;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.placecube.initializer.model.FragmentContext;
import com.placecube.initializer.model.PortletContext;

@Component(immediate = true, service = PageTemplateUtil.class)
public class PageTemplateUtil {

	@Reference
	private InitializerUtil initializerUtil;

	@Reference
	private LayoutLocalService layoutLocalService;

	@Reference
	private LayoutPageTemplateEntryLocalService layoutPageTemplateEntryLocalService;

	@Reference
	private Portal portal;

	public Layout configureContentPage(Set<FragmentContext> fragmentContexts, Set<PortletContext> porltetContexts, String structureContent, ServiceContext serviceContext, Long plid)
			throws PortalException {

		long layoutClassNameId = portal.getClassNameId(Layout.class);
		Layout draftLayout = initializerUtil.getDraftLayout(plid, layoutClassNameId);

		Map<String, String> fragmentEntryLinkIds = getFragmentEntryLinkIds(fragmentContexts, porltetContexts, serviceContext, layoutClassNameId, draftLayout);

		initializerUtil.addLayoutPageTemplateStructure(layoutClassNameId, structureContent, fragmentEntryLinkIds, serviceContext, draftLayout.getPlid());

		return initializerUtil.copyLayout(plid, draftLayout);
	}

	public Layout configureWidgetPage(String layoutTemplateId, Map<Integer, Set<PortletContext>> columnPortletContexts, ServiceContext serviceContext, long plid) throws PortalException {

		Layout layout = layoutLocalService.getLayout(plid);

		LayoutTypePortlet layoutTypePortlet = (LayoutTypePortlet) layout.getLayoutType();
		layoutTypePortlet.setLayoutTemplateId(serviceContext.getUserId(), layoutTemplateId, false);

		for (Map.Entry<Integer, Set<PortletContext>> columnPortlets : columnPortletContexts.entrySet()) {
			initializerUtil.configureLayoutColumn(layout, layoutTypePortlet, columnPortlets.getKey(), columnPortlets.getValue());
		}
		return layoutLocalService.updateLayout(layout.getGroupId(), layout.isPrivateLayout(), layout.getLayoutId(), layout.getTypeSettings());
	}

	public LayoutPageTemplateEntry createBasicPageTemplate(LayoutPageTemplateCollection layoutPageTemplateCollection, String pageTemplateName, ServiceContext serviceContext) throws PortalException {
		return layoutPageTemplateEntryLocalService.addLayoutPageTemplateEntry(serviceContext.getUserId(), serviceContext.getScopeGroupId(),
				layoutPageTemplateCollection.getLayoutPageTemplateCollectionId(), pageTemplateName, LayoutPageTemplateEntryTypeConstants.TYPE_BASIC, WorkflowConstants.STATUS_APPROVED, serviceContext);
	}

	public LayoutPageTemplateEntry createDisplayPageTemplate(String pageTemplateName, long classNameId, long classTypeId, ServiceContext serviceContext) throws PortalException {
		LayoutPageTemplateEntry layoutPageTemplateEntry = layoutPageTemplateEntryLocalService.addLayoutPageTemplateEntry(serviceContext.getUserId(), serviceContext.getScopeGroupId(), 0, classNameId,
				classTypeId, pageTemplateName, LayoutPageTemplateEntryTypeConstants.TYPE_DISPLAY_PAGE, WorkflowConstants.STATUS_APPROVED, serviceContext);
		layoutPageTemplateEntry.setDefaultTemplate(true);

		return layoutPageTemplateEntryLocalService.updateLayoutPageTemplateEntry(layoutPageTemplateEntry);
	}

	private Map<String, String> getFragmentEntryLinkIds(Set<FragmentContext> fragmentContexts, Set<PortletContext> porltetContexts, ServiceContext serviceContext, long layoutClassNameId,
			Layout draftLayout) throws PortalException {
		Map<String, String> fragmentEntryLinkIds = new LinkedHashMap<>();
		for (FragmentContext fragmentContext : fragmentContexts) {
			fragmentEntryLinkIds.put(fragmentContext.getPlaceholder(), initializerUtil.getFragmentEntryLinkIdForFragment(layoutClassNameId, fragmentContext, serviceContext, draftLayout));
		}
		for (PortletContext portletContext : porltetContexts) {
			fragmentEntryLinkIds.put(portletContext.getPlaceholder(), initializerUtil.getFragmentEntryLinkIdForPortlet(layoutClassNameId, portletContext, serviceContext, draftLayout));
		}
		return fragmentEntryLinkIds;
	}

}
