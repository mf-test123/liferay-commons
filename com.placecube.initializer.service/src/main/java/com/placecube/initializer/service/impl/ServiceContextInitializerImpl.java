package com.placecube.initializer.service.impl;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.initializer.service.ServiceContextInitializer;

@Component(immediate = true, service = ServiceContextInitializer.class)
public class ServiceContextInitializerImpl implements ServiceContextInitializer {

	@Override
	public ServiceContext getServiceContext(Group group) {
		ServiceContext serviceContext = new ServiceContext();
		serviceContext.setCompanyId(group.getCompanyId());
		serviceContext.setScopeGroupId(group.getGroupId());
		serviceContext.setUserId(group.getCreatorUserId());
		return serviceContext;
	}
}
