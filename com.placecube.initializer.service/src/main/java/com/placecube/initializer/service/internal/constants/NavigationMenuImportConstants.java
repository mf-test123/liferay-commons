package com.placecube.initializer.service.internal.constants;

public class NavigationMenuImportConstants {

	public static final String NAVIGATION_MENU_NAME = "navigationMenuName";
	public static final String LAYOUTS = "layouts";
	public static final String FRIENDLY_URL = "friendlyURL";
	public static final String PRIVATE = "private";
	public static final String URL = "url";

	private NavigationMenuImportConstants() {
	}

}
