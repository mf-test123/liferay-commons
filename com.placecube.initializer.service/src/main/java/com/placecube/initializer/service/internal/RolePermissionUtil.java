package com.placecube.initializer.service.internal;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.ResourceConstants;
import com.liferay.portal.kernel.service.ResourcePermissionLocalService;

@Component(immediate = true, service = RolePermissionUtil.class)
public class RolePermissionUtil {

	@Reference
	private ResourcePermissionLocalService resourcePermissionLocalService;

	public void addPermissions(Layout layout, long roleId, String[] actionIds) throws PortalException {
		long companyId = layout.getCompanyId();
		String primKey = String.valueOf(layout.getPrimaryKey());
		resourcePermissionLocalService.setResourcePermissions(companyId, Layout.class.getName(), ResourceConstants.SCOPE_INDIVIDUAL, primKey, roleId, actionIds);
	}

	public void removePermission(Layout layout, long roleId, String actionId) throws PortalException {
		long companyId = layout.getCompanyId();
		String primKey = String.valueOf(layout.getPrimaryKey());
		resourcePermissionLocalService.removeResourcePermission(companyId, Layout.class.getName(), ResourceConstants.SCOPE_INDIVIDUAL, primKey, roleId, actionId);
	}

}
