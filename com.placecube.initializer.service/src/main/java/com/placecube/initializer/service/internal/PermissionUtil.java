package com.placecube.initializer.service.internal;

import java.util.Map;
import java.util.Map.Entry;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.model.RoleConstants;
import com.liferay.portal.kernel.security.permission.ActionKeys;
import com.liferay.portal.kernel.service.RoleLocalService;

@Component(immediate = true, service = PermissionUtil.class)
public class PermissionUtil {

	@Reference
	private RoleLocalService roleLocalService;

	@Reference
	private RolePermissionUtil rolePermissionUtil;

	public void removeGuestPermissionView(Layout layout) throws PortalException {
		long companyId = layout.getCompanyId();
		Role guestRole = roleLocalService.getRole(companyId, RoleConstants.GUEST);
		rolePermissionUtil.removePermission(layout, guestRole.getRoleId(), ActionKeys.VIEW);
	}

	public void updateRolePermissions(Layout layout, Map<String, String[]> rolePermissionsToAdd, Map<String, String[]> rolePermissionsToRemove) throws PortalException {
		for (Entry<String, String[]> entry : rolePermissionsToAdd.entrySet()) {
			Role role = roleLocalService.getRole(layout.getCompanyId(), entry.getKey());
			rolePermissionUtil.addPermissions(layout, role.getRoleId(), entry.getValue());
		}
		for (Entry<String, String[]> entry : rolePermissionsToRemove.entrySet()) {
			Role role = roleLocalService.getRole(layout.getCompanyId(), entry.getKey());
			for (String actionId : entry.getValue()) {
				rolePermissionUtil.removePermission(layout, role.getRoleId(), actionId);
			}
		}
	}

}
