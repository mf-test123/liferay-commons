package com.placecube.initializer.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.osgi.service.component.annotations.Component;

import com.liferay.petra.string.StringPool;
import com.placecube.initializer.model.FragmentContext;
import com.placecube.initializer.model.LayoutContext;
import com.placecube.initializer.model.PortletContext;
import com.placecube.initializer.model.impl.FragmentContextImpl;
import com.placecube.initializer.model.impl.LayoutContextImpl;
import com.placecube.initializer.model.impl.PortletContextImpl;
import com.placecube.initializer.service.ModelCreationFactory;

@Component(immediate = true, service = ModelCreationFactory.class)
public class ModelCreationFactoryImpl implements ModelCreationFactory {

	@Override
	public LayoutContext createContentPageLayoutContext(String name, String friendlyURL, Boolean privateLayout) {
		return createLayoutContext(name, friendlyURL, privateLayout);
	}

	@Override
	public FragmentContext createFragmentContext(String id, String placeholder, String settings) {
		return new FragmentContextImpl(id, placeholder, settings);
	}

	@Override
	public LayoutContext createLayoutContext(String name, String friendlyURL, Boolean privateLayout) {
		LayoutContextImpl layoutContextImpl = new LayoutContextImpl(name);
		layoutContextImpl.setFriendlyURL(friendlyURL);
		layoutContextImpl.setPrivate(privateLayout);
		return layoutContextImpl;
	}

	@Override
	public LayoutContext createPageTemplateLayoutContext(String name) {
		return new LayoutContextImpl(name);
	}

	@Override
	public LayoutContext createPageTemplateLayoutContext(String name, String layoutTemplateId) {
		LayoutContextImpl layoutContextImpl = new LayoutContextImpl(name);
		layoutContextImpl.setLayoutTemplateId(layoutTemplateId);
		return layoutContextImpl;
	}

	@Override
	public PortletContext createPortletContext(String portletId, Map<String, String> portletPreferences) {
		return new PortletContextImpl(portletId, StringPool.BLANK, portletPreferences, StringPool.BLANK);
	}

	@Override
	public PortletContext createPortletContext(String portletId, Map<String, String> portletPreferences, String customInstanceId) {
		return new PortletContextImpl(portletId, StringPool.BLANK, portletPreferences, customInstanceId);
	}

	@Override
	public PortletContext createPortletContext(String id, String placeholder) {
		return new PortletContextImpl(id, placeholder, new HashMap<>(), StringPool.BLANK);
	}

	@Override
	public PortletContext createPortletContext(String id, String placeholder, String customInstanceId) {
		return new PortletContextImpl(id, placeholder, new HashMap<>(), customInstanceId);
	}

	@Override
	public LayoutContext createWidgetPageLayoutContext(String name, String friendlyURL, Boolean privateLayout, String layoutTemplateId) {
		LayoutContextImpl layoutContextImpl = new LayoutContextImpl(name);
		layoutContextImpl.setFriendlyURL(friendlyURL);
		layoutContextImpl.setPrivate(privateLayout);
		layoutContextImpl.setLayoutTemplateId(layoutTemplateId);
		return layoutContextImpl;
	}

}
