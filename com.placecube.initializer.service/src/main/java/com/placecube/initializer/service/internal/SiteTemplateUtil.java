package com.placecube.initializer.service.internal;

import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.LayoutSet;
import com.liferay.portal.kernel.model.LayoutSetPrototype;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.liferay.portal.kernel.service.LayoutSetLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.sites.kernel.util.SitesUtil;
import com.placecube.initializer.model.internal.SiteTemplateConfiguration;

@Component(immediate = true, service = SiteTemplateUtil.class)
public class SiteTemplateUtil {

	@Reference
	private LayoutLocalService layoutLocalService;

	@Reference
	private LayoutSetLocalService layoutSetLocalService;

	public void applySiteTemplatesToGroup(Group group, SiteTemplateConfiguration publicPagesConfig, SiteTemplateConfiguration privatePagesConfig) throws Exception {
		SitesUtil.updateLayoutSetPrototypesLinks(group, publicPagesConfig.getLayoutSetPrototypeId(), privatePagesConfig.getLayoutSetPrototypeId(), publicPagesConfig.isLayoutSetPrototypeLinkEnabled(),
				privatePagesConfig.isLayoutSetPrototypeLinkEnabled());
	}

	public void applyThemeToGroup(Group group, SiteTemplateConfiguration publicPagesConfig, SiteTemplateConfiguration privatePagesConfig) throws PortalException {
		if (publicPagesConfig.hasCustomLookAndFeel()) {
			layoutSetLocalService.updateLookAndFeel(group.getGroupId(), false, publicPagesConfig.getThemeId(), publicPagesConfig.getColorSchemeId(), publicPagesConfig.getCss());
		}

		if (privatePagesConfig.hasCustomLookAndFeel()) {
			layoutSetLocalService.updateLookAndFeel(group.getGroupId(), true, privatePagesConfig.getThemeId(), privatePagesConfig.getColorSchemeId(), privatePagesConfig.getCss());
		}
	}

	public SiteTemplateConfiguration getSiteTemplateConfigurationToApply(LayoutSet layoutSet, Optional<LayoutSetPrototype> layoutSetPrototype, boolean layoutSetPrototypeLinkEnabled)
			throws PortalException {

		SiteTemplateConfiguration result = new SiteTemplateConfiguration();

		if (layoutSetPrototype.isPresent()) {
			result.setDeleteLayouts(true);
			LayoutSetPrototype layoutSetPrototypeToUse = layoutSetPrototype.get();
			LayoutSet layoutSetToUse = layoutSetPrototypeToUse.getLayoutSet();
			result.setLayoutSetPrototypeId(layoutSetPrototypeToUse.getLayoutSetPrototypeId());
			result.setLayoutSetPrototypeLinkEnabled(layoutSetPrototypeLinkEnabled);
			result.setThemeId(layoutSetToUse.getThemeId());
			result.setColorSchemeId(layoutSetToUse.getColorSchemeId());
			result.setCss(layoutSetToUse.getCss());

		} else if (Validator.isNotNull(layoutSet)) {
			result.setLayoutSetPrototypeId(layoutSet.getLayoutSetPrototypeId());
			result.setLayoutSetPrototypeLinkEnabled(layoutSet.isLayoutSetPrototypeLinkEnabled());
			result.setThemeId(layoutSet.getThemeId());
			result.setColorSchemeId(layoutSet.getColorSchemeId());
			result.setCss(layoutSet.getCss());
		}

		return result;
	}

	public void removeExistingPages(Group group, SiteTemplateConfiguration publicPagesConfig, SiteTemplateConfiguration privatePagesConfig, ServiceContext serviceContext) throws PortalException {
		if (publicPagesConfig.isDeleteLayouts()) {
			layoutLocalService.deleteLayouts(group.getGroupId(), false, serviceContext);
		}

		if (privatePagesConfig.isDeleteLayouts()) {
			layoutLocalService.deleteLayouts(group.getGroupId(), true, serviceContext);
		}
	}

}
