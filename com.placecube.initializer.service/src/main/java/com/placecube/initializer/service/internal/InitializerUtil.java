package com.placecube.initializer.service.internal;

import java.io.InputStream;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.list.model.AssetListEntry;
import com.liferay.asset.list.service.AssetListEntryLocalService;
import com.liferay.fragment.contributor.FragmentCollectionContributorTracker;
import com.liferay.fragment.model.FragmentEntry;
import com.liferay.fragment.model.FragmentEntryLink;
import com.liferay.fragment.service.FragmentEntryLinkLocalService;
import com.liferay.fragment.service.FragmentEntryLocalService;
import com.liferay.layout.page.template.model.LayoutPageTemplateStructure;
import com.liferay.layout.page.template.service.LayoutPageTemplateStructureLocalService;
import com.liferay.layout.util.LayoutCopyHelper;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.model.LayoutConstants;
import com.liferay.portal.kernel.model.LayoutTypePortlet;
import com.liferay.portal.kernel.model.LayoutTypePortletConstants;
import com.liferay.portal.kernel.service.LayoutLocalService;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.FileUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.initializer.model.FragmentContext;
import com.placecube.initializer.model.LayoutContext;
import com.placecube.initializer.model.PortletContext;
import com.placecube.initializer.service.internal.constants.NavigationMenuImportConstants;

@Component(immediate = true, service = InitializerUtil.class)
public class InitializerUtil {

	private static final Log LOG = LogFactoryUtil.getLog(InitializerUtil.class);

	@Reference
	private AssetListEntryLocalService assetListEntryLocalService;

	@Reference
	private FragmentCollectionContributorTracker fragmentCollectionContributorTracker;

	@Reference
	private FragmentEntryLinkLocalService fragmentEntryLinkLocalService;

	@Reference
	private FragmentEntryLocalService fragmentEntryLocalService;

	@Reference
	private InitializerPrefsUtil initializerPrefsUtil;

	@Reference
	private LayoutCopyHelper layoutCopyHelper;

	@Reference
	private LayoutLocalService layoutLocalService;

	@Reference
	private LayoutPageTemplateStructureLocalService layoutPageTemplateStructureLocalService;

	@Reference
	private PermissionUtil permissionUtil;

	public void addLayoutPageTemplateStructure(Long layoutClassNameId, String structureContent, Map<String, String> fragmentEntryLinkIds, ServiceContext serviceContext, long plid)
			throws PortalException {
		LayoutPageTemplateStructure layoutPageTemplateStructure = layoutPageTemplateStructureLocalService.fetchLayoutPageTemplateStructure(serviceContext.getScopeGroupId(), layoutClassNameId, plid);
		if (Validator.isNull(layoutPageTemplateStructure)) {
			String structureDefinition = StringUtil.replace(structureContent, fragmentEntryLinkIds.keySet().toArray(new String[fragmentEntryLinkIds.size()]),
					fragmentEntryLinkIds.values().toArray(new String[fragmentEntryLinkIds.size()]));
			layoutPageTemplateStructureLocalService
					.addLayoutPageTemplateStructure(serviceContext.getUserId(), serviceContext.getScopeGroupId(), layoutClassNameId, plid, structureDefinition, serviceContext);
		}
	}

	public void configureLayoutColumn(Layout layout, LayoutTypePortlet layoutTypePortlet, Integer columnIndex, Set<PortletContext> portlets) throws PortalException {
		try {
			String columnId = LayoutTypePortletConstants.COLUMN_PREFIX + columnIndex;

			for (PortletContext portlet : portlets) {
				addPortlet(layout, layoutTypePortlet, columnId, portlet);
			}
		} catch (Exception e) {
			throw new PortalException(e);
		}
	}

	public Layout copyLayout(long plid, Layout draftLayout) throws PortalException {
		Layout layout = layoutLocalService.getLayout(plid);
		return copyLayout(layout, draftLayout);
	}

	public Layout copyLayout(Layout layout, Layout draftLayout) throws PortalException {
		try {
			layoutCopyHelper.copyLayout(draftLayout, layout);
			return layoutLocalService.updateLayout(layout.getGroupId(), layout.isPrivateLayout(), layout.getLayoutId(), new Date());
		} catch (Exception e) {
			throw new PortalException(e);
		}
	}

	public Layout createLayout(LayoutContext layoutContext, String type, ServiceContext serviceContext) throws PortalException {
		Locale locale = serviceContext.getLocale();
		Map<Locale, String> nameMap = Collections.singletonMap(locale, layoutContext.getName());
		Map<Locale, String> titleMap = Collections.singletonMap(locale, layoutContext.getTitle());
		long parentLayoutId = getParentLayoutId(serviceContext, layoutContext);
		String typeSettings = getTypeSettings(layoutContext.getTypeSettings());
		Map<Locale, String> friendlyURLMap = Collections.singletonMap(locale, layoutContext.getFriendlyURL());

		serviceContext.setExpandoBridgeAttributes(layoutContext.getExpandoValues());

		Layout layout = layoutLocalService
				.addLayout(serviceContext.getUserId(), serviceContext.getScopeGroupId(), layoutContext.isPrivate(), parentLayoutId, layoutContext.getClassNameId(), layoutContext.getClassPK(), nameMap,
						titleMap, Collections.emptyMap(), null, null, type, typeSettings, layoutContext.isHidden(), false, friendlyURLMap, serviceContext);

		try {
			InputStream pageIcon = layoutContext.getPageIcon();
			if (Validator.isNotNull(pageIcon)) {
				layout = layoutLocalService.updateIconImage(layout.getPlid(), FileUtil.getBytes(pageIcon));
			}
		} catch (Exception e) {
			LOG.error("Unable to configure layout image", e);
		}

		if (!layoutContext.isAddGuestPermissions()) {
			permissionUtil.removeGuestPermissionView(layout);
		}

		permissionUtil.updateRolePermissions(layout, layoutContext.getRolePermissionsToAdd(), layoutContext.getRolePermissionsToRemove());

		return layout;
	}

	public Optional<AssetListEntry> getContentSet(long groupId, String contentSetName) {
		DynamicQuery dynamicQuery = assetListEntryLocalService.dynamicQuery();
		dynamicQuery.add(RestrictionsFactoryUtil.eq("groupId", groupId));
		dynamicQuery.add(RestrictionsFactoryUtil.eq("title", contentSetName));
		List<AssetListEntry> assetListEntriesFound = assetListEntryLocalService.dynamicQuery(dynamicQuery);
		return assetListEntriesFound.isEmpty() ? Optional.empty() : Optional.ofNullable(assetListEntriesFound.get(0));
	}

	public Layout getDraftLayout(long plid, long layoutClassNameId) {
		return layoutLocalService.fetchLayout(layoutClassNameId, plid);
	}

	public String getFragmentEntryLinkIdForFragment(Long layoutClassNameId, FragmentContext fragmentContext, ServiceContext serviceContext, Layout layout) throws PortalException {
		FragmentEntry fragmentEntry = fragmentEntryLocalService.fetchFragmentEntry(serviceContext.getScopeGroupId(), fragmentContext.getId());
		if (Validator.isNull(fragmentEntry)) {
			fragmentEntry = fragmentCollectionContributorTracker.getFragmentEntries(serviceContext.getLocale()).get(fragmentContext.getId());
		}
		FragmentEntryLink fragmentEntryLink = fragmentEntryLinkLocalService
				.addFragmentEntryLink(serviceContext.getUserId(), serviceContext.getScopeGroupId(), 0, fragmentEntry.getFragmentEntryId(), layoutClassNameId, layout.getPlid(), fragmentEntry.getCss(),
						fragmentEntry.getHtml(), fragmentEntry.getJs(), fragmentEntry.getConfiguration(), fragmentContext.getSettings(), StringPool.BLANK, 0, fragmentEntry.getFragmentEntryKey(),
						serviceContext);
		return String.valueOf(fragmentEntryLink.getFragmentEntryLinkId());
	}

	public String getFragmentEntryLinkIdForPortlet(Long layoutClassNameId, PortletContext portletContext, ServiceContext serviceContext, Layout layout) throws PortalException {
		String portletId = portletContext.getPortletId();
		String instanceId = initializerPrefsUtil.getPortletInstanceId(portletContext);

		String jsonString = initializerPrefsUtil.getFragmentSettingsForPortlet(portletId, instanceId);
		String portletWithInstanceId = initializerPrefsUtil.getPortletIdWithInstanceIdDetails(portletId, instanceId);

		initializerPrefsUtil.savePortletPreferences(layout, portletWithInstanceId, portletContext);

		FragmentEntryLink fragmentEntryLink = fragmentEntryLinkLocalService
				.addFragmentEntryLink(serviceContext.getUserId(), serviceContext.getScopeGroupId(), 0, 0, layoutClassNameId, layout.getPlid(), StringPool.BLANK, StringPool.BLANK, StringPool.BLANK,
						StringPool.BLANK, jsonString, StringPool.BLANK, 0, null, serviceContext);

		return String.valueOf(fragmentEntryLink.getFragmentEntryLinkId());
	}

	public String getLayoutTypeSettingsForMenuItem(Layout layout) {
		StringBuilder typeSettings = new StringBuilder();
		typeSettings.append("groupId=" + layout.getGroupId());
		typeSettings.append(StringPool.NEW_LINE);
		typeSettings.append("layoutUuid=" + layout.getUuid());
		typeSettings.append(StringPool.NEW_LINE);
		typeSettings.append("privateLayout=" + layout.isPrivateLayout());
		typeSettings.append(StringPool.NEW_LINE);
		typeSettings.append("title=" + layout.getTitle(layout.getDefaultLanguageId()));
		typeSettings.append(StringPool.NEW_LINE);
		return typeSettings.toString();
	}

	public String getTypeSettingsForSubmenu(JSONObject jsonObject) {
		return "name=" + jsonObject.getString(NavigationMenuImportConstants.NAVIGATION_MENU_NAME);
	}

	public String getTypeSettingsForURL(JSONObject jsonObject) {
		StringBuilder typeSettings = new StringBuilder();
		typeSettings.append("name=" + jsonObject.getString(NavigationMenuImportConstants.NAVIGATION_MENU_NAME));
		typeSettings.append(StringPool.NEW_LINE);
		typeSettings.append("url=" + jsonObject.getString(NavigationMenuImportConstants.URL));
		typeSettings.append(StringPool.NEW_LINE);
		return typeSettings.toString();
	}

	private void addPortlet(Layout layout, LayoutTypePortlet layoutTypePortlet, String columnId, PortletContext portletContext) throws PortalException {
		String portletInstanceId = initializerPrefsUtil.getPortletInstanceId(portletContext);
		String instancePortletId = initializerPrefsUtil.getPortletIdWithInstanceIdDetails(portletContext.getPortletId(), portletInstanceId);

		String portletId = layoutTypePortlet.addPortletId(layout.getUserId(), instancePortletId, columnId, -1, false);
		initializerPrefsUtil.savePortletPreferences(layout, portletId, portletContext);
	}

	private long getParentLayoutId(ServiceContext serviceContext, LayoutContext layoutContext) {
		if (Validator.isNotNull(layoutContext.getParentLayoutFriendlyURL())) {
			Layout parentLayout = layoutLocalService.fetchLayoutByFriendlyURL(serviceContext.getScopeGroupId(), layoutContext.isPrivate(), layoutContext.getParentLayoutFriendlyURL());
			if (Validator.isNotNull(parentLayout)) {
				return parentLayout.getLayoutId();
			}
		}
		return LayoutConstants.DEFAULT_PARENT_LAYOUT_ID;
	}

	private String getTypeSettings(Map<String, String> typeSettingsValues) {
		return typeSettingsValues.entrySet().stream().map(entry -> String.join(StringPool.EQUAL, entry.getKey(), entry.getValue())).collect(Collectors.joining(StringPool.RETURN_NEW_LINE));
	}

}
