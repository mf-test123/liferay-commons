# Placecube Liferay Commons

The Placecube Liferay Commons project is a set of helper modules which can be used across different Liferay DXP projects, promoting code reuse and reducing duplication.

## Supported Versions

* Liferay DXP 7.2
* Java JDK 8

## Building From Source

Using [Gradle](https://gradle.org/) 4.x:

	gradle clean build

## License
Copyright (C) 2019-present Placecube Limited.

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.

## Layout initialiser using json files for content pages
The LayoutInitialiser service allows creating content and widget pages based on json files.

For content pages the json structure needs to be created retrieving the fragment details from the database. 
This is an example of the final json. 

```
{
	"pageName": "Welcome",
	"pageFriendlyURL": "/welcome",
	"private": false,
	"hidden": false,
	"addGuestPermissions": true,
	"pageTypeSettings": [],
	"fragments": [
		{
			"id": "BASIC_COMPONENT-html",
			"placeholder": "${PLACEHOLDER_HEADER_HTML}",
			"preferences": {
				"com.liferay.fragment.entry.processor.background.image.BackgroundImageFragmentEntryProcessor": {
					
				},
				"com.liferay.fragment.entry.processor.editable.EditableFragmentEntryProcessor": {
					"element-html": {
						"defaultValue": "Test",
						"config": {
							
						}
					}
				},
				"com.liferay.fragment.entry.processor.freemarker.FreeMarkerFragmentEntryProcessor": {
				}
			}
		}
	],
	"portlets": [
	],
	"contentSetPlaceholders": [],
	"contentStructureDefinition": {
		"nextColumnId": 5,
		"nextRowId": 4,
		"config": {
			
		},
		"structure": [
			{
				"columns": [
					{
						"fragmentEntryLinkIds": [
							"${PLACEHOLDER_HEADER_HTML}"
						],
						"size": "8",
						"columnId": "1"
					},
					{
						"fragmentEntryLinkIds": [
							"${PLACEHOLDER_SIGN_IN_PORTLET}"
						],
						"size": "4",
						"columnId": "2"
					}
				],
				"type": "1",
				"config": {
					"columnSpacing": true,
					"paddingVertical": "8",
					"backgroundImage": {
					},
					"containerType": "fixed",
					"backgroundColorCssClass": "",
					"paddingHorizontal": "8"
				},
				"rowId": "1"
			}
		]
	}
}
```



The fields to update for content pages in particular are: 'fragments' and 'contentStructureDefinition'.

Steps to get the full working json:
* Run the following sql query using the page url that you want to auto-create (Making sure that you are targeting the correct layout version.)

Query 1: ``SELECT l.friendlyURL, lptsr.data_ as contentStructureDefinitionJSON FROM LayoutPageTemplateStructureRel lptsr JOIN LayoutPageTemplateStructure lpts on  lpts.layoutPageTemplateStructureId = lptsr.layoutPageTemplateStructureId join Layout l on l.plid = lpts.classPK WHERE l.friendlyURL like '/PUT_HERE_YOUR_PAGE_URL%';`` 


* Create your page.json file like the example above


* Replace the full content of 'contentStructureDefinition' with the 'contentStructureDefinitionJSON' from query 1.


* You can see that in your contentStructureDefinition now there's the page structure with the list of fragments on your page, but fragments are referenced by fragmentEntryLinkId.
The next steps need to be repeated for each single fragment.

Query 2: ``SELECT fel.fragmentEntryId  as fragmentEntryId , fel.editableValues as preferencesJSON FROM FragmentEntryLink fel WHERE fel.fragmentEntryLinkId  = PUT_HERE_ID_FROM_THE_JSON_IN_QUERY_1;``

Query 3: ``SELECT fe.fragmentEntryKey from FragmentEntry fe  WHERE fe.fragmentEntryId  = PUT_HERE_FRAGMENT_ENTRY_ID_FROM_QUERY_2;``


*	For each fragment in the 'fragments' json array replace:
		
			* 	the 'id' value with the 'fragmentEntryId' from query 3
			* 	the 'placeholder' with a custom placeholder value
			* 	the 'preferences' with the '' from query 2
			
*	For each fragment in the 'contentStructureDefinition' json replace:

			*	the 'fragmentEntryLinkId' with the placeholder you defined above
			


e.g.
For each fragment in the 'fragments' json array you'll have this:

``` 
	{
			"id": "REPLACE_THIS_WITH_FRAGMENT_ENTRY_ID_FROM_QUERY_3",
			"placeholder": "${THIS_IS_YOUR_CUSTOM_PLACEHOLDER_VALUE}",
			"preferences": REPLACE_THIS_WITH_THE_VALUE_FROM_QUERY_2 **Make sure to include the '{/}' from the value extracted! 
	}
```

And in the 'contentStructureDefinition':

```
"fragmentEntryLinkIds": [
	"${THIS_IS_YOUR_CUSTOM_PLACEHOLDER_VALUE}"
]
```

