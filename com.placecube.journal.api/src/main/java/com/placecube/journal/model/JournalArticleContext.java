package com.placecube.journal.model;

import java.io.IOException;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.journal.model.JournalFolder;
import com.liferay.portal.kernel.util.StringUtil;

public class JournalArticleContext {

	private final String articleId;
	private final String content;
	private DDMStructure ddmStructure;
	private String folderName;
	private boolean indexable;
	private JournalFolder journalFolder;
	private final String title;
	private String urlTitle;

	public static JournalArticleContext init(String articleId, String title, ClassLoader classloader, String articlePath, String folderName) throws IOException {
		JournalArticleContext articleContext = new JournalArticleContext(articleId, title, StringUtil.read(classloader, articlePath), true);
		articleContext.folderName = folderName;
		return articleContext;
	}

	public static JournalArticleContext init(String articleId, String title, String content) {
		return new JournalArticleContext(articleId, title, content, true);
	}

	private JournalArticleContext(String articleId, String title, String content, boolean indexable) {
		this.articleId = articleId;
		this.title = title;
		this.content = content;
		this.indexable = indexable;
	}

	public String getArticleId() {
		return articleId;
	}

	public String getContent() {
		return content;
	}

	public DDMStructure getDDMStructure() {
		return ddmStructure;
	}

	public String getFolderName() {
		return folderName;
	}

	public boolean getIndexable() {
		return indexable;
	}

	public JournalFolder getJournalFolder() {
		return journalFolder;
	}

	public String getTitle() {
		return title;
	}

	public String getUrlTitle() {
		return urlTitle;
	}

	public void setDDMStructure(DDMStructure ddmStructure) {
		this.ddmStructure = ddmStructure;
	}

	public void setIndexable(boolean indexable) {
		this.indexable = indexable;
	}

	public void setJournalFolder(JournalFolder journalFolder) {
		this.journalFolder = journalFolder;
	}

	public void setUrlTitle(String urlTitle) {
		this.urlTitle = urlTitle;
	}

}
