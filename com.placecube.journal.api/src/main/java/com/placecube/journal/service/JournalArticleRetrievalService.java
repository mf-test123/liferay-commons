package com.placecube.journal.service;

import java.util.List;
import java.util.Locale;
import java.util.Optional;

import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.journal.model.JournalArticle;

public interface JournalArticleRetrievalService {

	List<AssetEntry> getAssetEntriesOrderedByTitle(List<AssetEntry> assetEntries, Locale locale);

	Optional<String> getDisplayURL(JournalArticle journalArticle, Locale locale);

	Optional<String> getFieldValue(JournalArticle journalArticle, String fieldName, Locale locale);

}
