package com.placecube.journal.service;

import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.model.JournalFolder;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.journal.model.JournalArticleContext;

public interface JournalArticleCreationService {

	JournalArticle getOrCreateArticle(JournalArticleContext journalArticleContext, ServiceContext serviceContext) throws PortalException;

	JournalArticle getOrCreateBasicWebContentArticle(String articleId, String articleTitle, String articleContent, JournalFolder journalFolder, ServiceContext serviceContext) throws PortalException;

	JournalFolder getOrCreateJournalFolder(String folderName, ServiceContext serviceContext) throws PortalException;

}
