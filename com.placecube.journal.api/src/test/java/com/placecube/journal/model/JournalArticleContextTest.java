package com.placecube.journal.model;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.portal.kernel.util.StringUtil;

@RunWith(PowerMockRunner.class)
@PrepareForTest(StringUtil.class)
public class JournalArticleContextTest extends PowerMockito {

	@Mock
	private ClassLoader mockClassLoader;

	@Before
	public void activateSetup() {
		mockStatic(StringUtil.class);
	}

	@Test(expected = IOException.class)
	public void init_WithArticleIdAndArticleTitleAndClassLoaderAndArticlePathAndFolderNameParameters_WhenExceptionReadingArticleContent_ThenThrowsIOException() throws IOException {
		String articlePath = "articlePathValue";
		when(StringUtil.read(mockClassLoader, articlePath)).thenThrow(new IOException());

		JournalArticleContext.init("articleId", "articleTitle", mockClassLoader, articlePath, "folderName");
	}

	@Test
	public void init_WithArticleIdAndArticleTitleAndClassLoaderAndArticlePathAndFolderNameParameters_WhenNoError_ThenInitialisesTheJournalArticleContext() throws IOException {
		String articlePath = "articlePathValue";
		String articleContent = "articleContentValue";
		String articleId = "articleIdValue";
		String articleTitle = "articleTitleValue";
		String folderName = "folderNameValue";
		when(StringUtil.read(mockClassLoader, articlePath)).thenReturn(articleContent);

		JournalArticleContext result = JournalArticleContext.init(articleId, articleTitle, mockClassLoader, articlePath, folderName);

		assertThat(result.getArticleId(), equalTo(articleId));
		assertThat(result.getTitle(), equalTo(articleTitle));
		assertThat(result.getContent(), equalTo(articleContent));
		assertThat(result.getFolderName(), equalTo(folderName));
		assertNull(result.getJournalFolder());
		assertNull(result.getDDMStructure());
		assertTrue(result.getIndexable());
		assertNull(result.getUrlTitle());
	}

}
