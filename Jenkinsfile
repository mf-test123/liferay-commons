#!groovy

pipeline {

	agent {
		label 'docker.local.jenkins.slave.jdk11'
	}

	environment {
		ARTEFACT_REPOSITORY = credentials('Artifactory (Jenkins)')
		SONAR_TOKEN         = credentials('sonarcloud-token')
	}

	stages {

		stage("Build") {

			steps {

				sh "./gradlew build javadoc -Partefact_repository_user=$ARTEFACT_REPOSITORY_USR -Partefact_repository_password=$ARTEFACT_REPOSITORY_PSW"

				junit allowEmptyResults: true, testResults: '**/build/test-results/*.xml'

			}

		}

		stage("Code quality analysis") {

			steps {

				script {

					if (!env.BRANCH_NAME.equals('master')) {

						env.PULL_REQUEST_KEY_ARG = '-Dsonar.pullrequest.key=' + env.BRANCH_NAME.replaceAll('PR-','')

					} else {

						env.PULL_REQUEST_KEY_ARG = ''

					}

				}

				sh "./gradlew jacocoTestReport sonarqube $PULL_REQUEST_KEY_ARG -Partefact_repository_user=$ARTEFACT_REPOSITORY_USR -Partefact_repository_password=$ARTEFACT_REPOSITORY_PSW"

				junit allowEmptyResults: true, testResults: '**/build/test-results/*.xml'

			}

		}

		stage("Publish") {

			when {
				branch "master"
			}

			steps {

				sh "./gradlew artifactoryPublish -Partefact_repository_user=$ARTEFACT_REPOSITORY_USR -Partefact_repository_password=$ARTEFACT_REPOSITORY_PSW"

			}

		}

		stage("Prepare & commit snapshots") {

			when {
				branch "master"
			}

			steps {

				sshagent (credentials: ['jenkins_ssh'], ignoreMissing: true) {

					sh "ssh-keyscan -H bitbucket.org >> ~/.ssh/known_hosts"
					sh "./gradlew prepareAndCommitSnapshots -Partefact_repository_user=$ARTEFACT_REPOSITORY_USR -Partefact_repository_password=$ARTEFACT_REPOSITORY_PSW"

				}

			}

		}

	}

	post {

		fixed {

			slackSend channel: '#build-issues',
				color: "good",
				message: "*${ currentBuild.currentResult }:* ${ env.JOB_NAME } build ${ env.BUILD_NUMBER }\n More info at: ${ env.BUILD_URL }"
		}

		regression {

			slackSend channel: '#build-issues',
				color: "danger",
				message: "*${ currentBuild.currentResult }:* ${ env.JOB_NAME } build ${ env.BUILD_NUMBER }\n More info at: ${ env.BUILD_URL }"

		}

	}

}

