package com.placecube.journal.service.impl;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.model.JournalFolder;
import com.liferay.journal.model.JournalFolderConstants;
import com.liferay.journal.service.JournalArticleLocalService;
import com.liferay.journal.service.JournalFolderLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.journal.internal.service.CreationUtils;
import com.placecube.journal.model.JournalArticleContext;
import com.placecube.journal.service.JournalArticleCreationService;

@Component(immediate = true, service = JournalArticleCreationService.class)
public class JournalArticleCreationServiceImpl implements JournalArticleCreationService {

	private static final Log LOG = LogFactoryUtil.getLog(JournalArticleCreationServiceImpl.class);

	@Reference
	private CreationUtils creationUtils;

	@Reference
	private JournalArticleLocalService journalArticleLocalService;

	@Reference
	private JournalFolderLocalService journalFolderLocalService;

	@Override
	public JournalArticle getOrCreateArticle(JournalArticleContext journalArticleContext, ServiceContext serviceContext) throws PortalException {
		try {
			return journalArticleLocalService.getLatestArticle(serviceContext.getScopeGroupId(), journalArticleContext.getArticleId());
		} catch (PortalException e) {
			LOG.debug(e);

			JournalFolder journalFolder = creationUtils.getOrCreateFolder(journalArticleContext, serviceContext);

			JournalArticle journalArticle = creationUtils.addArticle(journalArticleContext.getArticleId(), journalArticleContext.getTitle(), journalArticleContext.getContent(),
					journalArticleContext.getUrlTitle(), journalFolder, journalArticleContext.getDDMStructure(), journalArticleContext.getIndexable(), serviceContext);

			creationUtils.configureDefaultDisplayPage(journalArticle, journalArticleContext.getDDMStructure(), serviceContext);

			return journalArticle;
		}
	}

	@Override
	public JournalArticle getOrCreateBasicWebContentArticle(String articleId, String articleTitle, String articleContent, JournalFolder journalFolder, ServiceContext serviceContext)
			throws PortalException {
		try {
			return journalArticleLocalService.getLatestArticle(serviceContext.getScopeGroupId(), articleId);
		} catch (PortalException e) {
			LOG.debug(e);
			return creationUtils.addArticle(articleId, articleTitle, articleContent, StringPool.BLANK, journalFolder, null, true, serviceContext);
		}
	}

	@Override
	public JournalFolder getOrCreateJournalFolder(String folderName, ServiceContext serviceContext) throws PortalException {
		JournalFolder journalFolder = journalFolderLocalService.fetchFolder(serviceContext.getScopeGroupId(), folderName);
		return Validator.isNotNull(journalFolder) ? journalFolder
				: journalFolderLocalService.addFolder(serviceContext.getUserId(), serviceContext.getScopeGroupId(), JournalFolderConstants.DEFAULT_PARENT_FOLDER_ID, folderName, null, serviceContext);
	}

}
