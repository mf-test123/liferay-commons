package com.placecube.journal.service.impl;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.dynamic.data.mapping.storage.Fields;
import com.liferay.dynamic.data.mapping.util.DDMXML;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.model.JournalArticleConstants;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.journal.service.JournalArticleRetrievalService;

@Component(immediate = true, service = JournalArticleRetrievalService.class)
public class JournalArticleRetrievalServiceImpl implements JournalArticleRetrievalService {

	private static final Log LOG = LogFactoryUtil.getLog(JournalArticleRetrievalServiceImpl.class);

	@Reference
	DDMXML ddmxml;

	@Override
	public List<AssetEntry> getAssetEntriesOrderedByTitle(List<AssetEntry> assetEntries, Locale locale) {
		assetEntries.sort((o1, o2) -> o1.getTitle(locale).compareTo(o2.getTitle(locale)));
		return assetEntries;
	}

	@Override
	public Optional<String> getDisplayURL(JournalArticle journalArticle, Locale locale) {
		String friendlyURL = null;
		try {
			Map<Locale, String> friendlyURLMap = journalArticle.getFriendlyURLMap();
			friendlyURL = JournalArticleConstants.CANONICAL_URL_SEPARATOR + friendlyURLMap.getOrDefault(locale, friendlyURLMap.get(LocaleUtil.fromLanguageId(journalArticle.getDefaultLanguageId())));
		} catch (Exception e) {
			LOG.debug(e);
			LOG.warn("Unable to get article display url - " + e.getMessage());
		}
		return Optional.ofNullable(friendlyURL);
	}

	@Override
	public Optional<String> getFieldValue(JournalArticle journalArticle, String fieldName, Locale locale) {
		try {
			Fields fields = ddmxml.getFields(journalArticle.getDDMStructure(), journalArticle.getContent());
			String value = GetterUtil.getString(fields.get(fieldName).getValue(locale));
			return Validator.isNotNull(value) ? Optional.of(value) : Optional.empty();
		} catch (Exception e) {
			LOG.debug(e);
			LOG.warn("Unable to get article field value " + e.getMessage());
			return Optional.empty();
		}
	}

}
