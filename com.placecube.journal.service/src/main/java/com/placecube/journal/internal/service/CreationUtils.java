package com.placecube.journal.internal.service;

import java.io.File;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.asset.display.page.constants.AssetDisplayPageConstants;
import com.liferay.asset.display.page.service.AssetDisplayPageEntryLocalService;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.model.JournalArticleConstants;
import com.liferay.journal.model.JournalFolder;
import com.liferay.journal.model.JournalFolderConstants;
import com.liferay.journal.service.JournalArticleLocalService;
import com.liferay.journal.service.JournalFolderLocalService;
import com.liferay.layout.page.template.model.LayoutPageTemplateEntry;
import com.liferay.layout.page.template.service.LayoutPageTemplateEntryLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.journal.model.JournalArticleContext;

@Component(immediate = true, service = CreationUtils.class)
public class CreationUtils {

	@Reference
	private AssetDisplayPageEntryLocalService assetDisplayPageEntryLocalService;

	@Reference
	private JournalArticleLocalService journalArticleLocalService;

	@Reference
	private JournalFolderLocalService journalFolderLocalService;

	@Reference
	private LayoutPageTemplateEntryLocalService layoutPageTemplateEntryLocalService;

	@Reference
	private Portal portal;

	public JournalArticle addArticle(String articleId, String articleTitle, String articleContent, String articleURL, JournalFolder journalFolder, DDMStructure ddmStructure, boolean indexable,
			ServiceContext serviceContext) throws PortalException {
		long classPK = 0;
		Map<Locale, String> descriptionMap = null;
		String layoutUuid = null;
		Calendar now = Calendar.getInstance();
		int displayDateMonth = now.get(Calendar.MONTH);
		int displayDateDay = now.get(Calendar.DAY_OF_MONTH);
		int displayDateYear = now.get(Calendar.YEAR);
		int displayDateHour = 0;
		int displayDateMinute = 0;
		int expirationDateMonth = 0;
		int expirationDateDay = 0;
		int expirationDateYear = 0;
		int expirationDateHour = 0;
		int expirationDateMinute = 0;
		boolean neverExpire = true;
		int reviewDateDay = 0;
		int reviewDateYear = 0;
		int reviewDateMonth = 0;
		int reviewDateHour = 0;
		int reviewDateMinute = 0;
		boolean neverReview = true;
		boolean smallImage = false;
		String smallImageURL = null;
		File smallImageFile = null;
		Map<String, byte[]> images = null;

		String articleLocalizedContent = StringUtil.replace(articleContent, "[$LOCALE_DEFAULT$]", serviceContext.getLocale().toString());

		long folderId = Validator.isNotNull(journalFolder) ? journalFolder.getFolderId() : JournalFolderConstants.DEFAULT_PARENT_FOLDER_ID;

		String ddmStructureKey = "BASIC-WEB-CONTENT";
		String ddmTemplateKey = "BASIC-WEB-CONTENT";

		if (Validator.isNotNull(ddmStructure)) {
			ddmStructureKey = ddmStructure.getStructureKey();
			List<DDMTemplate> templates = ddmStructure.getTemplates();
			ddmTemplateKey = templates.isEmpty() ? StringPool.BLANK : templates.get(0).getTemplateKey();
		}

		return journalArticleLocalService.addArticle(serviceContext.getUserId(), serviceContext.getScopeGroupId(), folderId, JournalArticleConstants.CLASSNAME_ID_DEFAULT, classPK, articleId,
				Validator.isNull(articleId), JournalArticleConstants.VERSION_DEFAULT, Collections.singletonMap(serviceContext.getLocale(), articleTitle), descriptionMap, articleLocalizedContent,
				ddmStructureKey, ddmTemplateKey, layoutUuid, displayDateMonth, displayDateDay, displayDateYear, displayDateHour, displayDateMinute, expirationDateMonth, expirationDateDay,
				expirationDateYear, expirationDateHour, expirationDateMinute, neverExpire, reviewDateMonth, reviewDateDay, reviewDateYear, reviewDateHour, reviewDateMinute, neverReview, indexable,
				smallImage, smallImageURL, smallImageFile, images, articleURL, serviceContext);
	}

	public void configureDefaultDisplayPage(JournalArticle journalArticle, DDMStructure ddmStructure, ServiceContext serviceContext) throws PortalException {
		if (Validator.isNotNull(ddmStructure)) {
			long articleClassNameId = portal.getClassNameId(JournalArticle.class);

			LayoutPageTemplateEntry defaultLayoutPageTemplateEntry = layoutPageTemplateEntryLocalService.fetchDefaultLayoutPageTemplateEntry(serviceContext.getScopeGroupId(), articleClassNameId,
					ddmStructure.getStructureId());

			if (Validator.isNotNull(defaultLayoutPageTemplateEntry)) {
				assetDisplayPageEntryLocalService.addAssetDisplayPageEntry(serviceContext.getUserId(), serviceContext.getScopeGroupId(), articleClassNameId, journalArticle.getResourcePrimKey(),
						defaultLayoutPageTemplateEntry.getLayoutPageTemplateEntryId(), AssetDisplayPageConstants.TYPE_DEFAULT, serviceContext);
			}
		}
	}

	public JournalFolder getOrCreateFolder(JournalArticleContext journalArticleContext, ServiceContext serviceContext) throws PortalException {
		JournalFolder journalFolder = journalArticleContext.getJournalFolder();
		if (Validator.isNotNull(journalFolder)) {
			return journalFolder;
		} else {
			String folderName = journalArticleContext.getFolderName();
			if (Validator.isNotNull(folderName)) {
				journalFolder = journalFolderLocalService.fetchFolder(serviceContext.getScopeGroupId(), folderName);
				return Validator.isNotNull(journalFolder) ? journalFolder
						: journalFolderLocalService.addFolder(serviceContext.getUserId(), serviceContext.getScopeGroupId(), JournalFolderConstants.DEFAULT_PARENT_FOLDER_ID, folderName, null,
								serviceContext);
			}
		}
		return null;
	}
}
