package com.placecube.journal.internal.service;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.asset.display.page.constants.AssetDisplayPageConstants;
import com.liferay.asset.display.page.service.AssetDisplayPageEntryLocalService;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.model.JournalArticleConstants;
import com.liferay.journal.model.JournalFolder;
import com.liferay.journal.service.JournalArticleLocalService;
import com.liferay.journal.service.JournalFolderLocalService;
import com.liferay.layout.page.template.model.LayoutPageTemplateEntry;
import com.liferay.layout.page.template.service.LayoutPageTemplateEntryLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.Portal;
import com.liferay.portal.kernel.util.Validator;
import com.placecube.journal.model.JournalArticleContext;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class CreationUtilsTest {

	private static final String ARTICLE_CONTENT = "myArticle content with [$LOCALE_DEFAULT$] locale";
	private static final String ARTICLE_LOCALIZED_CONTENT = "myArticle content with " + Locale.CANADA.toString() + " locale";
	private static final long ARTICLE_PRIM_KEY = 7;
	private static final String ARTICLE_TITLE = "articleTitleValue";
	private static final String ARTICLE_URL = "articleURLValue";
	private static final long CLASS_NAME_ID = 5;
	private static final long FOLDER_ID = 3;
	private static final long GROUP_ID = 1;
	private static final Locale LOCALE = Locale.CANADA;
	private static final long STRUCTURE_ID = 4;
	private static final String STRUCTURE_KEY = "structureKeyValue";
	private static final long TEMPLATE_ID = 6;
	private static final String TEMPLATE_KEY = "templateKeyValue";
	private static final long USER_ID = 2;

	@InjectMocks
	private CreationUtils creationUtils;

	@Mock
	private AssetDisplayPageEntryLocalService mockAssetDisplayPageEntryLocalService;

	@Mock
	private DDMStructure mockDDMStructure;

	@Mock
	private DDMTemplate mockDDMTemplate;

	@Mock
	private JournalArticle mockJournalArticle;

	@Mock
	private JournalArticleContext mockJournalArticleContext;

	@Mock
	private JournalArticleLocalService mockJournalArticleLocalService;

	@Mock
	private JournalFolder mockJournalFolder;

	@Mock
	private JournalFolderLocalService mockJournalFolderLocalService;

	@Mock
	private LayoutPageTemplateEntry mockLayoutPageTemplateEntry;

	@Mock
	private LayoutPageTemplateEntryLocalService mockLayoutPageTemplateEntryLocalService;

	@Mock
	private Portal mockPortal;

	@Mock
	private ServiceContext mockServiceContext;

	@Test(expected = PortalException.class)
	@Parameters({ "true|true|true|true|true", "true|false|true|true|true", "true|true|false|true|true", "true|false|false|true|true", "false|true|true|true|true", "false|false|true|true|true",
			"false|true|false|true|false", "false|false|false|true|false", "true|true|true|false|false", "true|false|true|false|false", "false|true|true|false|false", "false|false|true|false|false" })
	public void addArticle_WhenExceptionCreatingArticle_ThenThrowsPortalException(boolean articleIdSpecified, boolean folderSpecified, boolean structureSpecified, boolean structureWithTemplates,
			boolean indexable) throws PortalException {
		mockServiceContextDetails();
		long folderId = getExpectedFolderId(folderSpecified);
		JournalFolder folder = getExpectedFolder(folderSpecified);
		DDMStructure structure = getExpectedStructure(structureSpecified, structureWithTemplates);
		String articleId = getExpectedArticleId(articleIdSpecified);
		String ddmStructureKey = getExpectedStructureKey(structureSpecified);
		String ddmTemplateKey = getExpectedTemplateKey(structureSpecified, structureWithTemplates);
		when(mockJournalArticleLocalService.addArticle(USER_ID, GROUP_ID, folderId, JournalArticleConstants.CLASSNAME_ID_DEFAULT, 0, articleId, Validator.isNull(articleId),
				JournalArticleConstants.VERSION_DEFAULT, Collections.singletonMap(LOCALE, ARTICLE_TITLE), null, ARTICLE_LOCALIZED_CONTENT, ddmStructureKey, ddmTemplateKey, null,
				Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH), Calendar.getInstance().get(Calendar.YEAR), 0, 0, 0, 0, 0, 0, 0, true, 0, 0, 0, 0, 0,
				true, indexable, false, null, null, null, ARTICLE_URL, mockServiceContext)).thenThrow(new PortalException());

		creationUtils.addArticle(articleId, ARTICLE_TITLE, ARTICLE_CONTENT, ARTICLE_URL, folder, structure, indexable, mockServiceContext);
	}

	@Test
	@Parameters({ "true|true|true|true|true", "true|false|true|true|true", "true|true|false|true|true", "true|false|false|true|true", "false|true|true|true|true", "false|false|true|true|true",
			"false|true|false|true|false", "false|false|false|true|false", "true|true|true|false|false", "true|false|true|false|false", "false|true|true|false|false", "false|false|true|false|false" })
	public void addArticle_WhenNoError_ThenReturnsTheNewlyCreatedArticle(boolean articleIdSpecified, boolean folderSpecified, boolean structureSpecified, boolean structureWithTemplates,
			boolean indexable) throws PortalException {
		mockServiceContextDetails();
		long folderId = getExpectedFolderId(folderSpecified);
		JournalFolder folder = getExpectedFolder(folderSpecified);
		DDMStructure structure = getExpectedStructure(structureSpecified, structureWithTemplates);
		String articleId = getExpectedArticleId(articleIdSpecified);
		String ddmStructureKey = getExpectedStructureKey(structureSpecified);
		String ddmTemplateKey = getExpectedTemplateKey(structureSpecified, structureWithTemplates);
		when(mockJournalArticleLocalService.addArticle(USER_ID, GROUP_ID, folderId, JournalArticleConstants.CLASSNAME_ID_DEFAULT, 0, articleId, Validator.isNull(articleId),
				JournalArticleConstants.VERSION_DEFAULT, Collections.singletonMap(LOCALE, ARTICLE_TITLE), null, ARTICLE_LOCALIZED_CONTENT, ddmStructureKey, ddmTemplateKey, null,
				Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH), Calendar.getInstance().get(Calendar.YEAR), 0, 0, 0, 0, 0, 0, 0, true, 0, 0, 0, 0, 0,
				true, indexable, false, null, null, null, ARTICLE_URL, mockServiceContext)).thenReturn(mockJournalArticle);

		JournalArticle result = creationUtils.addArticle(articleId, ARTICLE_TITLE, ARTICLE_CONTENT, ARTICLE_URL, folder, structure, indexable, mockServiceContext);

		assertThat(result, sameInstance(mockJournalArticle));
	}

	@Test
	public void configureDefaultDisplayPage_WhenStructureIsConfiguredAndDefaultTemplateFound_ThenAddsANewAssetDisplayPage() throws PortalException {
		mockServiceContextDetails();
		when(mockDDMStructure.getStructureId()).thenReturn(STRUCTURE_ID);
		when(mockPortal.getClassNameId(JournalArticle.class)).thenReturn(CLASS_NAME_ID);
		when(mockLayoutPageTemplateEntryLocalService.fetchDefaultLayoutPageTemplateEntry(GROUP_ID, CLASS_NAME_ID, STRUCTURE_ID)).thenReturn(mockLayoutPageTemplateEntry);
		when(mockLayoutPageTemplateEntry.getLayoutPageTemplateEntryId()).thenReturn(TEMPLATE_ID);
		when(mockJournalArticle.getResourcePrimKey()).thenReturn(ARTICLE_PRIM_KEY);

		creationUtils.configureDefaultDisplayPage(mockJournalArticle, mockDDMStructure, mockServiceContext);

		verify(mockAssetDisplayPageEntryLocalService, times(1)).addAssetDisplayPageEntry(USER_ID, GROUP_ID, CLASS_NAME_ID, ARTICLE_PRIM_KEY, TEMPLATE_ID, AssetDisplayPageConstants.TYPE_DEFAULT,
				mockServiceContext);
	}

	@Test(expected = PortalException.class)
	public void configureDefaultDisplayPage_WhenStructureIsConfiguredAndDefaultTemplateFoundAndExceptionAddingANewAssetDisplayPage_ThenThrowsPortalException() throws PortalException {
		mockServiceContextDetails();
		when(mockDDMStructure.getStructureId()).thenReturn(STRUCTURE_ID);
		when(mockPortal.getClassNameId(JournalArticle.class)).thenReturn(CLASS_NAME_ID);
		when(mockLayoutPageTemplateEntryLocalService.fetchDefaultLayoutPageTemplateEntry(GROUP_ID, CLASS_NAME_ID, STRUCTURE_ID)).thenReturn(mockLayoutPageTemplateEntry);
		when(mockLayoutPageTemplateEntry.getLayoutPageTemplateEntryId()).thenReturn(TEMPLATE_ID);
		when(mockJournalArticle.getResourcePrimKey()).thenReturn(ARTICLE_PRIM_KEY);
		when(mockAssetDisplayPageEntryLocalService.addAssetDisplayPageEntry(USER_ID, GROUP_ID, CLASS_NAME_ID, ARTICLE_PRIM_KEY, TEMPLATE_ID, AssetDisplayPageConstants.TYPE_DEFAULT,
				mockServiceContext)).thenThrow(new PortalException());

		creationUtils.configureDefaultDisplayPage(mockJournalArticle, mockDDMStructure, mockServiceContext);

	}

	@Test
	public void configureDefaultDisplayPage_WhenStructureIsConfiguredAndNoDefaultTemplateFound_ThenNoActionIsPerformed() throws PortalException {
		mockServiceContextDetails();
		when(mockDDMStructure.getStructureId()).thenReturn(STRUCTURE_ID);
		when(mockPortal.getClassNameId(JournalArticle.class)).thenReturn(CLASS_NAME_ID);
		when(mockLayoutPageTemplateEntryLocalService.fetchDefaultLayoutPageTemplateEntry(GROUP_ID, CLASS_NAME_ID, STRUCTURE_ID)).thenReturn(null);

		creationUtils.configureDefaultDisplayPage(mockJournalArticle, mockDDMStructure, mockServiceContext);

		verifyZeroInteractions(mockAssetDisplayPageEntryLocalService);
	}

	@Test
	public void configureDefaultDisplayPage_WhenStructureIsNull_ThenNoActionIsPerformed() throws PortalException {
		creationUtils.configureDefaultDisplayPage(mockJournalArticle, null, mockServiceContext);

		verifyZeroInteractions(mockAssetDisplayPageEntryLocalService, mockLayoutPageTemplateEntryLocalService);
	}

	@Test
	public void getOrCreateFolder_WhenFolderAlreadyConfiguredInTheArticleContext_ThenReturnsTheFolder() throws Exception {
		when(mockJournalArticleContext.getJournalFolder()).thenReturn(mockJournalFolder);

		JournalFolder result = creationUtils.getOrCreateFolder(mockJournalArticleContext, mockServiceContext);

		assertThat(result, sameInstance(mockJournalFolder));
	}

	@Test(expected = PortalException.class)
	public void getOrCreateFolder_WhenFolderNotConfiguredInArticleContextAndTheFolderNameIsConfiguredAndFolderDoesNotExistWithTheGivenNameAndExceptionCreatingTheFolder_ThenThrowsPortalException()
			throws Exception {
		String folderName = "folderNameValue";
		when(mockJournalArticleContext.getJournalFolder()).thenReturn(null);
		when(mockJournalArticleContext.getFolderName()).thenReturn(folderName);
		when(mockJournalFolderLocalService.fetchFolder(GROUP_ID, folderName)).thenReturn(null);
		mockServiceContextDetails();
		when(mockJournalFolderLocalService.addFolder(USER_ID, GROUP_ID, 0, folderName, null, mockServiceContext)).thenThrow(new PortalException());

		creationUtils.getOrCreateFolder(mockJournalArticleContext, mockServiceContext);
	}

	@Test
	public void getOrCreateFolder_WhenFolderNotConfiguredInArticleContextAndTheFolderNameIsConfiguredAndTheFolderAlreadyExistsWithSameName_ThenReturnsTheFolder() throws Exception {
		String folderName = "folderNameValue";
		mockServiceContextDetails();
		when(mockJournalArticleContext.getJournalFolder()).thenReturn(null);
		when(mockJournalArticleContext.getFolderName()).thenReturn(folderName);
		when(mockJournalFolderLocalService.fetchFolder(GROUP_ID, folderName)).thenReturn(mockJournalFolder);

		JournalFolder result = creationUtils.getOrCreateFolder(mockJournalArticleContext, mockServiceContext);

		assertThat(result, sameInstance(mockJournalFolder));
	}

	@Test
	public void getOrCreateFolder_WhenFolderNotConfiguredInArticleContextAndTheFolderNameIsConfiguredAndTheFolderDoesNotExistWithTheGivenName_ThenReturnsTheCreatedFolder() throws Exception {
		String folderName = "folderNameValue";
		when(mockJournalArticleContext.getJournalFolder()).thenReturn(null);
		when(mockJournalArticleContext.getFolderName()).thenReturn(folderName);
		when(mockJournalFolderLocalService.fetchFolder(GROUP_ID, folderName)).thenReturn(null);
		mockServiceContextDetails();
		when(mockJournalFolderLocalService.addFolder(USER_ID, GROUP_ID, 0, folderName, null, mockServiceContext)).thenReturn(mockJournalFolder);

		JournalFolder result = creationUtils.getOrCreateFolder(mockJournalArticleContext, mockServiceContext);

		assertThat(result, sameInstance(mockJournalFolder));
	}

	@Test
	public void getOrCreateFolder_WhenFolderNotConfiguredInArticleContextAndTheFolderNameIsNotConfigured_ThenReturnsNull() throws Exception {
		when(mockJournalArticleContext.getJournalFolder()).thenReturn(null);
		when(mockJournalArticleContext.getFolderName()).thenReturn("");

		JournalFolder result = creationUtils.getOrCreateFolder(mockJournalArticleContext, mockServiceContext);

		assertNull(result);
	}

	private String getExpectedArticleId(boolean articleIdSpecified) {
		return articleIdSpecified ? "articleValue" : null;
	}

	private JournalFolder getExpectedFolder(boolean folderSpecified) {
		return folderSpecified ? mockJournalFolder : null;
	}

	private long getExpectedFolderId(boolean folderSpecified) {
		if (folderSpecified) {
			when(mockJournalFolder.getFolderId()).thenReturn(FOLDER_ID);
			return FOLDER_ID;
		}
		return 0;
	}

	private DDMStructure getExpectedStructure(boolean structureSpecified, boolean structureWithTemplates) {
		if (structureSpecified) {
			when(mockDDMStructure.getStructureKey()).thenReturn(STRUCTURE_KEY);
			mockStructureTemplates(structureWithTemplates);
			return mockDDMStructure;
		}
		return null;
	}

	private String getExpectedStructureKey(boolean structureSpecified) {
		return structureSpecified ? STRUCTURE_KEY : "BASIC-WEB-CONTENT";
	}

	private String getExpectedTemplateKey(boolean structureSpecified, boolean structureWithTemplates) {
		if (structureSpecified) {
			return structureWithTemplates ? TEMPLATE_KEY : StringPool.BLANK;
		}
		return "BASIC-WEB-CONTENT";
	}

	private void mockServiceContextDetails() {
		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockServiceContext.getUserId()).thenReturn(USER_ID);
		when(mockServiceContext.getLocale()).thenReturn(LOCALE);
	}

	private void mockStructureTemplates(boolean structureWithTemplates) {
		List<DDMTemplate> templates = new ArrayList<>();
		when(mockDDMStructure.getTemplates()).thenReturn(templates);
		if (structureWithTemplates) {
			templates.add(mockDDMTemplate);
			when(mockDDMTemplate.getTemplateKey()).thenReturn(TEMPLATE_KEY);
		}
	}

}
