package com.placecube.journal.service.impl;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Locale;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.model.JournalFolder;
import com.liferay.journal.service.JournalArticleLocalService;
import com.liferay.journal.service.JournalFolderLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.StringUtil;
import com.placecube.journal.internal.service.CreationUtils;
import com.placecube.journal.model.JournalArticleContext;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
@PrepareForTest(StringUtil.class)
public class JournalArticleCreationServiceImplTest extends PowerMockito {

	private static final String ARTICLE_CONTENT = "articleContentValue";
	private static final String ARTICLE_ID = "articleIdValue";
	private static final String ARTICLE_TITLE = "articleTitleValue";
	private static final String ARTICLE_URL = "articleURLValue";
	private static final long GROUP_ID = 1;
	private static final Locale LOCALE = Locale.CANADA;
	private static final long USER_ID = 2;

	@InjectMocks
	private JournalArticleCreationServiceImpl journalArticleCreationServiceImpl;

	@Mock
	private CreationUtils mockCreationUtils;

	@Mock
	private DDMStructure mockDDMStructure;

	@Mock
	private JournalArticle mockJournalArticle;

	@Mock
	private JournalArticleContext mockJournalArticleContext;

	@Mock
	private JournalArticleLocalService mockJournalArticleLocalService;

	@Mock
	private JournalFolder mockJournalFolder;

	@Mock
	private JournalFolderLocalService mockJournalFolderLocalService;

	@Mock
	private ServiceContext mockServiceContext;

	@Before
	public void activateSetup() {
		mockStatic(StringUtil.class);
	}

	@Test
	public void getOrCreateArticle_WhenArticleFound_ThenReturnsTheArticle() throws PortalException {
		mockServiceContextDetails();
		mockJournalArticleContextDetails(true);
		when(mockJournalArticleLocalService.getLatestArticle(GROUP_ID, ARTICLE_ID)).thenReturn(mockJournalArticle);

		JournalArticle result = journalArticleCreationServiceImpl.getOrCreateArticle(mockJournalArticleContext, mockServiceContext);

		assertThat(result, sameInstance(mockJournalArticle));
	}

	@Test
	@Parameters({ "true", "false" })
	public void getOrCreateArticle_WhenNoArticleFound_ThenConfiguresTheDefaultDisplayPageForTheArticle(boolean indexable) throws PortalException {
		mockServiceContextDetails();
		mockJournalArticleContextDetails(indexable);
		when(mockJournalArticleLocalService.getLatestArticle(GROUP_ID, ARTICLE_ID)).thenThrow(new PortalException());
		when(mockCreationUtils.addArticle(ARTICLE_ID, ARTICLE_TITLE, ARTICLE_CONTENT, ARTICLE_URL, mockJournalFolder, mockDDMStructure, indexable, mockServiceContext)).thenReturn(mockJournalArticle);

		journalArticleCreationServiceImpl.getOrCreateArticle(mockJournalArticleContext, mockServiceContext);

		verify(mockCreationUtils, times(1)).configureDefaultDisplayPage(mockJournalArticle, mockDDMStructure, mockServiceContext);
	}

	@Test
	@Parameters({ "true", "false" })
	public void getOrCreateArticle_WhenNoArticleFound_ThenReturnsTheNewlyCreatedArticle(boolean indexable) throws PortalException {
		mockServiceContextDetails();
		mockJournalArticleContextDetails(indexable);
		when(mockJournalArticleLocalService.getLatestArticle(GROUP_ID, ARTICLE_ID)).thenThrow(new PortalException());
		when(mockCreationUtils.addArticle(ARTICLE_ID, ARTICLE_TITLE, ARTICLE_CONTENT, ARTICLE_URL, mockJournalFolder, mockDDMStructure, indexable, mockServiceContext)).thenReturn(mockJournalArticle);

		JournalArticle result = journalArticleCreationServiceImpl.getOrCreateArticle(mockJournalArticleContext, mockServiceContext);

		assertThat(result, sameInstance(mockJournalArticle));
	}

	@Test(expected = PortalException.class)
	@Parameters({ "true", "false" })
	public void getOrCreateArticle_WhenNoArticleFoundAndExceptionCreatingTheNewArticle_ThenThrowsPortalException(boolean indexable) throws PortalException {
		mockServiceContextDetails();
		mockJournalArticleContextDetails(indexable);
		when(mockJournalArticleLocalService.getLatestArticle(GROUP_ID, ARTICLE_ID)).thenThrow(new PortalException());
		when(mockCreationUtils.addArticle(ARTICLE_ID, ARTICLE_TITLE, ARTICLE_CONTENT, ARTICLE_URL, mockJournalFolder, mockDDMStructure, indexable, mockServiceContext))
				.thenThrow(new PortalException());

		journalArticleCreationServiceImpl.getOrCreateArticle(mockJournalArticleContext, mockServiceContext);
	}

	@Test(expected = PortalException.class)
	public void getOrCreateArticle_WhenNoArticleFoundAndExceptionRetrievingTheFolder_ThenThrowsPortalException() throws PortalException {
		mockServiceContextDetails();
		when(mockJournalArticleContext.getArticleId()).thenReturn(ARTICLE_ID);
		when(mockJournalArticleLocalService.getLatestArticle(GROUP_ID, ARTICLE_ID)).thenThrow(new PortalException());
		when(mockCreationUtils.getOrCreateFolder(mockJournalArticleContext, mockServiceContext)).thenThrow(new PortalException());

		journalArticleCreationServiceImpl.getOrCreateArticle(mockJournalArticleContext, mockServiceContext);
	}

	@Test
	public void getOrCreateBasicWebContentArticle_WhenArticleFound_ThenReturnsTheArticle() throws PortalException {
		mockServiceContextDetails();
		when(mockJournalArticleLocalService.getLatestArticle(GROUP_ID, ARTICLE_ID)).thenReturn(mockJournalArticle);

		JournalArticle result = journalArticleCreationServiceImpl.getOrCreateBasicWebContentArticle(ARTICLE_ID, ARTICLE_TITLE, ARTICLE_CONTENT, mockJournalFolder, mockServiceContext);

		assertThat(result, sameInstance(mockJournalArticle));
	}

	@Test
	public void getOrCreateBasicWebContentArticle_WhenNoArticleFound_ThenReturnsTheNewlyCreatedArticle() throws PortalException {
		mockServiceContextDetails();
		when(mockJournalArticleLocalService.getLatestArticle(GROUP_ID, ARTICLE_ID)).thenThrow(new PortalException());
		when(mockCreationUtils.addArticle(ARTICLE_ID, ARTICLE_TITLE, ARTICLE_CONTENT, StringPool.BLANK, mockJournalFolder, null, true, mockServiceContext)).thenReturn(mockJournalArticle);

		JournalArticle result = journalArticleCreationServiceImpl.getOrCreateBasicWebContentArticle(ARTICLE_ID, ARTICLE_TITLE, ARTICLE_CONTENT, mockJournalFolder, mockServiceContext);

		assertThat(result, sameInstance(mockJournalArticle));
	}

	@Test(expected = PortalException.class)
	public void getOrCreateBasicWebContentArticle_WhenNoArticleFoundAndExceptionCreatingTheNewArticle_ThenThrowsPortalException() throws PortalException {
		mockServiceContextDetails();
		when(mockJournalArticleLocalService.getLatestArticle(GROUP_ID, ARTICLE_ID)).thenThrow(new PortalException());
		when(mockCreationUtils.addArticle(ARTICLE_ID, ARTICLE_TITLE, ARTICLE_CONTENT, StringPool.BLANK, mockJournalFolder, null, true, mockServiceContext)).thenThrow(new PortalException());

		journalArticleCreationServiceImpl.getOrCreateBasicWebContentArticle(ARTICLE_ID, ARTICLE_TITLE, ARTICLE_CONTENT, mockJournalFolder, mockServiceContext);
	}

	@Test
	public void getOrCreateJournalFolder_WhenFolderAlreadyExistsWithSameName_ThenReturnsTheFolder() throws Exception {
		String folderName = "folderNameValue";
		mockServiceContextDetails();
		when(mockJournalFolderLocalService.fetchFolder(GROUP_ID, folderName)).thenReturn(mockJournalFolder);

		JournalFolder result = journalArticleCreationServiceImpl.getOrCreateJournalFolder(folderName, mockServiceContext);

		assertThat(result, sameInstance(mockJournalFolder));
	}

	@Test
	public void getOrCreateJournalFolder_WhenFolderDoesNotExistWithTheGivenName_ThenReturnsTheCreatedFolder() throws Exception {
		String folderName = "folderNameValue";
		when(mockJournalFolderLocalService.fetchFolder(GROUP_ID, folderName)).thenReturn(null);
		mockServiceContextDetails();
		when(mockJournalFolderLocalService.addFolder(USER_ID, GROUP_ID, 0, folderName, null, mockServiceContext)).thenReturn(mockJournalFolder);

		JournalFolder result = journalArticleCreationServiceImpl.getOrCreateJournalFolder(folderName, mockServiceContext);

		assertThat(result, sameInstance(mockJournalFolder));
	}

	@Test(expected = PortalException.class)
	public void getOrCreateJournalFolder_WhenFolderDoesNotExistWithTheGivenNameAndExceptionCreatingTheFolder_ThenThrowsPortalException() throws Exception {
		String folderName = "folderNameValue";
		when(mockJournalFolderLocalService.fetchFolder(GROUP_ID, folderName)).thenReturn(null);
		mockServiceContextDetails();
		when(mockJournalFolderLocalService.addFolder(USER_ID, GROUP_ID, 0, folderName, null, mockServiceContext)).thenThrow(new PortalException());

		journalArticleCreationServiceImpl.getOrCreateJournalFolder(folderName, mockServiceContext);
	}

	private void mockJournalArticleContextDetails(boolean indexable) throws PortalException {
		when(mockJournalArticleContext.getArticleId()).thenReturn(ARTICLE_ID);
		when(mockJournalArticleContext.getContent()).thenReturn(ARTICLE_CONTENT);
		when(mockJournalArticleContext.getTitle()).thenReturn(ARTICLE_TITLE);
		when(mockJournalArticleContext.getUrlTitle()).thenReturn(ARTICLE_URL);
		when(mockCreationUtils.getOrCreateFolder(mockJournalArticleContext, mockServiceContext)).thenReturn(mockJournalFolder);
		when(mockJournalArticleContext.getDDMStructure()).thenReturn(mockDDMStructure);
		when(mockJournalArticleContext.getIndexable()).thenReturn(indexable);
	}

	private void mockServiceContextDetails() {
		when(mockServiceContext.getScopeGroupId()).thenReturn(GROUP_ID);
		when(mockServiceContext.getUserId()).thenReturn(USER_ID);
		when(mockServiceContext.getLocale()).thenReturn(LOCALE);
	}
}
