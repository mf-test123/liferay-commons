package com.placecube.journal.service.impl;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.liferay.asset.kernel.model.AssetEntry;
import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.storage.Field;
import com.liferay.dynamic.data.mapping.storage.Fields;
import com.liferay.dynamic.data.mapping.util.DDMXML;
import com.liferay.journal.model.JournalArticle;
import com.liferay.journal.model.JournalArticleConstants;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.util.LocaleUtil;

@RunWith(PowerMockRunner.class)
@PrepareForTest(LocaleUtil.class)
public class JournalArticleRetrievalServiceImplTest extends PowerMockito {

	private JournalArticleRetrievalServiceImpl journalArticleRetrievalServiceImpl;

	@Mock
	private DDMXML mockDDMXML;

	@Mock
	private JournalArticle mockJournalArticle;

	@Mock
	private DDMStructure mockDDMStructure;

	@Mock
	private Fields mockFields;

	@Mock
	private Field mockField;

	@Mock
	private AssetEntry mockAssetEntry1;

	@Mock
	private AssetEntry mockAssetEntry2;

	@Mock
	private AssetEntry mockAssetEntry3;

	@Mock
	private AssetEntry mockAssetEntry4;

	@Test
	public void getAssetEntriesOrderedByTitle_WhenNoError_ThenReturnsTheAssetEntriesOrderedByTitle() {
		Locale locale = LocaleUtil.CANADA_FRENCH;
		when(mockAssetEntry1.getTitle(locale)).thenReturn("Beta");
		when(mockAssetEntry2.getTitle(locale)).thenReturn("Alpha");
		when(mockAssetEntry3.getTitle(locale)).thenReturn("Delta");
		when(mockAssetEntry4.getTitle(locale)).thenReturn("Charlie");

		List<AssetEntry> assetEntries = new ArrayList<>();
		assetEntries.add(mockAssetEntry1);
		assetEntries.add(mockAssetEntry2);
		assetEntries.add(mockAssetEntry3);
		assetEntries.add(mockAssetEntry4);

		List<AssetEntry> results = journalArticleRetrievalServiceImpl.getAssetEntriesOrderedByTitle(assetEntries, locale);

		assertThat(results, contains(mockAssetEntry2, mockAssetEntry1, mockAssetEntry4, mockAssetEntry3));
	}

	@Test
	public void getDisplayURL_WhenException_ThenReturnsEmptyOptional() throws PortalException {
		Locale locale = LocaleUtil.CANADA_FRENCH;
		when(mockJournalArticle.getFriendlyURLMap()).thenThrow(new PortalException());

		Optional<String> result = journalArticleRetrievalServiceImpl.getDisplayURL(mockJournalArticle, locale);

		assertThat(result.isPresent(), equalTo(false));
	}

	@Test
	public void getDisplayURL_WhenNoErrorAndFriendlyURLMapContainsValueForSpecifiedLocale_ThenReturnsOptionalWithURLForLocale() throws PortalException {
		Locale locale = LocaleUtil.CANADA_FRENCH;
		String expected = "expectedValue";
		Map<Locale, String> friendlyURLMap = new HashMap<>();
		friendlyURLMap.put(Locale.CHINESE, "chinaValue");
		friendlyURLMap.put(locale, expected);
		friendlyURLMap.put(Locale.ITALIAN, "italianValue");
		when(mockJournalArticle.getFriendlyURLMap()).thenReturn(friendlyURLMap);

		Optional<String> result = journalArticleRetrievalServiceImpl.getDisplayURL(mockJournalArticle, locale);

		assertThat(result.get(), equalTo(JournalArticleConstants.CANONICAL_URL_SEPARATOR + expected));
	}

	@Test
	public void getDisplayURL_WhenNoErrorAndFriendlyURLMapDoesNotContainValueForSpecifiedLocale_ThenReturnsOptionalWithURLForJournalDefaultLanguageId() throws PortalException {
		String expected = "expectedValue";
		Map<Locale, String> friendlyURLMap = new HashMap<>();
		friendlyURLMap.put(Locale.CHINESE, "chinaValue");
		friendlyURLMap.put(Locale.ITALIAN, expected);
		when(mockJournalArticle.getFriendlyURLMap()).thenReturn(friendlyURLMap);
		String languageId = "languageIdValue";
		when(mockJournalArticle.getDefaultLanguageId()).thenReturn(languageId);
		when(LocaleUtil.fromLanguageId(languageId)).thenReturn(Locale.ITALIAN);

		Optional<String> result = journalArticleRetrievalServiceImpl.getDisplayURL(mockJournalArticle, LocaleUtil.CANADA_FRENCH);

		assertThat(result.get(), equalTo(JournalArticleConstants.CANONICAL_URL_SEPARATOR + expected));
	}

	@Test
	public void getFieldValue_WhenError_ThenReturnsEmptyOptional() throws Exception {
		Locale locale = LocaleUtil.CANADA_FRENCH;
		String fieldName = "UnknownField";
		String xmlToParse = "xmlToParse";
		when(mockJournalArticle.getContent()).thenReturn(xmlToParse);
		when(mockJournalArticle.getDDMStructure()).thenReturn(mockDDMStructure);
		when(mockDDMXML.getFields(mockDDMStructure, xmlToParse)).thenThrow(new PortalException());

		Optional<String> result = journalArticleRetrievalServiceImpl.getFieldValue(mockJournalArticle, fieldName, locale);

		assertThat(result.isPresent(), equalTo(false));
	}

	@Test
	public void getFieldValue_WhenNoErrorAndFieldIsFoundWithAvalue_ThenReturnsOptionalWithTheFieldValue() throws Exception {
		Locale locale = LocaleUtil.CANADA_FRENCH;
		String fieldName = "UnknownField";
		String xmlToParse = "xmlToParse";
		String expected = "expectedValue";
		when(mockJournalArticle.getContent()).thenReturn(xmlToParse);
		when(mockJournalArticle.getDDMStructure()).thenReturn(mockDDMStructure);
		when(mockDDMXML.getFields(mockDDMStructure, xmlToParse)).thenReturn(mockFields);
		when(mockFields.get(fieldName)).thenReturn(mockField);
		when(mockField.getValue(locale)).thenReturn(expected);

		Optional<String> result = journalArticleRetrievalServiceImpl.getFieldValue(mockJournalArticle, fieldName, locale);

		assertThat(result.get(), equalTo(expected));
	}

	@Test
	public void getFieldValue_WhenNoErrorAndFieldIsFoundWithEmptyStringAsValue_ThenReturnsEmptyOptional() throws Exception {
		Locale locale = LocaleUtil.CANADA_FRENCH;
		String fieldName = "UnknownField";
		String xmlToParse = "xmlToParse";
		when(mockJournalArticle.getContent()).thenReturn(xmlToParse);
		when(mockJournalArticle.getDDMStructure()).thenReturn(mockDDMStructure);
		when(mockDDMXML.getFields(mockDDMStructure, xmlToParse)).thenReturn(mockFields);
		when(mockFields.get(fieldName)).thenReturn(mockField);
		when(mockField.getValue(locale)).thenReturn(" ");

		Optional<String> result = journalArticleRetrievalServiceImpl.getFieldValue(mockJournalArticle, fieldName, locale);

		assertThat(result.isPresent(), equalTo(false));
	}

	@Test
	public void getFieldValue_WhenNoErrorAndFieldIsFoundWithNullAsValue_ThenReturnsEmptyOptional() throws Exception {
		Locale locale = LocaleUtil.CANADA_FRENCH;
		String fieldName = "UnknownField";
		String xmlToParse = "xmlToParse";
		when(mockJournalArticle.getContent()).thenReturn(xmlToParse);
		when(mockJournalArticle.getDDMStructure()).thenReturn(mockDDMStructure);
		when(mockDDMXML.getFields(mockDDMStructure, xmlToParse)).thenReturn(mockFields);
		when(mockFields.get(fieldName)).thenReturn(mockField);
		when(mockField.getValue(locale)).thenReturn(null);

		Optional<String> result = journalArticleRetrievalServiceImpl.getFieldValue(mockJournalArticle, fieldName, locale);

		assertThat(result.isPresent(), equalTo(false));
	}

	@Test
	public void getFieldValue_WhenNoErrorAndNoFieldFound_ThenReturnsEmptyOptional() throws Exception {
		Locale locale = LocaleUtil.CANADA_FRENCH;
		String fieldName = "UnknownField";
		String xmlToParse = "xmlToParse";
		when(mockJournalArticle.getContent()).thenReturn(xmlToParse);
		when(mockJournalArticle.getDDMStructure()).thenReturn(mockDDMStructure);
		when(mockDDMXML.getFields(mockDDMStructure, xmlToParse)).thenReturn(mockFields);
		when(mockFields.get(fieldName)).thenReturn(null);

		Optional<String> result = journalArticleRetrievalServiceImpl.getFieldValue(mockJournalArticle, fieldName, locale);

		assertThat(result.isPresent(), equalTo(false));
	}

	@Before
	public void setUp() {
		mockStatic(LocaleUtil.class);

		journalArticleRetrievalServiceImpl = new JournalArticleRetrievalServiceImpl();
		journalArticleRetrievalServiceImpl.ddmxml = mockDDMXML;
	}
}
