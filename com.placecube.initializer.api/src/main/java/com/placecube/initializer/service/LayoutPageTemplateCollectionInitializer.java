package com.placecube.initializer.service;

import com.liferay.layout.page.template.model.LayoutPageTemplateCollection;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;

public interface LayoutPageTemplateCollectionInitializer {

	/**
	 * Returns the LayoutPageTemplateCollection found with the given name, or
	 * the newly created one if none exists
	 *
	 * @param collectionName the collection name
	 * @param serviceContext the service context
	 * @return LayoutPageTemplateCollection
	 * @throws PortalException any error whilst creating the new entry
	 */
	LayoutPageTemplateCollection getOrCreateLayoutPageTemplateCollection(String collectionName, ServiceContext serviceContext) throws PortalException;

}
