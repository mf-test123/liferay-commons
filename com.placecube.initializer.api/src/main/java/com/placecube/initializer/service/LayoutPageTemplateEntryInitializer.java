package com.placecube.initializer.service;

import com.liferay.layout.page.template.model.LayoutPageTemplateCollection;
import com.liferay.layout.page.template.model.LayoutPageTemplateEntry;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.initializer.model.LayoutContext;

public interface LayoutPageTemplateEntryInitializer {

	/**
	 * Creates a new LayoutPageTemplate entry
	 *
	 * @param layoutPageTemplateCollection the collection to add the template to
	 * @param layoutContext the layout context containing details of the
	 *            template to create
	 * @param serviceContext the service context
	 * @return LayoutPageTemplateEntry
	 * @throws PortalException any error whilst creating the new
	 *             LayoutPageTemplateEntry
	 */
	LayoutPageTemplateEntry createContentPageTemplate(LayoutPageTemplateCollection layoutPageTemplateCollection, LayoutContext layoutContext, ServiceContext serviceContext) throws PortalException;

	/**
	 * Creates a new LayoutPageTemplate entry of type Display page and sets it
	 * as default
	 *
	 * @param layoutContext the layout context containing details of the
	 *            template to create
	 * @param classNameId the classNameId
	 * @param classTypeId the classTypeId
	 * @param serviceContext the service context
	 * @return LayoutPageTemplateEntry
	 * @throws PortalException any error whilst creating the new
	 *             LayoutPageTemplateEntry
	 */
	LayoutPageTemplateEntry createDisplayPageTemplate(LayoutContext layoutContext, long classNameId, long classTypeId, ServiceContext serviceContext) throws PortalException;

	/**
	 * Creates a new LayoutPageTemplate entry of type widget page
	 *
	 * @param layoutPageTemplateCollection the collection to add the template to
	 * @param layoutContext the layout context containing details of the
	 *            template to create
	 * @param serviceContext the service context
	 * @return LayoutPageTemplateEntry
	 * @throws PortalException any error whilst creating the new
	 *             LayoutPageTemplateEntry
	 */
	LayoutPageTemplateEntry createWidgetPageTemplate(LayoutPageTemplateCollection layoutPageTemplateCollection, LayoutContext layoutContext, ServiceContext serviceContext) throws PortalException;

}
