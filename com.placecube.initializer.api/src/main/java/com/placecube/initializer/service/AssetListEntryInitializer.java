package com.placecube.initializer.service;

import java.util.Optional;

import com.liferay.asset.list.model.AssetListEntry;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;

public interface AssetListEntryInitializer {

	/**
	 * Returns an optional with the content set
	 *
	 * @param groupId the groupid
	 * @param contentSetName the content set name
	 * @return first content set found with the given name
	 */
	Optional<AssetListEntry> getContentSet(Long groupId, String contentSetName);

	/**
	 * Retrieves the AssetListEntry content set found with the specified name,
	 * or creates a new one if none is found
	 *
	 * @param contentSetName the content set name
	 * @param selectionTypeManual if selection type should be manual
	 * @param serviceContext the service context
	 * @return AssetListEntry
	 * @throws PortalException any error whilst creating the new content set
	 */
	AssetListEntry getOrCreateContentSet(String contentSetName, Boolean selectionTypeManual, ServiceContext serviceContext) throws PortalException;

}
