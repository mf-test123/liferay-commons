package com.placecube.initializer.service;

import com.liferay.layout.page.template.model.LayoutPageTemplateEntry;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.initializer.model.LayoutContext;

public interface LayoutInitializer {

	/**
	 * Returns the layout found with the given name, or the newly created
	 * content layout if none exists
	 *
	 * @param layoutContext the layout context containing details of the layout
	 *            to create
	 * @param serviceContext the service context
	 * @return Layout
	 * @throws PortalException any error whilst creating the new layout
	 */
	Layout getOrCreateContentLayout(LayoutContext layoutContext, ServiceContext serviceContext) throws PortalException;

	/**
	 * Returns the Layout found with the given friendlyURL, or the newly created
	 * Link to page layout if none exists
	 *
	 * @param layoutContext the layout context containing details of the layout
	 *            to create
	 * @param serviceContext the service context
	 * @return Layout
	 * @throws PortalException any error whilst creating the new layout
	 */
	Layout getOrCreateLinkToPageLayout(LayoutContext layoutContext, ServiceContext serviceContext) throws PortalException;

	/**
	 * Returns the Layout found with the given friendlyURL, or the newly created
	 * widget layout if none exists
	 *
	 * @param layoutContext the layout context containing details of the layout
	 *            to create
	 * @param serviceContext the service context
	 * @return Layout
	 * @throws PortalException any error whilst creating the new layout
	 */
	Layout getOrCreatePortletLayout(LayoutContext layoutContext, ServiceContext serviceContext) throws PortalException;

	/**
	 * Returns the Layout found with the given friendlyURL, or the newly created
	 * layout based on a page template if none exists
	 *
	 * @param layoutContext the layout context containing details of the layout
	 *            to create
	 * @param layoutPageTemplateEntry layout page template entry based on which
	 *            a layout will be created
	 * @param serviceContext the service context
	 * @return Layout
	 * @throws PortalException any error whilst creating the new layout
	 */
	Layout getOrCreatePageTemplateLayout(LayoutContext layoutContext, LayoutPageTemplateEntry layoutPageTemplateEntry, ServiceContext serviceContext) throws  PortalException;

}
