package com.placecube.initializer.service;

import java.util.Optional;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.model.LayoutSetPrototype;
import com.liferay.portal.kernel.service.ServiceContext;

public interface LayoutSetPrototypeInitializer {

	/**
	 * Updates the group's private and public pages with the specified site
	 * templates, removing all existing pages. If no site template is specified,
	 * then no changes are made to the layoutSet
	 *
	 * @param group the group to update
	 * @param publicPagesLayoutSetPrototype the site template for public pages
	 * @param publicLayoutSetPrototypeLinkEnabled if the site template for
	 *            public pages should have propagation enabled
	 * @param privatePagesLayoutSetPrototype the site template for private pages
	 * @param privateLayoutSetPrototypeLinkEnabled if the site template for
	 *            private pages should have propagation enabeld
	 * @param serviceContext the service context
	 * @throws PortalException any exception whilst configuring the pages
	 */
	void applySiteTemplateToGroup(Group group, Optional<LayoutSetPrototype> publicPagesLayoutSetPrototype, boolean publicLayoutSetPrototypeLinkEnabled,
			Optional<LayoutSetPrototype> privatePagesLayoutSetPrototype, boolean privateLayoutSetPrototypeLinkEnabled, ServiceContext serviceContext) throws PortalException;

}
