package com.placecube.initializer.service;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.site.navigation.model.SiteNavigationMenu;

public interface NavigationMenuInitializer {

	void configureSiteNavigationMenuEntries(ServiceContext serviceContext, ClassLoader classLoader, String resourcePath) throws PortalException;

	SiteNavigationMenu getOrCreateSiteNavigationMenu(ServiceContext serviceContext, String name, int type) throws PortalException;

}
