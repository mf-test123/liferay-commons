package com.placecube.initializer.service;

import com.liferay.portal.kernel.exception.PortalException;

public interface PermissionCheckerInitializer {

	/**
	 * Configures the first system administrator for the company as user in the
	 * permission checker
	 *
	 * @param companyId the company id
	 * @throws PortalException exception retrieving the administrator user or
	 *             configuring the permission checker
	 */
	void runAsCompanyAdministrator(long companyId) throws PortalException;

	/**
	 * Configures the specified userId as user in the permission checker
	 *
	 * @param userId the user id
	 */
	void runAsUser(long userId);

}
