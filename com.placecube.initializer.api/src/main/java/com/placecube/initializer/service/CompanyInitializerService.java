package com.placecube.initializer.service;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Company;
import com.placecube.initializer.model.CompanyContext;

public interface CompanyInitializerService {

	Company configureCompany(CompanyContext companyContext) throws PortalException;

	CompanyContext createCompanyContext(Company company, int maxUsers, ClassLoader resourceClassLoader, String logoResourcePath);

}