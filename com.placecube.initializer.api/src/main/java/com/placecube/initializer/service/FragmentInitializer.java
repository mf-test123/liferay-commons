package com.placecube.initializer.service;

import com.liferay.fragment.model.FragmentCollection;
import com.liferay.fragment.model.FragmentEntry;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;

public interface FragmentInitializer {

	FragmentEntry getOrCreateFragment(FragmentCollection fragmentCollection, ClassLoader classLoader, String basePath, ServiceContext serviceContext) throws PortalException;

	FragmentCollection getOrCreateFragmentCollection(String fragmentCollectionKey, String fragmentCollectionLabel, ServiceContext serviceContext) throws PortalException;

}
