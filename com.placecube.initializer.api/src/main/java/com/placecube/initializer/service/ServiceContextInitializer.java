package com.placecube.initializer.service;

import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.service.ServiceContext;

public interface ServiceContextInitializer {

	/**
	 * Creates a new service context object configured with the specified group,
	 * the company from the group's companyId and the user from the group's
	 * creator user
	 *
	 * @param group the group
	 * @return the configured service context
	 */
	ServiceContext getServiceContext(Group group);

}
