package com.placecube.initializer.service;

import java.util.Map;

import com.placecube.initializer.model.FragmentContext;
import com.placecube.initializer.model.LayoutContext;
import com.placecube.initializer.model.PortletContext;

public interface ModelCreationFactory {

	LayoutContext createContentPageLayoutContext(String name, String friendlyURL, Boolean privateLayout);

	FragmentContext createFragmentContext(String id, String placeholder, String settings);

	LayoutContext createLayoutContext(String name, String friendlyURL, Boolean privateLayout);

	LayoutContext createPageTemplateLayoutContext(String name);

	LayoutContext createPageTemplateLayoutContext(String name, String layoutTemplateId);

	PortletContext createPortletContext(String portletId, Map<String, String> portletPreferences);

	PortletContext createPortletContext(String portletId, Map<String, String> portletPreferences, String customInstanceId);

	PortletContext createPortletContext(String id, String placeholder);

	PortletContext createPortletContext(String id, String placeholder, String customInstanceId);

	LayoutContext createWidgetPageLayoutContext(String name, String friendlyURL, Boolean privateLayout, String layoutTemplateId);

}
