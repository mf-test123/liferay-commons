package com.placecube.initializer.service;

import com.liferay.dynamic.data.mapping.model.DDMStructure;
import com.liferay.dynamic.data.mapping.model.DDMTemplate;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.service.ServiceContext;
import com.placecube.initializer.model.DDMTemplateContext;

/**
 * The DDMInitializer interface provides methods for creating DDM related objects such as DDMStructures and DDMTemplates
 */
public interface DDMInitializer {

	/**
	 * Gets a new DDMTemplateContext representing a dynamic data listing template, which is used with {@link #getOrCreateDDMTemplate(DDMTemplateContext)} to get or create the template
	 *
	 * @param templateName        the template name
	 * @param templateKey         the template key
	 * @param structureId         the structureId referenced by the template
	 * @param resourcePath        the resource path to the structure definition
	 * @param resourceClassLoader the resource class loader for the structure definition
	 * @param serviceContext      the service context
	 * @return the DDMTemplateContext containing all the details of a dynamic data listing template
	 */
	DDMTemplateContext getDDMTemplateContextForDDL(String templateName, String templateKey, long structureId, String resourcePath, ClassLoader resourceClassLoader, ServiceContext serviceContext);

	/**
	 * Gets a new DDMTemplateContext representing a portlet display Template, which is used with {@link #getOrCreateDDMTemplate(DDMTemplateContext)} to get or create the template
	 *
	 * @param templateName        the template name
	 * @param templateKey         the template key
	 * @param templateClass       the template class
	 * @param resourcePath        the resource path to the structure definition
	 * @param resourceClassLoader the resource class loader for the structure definition
	 * @param serviceContext      the service context
	 * @return the DDMTemplateContext containing all the details of a dynamic data listing template
	 */
	DDMTemplateContext getDDMTemplateContextForPortletDisplay(String templateName, String templateKey, Class templateClass, String resourcePath, ClassLoader resourceClassLoader, ServiceContext serviceContext);

	/**
	 * Gets a new DDMTemplateContext representing a web content template, which is used with {@link #getOrCreateDDMTemplate(DDMTemplateContext)} to get or create the template
	 * @param templateName          the template name
	 * @param templateKey           the template key
	 * @param structureId           the structure id for which the template is to be added
	 * @param resourcePath          the resource path to the template definition
	 * @param resourceClassLoader   the resource class loader for the template definition
	 * @param serviceContext        the service context
	 * @return the DDMTemplateContext containing all the details of a dynamic data listing template
	 */
	DDMTemplateContext getDDMTemplateContextForWebContent(String templateName, String templateKey, long structureId, String resourcePath, ClassLoader resourceClassLoader,
			ServiceContext serviceContext);
	/**
	 * Gets or creates a DDMStructure.
	 *
	 * @param structureKey        the structure key
	 * @param resourcePath        the resource path to the structure definition
	 * @param resourceClassLoader the resource class loader for the structure definition
	 * @param structureClass      the structure class
	 * @param serviceContext      the service context
	 * @return the DDMStructure
	 * @throws PortalException the portal exception
	 */
	DDMStructure getOrCreateDDMStructure(String structureKey, String resourcePath, ClassLoader resourceClassLoader, Class structureClass, ServiceContext serviceContext) throws PortalException;

	/**
	 * Gets or creates a DDMTemplate.
	 *
	 * @param ddmTemplateContext the ddmTemplateContext
	 * @return the DDMTemplate
	 * @throws PortalException the portal exception
	 */
	DDMTemplate getOrCreateDDMTemplate(DDMTemplateContext ddmTemplateContext) throws PortalException;
}
