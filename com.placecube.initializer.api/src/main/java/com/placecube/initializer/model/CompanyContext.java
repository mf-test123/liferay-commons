package com.placecube.initializer.model;

import java.io.InputStream;

import com.liferay.portal.kernel.model.Company;
import com.liferay.ratings.kernel.RatingsType;

public interface CompanyContext {

	Company getCompany();

	InputStream getCompanyLogo();

	RatingsType getDefaultRatingsType();

	int getMaxUsers();

	void setRatingsType(RatingsType ratingsType);

}
