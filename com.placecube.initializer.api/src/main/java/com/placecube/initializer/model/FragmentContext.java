package com.placecube.initializer.model;

public interface FragmentContext {

	String getId();

	String getPlaceholder();

	String getSettings();

}
