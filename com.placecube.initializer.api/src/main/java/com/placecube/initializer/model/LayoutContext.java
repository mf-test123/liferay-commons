package com.placecube.initializer.model;

import java.io.InputStream;
import java.io.Serializable;
import java.util.Map;
import java.util.Set;

public interface LayoutContext {

	long getClassNameId();

	long getClassPK();

	Map<Integer, Set<PortletContext>> getColumnsPortletContexts();

	Map<String, Serializable> getExpandoValues();

	Set<FragmentContext> getFragmentContexts();

	String getFriendlyURL();

	String getLayoutTemplateId();

	String getLinkToLayoutFriendlyURL();

	String getName();

	InputStream getPageIcon();

	String getParentLayoutFriendlyURL();

	Set<PortletContext> getPortletContexts();

	Map<String, String[]> getRolePermissionsToAdd();

	Map<String, String[]> getRolePermissionsToRemove();

	String getStructureContent();

	String getTitle();

	Map<String, String> getTypeSettings();

	boolean isAddGuestPermissions();

	boolean isHidden();

	boolean isPrivate();

	void setAddGuestPermissions(boolean addGuestPermissions);

	void setClassNameId(long classNameId);

	void setClassPK(long classPK);

	void setColumnPortletContexts(Map<Integer, Set<PortletContext>> columnPortlets);

	void setExpandoValues(Map<String, Serializable> expandoValues);

	void setFragmentContexts(Set<FragmentContext> fragments);

	void setHidden(boolean hidden);

	void setLinkToLayoutFriendlyURL(String linkToLayoutFriendlyURL);

	void setPageIcon(InputStream inputStream);

	void setParentLayoutFriendlyURL(String parentLayoutFriendlyURL);

	void setPortletContexts(Set<PortletContext> portlets);

	void setRolePermissionsToAdd(Map<String, String[]> rolePermissionsToAdd);

	void setRolePermissionsToRemove(Map<String, String[]> rolePermissionsToRemove);

	void setStructureContent(String structureContent);

	void setTitle(String title);

	void setTypeSettings(Map<String, String> typeSettings);

}
