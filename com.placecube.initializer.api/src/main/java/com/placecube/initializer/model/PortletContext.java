package com.placecube.initializer.model;

import java.util.Map;

public interface PortletContext {

	void addMultivaluedPreference(String key, String[] values);

	void addPreference(String key, String value);

	String getCustomInstanceId();

	String getPlaceholder();

	String getPortletId();

	Map<String, String[]> getPortletMultivaluedPreferences();

	Map<String, String> getPortletPreferences();
}
