package com.placecube.initializer.model;

import java.io.File;
import java.util.Locale;
import java.util.Map;

import com.liferay.portal.kernel.service.ServiceContext;

/**
 * The DDMTemplateContext interface provides methods for building DDMTemplates
 * with
 * {@link com.placecube.initializer.service.DDMInitializer#getOrCreateDDMTemplate(DDMTemplateContext)}
 */
public interface DDMTemplateContext {

	/**
	 * Adds the key and value pair to the content placeholders map
	 *
	 * @param placeholderKey the placeholder key
	 * @param placeholderValue the placeholder value
	 */
	void addContentPlaceholder(String placeholderKey, String placeholderValue);

	/**
	 * Gets the cacheable value
	 *
	 * @return the cacheable
	 */
	boolean getCacheable();

	/**
	 * Gets the classNameId
	 *
	 * @return the class name id
	 */
	long getClassNameId();

	/**
	 * Returns a map of the configured content placeholders
	 *
	 * @return map of placeholderKey and placeholderValue
	 */
	Map<String, String> getContentPlaceholders();

	/**
	 * Gets the localised description values
	 *
	 * @return the description
	 */
	Map<Locale, String> getDescription();

	/**
	 * Gets the language
	 *
	 * @return the language
	 */
	String getLanguage();

	/**
	 * Gets the display mode
	 *
	 * @return the mode
	 */
	String getMode();

	/**
	 * Gets the class loader for template resource
	 *
	 * @return the resource class loader
	 */
	ClassLoader getResourceClassLoader();

	/**
	 * Gets the resourceClassNameId
	 *
	 * @return the resourceClassNameId
	 */
	long getResourceClassNameId();

	/**
	 * Gets resource path for template definition
	 *
	 * @return the resource path
	 */
	String getResourcePath();

	/**
	 * Gets the service context.
	 *
	 * @return the service context
	 */
	ServiceContext getServiceContext();

	/**
	 * Gets the small image
	 *
	 * @return the small image
	 */
	boolean getSmallImage();

	/**
	 * Gets the small image file
	 *
	 * @return the small image file
	 */
	File getSmallImageFile();

	/**
	 * Gets the small image URL
	 *
	 * @return the small image URL
	 */
	String getSmallImageUrl();

	/**
	 * Gets the structureId
	 *
	 * @return the structureId
	 */
	long getStructureId();

	/**
	 * Gets the template key
	 *
	 * @return the template key
	 */
	String getTemplateKey();

	/**
	 * Gets localised name values
	 *
	 * @return the name
	 */
	Map<Locale, String> getTemplateName();

	/**
	 * Gets the type
	 *
	 * @return the type
	 */
	String getType();

}
